FROM ryrith/uwsgi

COPY ./requirements.txt /app
RUN pip install -r requirements.txt

# add H524 directory
COPY ./project /app/project


# override app directory.
COPY ./.docker/web/app /app

# WKHTML
RUN wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz

RUN tar vxf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz

RUN rm wkhtmltox-0.12.4_linux-generic-amd64.tar.xz

RUN cp wkhtmltox/bin/wk* /usr/local/bin/

# DEFAULT ENV
ENV WKHTMLTOPDF_PATH /usr/local/bin/wkhtmltopdf
ENV DATABASE_URI mssql+pymssql://sa:UnderADM1N@67.211.218.52/H907?charset=utf8


# set container timezone to +7 PP
ENV TZ=Asia/Phnom_Penh
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

EXPOSE 60081
