@echo off
:: Change this configuration
SET PROJECT_DIR=%~dp0
SET PYTHON_PATH=%~dp0\env\Scripts\python.exe
SET WFASTCGI_PATH=%~dp0\wfastcgi.py
SET SITE_NAME=DEMO
SET SITE_PORT=80
SET SITE_HOST_NAME=
SET WSGI_HANDLER_VALUE=main.app
echo End configuration
SET APPCMD=%WinDir%\System32\Inetsrv\appcmd
echo Create site with bindings port
%APPCMD% add site /name:"%SITE_NAME%" /physicalPath:%PROJECT_DIR% /bindings:http/*:%SITE_PORT%:%SITE_HOST_NAME%
echo Create application pool for site
%APPCMD% add apppool /name:%SITE_NAME%
echo Link site to new pool
%APPCMD% set site /site.name:%SITE_NAME% /[path='/'].applicationPool:%SITE_NAME%
echo Add module mapping for site
%APPCMD% set config "%SITE_NAME%" -section:system.webServer/handlers /+"[name='%SITE_NAME%',path='*',verb='*',modules='FastCgiModule',scriptProcessor='%PYTHON_PATH%|%WFASTCGI_PATH%',resourceType='Unspecified']"
echo Add fastcgi setting
%APPCMD% set config -section:system.webServer/fastCgi /+"[fullPath='%PYTHON_PATH%',arguments='%WFASTCGI_PATH%']" /commit:apphost
echo Add WSGIHANDLER variable to fastcgi setting
%APPCMD% set config -section:system.webServer/fastCgi /+"[fullPath='%PYTHON_PATH%',arguments='%WFASTCGI_PATH%'].environmentVariables.[name='WSGI_HANDLER',value='%WSGI_HANDLER_VALUE%']" /commit:apphost
echo Open google chrome
start "" "c:\program files (x86)\google\chrome\application\chrome.exe" --new-window "http://localhost:%SITE_PORT%"
pause