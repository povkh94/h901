#!/bin/bash
#Declare variable
cp ./b24.pem ~/b24.pem
chmod 600 ~/b24.pem

SSHKEYPATH=~/b24.pem #Private key chmod 600 ~/.ssh/b24

APPNAME=h901
APPRUNPORT=9011
APPADDRESS=127.0.0.1
APPSHAREDIR=/var/www/$APPNAME/shared
STATIC=/var/www/$APPNAME/project/admin/
APPSTATIC=/var/www/$APPNAME/project/admin/static
MAINFILE=./main.py

PROJECTDIR=../project
PROJECTTARGETDIR=/var/www/$APPNAME
PROJECTSTATIC=../.docker/app/static

ENVFILE=$PROJECTTARGETDIR/project/requirements.txt #Path to requirements.txt
ENVPATH=/var/www/env/bin #Default path of env

SERVERADDRESS=192.168.1.93
USER=khun
GROUP=nginx
#End declaration

echo "create project directory in server..."
ssh -i $SSHKEYPATH root@$SERVERADDRESS mkdir $PROJECTTARGETDIR

echo "upload project to server..."
sudo rsync -avzhe "ssh -i $SSHKEYPATH" $PROJECTDIR --exclude="*.pyc" --exclude="*/module/share/static/*" root@$SERVERADDRESS:$PROJECTTARGETDIR
echo "upload project static file to server..."
sudo rsync -avzhe "ssh -i $SSHKEYPATH" $PROJECTSTATIC root@$SERVERADDRESS:$STATIC
echo "upload project main file to server..."
sudo rsync -avzhe "ssh -i $SSHKEYPATH" $MAINFILE root@$SERVERADDRESS:$PROJECTTARGETDIR

#Create env if have requirements.txt
if [ $ENVFILE != '' ]
ENVPATH=$PROJECTTARGETDIR/env/bin
then
echo "create env for project..."
ssh -i $SSHKEYPATH root@$SERVERADDRESS << EOF
virtualenv $PROJECTTARGETDIR/env
source $PROJECTTARGETDIR/env/bin/activate
pip install -r $ENVFILE
pip install uwsgi
deactivate
EOF
fi
#Share permission of project to everyone
ssh -i $SSHKEYPATH root@$SERVERADDRESS chmod 777 $PROJECTTARGETDIR

ssh -i $SSHKEYPATH root@$SERVERADDRESS << EOF

echo "create uwsgi configuration..."
cat > $PROJECTTARGETDIR/uwsgi.ini <<- EOM
[uwsgi]
module = main
callable = app

master = true
processes = 5
threads = 5
harakiri = 60
plugin = python

socket = uwsgi.sock
chmod-socket = 660
vacuum = true

die-on-term = true
EOM

echo "create service for project..."
cat > /etc/systemd/system/$APPNAME.service <<- EOM
[Unit]
Description=uWSGI instance to serve project
After=network.target

[Service]
User=$USER
Group=$GROUP
WorkingDirectory=$PROJECTTARGETDIR
Environment="PATH=$ENVPATH"
ExecStart=$ENVPATH/uwsgi --ini uwsgi.ini

[Install]
WantedBy=multi-user.target
EOM

echo "create nginx configuration..."
cat > /etc/nginx/conf.d/$APPNAME.conf <<- EOM
server {
    listen $APPRUNPORT;
    server_name $APPADDRESS;

    location /static {
        expires 30d;
        autoindex on;
        add_header Cache-Control private;
        alias $APPSTATIC;   
    }

    location / {
        uwsgi_send_timeout 600;        
        uwsgi_connect_timeout 600; 
        uwsgi_read_timeout 600;
        include uwsgi_params;
        uwsgi_pass unix:$PROJECTTARGETDIR/uwsgi.sock;
    }
    
    location /files {
        alias $APPSHAREDIR;
    }
}
EOM

echo "start app service..."
systemctl daemon-reload
systemctl stop $APPNAME
systemctl start $APPNAME
systemctl enable $APPNAME

echo "restart nginx..."
systemctl restart nginx

echo "Allow port through with firewall"
firewall-cmd --permanent --add-port=$APPRUNPORT/tcp
firewall-cmd --reload
EOF
