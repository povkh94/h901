#!/bin/bash
#Declare variable
cp ./b24.pem ~/b24.pem
chmod 600 ~/b24.pem

SSHKEYPATH=~/b24.pem #Private key chmod 600 ~/.ssh/b24

APPNAME=h901
APPRUNPORT=9011
APPADDRESS=127.0.0.1
APPSHAREDIR=/var/www/$APPNAME/shared
STATIC=/var/www/$APPNAME/project/admin/
APPSTATIC=/var/www/$APPNAME/project/admin/static
MAINFILE=./main.py

PROJECTDIR=../project
PROJECTTARGETDIR=/var/www/$APPNAME
PROJECTSTATIC=../.docker/app/static

ENVFILE=$PROJECTTARGETDIR/project/requirements.txt #Path to requirements.txt
ENVPATH=/var/www/env/bin #Default path of env

SERVERADDRESS=192.168.1.93
USER=khun
GROUP=nginx
#End declaration

echo "create project directory in server..."
ssh -i $SSHKEYPATH root@$SERVERADDRESS mkdir $PROJECTTARGETDIR

echo "upload project to server..."
sudo rsync -avzhe "ssh -i $SSHKEYPATH" $PROJECTDIR --exclude="config.py" --exclude="*.pyc" --exclude="*/module/share/static/*" root@$SERVERADDRESS:$PROJECTTARGETDIR
echo "upload project static file to server..."
sudo rsync -avzhe "ssh -i $SSHKEYPATH" $PROJECTSTATIC root@$SERVERADDRESS:$STATIC
echo "upload project main file to server..."
sudo rsync -avzhe "ssh -i $SSHKEYPATH" $MAINFILE root@$SERVERADDRESS:$PROJECTTARGETDIR

#Create env if have requirements.txt
if [ $ENVFILE != '' ]
ENVPATH=$PROJECTTARGETDIR/env/bin
then
echo "create env for project..."
ssh -i $SSHKEYPATH root@$SERVERADDRESS << EOF
virtualenv $PROJECTTARGETDIR/env
source $PROJECTTARGETDIR/env/bin/activate
pip install -r $ENVFILE
pip install uwsgi
deactivate
EOF
fi

ssh -i $SSHKEYPATH root@$SERVERADDRESS << EOF
echo "start app service..."
systemctl stop $APPNAME
systemctl start $APPNAME
EOF
