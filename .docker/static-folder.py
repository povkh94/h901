import distutils
import os
import shutil
import sys

if os.path.isdir('./app/static/'):
    shutil.rmtree('./app/static/')

for subdir, dirs, files in os.walk('../project/module'):
    for dir in dirs:
        static_dir = os.path.join(subdir, dir + '/static').replace('\\', '/')
        if os.path.isdir(static_dir):
            temp_dir = './app/static/{}'.format(dir)

            print 'Copying.. {} ==> {} ....'.format(static_dir, temp_dir)
            shutil.copytree(static_dir, temp_dir, )
