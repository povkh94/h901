# How to build docker image and start docker container 

## Prerequesit

To start a project in docker you need to 
1. Make sure docker is install on your local machine.
2. You need to understand what is docker? can use basic docker command.
3. Make sure you can run docker command any where (add docker bin to enviroment PATH of operation system)

## How to build docker image and start it
To build docker image you need to do the following step.
1. In /project/ you can keep every thing that override development folder such as configuration
2. Go /.docker then run command `docker-build.bat`
3. Enter project name in lower case `> Project:m` then hit <ENTER>
3. Everything is fine :)