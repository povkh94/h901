SET @echo off

SET /P p="Project:"
python ./static-folder.py

REM To make sure docker build context can access everything.
CD ..

docker stop %p%c
docker container prune  -f
docker build -f .docker/Dockerfile . -t %p%i

REM DELETE TEMP FOLDER.

RMDIR /S /Q "%CD%\.docker\app\static"


docker run --name %p%c -d -p 5000:80 %p%i
START chrome http://localhost:5000


PAUSE