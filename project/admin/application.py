# initialize database first
# import logging
# logging.basicConfig(filename='app.log',level=logging.DEBUG,format= '%(asctime)s - %(levelname)s - %(message)s - %(pathname)s, %(funcName)s, line %(lineno)d, process %(process)d, thread %(thread)d')
import logging

from edf.helper.jsonx import ExtEncoder
from flask import Flask, redirect, url_for, request, session
from flask_cdn import CDN
from project.admin import config

# app package


app = Flask(__name__)
app.json_encoder = ExtEncoder
app.config.from_object(config)
# CDN(app)

# initialize db
from project.core.database import db

@app.teardown_appcontext
def teardown_appcontext(resp):
    db.remove()
    db.rollback()
    return resp


from sqlalchemy.exc import SQLAlchemyError


@app.errorhandler(Exception)
def page_error(e):
    logging.exception(e)
    from sqlalchemy.exc import SQLAlchemyError
    if issubclass(e.__class__, SQLAlchemyError):
        db.rollback()
    return '%s' % e, 400


from project.module.module_lib import activate_module

ctx = app.app_context()
ctx.push()

# override current function
# import project.admin.current
# import project.admin.view

# load pluggable module ...
from project.module import system, system_security, share

activate_module(share)
activate_module(system)
activate_module(system_security)

from project.module import company, currency, payment, agri_bill

activate_module(company)
activate_module(currency)
activate_module(payment)
activate_module(agri_bill)

@app.route('/')
def index():
    return redirect(url_for('HomeView:home'))


ctx.pop()
# yet another conflict
# pretend for conflict
