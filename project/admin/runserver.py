# change entry dir
import sys
import os.path
current_dir = os.path.dirname(os.path.realpath(__file__))
target_dir = '\\'.join(current_dir.split('\\')[0:-2]) 
sys.path.insert(0,target_dir)

# running app server.
from os import environ
from project.admin.application import app
import flask_excel as excel
if __name__ == '__main__':
     HOST = environ.get('SERVER_HOST', '127.0.0.1')
     try:
         PORT = int(environ.get('SERVER_PORT', '9999'))
     except ValueError:
         PORT = 5555
     app.debug = False
     excel.init_excel(app)
     app.run(host=HOST, port=PORT,threaded=True)


