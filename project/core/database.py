from edf.base.database import *
from . import config


db, engine = create_session(config.SQLALCHEMY_DATABASE_URI)

# it will run auto migration if project
if config.SQLALCHEMY_LIVE_MIGRATION:
    from .migration.command import upgrade
    upgrade('head')