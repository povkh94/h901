﻿from project.core.database import db
from project.module.system.model import Lookup, LookupValue

def add_enum(enum):  
    lookup = Lookup(id = enum.__val__, name=enum.__name__) 
    lookup.ext__is_system = True
    lookup.ext__enable_add = True
    lookup.ext__enable_duplicate_value = True
    db.add(lookup) 
    for i, (k, v) in enumerate(enum.get_members().items()):
        db.add(LookupValue(lookup_id = enum.__val__,is_system=True,name = k, value = v))
    db.commit()
