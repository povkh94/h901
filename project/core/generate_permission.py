from flask import abort, make_response, jsonify, request, redirect, current_app
from edf.base.database import db
from ..module.system_security.model import *
from ..module.system_security.logic import permission_logic, role_logic

from ..admin.application import app

endpoints_to_ignore = [
    'SecurityView:change_password',
    'SecurityView:clear_session',
    'SecurityView:confirm_password',
    'SecurityView:login',
    'SecurityView:logout',
    'SecurityView:profile',
    'SecurityView:signup',
    'SecurityView:signup_success',
    'SecurityView:verify_email',
    'SocialView:connect_facebook',
    'SocialView:connect_facebook_callback',
    'SocialView:facebook_authorize',
    'SocialView:facebook_authorized',
    'BankPaymentView:download_payment',
    'CurrencyView:index_filter',
    'ExchangeRateView:index_filter',
    'InvoiceView:chunks',
    'LandView:sum_area',
    'LocationView:location_suggestion',
    'PaymentView:invoice_payment_suggestion',
    'VerifyBillingView:chunks',
    'VerifyBillingView:export_content_contracts',
    'VerifyBillingView:export_pdf',
    'VerifyBillingView:export_verify_content_contracts',
]


@app.route("/generate_permission")
def generate_permission():
    # all permissions id
    permission_id = []
    # endpoint that will be required permission to access
    endpoints = []
    for rule in app.url_map.iter_rules():
        endpoint = (rule.endpoint
                    .replace(':filter', '')
                    .replace(':suggestion', '')
                    .replace(':render_template', ''))
        if 'View' in endpoint \
                and endpoint not in endpoints_to_ignore:
            endpoints.append(endpoint)

    # links is now a list of url, endpoint tuples
    endpoints = list(set(endpoints))
    endpoints.sort()
    for endpoint in endpoints:
        name = None
        if db.query(Permission).filter(Permission.endpoint == endpoint).first():
            continue
        # finding parent permission
        parent_endpoint = endpoint.split(':')[:1][0]
        parent = db.query(Permission).filter(Permission.endpoint == parent_endpoint).first()
        parent_id = parent.id if parent else None
        # flush permission to database
        name = parent_endpoint
        if parent_id:
            name = endpoint.split(':')[1]

        permission = Permission()
        permission.name = name
        permission.endpoint = endpoint
        permission.parent_id = parent_id
        db.add(permission)
        db.flush()
        permission_id.append(permission.id)

    if endpoints:
        # add owner role full option
        role = db.query(Role).filter(Role.id == str(1)).first()
        if not role:
            role = Role()
            role.id = 1
            role.name = u'Administrator'
            role.description = u'Full options'
            role.scope = None
            role.permissions = ','.join(str(id) for id in permission_id)
        else:
            new_perm = ','.join(str(id) for id in permission_id)
            role.permissions += ',' + new_perm
        db.add(role)
        db.flush()
    db.commit()

    # delete endpoint
    del_endpoints = ['invoice_content', 'find_invoice_by_farmer_id', 'land_data', 'ResetPasswordView:verify',
                 'ResetPasswordView:verify_success', 'HomeView:home','update_data_to_gateway',
                 'HomeView:dashboard', 'get_existing_block', 'get_existing_block',
                 'ResetPasswordView:verify_reset_code', 'ResetPasswordView:reset_password',
                 'report_template', 'ResetPasswordView:verify_success', 'ResetPasswordView:verify',
                 're_contract_content', 'contract_content', 'HomeView:dashboard', 'detail_filter', 'detail_filter',
                 'BillingCycleView_land', 'activate_success', 'activate','receipt_content','ResetPasswordView','CurrencyView',
                     'PaymentView:invoice_payment_suggestion','CurrencyView:add','CurrencyView:detail','CurrencyView:remove','CurrencyView:edit','CurrencyView:index','ExchangeRateView','ExchangeRateView:index','ExchangeRateView:edit','ExchangeRateView:detail','ExchangeRateView:remove','HomeView']
    db.query(Permission).filter(Permission.name.in_(del_endpoints)).delete(synchronize_session=False)
    db.commit()

    shutdown_server()
    print "All Done. :)"
    return "Permission generated."


def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
