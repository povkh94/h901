# change entry dir
# - *- coding: utf- 8 - *-
import sys
import os.path

current_dir = os.path.dirname(os.path.realpath(__file__))
target_dir = '\\'.join(current_dir.split('\\')[0:-2])
sys.path.insert(0, target_dir)
from project.core.database import *


print 'init schema...'
init_schema(engine)

print '*********Import Permission*********'
from project.core.generate_permission import *

if __name__ == '__main__':
    import threading, webbrowser
    print '*********Generating permission*********'
    print ''
    # start browser on generate_permission url
    threading.Timer(1.25, lambda: webbrowser.open('http://localhost:5555/generate_permission')).start()
    app.run('localhost', 5555)
