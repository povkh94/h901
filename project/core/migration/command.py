import os
import alembic
from alembic.config import Config
from project.core import config

dir_path = os.path.dirname(os.path.realpath(__file__))
ini_path = os.path.join(dir_path, 'alembic.ini')


def upgrade(revision='head', sql=False):
    # > alembic upgrade head
    os.chdir(dir_path)
    ini_config = Config(ini_path)
    ini_config.set_main_option('sqlalchemy.url', config.SQLALCHEMY_DATABASE_URI)
    alembic.command.upgrade(ini_config, revision=revision, sql=sql)

def downgrade(revision='base', sql=False):
    # > alembic downgrade base
    os.chdir(dir_path)
    ini_config = Config(ini_path)
    ini_config.set_main_option('sqlalchemy.url', config.SQLALCHEMY_DATABASE_URI)
    alembic.command.downgrade(ini_config, revision=revision, sql=sql)


def upgrade_script():
    # > alembic upgrade head --sql
    upgrade(sql=True)


def downgrade_script():
    # > alembic downgrade head:base --sql
    downgrade(sql=True, revision='head:base')
