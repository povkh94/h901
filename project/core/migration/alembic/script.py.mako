"""${message}

Revision ID: ${up_revision}
Revises: ${down_revision | comma,n}
Create Date: ${create_date}

"""
from alembic import op
import sqlalchemy as sa

${imports if imports else ""}

# revision identifiers, used by Alembic.
revision = ${repr(up_revision)}
down_revision = ${repr(down_revision)}
branch_labels = ${repr(branch_labels)}
depends_on = ${repr(depends_on)}


# customize & additional code
def list_dict(data):
    from unicodecsv import DictReader
    from StringIO import StringIO
    f = StringIO()
    f.write(data.encode('utf-8'))
    f.seek(0)    
    return [row for row in DictReader(f,delimiter=',')] 

def bulk_insert(type,csv):
       op.bulk_insert(type, list_dict(csv), multiinsert = False) 


def upgrade():
    ${upgrades if upgrades else "pass"}


def downgrade():
    ${downgrades if downgrades else "pass"}
