# change entry dir
# - *- coding: utf- 8 - *-
import sys
import os.path

current_dir = os.path.dirname(os.path.realpath(__file__))
target_dir = '\\'.join(current_dir.split('\\')[0:-2])
sys.path.insert(0, target_dir)
from project.core.database import *
from project.module.system.model import *
from project.module.system_security.model import *
from project.module.company.model import *
from project.module.currency.model import *
from project.module.payment.model import *
from project.module.agri_bill.model import *
from project.module.payment import enums
from project.module.agri_bill import enum
from project.core.init_lookup import add_enum




print 'init schema...'
init_schema(engine)

print 'init data...'
db.add(Currency(id='USD', code='USD', name='US-Dollar', sign='$', round_digit=2))
db.add(Currency(id='KHR', code='KHR', name='Cambodia Riel', sign=u'៛', round_digit=2))
db.add(Currency(id='THB', code='THB', name='Thai Baht', sign='B', round_digit=2))

print 'init enum'
add_enum(enum.payment_method)
add_enum(enum.pay_status)
add_enum(enum.BankPaymentActionType)
add_enum(enum.BankPaymentResult)
add_enum(enum.InvoiceType)
add_enum(enum.PenaltyType)

db.add(Lookup(id='sex', name='Sex'))
db.add(LookupValue(id='m', value='Male', name=u'ប្រុស', lookup_id='sex'))
db.add(LookupValue(id='f', value='Female', name=u'ស្រី', lookup_id='sex'))
db.add(LookupValue(id='n', value='NA', name=u'មិនដឹង', lookup_id='sex'))

db.add(Lookup(id='b_status', name='Bill Status'))
db.add(LookupValue(id='start ', value='Start', name=u'ចាប់ផ្តើម', lookup_id='b_status'))
db.add(LookupValue(id='close', value='Close', name=u'បញ្ចប់', lookup_id='b_status'))
db.add(LookupValue(id='verify', value='Verify', name=u'ផ្ទៀងផ្ទាត់រួច', lookup_id='b_status'))

db.add(Lookup(id='mar_status', name='Marital Status'))
db.add(LookupValue(id='single', value='Single', name=u'លីវ', lookup_id='mar_status'))
db.add(LookupValue(id='divorced', value='Divorced', name=u'មេម៉ាយ', lookup_id='mar_status'))
db.add(LookupValue(id='widowed', value='Widowed', name=u'ពោះម៉ាយ', lookup_id='mar_status'))
db.add(LookupValue(id='married', value='Married', name=u'រៀបការរួច', lookup_id='mar_status'))
db.add(LookupValue(id='NA', value='NA', name=u'មិនដឹង', lookup_id='mar_status'))

db.add(Lookup(id='con_tem_id', name='contract_template_id'))
db.add(LookupValue(id='billing', value='billing', name=u'ចេញវិក្កយបត្រ', lookup_id='con_tem_id'))


db.add(Lookup(id='l_status', name='Land Status'))
db.add(LookupValue(id='irrigate', value='irrigated', name=u'យកទឹក', lookup_id='l_status'))
db.add(LookupValue(id='n_irrigate', value='non_irrigated', name=u'មិនយកទឹក', lookup_id='l_status'))

db.add(Lookup(id='in_status', name='Invoice Status'))
db.add(LookupValue(id='pending', value='pending', name=u'មិនទាន់បង់', lookup_id='in_status'))
db.add(LookupValue(id='partial', value='partial', name=u'បង់ហើយខ្លះ', lookup_id='in_status'))
db.add(LookupValue(id='void', value='void', name=u'ទុកជាមោឃៈ', lookup_id='in_status'))
db.add(LookupValue(id='paid', value='paid', name=u'បង់រួច', lookup_id='in_status'))

""" init status when delete records """
db.add(Lookup(id='activate', name='Activate Status'))
db.add(LookupValue(id='active', value='active', name=u'ដំណើរការ', lookup_id='activate'))
db.add(LookupValue(id='disactive', value='disactive', name=u'លុប', lookup_id='activate'))


db.add(Lookup(id='v_status', name='Verify Status'))
db.add(LookupValue(id='all_verify', value='all verify', name=u'ទាំងអស់', lookup_id='v_status'))
db.add(LookupValue(id='verified', value='verified', name=u'ផ្ទៀងផ្តាត់រួច', lookup_id='v_status'))
db.add(LookupValue(id='unverified', value='unverified', name=u'មិនផ្ទៀងផ្តាត់រួច', lookup_id='v_status'))

db.add(Location(id='1',code='PNA',        type='1',   name=u'គ្មាន',		latin_name='PNA',				prefix=u'ខេត្ត',	latin_prefix='Province',	parent_id=''))
db.add(Location(id='2',code='DNA',      type='2',   name=u'គ្មាន',		latin_name='DNA',		prefix=u'ស្រុក',	latin_prefix='District',	parent_id='1'))
db.add(Location(id='3',code='CNA',    type='3',   name=u'គ្មាន',			latin_name='CNA',				prefix=u'ឃុំ',	latin_prefix='Commune',		parent_id='2'))
db.add(Location(id='4',code='VNA',  type='4',   name=u'គ្មាន',		latin_name='VNA',		prefix=u'ភូមិ',	latin_prefix='Village',		parent_id='3'))




db.add(Tariff(id='1',name=u'បង្ហូរ',price=2000.00))
db.add(Tariff(id='2',name=u'បូមបន្តពីស្ថានីយ៍',price=2500.00))
db.add(Tariff(id='3',name=u'បូមពីប្រឡាយមេ',price=200.00))

# db.add(Season(id='1',name=u'វស្សា'))
# db.add(Season(id='2',name=u'ប្រាំ'))
# db.add(Season(id='3',name=u'សម្រក'))

db.add(ContractTemplate(id='1',name=u'កិច្ចសន្យាប្រើប្រាស់ទឹក',content_template=u'''<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; mso-line-height-alt: .1pt;" align="center"><span lang="KHM" style="font-family: 'Khmer OS Muol'; font-size: 10pt;">ព្រះរាជាណាចក្រកម្ពុជា</span></p>
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; mso-line-height-alt: .1pt;" align="center"><span lang="KHM" style="font-family: 'Khmer OS Muol'; font-size: 10pt;">ជាតិ សាសនា​ ព្រះមហាក្សត្រ</span></p>
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; mso-line-height-alt: .1pt;" align="center"><span style="mso-bidi-font-size: 11.0pt; font-family: Wingdings; mso-ascii-font-family: 'Khmer OS'; mso-hansi-font-family: 'Khmer OS'; mso-bidi-font-family: 'Khmer OS'; mso-char-type: symbol; mso-symbol-font-family: Wingdings;"></span><span style="mso-bidi-font-size: 11.0pt; font-family: Wingdings; mso-ascii-font-family: 'Khmer OS'; mso-hansi-font-family: 'Khmer OS'; mso-bidi-font-family: 'Khmer OS'; mso-char-type: symbol; mso-symbol-font-family: Wingdings;">&macr;</span><span style="mso-bidi-font-size: 11.0pt; font-family: Wingdings; mso-ascii-font-family: 'Khmer OS'; mso-hansi-font-family: 'Khmer OS'; mso-bidi-font-family: 'Khmer OS'; mso-char-type: symbol; mso-symbol-font-family: Wingdings;"></span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: 80%; text-align: center;"><span lang="KHM" style="line-height: 80%; font-family: 'Khmer OS Bokor'; font-size: 10pt;">សហគមន៍កសិករប្រើប្រាស់ទឹកធម្មវិន័យ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; លេខកិច្ចសន្យា&nbsp;<span style="font-family: 'Khmer Muol';">{{ contract_number or "-"}}</span></span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: 80%; text-align: center;"><u><span lang="KHM" style="font-size: 9.0pt; line-height: 80%; font-family: 'Khmer OS Muol';">កិច្ចសន្យាប្រើប្រាស់ទឹក</span></u><u></u></p>
<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; mso-line-height-alt: .1pt;" align="center"><u><span lang="KHM" style="font-size: 9.0pt; font-family: 'Khmer OS Muol';">សម្រាប់&nbsp;<strong>{{ billing[1] or "-"}}</strong> ឆ្នាំ&nbsp;<strong>{{ billing[0] or "-"}}</strong></span></u></p>
<p class="MsoNormal"><span style="font-size: 5.0pt; font-family: 'Khmer OS';">&nbsp;</span><span style="font-size: 10pt;"><span lang="KHM" style="mso-bidi-font-size: 11.0pt; font-family: 'Khmer OS';">កិច្ចសន្យានេះធ្វើឡើងរវាងកសិករ និង ​សហគមន៍កសិករប្រើប្រាស់ទឹកធម្ម​វិន័យ ដែលតំណាងដោយ<strong>លោក&nbsp;{{block.gm_block or "-"}}</strong>&nbsp;ប្រធាន<strong>ប្លុកទី</strong>{{block.name or "-"}}ដែលហៅកាត់ថាភាគី (ក)&nbsp;<strong>និង&nbsp;</strong></span><strong><span lang="KHM" style="mso-bidi-font-size: 11.0pt; font-family: 'Khmer OS';">កសិករឈ្មោះ&nbsp;{{farmer.name or "-"}}</span></strong><span lang="KHM" style="mso-bidi-font-size: 11.0pt; font-family: 'Khmer OS';">​&nbsp;<strong>ភេទ {{farmer.sex_kh or "-"}}</strong>&nbsp; រស់​នៅ​<strong>ភូមិ&nbsp;{{location.village or "-"}}&nbsp;</strong><strong>ឃុំ</strong>&nbsp;{{location.commune or "-"}}&nbsp;<strong>ស្រុក {{location.district or "-"}}&nbsp;</strong><strong>ខេត្ត&nbsp;{{location.province or "-"}}</strong>មាន​<strong>ទូរស័ព្ទ​លេខ&nbsp;{{farmer.phone or "-"}}</strong>ជា<strong>ប្តី/ប្រពន្ធ ឈ្មោះ&nbsp;{{farmer.spouse_name or "-"}}</strong>មានផ្ទៃដីដូចតារាងដែលមានភ្ជាប់ ហៅកាត់ថាភាគី (ខ)&nbsp;</span><span lang="KHM" style="mso-bidi-font-size: 11.0pt; font-family: 'Khmer OS';">ភាគីទាំងពីរបានព្រមព្រៀងគ្នាតាមលក្ខន្តិកៈដូចខាងក្រោម ៖</span></span></p>
<p class="MsoNormal"><span style="font-size: 10pt;"><strong><u><span lang="KHM" style="mso-bidi-font-size: 11.0pt; font-family: 'Khmer OS';">ភាគី (ក)</span></u></strong><u></u></span></p>
<p class="MsoNormal" style="mso-line-height-alt: .1pt; margin: 6.0pt 0in .0001pt 0in;"><span lang="KHM" style="font-family: 'Khmer OS'; font-size: 10pt;">ប្រការ ១៖ ផ្គត់ផ្គង់ទឹកគ្រប់គ្រាន់ ទៅតាមផែនការដែលសហគមន៍បានកំណត់​ លើកលែងតែករណីម៉ាស៊ីនបូមមានបញ្ហា​ ឬមានគ្រោះធម្មជាតិដែលបណ្តាលអោយប្រភពទឹក(ទន្លេ) មិនគ្រប់គ្រាន់សំរាប់ដំណើរការបូម។</span></p>
<p class="MsoNormal" style="mso-line-height-alt: .1pt; margin: 6.0pt 0in .0001pt 0in;"><span lang="KHM" style="font-family: 'Khmer OS'; font-size: 10pt;">ប្រការ ២៖ ចូលរួមដោះស្រាយបញ្ហាផ្សេងៗនៅក្នុងការប្រើប្រាស់ទឹក ។</span></p>
<p class="MsoNormal" style="mso-line-height-alt: .1pt; margin: 6.0pt 0in .0001pt 0in;"><span lang="KHM" style="font-family: 'Khmer OS'; font-size: 10pt;">ប្រការ ៣៖ ពុំទទួលខុសត្រូវក្នុងករណីកង្វះខាតទឹក ឬទឹកមិនដល់ស្រែកសិករ ដែលបណ្តាលមកពីមិនបានជីកប្រឡាយជើងក្អែបបញ្ចូលស្រែ។</span></p>
<p class="MsoNormal"><span style="font-size: 10pt;"><strong><u><span style="font-family: 'Khmer OS';"><span style="text-decoration-line: none;">&nbsp;</span></span></u></strong><strong><u><span lang="KHM" style="mso-bidi-font-size: 11.0pt; font-family: 'Khmer OS';">ភាគី (ខ)</span></u></strong><u></u></span></p>
<p class="MsoNormal" style="mso-line-height-alt: .1pt; margin: 6.0pt 0in .0001pt 0in;"><span lang="KHM" style="font-family: 'Khmer OS'; font-size: 10pt;">ប្រការ ១៖ គោរពតាមលក្ខខន្តិកៈរបស់សហគមន៍។</span></p>
<p class="MsoNormal" style="mso-line-height-alt: .1pt; margin: 6.0pt 0in .0001pt 0in;"><span lang="KHM" style="font-family: 'Khmer OS'; font-size: 10pt;">ប្រការ ២៖ ធានាបង់ថ្លៃសេវាប្រើប្រាស់ទឹកដែលបានកំណត់ដោយសហគមន៍ទៅតាមផ្ទៃដីផលិតរបស់ខ្លួន​​ ។</span></p>
<p class="MsoNormal" style="mso-line-height-alt: .1pt; margin: 6.0pt 0in .0001pt 0in;"><span lang="KHM" style="font-family: 'Khmer OS'; font-size: 10pt;">ប្រការ ៣៖ ចូលរួមធ្វើប្រឡាយជើងក្អែប ប្រឡាយដោះទឹក និងធានាថែទាំភ្លឺស្រែដោយខ្លួនឯង ។</span></p>
<p class="MsoNormal" style="mso-line-height-alt: .1pt; margin: 6.0pt 0in .0001pt 0in;"><span lang="KHM" style="font-family: 'Khmer OS'; font-size: 10pt;">ប្រការ ៤៖ គោរពតាមកាលវិភាគបែងចែកទឹករបស់សហគមន៍​ និង​ចូលរួម​សន្សំសំចៃទឹក មិនខ្ជះខ្ជាយទឹក ដោយទទួលខុសត្រូវក្នុងការបិទ បើកទុយោបង្ហូរទឹកដោយខ្លួនឯង ។</span></p>
<p class="MsoNormal" style="mso-line-height-alt: .1pt; margin: 6.0pt 0in .0001pt 0in;"><span style="font-size: 10pt;"><span lang="KHM" style="mso-bidi-font-size: 11.0pt; font-family: 'Khmer OS';">ប្រការ ៥៖ មិនត្រូវបង្កឲ្យមានទំនាស់ក្នុងការប្រើប្រាស់ទឹកនៅក្នុងសហគមន៍ ។</span><span lang="KHM" style="mso-bidi-font-size: 11.0pt; font-family: 'Khmer OS';">ក្នុងករណីភាគីណាមួយមិនគោរពតាមប្រការដែលមានចែងនៅក្នុងកិច្ចសន្យាខាងលើ នឹងត្រូវទទួលខុសត្រូវចំពោះមុខច្បាប់​ ហើយ​សហគមន៍មាន​សិទ្ធិ​បញ្ឈប់សមាជិកភាព​ក្នុង​ការ​ប្រើ​ប្រាស់​ទឹក​ ៕</span></span></p>
<p class="MsoNormal" style="mso-line-height-alt: .1pt; margin: 6.0pt 0in .0001pt 0in;"><span style="font-size: 10pt;"><strong><span lang="KHM" style="mso-bidi-font-size: 11.0pt; font-family: 'Khmer OS';">សំគាល់៖&nbsp;</span></strong><span lang="KHM" style="mso-bidi-font-size: 11.0pt; font-family: 'Khmer OS';">កិច្ចសន្យាប្រើប្រាស់ទឹកនេះធ្វើឡើងចំនួន ០២ច្បាប់ ទុកជូនសហគមន៍ ០១ច្បាប់ និង កសិករ ០១ច្បាប់ ៕</span></span></p>
<p class="MsoNormal" style="mso-line-height-alt: .1pt; margin: 6.0pt 0in .0001pt 0in;">&nbsp;</p>
<p class="MsoNormal" style="margin: 6pt 0in 0.0001pt; text-align: center;">&nbsp;<span style="font-size: 10pt;"><u><span lang="KHM" style="font-family: 'Khmer OS Muol';">ព័ត៌មានស្រែលំអិត​​ និង​ការ​យក​ទឹក</span></u></span>&nbsp;</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 10pt;"><strong><span lang="KHM" style="mso-bidi-font-size: 11.0pt; font-family: 'Khmer OS';">ប្រភេទ​ពូជៈ............&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ថ្ងៃព្រោះស្រូវៈ ...... / ...... / ២០១&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ថ្ងៃប្រមូលផលៈ ..... / ..... / ២០១</span></strong></span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;"><span style="font-size: 10pt;"><span lang="KHM" style="mso-bidi-font-size: 11.0pt; font-family: 'Khmer OS';">{{ land.table | to_table(blank=14) | safe or "-"}}</span></span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: left;"><span style="font-size: 10pt;"><span lang="KHM" style="mso-bidi-font-size: 11.0pt; font-family: 'Khmer OS';"><strong>សង្ខេប ក្បាលដីសរុបមានចំនួន</strong>&nbsp;<strong>{{total_area | format_currency() or "-"}}​ អា</strong></span></span></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;">&nbsp;</p>
<p class="MsoNormal" style="text-align: center;"><strong><span lang="KHM" style="font-size: 10.0pt; font-family: 'Khmer OS';">ស្នាមមេដៃភាគី (ក)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ស្នាមមេដៃភាគី (ខ)</span></strong><strong><span style="font-size: 10.0pt; font-family: 'Khmer OS';">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span lang="KHM">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ស្នាមមេដៃអ្នក​ជួល​ដី</span></span></strong></p>
<p class="MsoNormal" style="text-align: left; padding-left: 60px;"><span lang="KHM" style="font-size: 11.0pt; line-height: 115%; font-family: 'Khmer OS'; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: KHM;">&nbsp; &nbsp;</span></p>
<p class="MsoNormal" style="text-align: left; padding-left: 60px;">&nbsp;</p>
<p class="MsoNormal" style="text-align: left; padding-left: 60px;"><span lang="KHM" style="font-size: 11.0pt; line-height: 115%; font-family: 'Khmer OS'; mso-fareast-font-family: Calibri; mso-fareast-theme-font: minor-latin; mso-ansi-language: EN-US; mso-fareast-language: EN-US; mso-bidi-language: KHM;">&nbsp; &nbsp;ទទួលស្គាល់ដោយៈ</span></p>
<p class="MsoNormal" style="padding-left: 60px; text-align: left;">&nbsp; &nbsp;<span lang="KHM" style="mso-bidi-font-size: 11.0pt; line-height: 115%; font-family: 'Khmer OS';">ប្រធានសហគមន៍&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span><span style="font-family: 'Khmer OS'; font-size: 11pt;">ប្តី</span><span style="font-family: 'Khmer OS'; font-size: 11pt;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><span style="font-family: 'Khmer OS'; font-size: 11pt;">ប្រពន្ធ</span></p>''',
                        contract_template_id='billing'))


db.add(Setting(
       id='KKGWvCkm',
       name='default_sequence',
       datatype='json',
       value='{"farmer-code": "{n:6}","invoice-code":"INV{n:6}","payment-code": "RPT{n:6}"}',
       is_active='1'))

# update farmer-code to farmer-code:{n:6}
db.commit()

print '*********Import Permission*********'
from project.core.generate_permission import *

if __name__ == '__main__':
    import threading, webbrowser
    print '*********Generating permission*********'
    print ''
    # start browser on generate_permission url
    threading.Timer(1.25, lambda: webbrowser.open('http://localhost:5555/generate_permission')).start()
    app.run('localhost', 5555)
