__info__ = {
    "name": "system_security",
    "version" : "1.0.0",
    "title": "System Security Module",
    "summary": "System module for security User, Role, Permission, Setting etc.",
    "author": "RY Rith",
    "website": "http://project.com",
    "depends": ['share']
}


def activate(**kwargs):
    import model
    import current
    import auth
    import view
    import widget


def install(**kwargs):
    pass


def uninstall(**kwargs):
    pass
