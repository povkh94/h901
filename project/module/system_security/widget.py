from flask import url_for
from flask_babel import lazy_gettext as _
from ..share.lib.widget import *


# sidebar navigation register
# register_widget('sidebar', order=1000, widget=NavWidget(
#     id='security', title=_('Security'), icon='fa fa-shield',
#     children=[
#         NavWidget(id='user', title=_('User'), icon='fa fa-user', url=lambda: url_for('.UserView:index')),
#         NavWidget(id='role', title=_('Role'), icon='fa fa-file', url=lambda: url_for('.RoleView:index')),
#     ]
# ))
# register_widget('sidebar', order=1001, widget=Widget(id='create-store-widget', text="""
# <ul style="margin-top:40px;border:1px solid rgba(255,255,255,.1);border-left:0;border-right:0;padding:15px;background:rgba(0,0,0,.1);">
#    <li>
#        <span style="display:block;text-align:center;margin-bottom:10px;">
#        Access user manual and forum.</span>
#        <a role="button" class="btn" href="http://google.com" data-ajax="false" target="_blank"
#        style="background:#F44336 !important;box-shadow:0px 0px 2px 0px rgba(0,0,0,0.12), 0px 2px 2px 0px rgba(0,0,0,0.24);">
#            Get Help
#        </a>
#    </li>
# </ul>"""))


# sidebar-profile
register_widget('sidebar-profile', order=100, widget=Widget(id='profile-photo', text="""
<li style="text-align:center;">
    {% if user.picture %}
    <div style="width:50px;height:50px;border-radius:100%;overflow:hidden;display:inline-block;margin:5px;">
        <img src="{{ user.picture }}" style="width:100%;height:100%;margin:0;object-fit:contain;" />
    </div>
    {% else %}
    <i class="fa fa-user-circle fa-3x" style="color:#337ab7;margin:5px;"></i>
    {% endif %}
    <span style="color:black;display:block;">{{ user.email or '' }}</span>
</li>"""))

register_widget('sidebar-profile', order=101, widget=Widget(id='profile-photo-devider', text="""
<li class="divider"></li>"""))

register_widget('sidebar-profile', order=102, widget=Widget(id='profile-view', text="""
                <li>
                    <a href="{{url_for('SecurityView:profile',back_url=url_for(".HomeView:dashboard"))}}" data-ajax="false">
                        <i class="fa fa-user-o"></i>
                        {{ _("My Profile") }}
                    </a>
                </li>"""))



# register_widget('sidebar-profile', order=103, widget=Widget(id='profile-change-password', text="""
#                 <li>
#                     <a href="{{url_for('SecurityView:change_password')}}">
#                         <i class="fa fa-key"></i>
#                         {{ _('Change Password') }}
#                     </a>
#                 </li>"""))
register_widget('sidebar-profile', order=103, widget=Widget(id='company', text="""
                <li>
                    <a href="{{url_for('.CompanyView:index')}}" data-ajax="false">
                        <i class="fa fa-info"></i>
                        {{ _('Company Info') }}
                    </a>
                </li>"""))

register_widget('sidebar-profile', order=104, widget=Widget(id='profile-change-password-devider', text="""
                <li class="divider"></li>"""))

register_widget('sidebar-profile', order=104, widget=Widget(id='profile-logout', text="""
                <li>
                    <a href="{{url_for('SecurityView:logout')}}" data-ajax="false">
                        <i class="fa fa-sign-out"></i>
                        {{ _("Logout") }}
                    </a>
                </li>"""))

# ------- treeview ---------- #
register_widget_text('style', id='style-tree-view', text=
"""
<!--TreeView-->
<link rel="stylesheet" type="text/css" href="{{ url_for('share.static', filename='content/jquery.bonsai.css') }}"/>
""")
register_widget_text('script', id='script-tree-view', text=
    """
    <!-- tree view -->
    <script type="text/javascript" src="{{ url_for('share.static', filename='scripts/jquery.bonsai.js') }}"></script>
    <script type="text/javascript" src="{{ url_for('share.static', filename='scripts/jquery.qubit.js') }}"></script>
""")

# ------- line-sdk ---------- #
register_widget_text('script', id="script-line-sdk", text="""
<!-- line -->
<script src="//scdn.line-apps.com/n/line_it/thirdparty/loader.min.js" async="async" defer="defer"></script>
""")

# ------- facebook-sdk ---------- #
register_widget_text('script', id="script-facebook-sdk", text=
"""
<!-- facebook sdk -->
<script type="text/javascript">
    window.fbAsyncInit = function () {
        FB.init({
            appId: "1766125303427418",
            xfbml: true,
            cookie: true,
            version: 'v2.10'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, "script", "facebook-jssdk"));
</script>
<!-- end facebook sdk -->
""")


# ------- dashboard-widget ------------- #
register_widget_text('dashboard', id='system_security-widget-test', text=
"""
<!-- payment histories -->
<div class="col-xs-12 col-sm-12 col-md-6">
    <div class="panel">
        <div class="panel-heading clearfix">
            <strong class="pull-left">
                {{ _('Dashboard 1') }}
            </strong>
        </div>
        <div class="panel-body" style="height:332px;overflow:auto;">
        </div>
    </div>
</div>  
<div class="col-xs-12 col-sm-12 col-md-6">
    <div class="panel">
        <div class="panel-heading">
            <strong class="pull-left">
                {{ _('Dashboard 2') }}
            </strong>
        </div>
        <div class="panel-body" style="height:332px;overflow:auto;">
        </div>
    </div>
</div> 
""")

register_widget_text('dashboard', id='system_security-widget-test', text=
"""
<!-- payment histories -->
<div class="col-xs-12 col-sm-12 col-md-6">
    <div class="panel">
        <div class="panel-heading clearfix">
            <strong class="pull-left">
                {{ _('Setting') }}
            </strong>
        </div>
        <div class="panel-body" style="height:332px;overflow:auto;">
        </div>
    </div>
</div>   
""")

