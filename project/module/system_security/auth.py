from functools import wraps
from flask import current_app as app, abort, make_response, jsonify, request, redirect, url_for, Response, session
from .model import *
from .logic import permission_logic
from .logic import user_logic



def check_auth(email, password):
    user = user_logic.default.authenticate(email, password)  # type: User
    return user


def redirect_login():
    return redirect(url_for('.SecurityView:login'))


def login(user, response=None):
    session['user_id'] = user.id
    session['user_name'] = user.name
    if response:
        expire_date = datetime.now() + timedelta(days=7300)
        response.set_cookie('user_id', str(user.id), expires=expire_date)
    return True


def logout(response=None):
    session.clear()
    if response:
        response.set_cookie('user_id', '', expires=0)
    return True


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        # logged in session_id
        if session.get('user_id'):
            return f(*args, **kwargs)

        # remembered in cookied
        if request.cookies.get('user_id'):
            user = user_logic.default.find(request.cookies.get('user_id'))
            if user:
                login(user)
                return f(*args, **kwargs)

        auth = request.authorization
        if not auth or not check_auth(auth.email, auth.password):
            if request.args.get('X-Requested-With') == 'XMLHttpRequest':
                return authenticate()
            else:
                return redirect_login()

    return decorated


# endpoint-to-permission map
all_permissions = dict()
@app.before_request
def check_authorized_request():
    global all_permissions
    if not all_permissions:
        all_permissions = {p.endpoint:p.id for p in permission_logic.default.actives()}

    if not request.endpoint:
        return None
    perm_id = all_permissions.get(request.endpoint)
    # permission need
    if not perm_id:
        return None

    #user_id = current.user_id()
    user = user_logic.default.find(current.user_id())
    if not user:
       logout()
       return redirect(url_for('.SecurityView:login'))
    else:
        user_id = current.user_id()
        if user_id:
            if not session.get('user_permissions'):
                session['user_permissions'] = permission_logic.default.user_permissions(user_id=user_id)

            if perm_id in session.get('user_permissions'):
                return None

            if request.is_xhr:
                abort(make_response(jsonify(message="Permission need", code=perm_id, endpoint=request.endpoint), 401))
            # from edf_admin.auth import logout
                logout()
                return redirect(url_for('.SecurityView:login'))
        else:
            # from edf_admin.auth import logout
            # logout()
            return redirect(url_for('.SecurityView:login'))
