from edf.base.model import current
from flask import current_app, Flask, session, request
from .model import *
from .logic import user_logic


@current.promote('user_id')
def current_user_id():
    return session.get('user_id') or None


@current.promote('user')
def current_user():
    uid = current.user_id()
    if uid:
        user = user_logic.default.find(id=uid)
        return user
    return None


@current.promote('user_name')
def current_user_name():
    user = current.user()
    if user:
        if isinstance(user, dict):
            return user.get('name') or user.get('email')
        else:
            return getattr(user, 'name')
    return '-'


@current.promote('scope')
def current_scope():
    # default scope is blank
    return None
