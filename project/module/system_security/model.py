from edf.base.model import *
from edf.helper.sequence import randomstring

current._gid = lambda: randomstring()
current._key = lambda: String(50)


class User(Base, TrackMixin, ExtMixin):
    __tablename__ = 'sys_user'
    id = Column(Key(), primary_key=True, default=gid)
    name = Column(String(50))
    full_name = Column(String(250))
    password = Column(String(250))
    email = Column(String(250))
    phone = Column(String(250))
    picture = Column(String(250))
    facebook_id = Column(String(250))

    links = relationship('UserLink',
                         primaryjoin='and_(foreign(User.id) == remote(UserLink.user_id),UserLink.is_active == True)',
                         uselist=True)


class UserLink(Base, TrackMixin, ExtMixin):
    __tablename__ = 'sys_user_link'
    id = Column(Key(), primary_key=True, default=gid)
    user_id = Column(Key(), index=True)
    link_type = Column(Key(), index=True)
    link_id = Column(Key(), index=True)
    note = Column(String(MAX))


class Component(Base, TrackMixin, ExtMixin):
    __tablename__ = 'sys_component'
    id = Column(Key(), primary_key=True, default=gid)
    name = Column(String(50))
    title = Column(String(100))
    description = Column(String(MAX))
    meta_data = Column(Text())
    position = Column(Integer())
    parent_id = Column(Key())
    component_type = Column(Integer())
    app_target = Column(Integer())
    scope = Column(String(50))
    is_system = Column(Boolean)

    children = relationship('Component', primaryjoin='foreign(Component.id) == remote(Component.parent_id)',
                            uselist=True, viewonly=True)

    @property
    def component(self):
        if not hasattr(self, '_component') and self.meta_data:
            exec ('component = None\n' + self.meta_data, globals())
            self._component = component
        return self._component

    @property
    def widget(self):
        return self.component.get_widget()


class Token(Base, TrackMixin, ExtMixin):
    __tablename__ = 'sys_token'
    id = Column(Key(), primary_key=True, default=gid)
    auth_type = Column(Integer, index=True)
    auth_id = Column(Key(), index=True)  # user id, store id , api id
    access_key = Column(String(500))
    secret_key = Column(String(500))
    issue_date = Column(DateTime)
    timeout = Column(Integer)  # timeout in second
    expire_date = Column(DateTime)
    locked = Column(Boolean)


class Role(Base, TrackMixin, ExtMixin):
    __tablename__ = 'sys_role'
    id = Column(Key(), primary_key=True, default=gid)
    name = Column(String(50))
    description = Column(String(500))
    permissions = Column(Text())  # permission_id with comma separator
    scope = Column(String(50))


class Permission(Base, TrackMixin, ExtMixin):
    __tablename__ = 'sys_permission'
    id = Column(Key(), primary_key=True, default=gid)
    name = Column(String(100))
    endpoint = Column(String(500))
    description = Column(String(500))
    parent_id = Column(Key())

    children = relationship('Permission',
                            primaryjoin='foreign(Permission.id) == remote(Permission.parent_id)',
                            uselist=True)