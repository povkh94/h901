from ..auth import login, logout
from ...share.view.base_view import *
from flask import make_response
from flask_babel import lazy_gettext as _

from ..logic import user_logic
from ..model import User


def set_login_session(user):
    session['user_id'] = user.id
    session['user_name'] = user.name
    session['user_fullname'] = user.full_name
    session['current_company_id'] = user.ext.get('current_company_id', 0)
    return True


class LoginForm(Form):
    email = TextField(
        u'Email',
        render_kw={
            'placeholder': _(u'Email'),
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    password = PasswordField(
        u'Password',
        render_kw={
            'placeholder': _(u'Password'),
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    remember_me = CheckBoxField(
        '',
        checkbox_label=_(u'Remember me'),
        render_kw={}
    )


class SignupForm(Form):
    name = TextField(
        u'Name',
        render_kw={
            'data-val': 'true',
            'required': 'required',
            'data-val-required': _(u'Input Required'),
            'placeholder': _('Email address')
        }
    )
    full_name = TextField(
        u'Full Name',
        render_kw={
            'required': 'required',
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    email = TextField(
        _('Email'),
        render_kw={
            'required': 'required',
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'data-val-remote': _(u'Email already exist'),
            'data-val-remote-additionalfields': 'email',
            'data-val-remote-url': 'verify_email'
        },
        validators=[Email()]
    )
    phone = TextField(
        u'Phone',
        render_kw={
            'required': 'required',
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    password = PasswordField(
        u'Password',
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    confirm_password = PasswordField(
        u'Confirm Password',
        render_kw={
            'required': 'required',
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'data-val-remote': _(u'Password Incorrect'),
            'data-val-remote-additionalfields': 'password,confirm_password',
            'data-val-remote-url': 'confirm_password'
        }
    )


class ChangePasswordForm(FlaskForm):
    id = HiddenField()
    old_password = PasswordField(
        _(u'Old Password'),
        render_kw = {
            'data-val':'true',
            'data-val-required':_(u'Input Required')
        }
    )
    new_password = PasswordField(
        _(u'New Password'),
        render_kw = {
            'data-val':'true',
            'data-val-required':_(u'Input Required')
        }
    )

class SecurityView(AdminView):
    route_base = '/security'

    @route('/login', methods=['GET', 'POST'])
    def login(self):
        form = LoginForm()
        error = None

        # if form.validate_on_submit():
        #     user = user_logic.default.authenticate(form.email.data,  form.password.data)
        #     if user aauser_logic.verify_password(user.password,form.password.data):
        #         response = make_response(redirect(url_for('.HomeView:home')))
        #         # process login and save cookie
        #         login(user, response)
        #         return response
        #     else:
        #         error = _('The username or password you entered is incorrect.')

        if form.validate_on_submit():
            user = user_logic.default.authenticate(form.email.data, form.password.data)
            if user:
                response = make_response(redirect(url_for('.HomeView:home')))
                # process login and save cookie
                login(user, response)
                return response
            else:
                error = _('The username or password you entered is incorrect.')

        return render_template(
            'security/login.html',
            title=u'Login',
            form=form,
            error=error
        )

    @route('/logout', methods=['GET'])
    def logout(self):
        response = make_response(redirect(url_for('.SecurityView:login')))
        logout(response)
        return response

    @route('/signup', methods=['GET', 'POST'])
    def signup(self):
        form = SignupForm()

        if form.validate_on_submit():
            user = from_dict(form.data,to_cls=User)
            user.full_name = form.full_name.data
            user.email = form.email.data or form.name.data
            user.name = form.name.data or form.email.data
            user.phone = form.phone.data or ''
            user.password = form.password.data
            # use signup instead of add because signup will add other related data such as roles, setting...etc.
            user_logic.default.signup(user)

            resp = make_response(redirect(url_for('.SecurityView:signup_success')))
            login(user, resp)
            return resp

        return render_template(
            'security/signup.html',
            title=u'Signup',
            form=form
        )

    @route('/signup_success', methods=['GET', 'POST'])
    def signup_success(self):
        return render_template(
            'security/signup_success.html',
            title=u'Signup Success'
        )

    @app.route('/profile')
    def profile(self):
        back_url = request.args.get('back_url','')
        if back_url:
            return redirect(url_for('.UserView:detail', id=current.user_id(),back_url=back_url ))
        return redirect(url_for('.UserView:detail', id=current.user_id()))

    @route('/change_password', methods=['GET', 'POST'])
    def change_password(self):
        form = ChangePasswordForm()
        form.id.data = current.user_id()
        error = None

        if form.validate_on_submit():
            user = user_logic.default.authenticate(current.user_name(), form.old_password.data)  #type:User
            if user:
                user.password = form.new_password.data
                user_logic.default.change_password(user)
                return redirect(url_for('.HomeView:dashboard'))
            else:
                error = _('Invalid old password.')

        return render_template(
            'security/change_password.html',
            title=_(u'Change Password'),
            form=form,
            error=error
        )


    # Remote
    # Check if password is match
    @route('/confirm_password', methods=['GET', 'POST'])
    def confirm_password(self):
        password = request.args.get('password') or request.args.get('new_password')
        confirm_password = request.args.get('confirm_password')

        if password == confirm_password:
            return json.dumps(True)
        else:
            return json.dumps(False)

    # Remote
    # Check if the email is already exist
    @route('/verify_email', methods=['GET', 'POST'])
    def verify_email(self):
        email = request.args.get('email')
        user = user_logic.default.find(email=email)
        if user:
            return json.dumps(False)
        else:
            return json.dumps(True)

    @route('/clear_session', methods=['GET'])
    def clear_session(self):
        session.clear()
        return 'clear'


SecurityView.register(app)
