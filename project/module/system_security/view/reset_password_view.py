from ...share.view.base_view import *
from flask import make_response, url_for, current_app, Flask
from flask_babel import lazy_gettext as _

from ..logic import token_logic
from ..logic import user_logic
from ..model import *
from ...share.lib.email import send_email
from ...share.view.base_view import *

app = current_app  # type: Flask


class VerifyUserForm(Form):
    email = TextField(
        _(u'Email'),
        render_kw={
            'placeholder': _(u'Email'),
            'data-val': 'true',
            'data-val-required': u'Input Required',
            'data-val-email': _(u'Please input a valid email address')
        }
    )


class VerifyResetCodeForm(Form):
    reset_code = PasswordField(
        _(u'Reset Code'),
        render_kw={
            'placeholder': _(u'Reset Code'),
            'data-val': 'true',
            'data-val-required': u'Input Required'
        }
    )


class ResetPasswordForm(Form):
    reset_code = HiddenField()
    new_password = PasswordField(
        _(u'New Password'),
        render_kw={
            'placeholder': _(u'New Password'),
            'data-val': 'true',
            'data-val-required': u'Input Required'
        }
    )


def verify_user(email):
    reset_password_url = url_for('.ResetPasswordView:reset_password',reset_code='',_external=True)+'{reset_code}'
    app.config.setdefault('RESET_PASSWORD_URL', reset_password_url)

    user = user_logic.default.find(email=email)
    if user:
        token = token_logic.default.generate_token(
            auth_type=100,
            auth_id=user.id,
            timeout=7200,
        )
        # email info
        receivers = [email]
        subject = 'Reset Password'
        url = app.config.get('RESET_PASSWORD_URL').format(reset_code=token.access_key)
        message_html = "<p style='font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;font-size:16px;'>Dear " + user.full_name + ",</p> \
                        <p style='font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;font-size:16px;'>Somebody recently asked to reset your B24 App password. If you did not request this, please ignore this email. It will expire in 2 hours.</p> \
                        <p style='font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;font-size:16px;'>To reset your password, please click the button below.</p> \
                        <a href='" + url + "' style='font-family:LucidaGrande,tahoma,verdana,arial,sans-serif;font-size:16px;padding:10px;background-color:#337ab7;border:1px solid #2e6da4;border-radius:4px;text-decoration:none;font-weight:bold;color:#fff;display:inline-block;'>Reset Password</a>"
        send_email(receivers=receivers, subject=subject, message_html=message_html)
    return user


def verify_reset_code(reset_code):
    """
    :param reset_code:
    :rtype : User
    """
    token = token_logic.default.verify_token(access_key=reset_code, secret_key='')  # type:Token
    user = user_logic.default.find(id=token.auth_id)  # type: User
    return user


class ResetPasswordView(AdminView):
    route_base = '/reset_password'

    @route('/verify', methods=['GET', 'POST'])
    def verify(self):
        form = VerifyUserForm()
        error = None
        if form.validate_on_submit():
            user = verify_user(email=form.email.data)
            if user:
                return redirect(url_for('.ResetPasswordView:verify_success'))
            else:
                error = _('User not found in system.')
        return render_template(
            'reset_password/verify.html',
            title=_('Verify User'),
            form=form,
            error=error
        )

    @route('/verify/success', methods=['GET'])
    def verify_success(self):
        return render_template(
            'reset_password/verify_success.html',
            title=_(u'Verify Success')
        )

    @route('/verify_reset_code', methods=['GET', 'POST'])
    def verify_reset_code(self):
        form = VerifyResetCodeForm()
        error = None

        if form.validate_on_submit():
            user = verify_reset_code(reset_code=form.reset_code.data)
            if user:
                return redirect(url_for('.ResetPasswordView:reset_password', reset_code=form.reset_code.data))
            else:
                error = _('Reset code is invalid.')

        return render_template(
            'reset_password/verify_reset_code.html',
            title=_(u'Verify Reset Code'),
            form=form,
            error=error
        )

    @route('/<reset_code>', methods=['GET', 'POST'])
    def reset_password(self, reset_code):
        form = ResetPasswordForm()
        form.reset_code.data = reset_code
        error = None

        user = verify_reset_code(reset_code=form.reset_code.data)  # type: User
        if not user:
            return redirect(url_for('.ResetPasswordView:verify'))

        if form.validate_on_submit():
            user.password = form.new_password.data
            user_logic.default.change_password(user)
            resp = make_response(redirect(url_for('.HomeView:home')))
            login(user, resp)
            return resp

        return render_template(
            'reset_password/reset_password.html',
            title=_(u'Reset Password'),
            form=form,
            error=error
        )


ResetPasswordView.register(app)
