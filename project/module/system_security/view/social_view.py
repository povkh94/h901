﻿from ..auth import login
from flask import make_response
from flask_oauthlib.client import OAuth, OAuthException

from project.admin import config as conf
from ...share.view.base_view import *
from ..model import *
from ..logic import user_logic

oauth = OAuth()

facebook = oauth.remote_app('facebook',
                            base_url='https://graph.facebook.com/',
                            request_token_url=None,
                            access_token_url='/oauth/access_token',
                            access_token_method='GET',
                            authorize_url='https://www.facebook.com/dialog/oauth',
                            consumer_key=conf.APP_ID,
                            consumer_secret=conf.APP_SECRET,
                            request_token_params=conf.SCOPE
                            )


@facebook.tokengetter
def get_facebook_oauth_token():
    return session.get('oauth_token')


class SocialView(FlaskView):
    route_base = '/socials/'

    @route('/facebook_authorize')
    def facebook_authorize(self):
        callback = url_for(
            '.SocialView:facebook_authorized',
            next=request.args.get('next') or request.referrer or None,
            _external=True
        )
        return facebook.authorize(callback=callback)

    @route('/facebook_authorized')
    def facebook_authorized(self):
        resp = facebook.authorized_response()
        next = request.args.get('next')

        if resp is None:
            return 'Access denied: reason=%s error=%s' % (
                request.args['error_reason'],
                request.args['error_description']
            )

        if isinstance(resp, OAuthException):
            return 'Access denied: %s' % resp.message

        session['oauth_token'] = (resp['access_token'], '')
        return redirect(next)

    @route('/connect_facebook', methods=['GET', 'POST'])
    def connect_facebook(self):
        return redirect(url_for('.SocialView:facebook_authorize',
                                next=url_for('.SocialView:connect_facebook_callback')))

    @route("/connect_facebook_callback")
    def connect_facebook_callback(self):
        me = facebook.get('/me?fields=id,name,picture,email')
        facebook_id = me.data['id']
        user = user_logic.default.find(facebook_id=facebook_id)
        if not user:
            user = User()
            user.name = me.data.get('email', me.data['name'])
            user.full_name = me.data['name']
            user.email = me.data.get('email', u'')
            user.facebook_id = facebook_id
            user.picture = me.data['picture']['data']['url'] or u''
            user.password = u'LogINBYFACEb00k'
            # not use default logic because need 
            # to to have transaction during linked account
            logic = user_logic.H701UserLogic()
            logic.is_commit = False
            logic.add(user)
            logic.link(user, link_type='user-facebook', link_id=user.facebook_id, note=u"Facebook Login")
            logic.is_commit = True
            logic.db_commit()

            # signup success 
            response = make_response(redirect(url_for('.SecurityView:signup_success')))
            login(user, response)
            return response

        # login success
        response = make_response(redirect(url_for('.HomeView:home')))
        login(user, response)
        return response

        # @route('/disconnect_facebook', methods=['GET', 'POST'])
        # def disconnect_facebook(self):
        #    return redirect(url_for('admin.SocialView:facebook_authorize',
        #                            next=url_for('admin.SocialView:disconnect_facebook_callback')))

        # @route("/disconnect_facebook_callback")
        # def disconnect_facebook_callback(self):
        #    facebook.delete('/me/permissions')
        #    user_id = get_session('user_id', 0)
        #    user = users.find(user_id)
        #    if user:
        #        user.facebook_id = None
        #        if user.setting_data:
        #            setting = json.loads(user.setting_data)
        #            setting['facebook_id'] = ''
        #            setting['facebook_name'] = ''
        #            user.setting_data = setting
        #        users.update(user)
        #    del_session('oauth_token')
        #    return redirect(url_for('admin.HomeView:home') + '#/settings/account')


SocialView.register(app)
