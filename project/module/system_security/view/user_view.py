from ...agri_bill.logic import block_logic
from ...share.view.base_view import *
from flask_babel import lazy_gettext as _

from ..logic import role_logic
from ..logic import user_logic
from ..model import *


# class ChangePasswordForm(FlaskForm):
#     id = HiddenField()
#     old_password = PasswordField(
#         _(u'Old Password'),
#         render_kw = {
#             'data-val':'true',
#             'data-val-required':_(u'Input Required')
#         }
#     )
#     password = PasswordField(
#         _(u'Password'),
#         render_kw = {
#             'data-val':'true',
#             'data-val-required':_(u'Input Required')
#         }
#     )


class LoginForm(Form):
    email = TextField(
        u'Email',
        render_kw={
            'placeholder': _(u'Email'),
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    password = PasswordField(
        u'Password',
        render_kw={
            'placeholder': _(u'Password'),
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    remember_me = CheckBoxField(
        '',
        checkbox_label=_(u'Remember me'),
        render_kw={}
    )


class UserForm(FlaskForm):
    id = HiddenField()
    name = TextField(
        _(u'Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            # 'readonly': 'readonly'
        }
    )
    full_name = TextField(
        _(u'Full Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),

        }
    )
    email = TextField(
        _(u'Email'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'data-val-remote': _(u'Email already exist'),
            'data-val-remote-additionalfields': 'email',
            'data-val-remote-url': '/security/verify_email'
        }
    )
    phone = TextField(
        _(u'Phone'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )

    password = PasswordField(
        _(u'Password'),
        render_kw={
            'placeholder': _(u'Password'),
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )

    confirm_password = PasswordField(
    _(u'Confirm Password'),
        render_kw={
            'required': 'required',
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'data-val-remote': _(u'Password Incorrect'),
            'data-val-remote-additionalfields': 'password,confirm_password',
            'data-val-remote-url': '/security/confirm_password'
        }
    )
    # ext__roles  = HiddenField()
    ext__roles = TreeViewField(_('Roles'),
                               query=role_logic.default.search,
                               all_visible_text=_('ALL')
                               )

    # ext__block_ids = Select2Field(
    #     label=_(u'Block Manager'),
    #     remote_url='/block/suggestion',
    #     allow_blank=True,
    #     blank_text=_(u'Block Manager'),
    #     min_input_length=0,
    #     max_selection_length=20,
    #     text_factory=lambda x: block_logic.default.find(x),
    #     multiple=True
    # )
    # ext data
    ext_data = HiddenField()
    picture = UploadImageField(_('Picture'), multiple=False)

# class UserForm(FlaskForm):
#     id = HiddenField()
#     name = TextField(
#         _(u'Name'),
#         render_kw={
#             'data-val': 'true',
#             'data-val-required': _(u'Input Required'),
#             # 'readonly': 'readonly'
#         }
#     )
#     full_name = TextField(
#         _(u'Full Name'),
#         render_kw={
#             'data-val': 'true',
#             'data-val-required': _(u'Input Required'),
#
#         }
#     )
#     email = TextField(
#         _(u'Email'),
#         render_kw={
#             'data-val': 'true',
#             'data-val-required': _(u'Input Required'),
#             'data-val-remote': _(u'Email already exist'),
#             'data-val-remote-additionalfields': 'email',
#             'data-val-remote-url': '/security/verify_email'
#         }
#     )
#     phone = TextField(
#         _(u'Phone'),
#         render_kw={
#             'data-val': 'true',
#             'data-val-required': _(u'Input Required')
#         }
#     )
#
#     password = PasswordField(
#         _(u'Password'),
#         render_kw={
#             'placeholder': _(u'Password'),
#             'data-val': 'true',
#             'data-val-required': _(u'Input Required')
#         }
#     )
#
#     confirm_password = PasswordField(
#     _(u'Confirm Password'),
#         render_kw={
#             'required': 'required',
#             'data-val': 'true',
#             'data-val-required': _(u'Input Required'),
#             'data-val-remote': _(u'Password Incorrect'),
#             'data-val-remote-additionalfields': 'password,confirm_password',
#             'data-val-remote-url': '/security/confirm_password'
#         }
#     )
#     # ext__roles  = HiddenField()
#     ext__roles = TreeViewField(_('Roles'),
#                                query=role_logic.default.search,
#                                all_visible_text=_('ALL')
#                                )
#
#     # ext__block_ids = Select2Field(
#     #     label=_(u'Block Manager'),
#     #     remote_url='/block/suggestion',
#     #     allow_blank=True,
#     #     blank_text=_(u'Block Manager'),
#     #     min_input_length=0,
#     #     max_selection_length=20,
#     #     text_factory=lambda x: block_logic.default.find(x),
#     #     multiple=True
#     # )
#     # ext data
#     ext_data = HiddenField()
#     picture = UploadImageField(_('Picture'), multiple=False)

class UserEditForm(FlaskForm):
    id = HiddenField()
    name = TextField(
        _(u'Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            # 'readonly': 'readonly'
        }
    )
    full_name = TextField(
        _(u'Full Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),

        }
    )
    email = TextField(
        _(u'Email'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'data-val-remote-additionalfields': 'email',
            'data-val-remote-url': '/security/verify_email'
        }
    )
    phone = TextField(
        _(u'Phone'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )

    password = PasswordField(
        _(u'Password'),
        render_kw={
            'placeholder': _(u'Password'),
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )

    confirm_password = PasswordField(
    _(u'Confirm Password'),
        render_kw={
            'required': 'required',
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'data-val-remote': _(u'Password Incorrect'),
            'data-val-remote-additionalfields': 'password,confirm_password',
            'data-val-remote-url': '/security/confirm_password'
        }
    )
    # ext__roles  = HiddenField()
    ext__roles = TreeViewField(_('Roles'),
                               query=role_logic.default.search,
                               all_visible_text=_('ALL')
                               )

    # ext__block_ids = Select2Field(
    #     label=_(u'Block Manager'),
    #     remote_url='/block/suggestion',
    #     allow_blank=True,
    #     blank_text=_(u'Block Manager'),
    #     min_input_length=0,
    #     max_selection_length=20,
    #     text_factory=lambda x: block_logic.default.find(x),
    #     multiple=True
    # )
    # ext data
    ext_data = HiddenField()
    picture = UploadImageField(_('Picture'), multiple=False)



class ChangePasswordForm(FlaskForm):
    id = HiddenField()
    username = TextField(
        _(u'User Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'readonly': 'readonly'
        }
    )

    current_password = PasswordField(
        _(u'Current Password'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )

    new_password = PasswordField(
        _(u'New Password'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    confirm_password = PasswordField(
        _(u'Confirm Password'),
        render_kw={
            'required': 'required',
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'data-val-remote': _(u'Password Incorrect'),
            'data-val-remote-additionalfields': 'new_password,confirm_password',
            'data-val-remote-url': '/security/confirm_password'
        }
    )


class UserTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    id = DataColumn("Id", visible=False)
    name = LinkColumn(_(u'Name'), get_url=lambda row: url_for('.UserView:detail', id=row.id))
    full_name = DataColumn(_(u'Full Name'))
    email = DataColumn(_(u'Email'))
    phone = DataColumn(_(u'Phone'))
    action = ActionColumn('', endpoint='UserView')


class UserView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = User
        self.v_logic = user_logic.default
        self.v_logic.is_commit = False
        self.v_table_index_class = UserTable
        self.v_form_add_class = UserForm
        # self.v_forms = UdatePasswordForm
        self.v_template = 'user'
        self.v_logic_order_by = ''
        self.v_breadcrum.append({'url': "/setting/index", 'title': _('Setting')})

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _(u"User")

    def _on_detail_rendering(self, *args, **kwargs):
        self.v_data['obj'].roles = role_logic.default.search().all()
        back_url = request.args.get('back_url') or ''
        if back_url:
            self.v_data['back_url'] = back_url
        else:
            self.v_data['back)url'] = url_for(".UserView:index")
        form = self.v_data['form']
        delattr(form, 'password')
        delattr(form, 'confirm_password')

    def _on_edit_rendering(self, *args, **kwargs):
        form = self.v_data['form']
        if form:
            delattr(form, 'password')
            delattr(form, 'confirm_password')

    def _on_edited(self, *args, **kwargs):
        pass
        # return redirect(url_for('.HomeView:dashboard'))
        # self.v_redirect_fn = lambda: url_for('.HomeView:dashboard')

    @route('/current', methods=['GET'])
    def current(self):
        u = current.user()
        return redirect(url_for('.UserView:detail', id=u.id))

    @route('/activate', methods=['GET'])
    def activate(self):
        u = current.user()
        u.ext__activated = 'email'
        user_logic.default.update(u)
        return redirect(url_for('.UserView:activate_success'))

    @route('/activate_success')
    def activate_success(self):
        return self.render_template(
            '/activate_success.html',
            title=_('Activation Success')
        )

    @route('/change_password/<id>', methods=['GET', 'POST'])
    def change_password(self, id):
        user = user_logic.default.find(id=id)  # type:User
        form = ChangePasswordForm()
        form.id.data = user.id
        error = None
        if form.validate_on_submit():
            user = user_logic.default.authenticate(user.email, form.current_password.data)
            if user:
                user.password = form.new_password.data
                user_logic.default.change_password(user)
                return redirect(url_for('.SecurityView:logout'))
            else:
                error = _('Invalid Current password.')
            # form.new_password.data = form.new_password.data
            # form.confirm_password.data = form.confirm_password.data
            # form.current_password.data = form.confirm_password.data
        return render_template(
            'user/change_password.html',
            title=_(u'Change Password'),
            form=form,
            error=error, view=UserView
        )
    @route('/edit/<id>',methods=['GET','POST'])
    def edit(self, id):
        user = user_logic.default.find(id=id)
        form = UserEditForm(obj=user)
        error = None
        if user:
            delattr(form, 'password')
            delattr(form, 'confirm_password')
            if form.validate_on_submit():
                user = from_dict(form.data, to_cls=User)
                user_logic.default.update(user)
                return redirect(url_for('.HomeView:dashboard'))
            # else:
            #     error = _('Somethin goes wrong')
        return render_template(
            'user/edit.html',
            title=_(u'User'),
            form=form,
            error=error, view=UserView,obj=user
        )

    @route('/remove/<id>', methods=['GET', 'POST'])
    def remove(self, id=0):
        obj = self.v_logic.find(id)
        canremove = self.v_logic.canremove(id)
        form = self.v_forms['remove'](obj=obj)
        if request.method == 'POST' and form.validate():
            try:
                check = self.v_logic.remove(obj)
                if check == False:
                    return render_template('user/remove-all.html', view={"canremove": "False"})
                else:
                    return redirect(url_for(".UserView:index"))
            except LogicError, e:
                db.rollback()
                form.id.errors.append(str(e))
        self.v_data['form'] = form

        self.v_data['return_url'] = ''

        # add breadcrumb to view object
        self.v_data['breadcrumb'] = [
            {'url': url_for(self.v_data['endpoints']['index']), 'title': self.v_data['title']},
            {'url': url_for(self.v_data['endpoints']['detail'], id=form.id.data), 'title': obj.__str__()},
            {'url': '', 'title': _('Delete')}
        ]
        return self.render_template(self.v_templates.get('remove'), canremove=canremove)


UserView.register(app)
