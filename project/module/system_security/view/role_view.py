from flask_babel import lazy_gettext as _
from ...share.view.base_view import *
from ..model import *
from ..logic import role_logic
from ..logic import permission_logic


class RoleForm(FlaskForm):
    id = HiddenField()
    scope = HiddenField()
    name = TextField(
        _(u'Name'),
        render_kw={
            'is_half_width': 'false',
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    description = TextField(
        _(u'Description'),
        render_kw={'is_half_width': 'false'}
    )
    permissions = TreeViewField(_('Permissions'),
                                query=permission_logic.default.get_parent_permission,
                                get_child='children',
                                all_visible_text=_('ALL'))


class RoleFilterForm(FlaskForm):
    search = TextField(
        _(u'Search'),
        render_kw={
            'placeholder': _(u'Search')
        }
    )

    scope = HiddenField()


class RoleTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    id = DataColumn('Id', visible=False)
    name = LinkColumn(_(u'Name'), get_url=lambda x: url_for('.RoleView:detail', id=x.id))
    description = DataColumn(_(u'Description'))
    action = ActionColumn('', endpoint='.RoleView')


class RoleView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Role
        self.v_logic = role_logic.default
        self.v_table_index_class = RoleTable
        self.v_form_add_class = RoleForm
        self.v_form_filter_class = RoleFilterForm
        self.v_template = 'role'
        self.v_breadcrum.append({'url': "/setting/index", 'title': _('Setting')})


    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _(u"Role")

    def _on_index_rendering(self, *args, **kwargs):
        self.v_data['form'].scope.data = current.scope()

    def _on_add_rendering(self, *args, **kwargs):
        self.v_data['form'].scope.data = current.scope()


RoleView.register(app)
