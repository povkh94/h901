from edf.base.logic import *
from flask import session

from ..model import *


class RoleLogic(LogicBase):
    def __init__(self):
        self.__classname__ = Role

    def update(self, obj, _commit=True):
        super(RoleLogic, self).update(obj)
        del session['user_permissions']
    def find(self, id, update=False):
        """
        Find a logic based on id

        :type update: bool
        :type id: int
        :rtype: Role
        """
        return super(RoleLogic, self).find(id)

    def find_user_link(self, user_id, role_id):
        user_link = (db.query(UserLink)
                     .filter(UserLink.user_id == user_id)
                     .filter(UserLink.link_id == role_id)
                     .filter(UserLink.link_type == 'user-role')
                     ).one_or_none()
        return user_link

    def user_has_role(self, user_id, role_id):
        user_link = self.find_user_link(user_id, role_id)
        return user_link and user_link.is_active

    def user_roles(self, user_id):
        role_id_list = [r.link_id for r in db.query(UserLink.link_id)
            .filter(UserLink.user_id == user_id)
            .filter(UserLink.link_type == 'user-role')
            .all()]
        return role_id_list

    def assign_role(self, user_id, role_id):
        user_link = self.find_user_link(user_id, role_id)
        if not user_link:
            user = UserLink()
            user.user_id = user_id
            user.link_type = 'user-role'
            user.link_id = role_id
            db.add(user)

        else:
            user_link.is_active = True
        db.flush()

    def unassign_role(self, user_id, role_id):
        user_link = self.find_user_link(user_id, role_id)
        if user_link:
            user_link.is_active = False
            db.flush()

    def search(self, **kwargs):
        search = kwargs.get('search') or ''
        company_id = kwargs.get('company_id') or current.scope() or None

        q = self.actives()
        if search:
            pass
        if company_id:
            q = q.filter(or_(self.__classname__.scope == company_id))

        return q


default = RoleLogic()
