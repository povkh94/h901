from edf.base.logic import *
from ..model import *
from . import role_logic


class PermissionLogic(LogicBase):

    def __init__(self):
        self.__classname__ = Permission 


    def user_permissions(self, user_id):
        role_ids = role_logic.RoleLogic().user_roles(user_id)
        q = (db.query(Role)
             .filter(Role.id.in_(role_ids))
             .all())
        all_permissions = ','.join([r.permissions for r in q])
        return all_permissions

    def user_has_permission(self, user_id, permission_id):
        all_permissions = self.user_permissions(user_id)
        return permission_id in all_permissions

    def permission_need(self, user_id, endpoint):
        """
        Return a permission need to process page, if user granted with permission
        method will return None

        1. check is user authorize
        need = PermissionLogic().permission_need(user_id,endpoint):
        if not need:
            # something
        2. message to display need permission
        need = PermissionLogic().permission_need(user_id,endpoint):
        if need:
            # need.id, need.note ...

        :rtype: Permission
        :type endpoint: str
        :type user_id: str
        """
        if not endpoint:
            return None
        permission_id = None
        # finding permission id of the end point
        permission = (db.query(Permission)
                      .filter(Permission.endpoint == endpoint)
                      .filter(Permission.is_active)
                      .first())
        if permission:
            permission_id = permission.id
        # if endpoint cannot find in database
        # assume it not protected in permission
        if not permission_id:
            return None

        # if permission is required and user not yet login
        if permission and not user_id:
            return permission

        user_links = (db.query(UserLink)
                      .filter(UserLink.is_active)
                      .filter(UserLink.user_id == user_id)
                      .filter(UserLink.link_type == 'user-role')
                      .all())
        roles_ids = [ul.link_id for ul in user_links]
        roles = (db.query(Role)
                 .filter(Role.id.in_(roles_ids))
                 .filter(Role.is_active)
                 .all())
        authorized_permissions_ids = ';'.join([r.permissions for r in roles])
        if permission_id not in authorized_permissions_ids:
            return permission

    def get_parent_permission(self):
        q = db.query(Permission).filter(Permission.parent_id == None, Permission.is_active)
        return q


default = PermissionLogic()
