﻿from edf.base.logic import *
from ..model import *


class UserLinkLogic(LogicBase):
    def __init__(self):
        self.__classname__ = UserLink

    def get_avaiable_link_ids(self, link_type=None):
        user_id = current.user_id()
        link_ids = []
        if link_type:
            r = db.query(UserLink).filter(UserLink.is_active, UserLink.user_id == user_id,
                                          UserLink.link_type == link_type).all()
            if r:
                link_ids = [i.link_id for i in r]

        return link_ids


default = UserLinkLogic()
