from edf.base.logic import *
from ..model import *

import uuid
import random


# function returns tuple of (access_key, token_key)
def _default_generator(auth_type, auth_id):
    access_key = str(uuid.uuid4())
    secret_key = ''
    return access_key, secret_key


def _api_generator(auth_type, auth_id):
    access_key = str(uuid.uuid4()).replace('-', '')[:16]
    secret_key = str(uuid.uuid4())
    return access_key, secret_key


def _opt4_generator(auth_type, auth_id):
    access_key = str(uuid.uuid4())
    secret_key = str(random.randint(1000, 9999))
    return access_key, secret_key


def _opt6_generator(auth_type, auth_id):
    access_key = str(uuid.uuid4())
    secret_key = str(random.randint(100000, 999999))
    return access_key, secret_key


generators = dict()
generators['default'] = _default_generator
generators['api'] = _api_generator
generators['otp6'] = _opt6_generator
generators['otp4'] = _opt4_generator


def generate_secret_key(auth_type, auth_id, access_key):
    return


class TokenError(Exception):
    pass


class TokenNotFoundError(TokenError):
    def __init__(self, message='Token not found!'):
        super(TokenNotFoundError, self).__init__(message)


class TokenLockedError(TokenError):
    def __init__(self, message='Token is temporary locked!'):
        super(TokenLockedError, self).__init__(message)


class TokenExpireError(TokenError):
    def __init__(self, message='Token is expired!'):
        super(TokenExpireError, self).__init__(message)


class TokenLogic(LogicBase):
    def __init__(self):
        self.__classname__ = Token

    def generate_token(self, auth_type, auth_id, timeout=0, generator='default'):
        token = Token()
        token.auth_type = auth_type
        token.auth_id = auth_id
        gen_fn = generators.get(generator, generators.get('default'))
        token.access_key, token.secret_key = gen_fn(auth_type, auth_id)
        token.issue_date = datetime.now()
        token.timeout = timeout
        token.expire_date = token.issue_date + timedelta(seconds=timeout)
        self.add(token)
        return token

    def verify_token(self, access_key, secret_key):
        token = (db.query(Token)
                 .filter(Token.access_key == access_key)
                 .filter(Token.secret_key == secret_key)
                 .first())
        if not token:
            raise TokenNotFoundError()
        if token.locked:
            raise TokenLockedError()
        if token.timeout != 0 and token.expire_date < datetime.now():
            raise TokenExpireError()
        return token


default = TokenLogic()
