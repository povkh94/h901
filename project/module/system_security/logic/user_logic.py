﻿from ...share.logic.base_logic import *
from . import role_logic
from ..model import *
from . import user_link_logic
from werkzeug.security import  generate_password_hash, check_password_hash
from ...share.view.base_view import *
from ...agri_bill.model import *



def hash_password(password):
    return generate_password_hash(password) # pwd_context.encrypt(password)


def verify_password(password_hash, password):
    return check_password_hash(password_hash, password) # pwd_context.verify(password1, password2)

class UserLogic(LogicBase):

    def __init__(self):
        self.__classname__ = User

    def search(self, **kwargs):
        search = kwargs.get('search') or ''
        company_id = kwargs.get('company_id') #or current.scope() or None

        q = self.actives()
        if search:
            q = q.filter(or_
                         (func.lower(self.__classname__.name).like('%' + search.lower() + '%')))
        if company_id:
            q = q.filter(UserLink.user_id == User.id, UserLink.link_id == company_id,
                         UserLink.link_type == str('user-scope'))

        return q

    @property
    def default_ext(self):
        ext = {
            'current_scope': current.scope(),
            'facebook_id': '',
            'facebook_name': '',
            'language': 'en-US'
        }
        return ext

    def find(self, id=0, name='', email=''):
        if id:
            return db.query(User).filter(User.id == id).first()
        if name:
            return db.query(User).filter(User.is_active, User.name == name.lower()).first()
        if email:
            return db.query(User).filter(User.is_active, User.email == email.lower()).first()

    def add(self, obj):
        # add user
        obj.email = obj.email.lower()
        obj.password = hash_password(obj.password)
        obj.ext__current_scope = obj.ext__current_scope or current.scope()
        obj.is_commit = False
        if not obj.ext:
            obj.ext = self.default_user_setting()

        super(UserLogic, self).add(obj)


        # assign user link with role
        if obj.ext__roles:
            roles = obj.ext__roles.split(',')
            for role in roles:
                if role and role != '0':
                    role_logic.default.assign_role(obj.id, role)

        db.commit()

    def update(self, obj):
        # update user
        old = self.find(obj.id)
        obj.email = old.email
        obj.password = old.password
        obj.is_commit = False
        copyupdate(obj, old)

        super(UserLogic, self).update(old,False)

        # unassigned user link with role
        role_id_list = role_logic.default.user_roles(old.id)
        if role_id_list:
            for role_id in role_id_list:
                role_logic.default.unassign_role(old.id, role_id)

        # assign user link with role
        if obj.ext__roles:
            roles = obj.ext__roles.split(",")
            for role in roles:
                if role and role != '0':
                    role_logic.default.assign_role(obj.id, role)
        db.commit()

    def signup(self, user):
        user.ext__roles = "1"
        self.add(user)
        self.db_commit()
    def add_with_link(self, user, company):
        from project.module.company.logic.company_logic import CompanyLogic

        # add company
        company_lg = CompanyLogic()
        company_lg.is_commit = False
        company_lg.add(company)

        default_admin_role = role_logic.default.find('1')
        admin = Role()
        copyupdate(default_admin_role, admin)
        admin.scope = company.id


        db.add(admin)
        db.flush()

        # add user
        user.ext__current_company_id = company.id
        user.ext__roles = admin.id
        self.add(user)


        return user

    def authenticate(self, email, password):
        user = self.find(email=email)
        if user:
            verify = verify_password(user.password,password)
            if verify:
                return user
            else:
                return None
            return user
        else:
            return None

    def change_password(self, obj):
        if obj.password != "HACKER":
            old = self.find(obj.id)
            obj.password = hash_password(obj.password)
            copyupdate(obj, old)
            db.commit()

    def update_pwd_to_hash(self,user_id):
        user = self.find(id=user_id)
        user.password = 'tahel123'
        self.change_password(user)

    def remove(self, obj, _commit=True):
        if obj == current.user():
            return False
        elif obj.is_active == True:
            obj.is_active = False
        else:
            return False
        db.flush()
        self.trigger(self.EVENT_REMOVED, obj=obj)
        self._commit(_commit=_commit)
        return True


default = UserLogic()
