import os
import jinja2
import pkgutil
from flask import current_app, Flask, send_from_directory, render_template_string
from wtforms.widgets import HTMLString, html_params


def activate_module(m, app=None):
    """
    :param m: module object from import statement
    :param app: flask app
    :type app: flask.Flask
    :return: None
    """
    if not app:
        app = current_app  # type: Flask

    # correct module must be contains info property
    if hasattr(m, '__info__'):
        # mark it as current activated module.
        app.config['LAST_MODULE'] = m.__info__.get('name') or m.__name__
        # check if module contains /template folder
        # register jinja loader to able to load module template first
        template_path = os.path.join(m.__path__[0], 'template')
        if os.path.isdir(template_path):
            new_loader = jinja2.ChoiceLoader([
                jinja2.FileSystemLoader([template_path]),
                app.jinja_loader,  # last take precedent
            ])
            app.jinja_loader = new_loader

        # check if module contains /static folder
        # method will automatically enable url_for('<module_name>.static','some-file.js')
        static_path = os.path.join(m.__path__[0], 'static')
        if static_path.startswith('.'):
            static_path = static_path.replace('\\','/').replace('./', os.getcwd()+'/')
        if os.path.isdir(static_path):
            static_url_rule = '/static/{mn}/<path:filename>'.format(mn=m.__info__.get('name', m.__name__))
            static_endpoint = '{mn}.static'.format(mn=m.__info__.get('name', m.__name__))

            @app.route(static_url_rule, endpoint=static_endpoint)
            def module_static(filename):
                # return static_path+'/'+filename
                return send_from_directory(static_path, filename)  # '?v='+m.__info__.get('version','0.0')

        translate_path = os.path.join(m.__path__[0], 'translate')
        if translate_path.startswith('.'):
            translate_path = translate_path.replace('\\','/').replace('./', os.getcwd()+'/')

        if os.path.isdir(translate_path):
            app.config['BABEL_TRANSLATION_DIRECTORIES'] = app.config.get('BABEL_TRANSLATION_DIRECTORIES','')+';'+translate_path+'/'

        if hasattr(m, 'activate'):
            m.activate(app=app)

