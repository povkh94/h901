from edf_admin.form.dynamicselectfield import DynamicSelectField
from edf_admin.form.select2field import Select2Field
from flask_wtf import FlaskForm
from wtforms import HiddenField, TextField, TextAreaField, DateField
from flask_babel import lazy_gettext as _

from project.module.agri_bill import enum
from project.module.agri_bill.enum import payment_method
from project.module.agri_bill.export import export_pdf
from project.module.agri_bill.logic import billing_logic
from project.module.agri_bill.table import AmountColumn
from project.module.currency.logic import currency_logic
from project.module.payment import enums
from project.module.payment.logic import payment_logic
from project.module.payment.model import Payment
from project.module.system.logic import lookup_logic
from ...share.view.base_view import *


class PaymentForm(FlaskForm):
    id = HiddenField()
    ext_data = HiddenField()
    invoice_id = HiddenField()
    company_id = HiddenField()
    code = TextField(
        _("Invoice Code"),
        render_kw={
            'data-val': 'true',
            'placeholder': _('Search'),
            'autocomplete': 'off',
            'input-group-btn': True,
            'input-group-btn-label': _("Search"),
            'input-group-btn-id': 'btn-search',
            'btn-color': 'btn-primary'
        }
    )

    customer_id = Select2Field(
        label=_(u'Customer'),
        remote_url='/payment/invoice_payment_suggestion',
        allow_blank=True,
        blank_text='',
        min_input_length=0,
        text_factory=lambda x: payment_logic.default.get_customer(x),
        multiple=False
    )

    customer_name = HiddenField()

    currency_id = DynamicSelectField(
        _("Currency"),
        query=[currency_logic.default.find('KHR')],
        get_pk='id',
        get_label='sign',
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }

    )
    pay_amount = TextField(
        _('Pay Amount'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off',
            'input-group-btn': True,
            'input-group-btn-label': _("Pay All"),
            'input-group-btn-id': 'pay-all'
        }
    )
    pay_date = DateField(
        _(u'Pay Date'),
        render_kw={
            'role': 'date',
            'data-val': 'true',
            'data-val-required': u'Input Required',
            'addon': 'fa-calendar'
        }
    )
    ending_balance = TextField(
        _(u'Ending Balance'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'readonly': 'readonly',
            'format': 'decimal'
        }
    )
    payment_method = DynamicSelectField(
        _(u'Payment Method'),
        query=lookup_logic.default.get_values(enum.payment_method.__val__),
        get_pk="id",
        get_label='value',
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    partial_payment = CheckBoxField(
        label='',
        checkbox_label=_(u"Partial Payment")
    )

    redirect_url = HiddenField()
    billing_id = DynamicSelectField(
        _(u'Season'),
        query_factory=billing_logic.default.find_billing,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All Season'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )


def lambda_format_value(row):
    result = '<td class="decimal">%s</td>' % row.pay_amount
    return render_template_string(result)


def lambda_total_amount(row):
    details = row.ext__details
    x = 0
    for detail in details:
        total_amount = detail.get('ext_data').get('total_amount') or detail.get('total_amount')
        paid_amount = detail.get('ext_data').get('paid_amount')
        if row.payment_method == '10013':
            paid_amount = detail.get('paid_amount')
        if not total_amount:
            total_amount = 0
        if paid_amount == '-' or paid_amount == "" or not paid_amount:
            paid_amount = 0
        x += float(total_amount) - float(paid_amount)
    from edf_admin.filter import format_usage

    return format_usage(x)


def lambda_customer(row):
    try:
        if hasattr(row.customer, 'farmer'):
            return row.customer.farmer
        else:
            return row.customer_name
    except Exception as e:
        return '-'


class PaymentTable(Table):
    rown = RowNumberColumn(_(u'No.'))
    code = LinkColumn(_(u"Payment Code"), get_url=lambda row: url_for('.PaymentView:detail', id=row.id))
    customer_name = LamdaColumn(_(u"Farmer"), get_value=lambda row: lambda_customer(row))
    payment_method = LamdaColumn(_(u'Payment Method'),
                                 get_value=lambda row: _('CASH') if row.payment_method == '10011' else _(
                                     'SmartLuy') if row.payment_method == '10012' else _('E_Money'))
    total_amount = LamdaColumn(_(u"Pay Amount"), get_value=lambda row: lambda_total_amount(row),
                               render_kw={'class': 'decimal'})
    pay_amount = AmountColumn(_(u"Paid Amount"))
    ending_balance = AmountColumn(_(u"Ending Balance"))
    currency = LamdaColumn(_(u"Currency"), get_value=lambda row: row.currency.sign)
    pay_date = DateColumn(_(u"Pay Date"))
    action = ActionColumn(
        '',
        endpoint='.PaymentView',
        more=lambda row: [
            {'label': _(u'Preview Receipt'), 'url': url_for('.PaymentView:preview_receipt', id=row.id)}
        ]
    )


class PaymentFilterForm(FilterForm):
    invoice_id = HiddenField()
    payment_method = DynamicSelectField(
        _(u'Payment Method'),
        query=lookup_logic.default.get_values(enum.payment_method.__val__),
        get_pk="id",
        get_label='value',
        allow_blank=True,
        blank_text=_(u'Payment Method'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    billing_id = DynamicSelectField(
        _(u'Season'),
        query_factory=billing_logic.default.find_billing,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All Season'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    issue_date = DateField(
        render_kw={
            'class': 'form-control input-tip date',
            'autocomplete': 'off',
            'placeholder': _(u'Issue Date'),
            'role': 'date',
            'data-val': 'true',
            'addon': 'fa-calendar'
        }

    )
    due_date = DateField(
        render_kw={
            'class': 'form-control input-tip date',
            'autocomplete': 'off',
            'placeholder': _(u'Due Date'),
            'role': 'date',
            'data-val': 'true',
            'addon': 'fa-calendar'
        }

    )

    redirect_url = HiddenField()


class PaymentView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Payment
        self.v_logic = payment_logic.default
        self.v_table_index_class = PaymentTable
        self.v_form_add_class = PaymentForm
        self.v_form_filter_class = PaymentFilterForm
        self.v_template = 'payment'
        self.v_logic_order_by = 'code desc'

    def _on_index_rendering(self, *args, **kwargs):
        # self.v_data['form'].customer_id.data = ''
        id = request.args.get('id', '')
        back_url = request.args.get('back_url') or None
        obj = self.v_logic.find(id=id)
        if obj:
            self.v_data['form'].search.data = obj.code
            self.v_data['form'].issue_date.data = obj.pay_date
            self.v_data['back_url'] = back_url
        invoice_id = request.args.get('invoice_id')
        if invoice_id:
            self.v_data['form'].invoice_id.data = invoice_id

    def _on_detail_rendering(self, *args, **kwargs):
        id = self.v_data['form'].id.data
        payment = payment_logic.default.find(id=id)
        self.v_data['form'].payment_method.data = payment.payment_method
        details = []
        for inv in payment.ext__details:
            obj = payment_logic.default.find_invoice(code=inv.get('code'))
            if obj:
                inv['billing_name'] = obj.billing.name
                details.append(inv)
        if details:
            payment.ext__details = details
        self.v_data['form'].ext_data.data = json.dumps(payment.ext)

    def _on_add_rendering(self, *args, **kwargs):
        redirect_url = request.args.get('redirect_url') or ''
        if redirect_url:
            self.v_data['form'].redirect_url.data = redirect_url
        self.v_data['form'].pay_date.data = datetime.now()
        invoice_code = request.args.get('invoice_code')
        if invoice_code:
            invoice = payment_logic.default.get_invoices(invoice_code=invoice_code)
            if invoice:
                results = {
                    'id': invoice[0].farmer.id,
                    'text': invoice[0].farmer.name,
                    'name': invoice[0].farmer.name,
                    'payable_balance': 0,
                    'invoices': [dict(
                        id=invoice[0].id,
                        code=invoice[0].code,
                        issue_date=invoice[0].issue_date,
                        total_amount=str(invoice[0].total_amount),
                        paid_amount=str(invoice[0].paid_amount),
                        billing_name=invoice[0].billing.name,
                        balance=str(invoice[0].total_amount - invoice[0].paid_amount),
                        currency_id=invoice[0].currency.sign
                    )]
                }
                self.v_data['customer'] = json.dumps(results)
                self.v_data['form'].customer_id.data = invoice[0].farmer.id
        else:
            self.v_data['customer'] = ''
            self.v_data['form'].customer_id.data = ''

    def _on_added(self, *args, **kwargs):
        redirect_url = self.v_data['form'].redirect_url.data
        if redirect_url:
            self.v_redirect_fn = lambda: url_for('.InvoiceView:index')
            return self.v_redirect_fn

    @route('/invoice_payment_suggestion')
    def invoice_payment_suggestion(self):
        term = request.args.get('term') or ''
        customer_id = request.args.get('customer_id') or 0
        limit = request.args.get('limit') or 10
        suggests = payment_logic.default.get_customer_list(search=term).limit(limit).all()
        results = [dict(
            id=s.id,
            text=s.name + '-' + s.spouse_name if s.name and s.spouse_name else s.code + '-' + s.name,
            obj=s,
            invoices=[dict(
                id=i.id,
                code=i.code,
                issue_date=i.issue_date,
                total_amount=i.total_amount,
                paid_amount=i.paid_amount,
                balance=i.total_amount - i.paid_amount,
                currency_id=i.currency.sign,
                billing_name=i.billing.name
            ) for i in payment_logic.default.get_invoices(farmer_id=s.id)]
        ) for s in suggests]
        data = {'results': results}
        return json.dumps(data)

    @route('/preview_receipt_on_invoice/<id>', methods=['GET'])
    def preview_receipt_on_invoice(self, id):
        return render_template("payment/receipt_preview-in-invoice.html", id=id, layout=session.get('layout', 'breadcrumb'),
                               view={})

    @route('/preview_receipt/<id>', methods=['GET'])
    def preview_receipt(self, id):
        back_url = request.args.get('back_url') or None
        if back_url:
            return render_template("payment/receipt_preview-2.html", id=id, layout=session.get('layout', 'breadcrumb'),
                                   view={}, back_url=back_url)

        return render_template("payment/receipt_preview-2.html", id=id, layout=session.get('layout', 'breadcrumb'),
                               view={})

    @route('/receipt_content/<id>')
    def receipt_content(self, id):
        payment = payment_logic.default.find(id)
        date = datetime.now()
        return render_template("payment/receipt_content-2.html", payment=payment, date=date)

    @route("/update_template/<id>")
    def update_template(self, id):
        if id:
            update = payment_logic.default.update_receipt_template(id=id)
            return update
        return 0


PaymentView.register(app)
