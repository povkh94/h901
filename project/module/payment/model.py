from ..share.model import *
from ..agri_bill.model import *


class Payment(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_payment'
    id = c.PkColumn()
    code = c.CodeColumn()
    pay_date = c.DateTimeColumn()
    currency_id = c.FkColumn()
    customer_id = c.FkColumn()
    customer_name = c.TitleColumn()
    pay_amount = c.DecimalColumn()
    ending_balance = c.DecimalColumn()  # due after pay
    payment_method = c.FkColumn()
    company_id = c.FkColumn()
    note = c.NoteColumn()
    partial_payment = c.BooleanColumn()  # make sure payment is partial or not
    currency = relationship('Currency', primaryjoin='foreign(Payment.currency_id) == remote(Currency.id)')
    customer = relationship('Farmer', primaryjoin='foreign(Payment.customer_id) == remote(Farmer.id)')

    @property
    def farmer_name(self):
        return self.customer.name + '{spouse_name}'.format(spouse_name= ' - ' + self.
                                                              customer.spouse_name if self.
                                                              customer.spouse_name else '') + '{code}'.format(
            code=' - ' + self.customer.code)


class PaymentDetail(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_payment_detail'
    id = c.PkColumn()
    payment_id = c.FkColumn()
    invoice_id = c.FkColumn()
    pay_amount = c.DecimalColumn()
    due_after_pay = c.DateTimeColumn()  # balanced during pay date.
    payment = relationship('Payment', primaryjoin='foreign(PaymentDetail.payment_id) == remote(Payment.id)')
    invoice = relationship('Invoice', primaryjoin='foreign(PaymentDetail.invoice_id) == remote(Invoice.id)')
