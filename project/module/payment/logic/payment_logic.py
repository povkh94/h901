from datetime import timedelta

from edf.helper.convert import to_datetime
from sqlalchemy import func, or_, and_, distinct
from sqlalchemy.orm import make_transient

from project.module.share.logic.base_logic import LogicBase, current, db
from project.module.payment.model import Payment, PaymentDetail, copyupdate, to_decimal, Farmer, Setting
from project.module.system.logic import autono_logic


class PaymentLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = Payment

    def new(self):
        seq_id = current.company().ext.get('seq', dict(payment='0')).get('payment-code', '0')
        obj = self.__classname__()
        # if seq_id:
        #     obj.code = autono_logic.default.next_no(id=seq_id)
        return obj

    def add(self, obj):
        seq_id = current.company().ext.get('seq', dict(payment='')).get('payment-code', '')
        if seq_id:
            obj.code = autono_logic.default.next_no(id=seq_id, update=True)
        self.is_commit = False
        if isinstance(obj.pay_amount, str) or isinstance(obj.pay_amount, unicode):
            obj.pay_amount = obj.pay_amount if "," not in obj.pay_amount else obj.pay_amount.replace(",", "")
        obj.customer_name = obj.ext__customer_name
        if obj.ext__customer_id:
            from project.module.agri_bill.logic import farmer_logic
            customer = farmer_logic.default.find(obj.ext__customer_id)
            if hasattr(customer, 'farmer'):
                obj.customer_name = customer.farmer
        db.add(obj)
        db.flush()
        if obj.ext__details:
            for detail in obj.ext__details:
                payment_detail = PaymentDetail()
                payment_detail.invoice_id = detail['ext_data']['invoice_id']
                payment_detail.payment_id = obj.id
                payment_detail.pay_amount = detail['pay_amount']
                payment_detail.due_after_pay = obj.pay_date
                db.add(payment_detail)
                db.flush()
        self._event('added', obj=obj)
        db.commit()

    def search(self, **kwargs):
        search = kwargs.get('search', '')
        customer_id = kwargs.get('customer_id', '')
        billing_id = kwargs.get('billing_id', '')
        payment_method = kwargs.get('payment_method', '')
        issue_date = kwargs.get('issue_date', '')
        due_date = kwargs.get('due_date', '')
        invoice_id = kwargs.get('invoice_id', '')
        if due_date:
            due_date = to_datetime(due_date) + timedelta(days=1)
        q = self.actives()
        if search:
            search = search.strip()
            farmer_ids = []
            if ',' in search:
                search = search.replace(',', '')
            farmers = (
                db.query(Farmer)
                    .filter(
                    or_(
                        func.lower(Farmer.code).like('%' + search.lower() + '%'),
                        func.lower(Farmer.spouse_name).like('%' + search.lower() + '%'),
                        func.lower(Farmer.name).like('%' + search.lower() + '%')
                    )
                ).all()
            )
            if farmers:
                for farmer in farmers:
                    farmer_ids.append(farmer.id)
            # if farmer_ids:
            #     q = q.filter(self.__classname__.customer_id.in_(farmer_ids))
            q = q.filter(or_(
                func.lower(self.__classname__.code).like('%' + search.lower() + '%'),
                func.lower(self.__classname__.pay_amount).like('%' + search.lower() + '%'),
                self.__classname__.customer_id.in_(farmer_ids))
            )
        if customer_id:
            q = q.filter(self.__classname__.customer_id == customer_id)
        if payment_method:
            q = q.filter(self.__classname__.payment_method == payment_method)
        if issue_date:
            q = q.filter(self.__classname__.pay_date >= issue_date)
        if due_date:
            q = q.filter(self.__classname__.pay_date <= due_date)
        if issue_date and due_date:
            q = q.filter(
                and_(self.__classname__.pay_date >= issue_date, self.__classname__.pay_date < due_date)
            )
        if billing_id:
            q_invoice = self.get_invoices(billing_id=billing_id)
            q_payment = db.query(distinct(PaymentDetail.payment_id)).filter(PaymentDetail.invoice_id.in_(q_invoice))
            q = q.filter(self.__classname__.id.in_(q_payment))
        if invoice_id:
            q_payment = db.query(distinct(PaymentDetail.payment_id)).filter(PaymentDetail.invoice_id == invoice_id)
            q = q.filter(self.__classname__.id.in_(q_payment))
        return q

    def update(self, obj, _commit=False):
        payment = self.find(id=obj.id)
        payment.pay_date = obj.pay_date
        payment.payment_method = obj.payment_method
        db.add(payment)
        db.flush()
        payment_details = db.query(PaymentDetail).filter(PaymentDetail.payment_id == payment.id).all()
        for payment_detail in payment_details:
            payment_detail.due_after_pay = payment.pay_date
            db.add(payment_detail)
            db.flush()
        db.commit()

    def get_customer_list(self, **kwargs):
        return []

    def get_invoices(self, **kwargs):
        return []

    def find_invoice(self, **kwargs):
        return object

    def get_customer(self, **kwargs):
        return object

    def find_billing(self, **kwargs):
        return []

    def delete_payment(self, payment_id):
        payment = db.query(Payment).filter(Payment.id == payment_id).first()
        payment.is_active = False
        db.add(payment)
        db.flush()
        payment_details = db.query(PaymentDetail).filter(PaymentDetail.payment_id == payment_id).all()
        if payment_details:
            for detail in payment_details:
                detail.is_active = False
                db.add(detail)
                db.flush()
        # CLEAR INVOICES
        # result = (db.query(PaymentDetail).filter(PaymentDetail.payment_id == payment_id).delete())
        # db.query(Payment).filter(Payment.id == payment_id).delete()
        self.is_commit = False
        db.commit()

    def find_receipt(self, invoice_id):

        if invoice_id:
            q = db.query(PaymentDetail).filter(PaymentDetail.invoice_id == invoice_id, PaymentDetail.is_active).first()
            if q:
                return db.query(Payment).filter(Payment.id == q.payment_id).first()

    def find_receipt_template(self, is_active=''):
        if is_active:
            return db.query(Setting).filter(Setting.datatype == 'receipt-template', Setting.is_active).first()
        return db.query(Setting).filter(Setting.datatype == 'receipt-template').all()

    def update_receipt_template(self, id):
        if id:
            row = db.query(Setting).filter(Setting.datatype == 'receipt-template', Setting.is_active).first()
            row.is_active = 0
            db.add(row)
            db.flush()
            template = db.query(Setting).filter(Setting.id == id).first()
            template.is_active = 1
            db.add(template)
            db.commit()
            return 1
        return 0


default = PaymentLogic()
