__info__ = {
    "name": "payment",
    "title": "payment Module",
    "summary": "System Module for Sequence, Component, Role, Permission, Setting etc.",
    "author": "RY Rith",
    "website": "http://project.com",
    "depends": ['share']
}


def activate(**kwargs):
    from . import view


def install():
    pass


def uninstall():
    pass
