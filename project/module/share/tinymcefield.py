# required 'WTForms'+'Flask-WTF'
# code customize
# by : RY Rith
# date : 2017-03-23

from wtforms import widgets
from wtforms.compat import text_type, string_types
from wtforms.fields import Field
from wtforms.validators import ValidationError
from wtforms.widgets import HTMLString, html_params


class TinyMCEWidget(object):
    html_params = staticmethod(html_params)

    def __init__(self):
        pass

    def __call__(self, field, **kwargs):
        if field.data == u'None':
            field.data = None
        html = """
<div class="form-group" id="{id}-wrap" %s>
    <textarea id="{id}-editor">{data}</textarea>
    <input type="hidden" name="{id}" id="{id}" value="" class="form-control" style="display:none;" /> 
</div> 
<script type="text/javascript" src="/static/share/helpers/tinymce/tinymce.min.js"></script> 
<script>
    $(document).ready(function () {
        $(".mce-container").remove();
        tinymce.init({
          selector: 'textarea:not(#address)',
          {height}
          statusbar:false,menubar:false,
          toolbar:"undo redo | fontselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | link image {custom_toolbar_texts} | fontsizeselect",
          font_formats: 'Hanuman=Hanuman;Khmer OS=Khmer OS;Khmer Muol=Khmer Muol;Khmer OS Muol Light=Khmer OS Muol Light;Khmer OS Siemreap=Khmer OS Siemreap;Tacteing=Tacteing;Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats',
          content_css : "/static/share/styles/font.css",
          theme_advanced_fonts: 'Khmer OS=Khmer OS;Khmer Muol=Khmer Muol;Khmer OS Muol Light=Khmer OS Muol Light;Khmer OS Siemreap=Khmer OS Siemreap;Tacteing=Tacteing',
          init_instance_callback: function (editor) { 
            editor.on('change', function (e) { 
               for (i=0; i < tinyMCE.editors.length; i++){
                    var content = tinyMCE.editors[i].getContent();
                    var id = tinyMCE.editors[i].id.split("-")[0];
                    $('#' + id).val(content);
                }
            });
            var id = editor.id.split("-")[0];
        
            $('#' + id).val(editor.getContent());
            
          },
          setup: function(editor) {
           {editor}
          },
          fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt"
        }); 
    });
</script>""".replace('{id}', field.id) \
            .replace('{data}', field.data or '') \
            .replace('{height}', 'height:"%s",' % str(field.height))
        marcro_toolbar_data = field.marcro_toolbar_data
        toolbars_name = []
        editors = []
        if marcro_toolbar_data and (isinstance(marcro_toolbar_data, list) or isinstance(marcro_toolbar_data, tuple)):
            for data in marcro_toolbar_data:
                toolbars_name.append(data.get("toolbar_name"))
                marcro_manus = data.get("marcro_menu")
                marcro = []
                for marcro_manu in marcro_manus:
                    manu = []
                    for base_menu in marcro_manu.get('menu') or []:
                        sub_menu = []
                        for child_menu in base_menu.get('menu_data') or []:
                            sub_menu.append(
                                "{text:'{marcro_text}',onclick: function() {editor.insertContent('{marcro_value}');}}"
                                .replace("{marcro_text}", child_menu.get('marcro_text'))
                                .replace("{marcro_value}", child_menu.get('marcro_value'))
                                )

                        manu.append("{text:'{menu_text}',menu:[{menu}]}"
                                    .replace("{menu_text}", base_menu.get('menu_name'))
                                    .replace("{menu}", ",".join(sub_menu))
                                    )
                    marcro.append(",".join(manu))
                editors.append("""editor.addButton('{toolbar_name}', {
		                            type: 'menubutton',
		                            text: '{toolbar_text}',
		                            icon: false,
		                            menu: [{manu}]
	                            })
                            """.replace("{toolbar_name}", data.get("toolbar_name")) \
                               .replace("{toolbar_text}", data.get("toolbar_text")) \
                               .replace("{manu}", ",".join(marcro))
                               )
        html = html.replace('{custom_toolbar_texts}', " | ".join(toolbars_name)).replace('{editor}', ",".join(editors))
        # html  = html % html_params(**field.render_kw)

        return HTMLString(html)


class TinyMCEField(Field):
    widget = TinyMCEWidget()

    def __init__(self, label=None, validators=None, render_kw={}, marcro_toolbar_data=[], height="", **kwargs):
        super(TinyMCEField, self).__init__(label, validators, **kwargs)
        self.render_kw = render_kw or dict();
        self.marcro_toolbar_data = marcro_toolbar_data
        self.height = height

    def process_data(self, value):
        try:
            self.data = text_type(value)
        except (ValueError, TypeError):
            self.data = None

    def process_formdata(self, valuelist):
        if valuelist:
            self.data = text_type(valuelist[0])

    def pre_validate(self, form):
        pass
