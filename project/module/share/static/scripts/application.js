window.fbAsyncInit = function () {
    FB.init({
        appId: "573048623069824",
        xfbml: true,
        cookie: true,
        version: 'v3.0'
    });
};
// (function (d, s, id) {
// //     var js, fjs = d.getElementsByTagName(s)[0];
// //     if (d.getElementById(id)) return;
// //     js = d.createElement(s);
// //     js.id = id;
// //     js.src = "//connect.facebook.net/en_US/sdk.js";
// //     fjs.parentNode.insertBefore(js, fjs);
// // }(document, "script", "facebook-jssdk"));

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0&appId=573048623069824&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

var VALID_IMAGE_TYPES = ["image/gif", "image/jpeg", "image/png"];

$(document).ready(function () {
    // scrollbar
    //$(".sidebar-body").mCustomScrollbar({
    //    autoHideScrollbar: true,
    //    scrollInertia: 0
    //});

    // sidebar toggle event
    $(".sidebar-body > ul li a").on("click", function () {
        $(this).next("ul").toggle();
    });
    $(".navbar-toggle").die().live("click", function () {
        $(".fixed-sidebar-modal").addClass("show-xs");
        $(".fixed-sidebar").addClass("show-xs").show("slide", {direction: "left"}, 350);
    });
    $(".fixed-sidebar-modal, .fixed-sidebar.show-xs a[href]").die().live("click", function () {
        $(".fixed-sidebar-modal").removeClass("show-xs");
        $(".fixed-sidebar").toggle("slide", {direction: "left"}, 350, function () {
            $(".fixed-sidebar").removeClass("show-xs")
                .css("display", "");
        });
    });

    // sidebar active event
    //$(".sidebar ul li.active a").css("background", "#337ab7").css("color", "#fff");
    $(".sidebar ul li a[href]").on("click", function () {
        //$(".sidebar ul li.active a:not(#btn-activate-supplier)").css("background", "initial");
        //$(".sidebar ul:not(.dropdown-menu) li.active a").css("color", "rgba(255,255,255,.7)");
        //$(".sidebar ul.dropdown-menu li.active a").css("color", "#000");

        $(".sidebar ul li").removeClass("active");
        $(this).parent().addClass("active");
        //$(this).not("#btn-activate-supplier").css("background", "#337ab7").css("color", "#fff");
    });

    // dropdown menu
    $(".sidebar-header .dropdown-menu a").click(function () {
        $(".sidebar-header .dropdown-toggle").click();
    });

    // ajax loader
    app.wait = function () {
        $("#loading").css("width", "100%");
    };

    app.ready = function () {
        $("#loading").css("width", "0%");
    };

  $(document).not("[id$='-editor']").ajaxStart(function () {
        app.wait();
    }).ajaxStop(function () {
        app.ready();
    });

    $(document).on('submit','#wrapper',function () {
        app.wait();
    }).ajaxStop(function () {
        app.ready();
    });
    // quick add dialog
    window.openQuickAdd = function (url, onload, onhidden) {
        var id = Math.random().toString().replace('0.', 'm');
        $('<div id="' + id + '" class="modal fade" role="dialog"></div>')
            .load(url + '?x-layout=modal&x-modal-id=' + id, function (result) {
                $(this).modal({show: true})
                    .on('hidden.bs.modal', function () {
                        $("#" + id).remove();
                        $("#modal-callback").html('');
                        if (typeof (onhidden) == 'function') {
                            onhidden(id);
                        }
                    });
                if (typeof (onload) == 'function') {
                    onload(result, id);
                }
            });
    };

    // copy to clipboard
    function set_tooltip(target, trigger, placement) {
        $(target).tooltip({
            trigger: trigger,
            placement: placement
        });
    }

    function show_tooltip(target, message) {
        $(target).tooltip("hide")
            .attr("data-original-title", message)
            .tooltip("show");
    }

    function hide_tooltip(target) {
        setTimeout(function () {
            $(target).tooltip("destroy");
        }, 1000);
    }

    var clipboard = new Clipboard("[data-clipboard-target]");
    clipboard.on('success', function (e) {
        var data_tooltip_target = e["trigger"]["attributes"]["data-tooltip-target"];
        if (data_tooltip_target == null) {
            alert("null");
        }
        else {
            var target = data_tooltip_target["value"];
            set_tooltip(target, "click", "bottom")
            show_tooltip(target, "{{ _('Copied') }}");
            hide_tooltip(target);
        }
        e.clearSelection();
    });

    app.changeSelectedMenu = function () {
        //debugger;
        if ($('.sidebar').find('a[href="' + window.location.pathname + '"]').length > 0) {
            //  have menu that match with url
            $('.sidebar a[href="' + window.location.pathname + '"]').each(function () {
                $(".sidebar ul li").removeClass("active");
                $(this).parent().addClass("active");
            });
        }
        else if ($('.sidebar').find('a[href="' + window.location.pathname + '"]').length == 0) {
            //  don't have menu that match with url
            if ($('.sidebar').find('a[href^="/' + window.location.pathname.split('/')[1] + '"]').length > 0) {
                //  have menu that start with url
                $(".sidebar ul li").removeClass("active");
                $('.sidebar').find('a[href^="/' + window.location.pathname.split('/')[1] + '"]').parent().addClass("active");
            }
            else {
                var map = {}
                map['/setting/index'] = ['/project/', '/propertytype/', '/paymentterm/', '/propertygroup/', '/customertype/',
                    '/salepersontype/', '/loantype/', '/contracttemplate/', '/user/', '/role/', '/company/', '/calendar/',
                    '/setting/sequence', '/bill24/update-config']
                if ($.inArray(window.location.pathname, map['/setting/index']) > -1) {
                    $(".sidebar ul li").removeClass("active");
                    $(".sidebar #setting").parent().addClass("active");
                }
            }
        }

    };
    app.changeSelectedMenu();


    //$("#goto-dashboard").click();
    if (!window.hasOwnProperty('on_popstate_handler')) {
        window.on_popstate_handler = function () {
            if (!window.userInteractionInHTMLArea) {
                $('#redirect-url').attr('href', window.location.href).trigger('click');
            }
        };
        $(window).on('popstate', window.on_popstate_handler);
        app.changeSelectedMenu();
    }


    //notification
    app.notification = function (message) {
        $("#notification").fadeIn("slow");
        setTimeout(function () {
            $("#notification").fadeOut("slow");
        }, 2500);
        $("#notification .notification-message").text(message);
    };
    $(".dismiss").die().live("click", function () {
        $("#notification").fadeOut("slow");
    });


});
