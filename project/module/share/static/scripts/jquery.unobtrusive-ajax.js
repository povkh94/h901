﻿/// <reference path="jquery-1.5.1.js" />

/*!
** Unobtrusive Ajax support library for jQuery
** Copyright (C) Microsoft Corporation. All rights reserved.
*/

/*jslint white: true, browser: true, onevar: true, undef: true, nomen: true, eqeqeq: true, plusplus: true, bitwise: true, regexp: true, newcap: true, immed: true, strict: false */
/*global window: false, jQuery: false */

(function ($) {
    var data_click = "unobtrusiveAjaxClick",
        data_validation = "unobtrusiveValidation";

    function getFunction(code, argNames) {
        var fn = window, parts = (code || "").split(".");
        while (fn && parts.length) {
            fn = fn[parts.shift()];
        }
        if (typeof (fn) === "function") {
            return fn;
        }
        argNames.push(code);
        return Function.constructor.apply(null, argNames);
    }

    function isMethodProxySafe(method) {
        return method === "GET" || method === "POST";
    }

    function asyncOnBeforeSend(xhr, method) {
        if (!isMethodProxySafe(method)) {
            xhr.setRequestHeader("X-HTTP-Method-Override", method);
        }
    }

    function asyncOnSuccess(element, data, contentType) {
        var mode;

        if (contentType.indexOf("application/x-javascript") !== -1) {  // jQuery already executes JavaScript for us
            return;
        }

        mode = (element.getAttribute("data-ajax-mode") || "").toUpperCase();
        $(element.getAttribute("data-ajax-update")).each(function (i, update) {
            var top;

            switch (mode) {
                case "BEFORE":
                    top = update.firstChild;
                    $("<div />").html(data).contents().each(function () {
                        update.insertBefore(this, top);
                    });
                    break;
                case "AFTER":
                    $("<div />").html(data).contents().each(function () {
                        update.appendChild(this);
                    });
                    break;
                default:
                    $(update).html(data);
                    break;
            }
        });

    }

    function asyncRequest(element, options) {
        var confirm, loading, method, duration;
        confirm = element.getAttribute("data-ajax-confirm");
        if (confirm && !window.confirm(confirm)) {
            return;
        }

        loading = $(element.getAttribute("data-ajax-loading"));
        duration = element.getAttribute("data-ajax-loading-duration") || 0;

        $.extend(options, {
            type: element.getAttribute("data-ajax-method") || undefined,
            url: element.getAttribute("data-ajax-url") || undefined,
            beforeSend: function (xhr) {
                var result;
                asyncOnBeforeSend(xhr, method);
                result = getFunction(element.getAttribute("data-ajax-begin"), ["xhr"]).apply(this, arguments);
                if (result !== false) {
                    loading.show(duration);
                }
                return result;
            },
            complete: function (e) {
                //find first input element
                //$("input:visible:enabled:not(.datetime), select:visible:enabled,textarea:enabled").first().focus();

                // register validation
                $.validate.unobtrusive($);
                $('form').validator({
                    feedback: {
                        success: 'glyphicon-ok',
                        error: 'glyphicon-remove'
                    }
                }).on('submit', function (e) {
                    if (e.isDefaultPrevented()) {
                        return false
                    }
                });

                loading.hide(duration);

                getFunction(element.getAttribute("data-ajax-complete"), ["xhr", "status"]).apply(this, arguments);
                // ready
                app.ready();
                // display message on error or unauthorized.
                if (e.status == 200 || e.status == 302) {
                    //find first input element
                    //$("input:visible:enabled:not(.datetime), select:visible:enabled,textarea:enabled").first().focus();

                    // register validation
                    $.validate.unobtrusive($);
                    $('form').validator({
                        feedback: {
                            success: 'glyphicon-ok',
                            error: 'glyphicon-remove'
                        }
                    }).on('submit', function (e) {
                        if (e.isDefaultPrevented()) {
                            return false
                        }
                    });
                }
                if (e.status == 400 || e.status == 404 || e.status == 500) {
                    if (e.status == 404) {
                        e.responseText = "404 - Not found";
                    }
                    if (e.status == 500) {
                        e.responseText = "500 - Server error";
                    }
                    var html = $("#modal-error").html();
                    if (html) {
                        html = html.replace('{text}', e.responseText);
                        $('<div class="modal fade" role="dialog">')
                            .append(html)
                            .modal('show');
                    } else {
                        alert('Page Error : ' + e.responseText)
                    }
                }
                if (e.status == 401) {
                    var html = $("#modal-unauthorized").html();
                    if (html) {
                        $('<div class="modal fade" role="dialog">')
                            .append(html)
                            .modal('show');
                    } else {
                        alert('Unauthorized Access!')
                    }
                }
                if (e.status == 406) {
                    var html = $("#modal-method-not-accept").html();
                    if (html) {
                        html = html.replace('{text}', $.parseJSON(e.responseText).message);
                        $('<div class="modal fade" role="dialog">')
                            .append(html)
                            .modal('show');
                    } else {
                        alert('Not Acceptable!')
                    }
                }
            },
            success: function (data, status, xhr) {

                asyncOnSuccess(element, data, xhr.getResponseHeader("Content-Type") || "text/html");
                getFunction(element.getAttribute("data-ajax-success"), ["data", "status", "xhr"]).apply(this, arguments);
            },
            error: getFunction(element.getAttribute("data-ajax-failure"), ["xhr", "status", "error"])

        });

        options.data.push({name: "X-Requested-With", value: "XMLHttpRequest"});
        options.data.push({name: "isAjax", value: true});
        method = options.type.toUpperCase();
        if (!isMethodProxySafe(method)) {
            options.type = "POST";
            options.data.push({name: "X-HTTP-Method-Override", value: method});
        }

        $.ajax(options);
    }

    function validate(form) {
        var validationInfo = $(form).data(data_validation);
        return !validationInfo || !validationInfo.validate || validationInfo.validate();
    }

    if (!window.hasOwnProperty('onUnobtrusiveClickHandler')){
        window.onUnobtrusiveClickHandler = function (evt) {
            evt.preventDefault();
            app.wait();
            var obj = $(this);

            if (obj.attr('href') == '#' || obj.attr('href') == 'javascript:void(0)') return false;

            var updateTarget = obj.attr("data-ajax-update") || "#content";
            obj.attr("data-ajax-update", updateTarget);

            var loading = obj.attr("data-ajax-loading") || "#ajaxCall";
            obj.attr("data-ajax-loading", loading);

            // window.location.hash = $(this).attr('href');
            if(!$(this).attr('redirect')) {
                history.pushState(null, null, $(this).attr('href'));
            }

            asyncRequest(this, {
                url: this.href,
                type: "GET",
                data: []
            });

            // close modal target.
            var mid = obj.attr("data-modal-target")
            if (mid) {
                $(mid).modal('toggle');
            }
            return false;
        };
        $(document).on('click',"#wrapper a[href][data-ajax!=false],[data-ajax=true],.modal-dialog a[href][data-ajax!=false]",
            window.onUnobtrusiveClickHandler);
    }

    if (!window.hasOwnProperty('onUnobtrusiveSubmitHandler')){
        window.onUnobtrusiveSubmitHandler = function (evt) {
            var clickInfo = $(this).data(data_click) || [];
            evt.preventDefault();
            if (!validate(this)) {
                return;
            }
            app.wait();
            var obj = $(this);
            var updateTarget = obj.attr("data-ajax-update") || "#content";
            obj.attr("data-ajax-update", updateTarget);

            obj.attr("method", obj.attr("method") || "POST");
            var loading = obj.attr("data-ajax-loading") || "#ajaxCall";
            obj.attr("data-ajax-loading", loading);
            asyncRequest(this, {
                url: this.action,
                type: this.method || "POST",
                contentType: this.enctype,
                data: clickInfo.concat($(this).serializeArray())
            });

            // close modal target.
            var mid = obj.attr("data-modal-target");
            if (mid) {
                $(mid).modal('toggle');
            }
        };
        $(document).on('submit',"#filter-wrapper form[data-ajax-update],#wrapper form[data-ajax!=false],form[data-ajax-update],form[data-ajax-update],.modal-dialog form[data-ajax!=false]",
                window.onUnobtrusiveSubmitHandler);
    }

}(jQuery));