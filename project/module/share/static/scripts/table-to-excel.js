//export to excel
function fnExcelReport(click_id, filename, table_id) {
    var new_table_name = table_id + '-clone';
    var clone_table = $("#" + table_id)[0].cloneNode(true);
    clone_table.setAttribute('id', new_table_name);

    $('tr a', clone_table).each(function () {
        $(this).replaceWith($('<span>' + this.innerHTML + '</span>'));
    });

    $("tr input[type='text']", clone_table).each(function () {
       // $("<span />", {text: this.value}).insertAfter(this);
        $(this).replaceWith($('<span>' + this.value + '</span>'));
        //$(this).hide();
    });

    $("tr input[type='checkbox']", clone_table).each(function () {
        $(this).replaceWith($('<span></span>'));
    });

    $("tr select option:selected", clone_table).each(function () {
        //$("<span />", {text: $(this).text()}).insertAfter(this);
        $(this).parent().siblings().replaceWith($('<span></span>'));
        $(this).parent().replaceWith($('<span>' + $(this).text() + '</span>'));
       // $(this).hide();
    });


    // append to body for access data in table
    document.body.appendChild(clone_table);
    $('#' + new_table_name).hide();

    jQuery(function ($) {
        $('#' + new_table_name + ' th[class^=no-export]').remove();
        $('#' + new_table_name + ' td[class^=no-export]').remove();
        $('#' + new_table_name + ' td[class^=no-print]').parent().hide();
    })
    var tab_text = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
    tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';
    tab_text = tab_text + '<x:Name>"' + filename + '"</x:Name>';
    tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
    tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
    tab_text = tab_text + "<table border='1px' style='font-family:Kh Siemreap; font-size:12px;'>";
    tab_text = tab_text + $('#' + new_table_name).html();
    tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
    tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
    tab_text = tab_text + '</table></body></html>';

    var data_type = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8;';

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        if (window.navigator.msSaveBlob) {
            var blob = new Blob([tab_text], {
                type: "application/csv;charset=utf-8;"
            });
            navigator.msSaveBlob(blob, filename + '.xls');
        }
    } else {
        $('#' + click_id).attr('href', data_type + ', ' + encodeURIComponent(tab_text));
        $('#' + click_id).attr('download', filename + '.xls');
    }

    $('#' + new_table_name).replaceWith('');
}