from flask import current_app, Flask, redirect, url_for
from flask_babel import Babel

app = current_app  # type: Flask

# initialize babel
babel = Babel(app)


# switch language
@babel.localeselector
def get_local():
    from flask import request, session
    if 'language' in request.cookies:
        session['language'] = request.cookies.get('language')
        return request.cookies.get('language')
    else:
        return 'km'


@babel.timezoneselector
def get_local():
    return 'UTC'


@app.route('/language/<code>')
def language(code):
    from datetime import datetime, timedelta
    from flask_babel import refresh
    from flask import make_response
    expire_date = datetime.now() + timedelta(days=7300)
    response = make_response(redirect(url_for('.HomeView:home')))
    response.set_cookie('language', code, expires=expire_date)
    refresh()
    return response
