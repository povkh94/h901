from flask import session
from edf.base.model import *

try:
    from ..system_security.logic import user_logic
    from ..system_security.model import User

except ImportError:
    class FakeUser(TrackMixin, ExtMixin):
        __tablename__ = 'fake_user'
        id = Column(Key(), primary_key=True, default=gid)
        name = Column(String(50))
        password = Column(String(250))
    user_logic = None
    User = FakeUser


@current.promote('user_id')
def _current_user_id():
    return session.get('user_id', None)


@current.promote('user_name')
def _current_user_name():
    return session.get('user_name', '-')


@current.promote('user')
def _current_user():
    uid = current.user_id()
    if uid and user_logic:
        user = user_logic.default.find(uid)
        return user or User(id=None, name = '-', password='')

    return User(id=None, name = '-', password='')


