﻿import calendar
from decimal import Decimal

import os
from flask import current_app as app, render_template_string, request
from edf.helper import convert
from flask_babel import lazy_gettext as _
from wtforms.widgets import HTMLString


@app.template_filter('format_currency')
def format_currency(value, sign='', ):
    if value == "-":
        value = 0
    return '{0:,.2f}'.format(float(value)) + ' ' + sign


@app.template_filter('format_quantity')
def format_quantity(value):
    if value:
        return '{0:0.2f}'.format(value)
    else:
        return '-'


@app.template_filter('format_usage')
def format_usage(value, unit=''):
    if value:
        return '{0:,.0f}'.format(convert.to_decimal(value)) + ' ' + unit
    else:
        return '-'


@app.template_filter('to_float')
def to_float(value):
    return float(value)


@app.template_filter('format_time')
def format_time(value):
    if value:
        return value.strftime('%H:%M')
    else:
        return ''


@app.template_filter('format_datetime')
def format_datetime(value):
    if value:
        return value.strftime('%Y-%m-%d %H:%M')
    else:
        return ''


@app.template_filter('format_date')
def format_date(value):
    if value and not isinstance(value, str):
        return value.strftime('%d-%m-%Y')
    if value and isinstance(value, str):
        return value
    else:
        return ''


@app.template_filter('format_day')
def format_day(value):
    if value and not isinstance(value, str):
        d = {'mon': _('Mon'), 'tue': _('Tue'), 'wed': _('Wed'), 'thu': _('Thu'), 'fri': _('Fri'), 'sat': _('Sat'),
             'sun': _('Sun')}
        return d.get(value.strftime("%a").lower())
    if value and isinstance(value, str):
        return value
    else:
        return ''


@app.template_filter('to_dict')
def to_dict(value):
    import json
    value = json.loads(value)
    return value


@app.template_filter('to_json')
def to_json(value):
    import json
    value = json.dumps(value)
    return value


@app.template_filter('total')
def total(rows, val, predicate):
    t = 0
    tmp = []
    for row in rows:
        for k, v in predicate.iteritems():
            if getattr(row, k) == v:
                t = getattr(row, val)

            else:
                t = 0
                break
        tmp.append(t)

    return sum(tmp)
@app.template_filter('count')
def count(rows, val, predicate):
    t = 0
    tmp = []
    for row in rows:
        for k, v in predicate.iteritems():
            if getattr(row, k) == v:
                t = getattr(row, val)

            else:
                t = 0
                break
        tmp.append(t)
    tmp = set(tmp)
    return len(tmp)


# Rakky
@app.template_filter('total_percent')
def total(rows, val1, val2, val3, predicate):
    t1 = 0
    t2 = 0
    result = 0
    tmp1 = []
    tmp2 = []
    for row in rows:
        for k, v in predicate.iteritems():
            if getattr(row, k) == v:
                t1 = getattr(row, val1)
                t2 = getattr(row, val2)
            else:
                t1 = 0
                t2 = 0
                break
        tmp1.append(t1)
        tmp2.append(t2)
        total_cost = sum(tmp1)
        total_paid = sum(tmp2)
        if total_cost == 0:
            total_cost = 1
        result = (total_paid * 100) / total_cost
        # if result > 0 and result < 0.5:
        #     result = round(result, 2)
        #     if val3:
        #         result = 100 - result
        # elif result > 0.5:
        #     result = round(result, 2)
        #     if val3:
        #         result = 100 - result
        if result > 0:
            if val3:
                result = 100 - result
        else:
            if val3:
                result = 100

    return '{0:,.2F}'.format(result)


# Rakky
@app.template_filter('to_month_name')
def to_month_name(val):
    months = [u'មករា', u'កុម្ភៈ', u'មីនា', u'មេសា', u'ឧសភា', u'មិថុនា', u'កក្កដា', u'សីហា', u'កញ្ញា', u'តុលា',
              u'វិចិក្កា',
              u'ធ្នូ']

    if val and not isinstance(val, str):
        year = val.strftime('%Y')
        month = int(val.strftime('%m'))
        day = val.strftime('%d')
        # value = calendar.month_name[2]
        # if request.cookies.get('language') == 'km' or request.cookies.get('language') == None:
        #     value.replace('January', u'មករា')\
        #               .replace('February', u'កុម្ភៈ')\
        #               .replace('March', u'មីនា')\
        #               .replace('April', u'មេសា')\
        #               .replace('May', u'ឧសភា')\
        #               .replace('June', u'មិថុនា')\
        #               .replace('July', u'កក្កដា')\
        #               .replace('August', u'សីហា')\
        #               .replace('September', u'កញ្ញា')\
        #               .replace('October', u'តុលា')\
        #               .replace('November', u'វិច្ឆិកា')\
        #               .replace('December', u'ធ្នូ')
        #     return year+"-"+value+"-"+day
        # else:
        #     return value[:3]
        if request.cookies.get('language') == 'km' or request.cookies.get('language') == None:
            return year + "-" + months[month - 1] + "-" + day
        else:
            return format_date(val)


# format value no point(.)
@app.template_filter('format_value')
def format_value(value, sign=''):
    if not value:
        value = 0
    value = str(value)
    if value == "-" or not value:
        value = 0
    elif "," in value:
        value = int(value.replace(',', ''))
    value = abs(float(value))
    return '{0:,.0F}'.format(value) + ' ' + sign


# format integer
@app.template_filter('format_integer')
def format_integer(value, sign=''):
    return '{0:,.0F}'.format(value)


@app.template_filter('export_image_pdf')
def export_image_pdf(url):
    from project.admin.config import SHARE_IMAGES
    url = url.split("/")
    # current_dir = os.path.dirname(os.path.abspath(__file__))
    # target_dir = os.sep.join(current_dir.split(os.sep)[0:-2])
    # image_url = os.sep.join([target_dir, 'module', 'share', 'static', 'images', url[-1]])
    url = url[4:]
    url.insert(0, SHARE_IMAGES)
    image_url = os.sep.join(url)
    return image_url


# convert Area from Are to Heta
@app.template_filter('to_ha')
def to_ha(value):
    if value:
        value = value / 100
        return '{0:,.2F}'.format(float(value))
    else:
        return '-'


# convert tariff price from area to heta
@app.template_filter('to_ha_cost')
def to_ha_cost(value):
    if value:
        value = value * 100
        return '{0:,.0F}'.format(float(value))
    else:
        return '-'


# From Static
@app.template_filter('image_pdf')
def export_image_pdf(url):
    url = url.split("/")
    current_dir = os.path.dirname(os.path.abspath(__file__))
    target_dir = os.sep.join(current_dir.split(os.sep)[0:-2])
    image_url = os.sep.join([target_dir, 'module', 'share', 'static', 'images', url[-1]])
    return image_url


@app.template_filter('style_pdf')
def style_pdf(url):
    url = url.split("/")
    current_dir = os.path.dirname(os.path.abspath(__file__))
    target_dir = os.sep.join(current_dir.split(os.sep)[0:-2])
    style_url = os.sep.join([target_dir, 'module', 'share', 'static', 'styles', url[-1]])
    return style_url


@app.template_filter('to_table')
def to_table(table, blank=0, column_count=0, blank_min_height='20px', visible_rowno_for_blank=True):
    length_table = len(table.data)
    row = blank - length_table
    if not column_count:
        column_count = len(table._fields)
    try:
        html = """
<style>
td, th{
    border: 1px solid #ccc;
    padding : 5px;
}
td:not(:last-child), th:not(:last-child){
    border-right: 0;
}
tbody td{
    border-top: 0;
}
.blank-tr{
    min-height: {{blank_min_height}};
}
</style>
<table id="agreement-table" cellspacing="0" style="width:100%;font-family:'Khmer OS'; font-size:11px;">
    <thead style="text-align:left;">
    {{ table.render_header() }}
    </thead>
    <tbody style="text-align:left;">
    {{ table.render_rows() }}
    {% if blank and (table.data | length) < blank %}
        {% for i in range(blank - (table.data | length)) %}
            <tr>
                {% if visible_rowno_for_blank %}
                    <td><div class="blank-tr">{{ (table.data | length) + i + 1 }}</div></td>
                    {% for _ in range(column_count-1) %}
                        <td><div class="blank-tr"></div></td>
                    {% endfor %}
                {% else %}
                    {% for _ in range(column_count) %}
                        <td><div class="blank-tr"></div></td>
                    {% endfor %}
                {% endif %}
            </tr>
        {% endfor %}
    {% endif %}
    </tbody>
</table>
<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
<script>
        $(document).ready(function(){
         $("#agreement-table tbody tr td input[select_tariff]").each(function(){
            var select_tariff=$(this)[0];
            var select_tariff_id=$(select_tariff).attr("select_tariff");
            var tariff_ids = $(this).parent().parent().find("td[tariff_id]");
            for(var i=0;i<tariff_ids.length;i++){
                var tariff_id =$($(tariff_ids)[i]).attr("tariff_id");
                if (select_tariff_id == tariff_id){
                    var obj = $($(tariff_ids)[i]);
                    obj = $(obj)[0];
                    obj = $(obj).find("input")[0];
                    $(obj).attr("checked",true);
                    break;
                }
            }
            
     });
        });
    
</script>
"""
        r = render_template_string(HTMLString(html),
                                   table=table,
                                   blank=blank,
                                   column_count=column_count,
                                   blank_min_height=blank_min_height,
                                   visible_rowno_for_blank=visible_rowno_for_blank)
        return r
    except:
        return ''


@app.template_filter('format_monthly')
def format_monthly(value):
    from ..agri_bill import convert
    if value:
        return '%s-%s' % (convert.to_month_name(value.month), value.year)
    else:
        return ''


@app.template_filter('to_riel')
def to_riel(val):
    try:
        import numpy as np
        if val == "-":
            val = 0
        if ',' in str(val):
            val = str(val).replace(',', '')
        value = np.ceil(float(val))
        v1 = value % 100  # get value the last two digits for round if v1 > 0 and v<= 99 to 100 riel
        if v1 > 0 and v1 <= 99:
            value = (value - v1) + 100
        return '{0:,.0F}'.format(value)
    except Exception as e:
        return '0'
