from flask import url_for
from flask_babel import lazy_gettext as _
from .lib.widget import *

register_widget('sidebar', order=0, widget=NavWidget(
    id='home', title=_('Home'), icon='fa fa-home',url=lambda: url_for('.HomeView:dashboard'))
)


