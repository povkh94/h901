from edf.base.model import current
from flask import current_app as app, url_for, request, session  # type:Flask

from project.module.share.lib.widget import render_widget


class Html(str):
    def __call__(self, *args, **kwargs):
        return self


@app.context_processor
def app_preprocessor():

    layout = session.get('layout','breadcrumb')
    if layout == 'full':

        return dict(
            current=current,
            user=current.user(),
            v=app.config.get('STATIC_VERISON','v0.0'),
            render_widget=render_widget,
            home_url=Html(url_for('.HomeView:dashboard')),
            title='Sample App',
        )
    else:
        return dict(
            current=current,
            user=current.user(),
            v=app.config.get('STATIC_VERISON', 'v0.0'),
            render_widget=render_widget,
            home_url=Html(url_for('.HomeView:dashboard')),
            title='Sample App',
        )

@app.before_request
def before_request():
    if request.endpoint != 'static':
        if request.args.get('isAjax') == 'true':
            session['layout'] = 'breadcrumb'
        elif request.is_xhr:
            session['layout'] = 'breadcrumb'
        else:
            session['layout'] = 'full'
