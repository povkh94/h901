__info__ = {
    "name": "share",
    "title": "Share Module",
    "summary": "",
    "version": "1.0.0",
    "author": "RY Rith",
    "website": "http://project.com",
    "depends": []
}


def activate(**kwargs):
    import app_filter
    import app_processor
    import app_translate
    import view
    import widget
    import current



def install(**kwargs):
    pass


def uninstall(**kwargs):
    pass
