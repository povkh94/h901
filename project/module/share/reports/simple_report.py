﻿from project.module.share.reports.base_report import *


class SimpleReport(Report):
   
    def __init__(self, *args, **kwargs):
        r = super(SimpleReport, self).__init__(**kwargs)
        self.get_result = kwargs.get('get_result') or self.get_result
        self.get_table = kwargs.get('get_table') or self.get_table
        self.get_filter_form = kwargs.get('get_filter_form') or self.get_filter_form
        self.get_widget = kwargs.get('get_widget') or self.get_widget
        self.templates = kwargs.get('templates',list())
        return r;
          