﻿from project.core.model import *
from project.module.agri_bill.logic import component_logic
from project.module.agri_bill.logic.component_logic import ComponentLogic
from ..view.base_view import *
from flask import render_template, render_template_string
from flask_babel import lazy_gettext as _

import itertools

system_reports = dict()


def register_component(c):
    lg = ComponentLogic()
    p = lg.find(name=c.parent)
    co = c.to_component()
    if p:
        co.parent_id = p.id
    component_logic.default.register(co)


class ComponetObject(object):

    @property
    def widget(self):
        return getattr('title', self)

    def to_component(self):
        c = Component()
        c.name = self.name
        c.title = self.title
        c.description = self.description
        c.meta = """from {mdl} import {cls} as ComponentClass""".format(mdl=self.__class__.__module__,
                                                                        cls=self.__class__.__name__)
        c.target = enums.app_target.all
        c.type = None
        c.position = 0

        return c


class ReportGroup(ComponetObject):

    def __init__(self, *args, **kwargs):
        self.name = kwargs.get('name', 'Report Group')
        self.title = kwargs.get('title', 'Report Title')
        self.description = kwargs.get('description', '')
        self.parent = kwargs.get('parent', None)
        self.reports = kwargs.get('reports', [])
        self.groups = kwargs.get('groups', [])

    def register_report(self, report):
        group = self.get_group(report.parent)
        if group:
            group.reports.append(report)
        self.reports.append(report)

    def register_group(self, group):
        self.groups.append(group)

    def get_group(self, name):
        r = [g for g in self.groups if g.name == name]
        if r:
            return r[0]

    def get_report(self, name):
        s = [r for r in self.reports if r.name == name]
        if s:
            return s[0]

    def to_component(self):
        c = super(ReportGroup, self).to_component()
        c.type = enums.component_type.report_group
        return c


class Report(ComponetObject):

    def __init__(self, *args, **kwargs):
        self.name = kwargs.get('name', 'Report Group')
        self.title = kwargs.get('title', 'Report Title')
        self.description = kwargs.get('description', '')
        self.parent = kwargs.get('parent', None)
        self.index_template = kwargs.get('index_template', 'reports/default/index.html')
        self.result_template = kwargs.get('result_template', 'reports/default/result.html')
        self.templates = kwargs.get('templates', list())

    def get_template(self, template):
        r = template
        if template.startswith('c:'):
            cid = template[2:]
            from project.core.logic import component_logic
            c = component_logic.default.find(id=cid)
            if c:
                r = c.meta_data
        return r

    def get_filter_form(self, **kwargs):
        class ReportFilterForm(FlaskForm):
            search = TextField(_('Search'))

        return ReportFilterForm(**kwargs)

    def get_table(self, **kwargs):
        class ReportTable(Table):
            rowno = RowNumberColumn(_(u'No'))
            phone = DataColumn(_(u'Phone'))
            email = DataColumn(_(u'Email'))
            address = DataColumn(_(u'Address'))

        return ReportTable(**kwargs)

    def get_widget(self):
        return render_template_string(
            '<a href="{{url}}">{{title}}</a>'
            , title=self.title
            , url=url_for('ReportView:index', report_name=self.name))

    def get_result(self, **kwargs):
        return []

    def to_component(self):
        c = super(Report, self).to_component()
        c.type = enums.component_type.report
        return c

    # def render_index(self):
    #     table = self.get_table()
    #     form = self.get_filter_form()
    #     title = self.title
    #     if hasattr(self, 'templates') and hasattr(form, 'template'):
    #         form.template.choices = [(h.name, h.title) for h in self.templates]
    #
    #     filter_data = json.loadx(session.get('report/%s/filter' % self.name, '{}'))
    #     for k, v in filter_data.iteritems():
    #         if hasattr(form, k):
    #             getattr(form, k).data = v
    #     for k, v in request.args.iteritems():
    #         if hasattr(form, k):
    #             getattr(form, k).data = v
    #
    #     render = render_template if self.index_template.endswith('.html') else render_template_string
    #     view = {"title": title}
    #     return render(
    #         self.index_template,
    #         report=self,
    #         form=form,
    #         table=table,
    #         view=view
    #     )

    def render_index(self):
        table = self.get_table()
        form = self.get_filter_form()
        title = self.title
        if hasattr(self, 'templates') and hasattr(form, 'template'):
            form.template.choices = [(h.name, h.title) for h in self.templates]

        filter_data = json.loadx(session.get('report/%s/filter' % self.name, '{}'))
        for k, v in filter_data.iteritems():
            if hasattr(form, k):
                getattr(form, k).data = v
        for k, v in request.args.iteritems():
            if hasattr(form, k):
                getattr(form, k).data = v

        filter_result = self.render_result(from_index=True, filter_data=filter_data)

        render = render_template if self.index_template.endswith('.html') else render_template_string
        view = {"title": title}
        return render(
            self.index_template,
            report=self,
            form=form,
            table=table,
            filter_result=filter_result,
            view=view
        )

    def render_result(self, from_index=False, filter_data={}):
        offset = request.args.get('offset', 0)
        # limit  = request.args.get('limit',15)
        form = self.get_filter_form()
        #  get filter_data from index or form
        filter_data = filter_data or form.data
        #  change u'None' to None
        for k, v in filter_data.iteritems():
            if v and v in ('None', '__None'):
                filter_data[k] = None

        q = self.get_result(**filter_data)
        if hasattr(q, 'all'):
            pagging = DataView(q, offset=offset)
            table = self.get_table(data=pagging.result, report_name=filter_data.get('report_name') or '')
        else:
            table = self.get_table(data=q)
        #  no need to store data to session if its call by index
        if not from_index:
            filter_data = json.dumps(filter_data)
            session['report/%s/filter' % self.name] = filter_data
        render = render_template if self.result_template.endswith('.html') else render_template_string
        return render(
            self.result_template,
            report=self,
            form=form,
            table=table,
            result=q
        )


class SystemReportCenter(object):
    reports = dict()
    center = ReportGroup(name='system-center', title='System Report')

    @classmethod
    def register(cls, report):
        cls.reports[report.name] = report
        cls.center.register_report(report)
