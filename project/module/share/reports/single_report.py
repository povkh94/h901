﻿from flask_babel import lazy_gettext as _
from wtforms import SelectField

from project.module.share.reports.base_report import *


class SingleRowReportFilterForm(FlaskForm):
        id = HiddenField(_('Id'))
        template = SelectField(_('Template'),choices=[('t1','Template1'),('t2','Template2')]) 

class SingleRowReport(Report):
   
    def __init__(self, *args, **kwargs):
        r = super(SingleRowReport, self).__init__(**kwargs)
        self.get_result = kwargs.get('get_result') or self.get_result
        self.templates = kwargs.get('templates',list())
        return r;
         
    def get_filter_form(self):
        return SingleRowReportFilterForm() 
     
    def get_table(self,**kwargs):
        return None

    def get_source(self, **kwargs):
        return kwargs.get('id') or 0

    def get_widget(self): 
        return render_template_string(
            '<a href="{{url}}">{{title}}</a>'
            ,title = self.title 
            ,url   = url_for('ReportView:index',report_name =self.name))


    def render_index(self):
        table = self.get_table()
        form = self.get_filter_form()

        if hasattr(self, 'templates') and hasattr(form, 'template'):
            form.template.choices = [(h.name, h.title) for h in self.templates]

        filter_data = json.loadx(session.get('report/%s/filter' % self.name, '{}'))
        for k, v in filter_data.iteritems():
            if hasattr(form, k):
                getattr(form, k).data = v
        for k, v in request.args.iteritems():
            if hasattr(form, k):
                getattr(form, k).data = v

        filter_result = self.render_result(from_index=True, filter_data=filter_data)

        render = render_template if self.index_template.endswith('.html') else render_template_string
        return render(
            self.index_template,
            report=self,
            form=form,
            table=table,
            filter_result=filter_result
        )

    # def render_index(self):
    #     table = self.get_table()
    #     form = self.get_filter_form()
    #     if hasattr(self,'templates') and hasattr(form,'template'):
    #          form.template.choices = [(t[0], t[1]) for t in self.templates]
    #
    #     if not form.is_submitted():
    #         form.template.data= self.result_template
    #         filter_data = json.loadx(session.get('report/%s/filter'%self.name,'{}'))
    #         for k,v in filter_data.iteritems():
    #             if hasattr(form,k):
    #                 getattr(form,k).data=v
    #         for k,v in request.args.iteritems():
    #             if hasattr(form,k):
    #                 getattr(form,k).data=v
    #
    #     if form.is_submitted():
    #         filter_data = json.dumps(form.data)
    #         session['report/%s/filter'%self.name]=filter_data
    #
    #     result =  self.get_result(**form.data)
    #     template = self.get_template(form.template.data or self.result_template)
    #     render = render_template if template.endswith('.html') else  render_template_string
    #
    #     return render(
    #         template,
    #         report = self,
    #         form = form,
    #         table = table,
    #         result =  result
    #     )

    def render_result(self):
        return None