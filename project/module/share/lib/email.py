from flask import current_app, Flask, url_for
from flask_mail import Mail, Message

app = current_app  # type:Flask

'''
MAIL_SERVER = 'mail.ubill24.com'
MAIL_PORT = 465
MAIL_USERNAME = 'estore@ubill24.com'
MAIL_PASSWORD =  '!7ekBIP%O#)z'
MAIL_DEFAULT_SENDER = 'estore@ubill24.com'
MAIL_USE_TLS = False
MAIL_USE_SSL = True
'''


def send_email(receivers, cc=[''], bcc=[''], subject='Email', message='This is email from EStore', message_html='',
               attachment=None, attachment_name='', attachment_type='pdf'):
    app.config.setdefault('MAIL_SERVER', 'mail.ubill24.com')
    app.config.setdefault('MAIL_PORT', 465)
    app.config.setdefault('MAIL_USERNAME', 'estore@ubill24.com')
    app.config.setdefault('MAIL_PASSWORD', '!7ekBIP%O#)z')
    app.config.setdefault('MAIL_DEFAULT_SENDER', 'estore@ubill24.com')
    app.config.setdefault('MAIL_USE_TLS', False)
    app.config.setdefault('MAIL_USE_SSL', True)

    mail_ext = Mail(app)
    mail_to_be_sent = Message(subject=subject, recipients=receivers, cc=cc, bcc=bcc)

    if message_html:
        mail_to_be_sent.html = message_html
    else:
        mail_to_be_sent.body = message

    # pdf
    if attachment_type == 'pdf':
        default_content_type = 'application/pdf'
        default_attachment_type = '.pdf'
        default_attachment_name = 'attachment'

    attachment_name = (attachment_name if attachment_name else default_attachment_name) + default_attachment_type

    if attachment:
        mail_to_be_sent.attach(attachment_name, default_content_type, attachment)

    mail_ext.send(mail_to_be_sent)
