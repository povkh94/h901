from flask import Flask, current_app, request, url_for, render_template_string
import random
app = current_app  # type:Flask


class Widget(object):
    def __init__(self, id, text, order=0):
        self.id = id
        self.text = text
        self.order = order
        self.module = None

    def render(self):
        return render_template_string(self.text, widget=self)

    def __call__(self, *args, **kwargs):
        return self.render()

    def __str__(self):
        return self.text

    def __html__(self):
        return self.render()


class NavWidget(Widget):
    def __init__(self, id, title, url='', description='', icon='', children=list(),
                 block='sidebar', order=0, custom_render=None, template_text=''):
        """
        :param url: string url or function that return url for this link
        :param title: title that will display in when render
        :param description: detail description if need.
        :param icon: icon that will display when render if need.
        :param id: id that will use to check with permission_id
        :param children: list of children link related
        :param template_text: text for customize rendering
        :param custom_render: function for customising render
        """
        self.url = url
        self.title = title
        self.description = description
        self.icon = icon
        self.id = id
        self.block = block
        self.order = order
        self.children = children
        self.custom_render = custom_render
        self.template_text = template_text


    @property
    def href(self):
        url = self.url
        if self.url and callable(self.url):
            url = self.url()
        return url

    def render(self):
        if self.custom_render and callable(self.custom_render):
            return self.custom_render()

        if not self.template_text:
            self.template_text = """
            <li> 
                {% if not widget.children %}
                <a id="{{ widget.id }}" href="{{widget.href}}">
                    <i class="{{widget.icon}}"></i>
                    {{widget.title}}
                </a>
                {% else %}
                    <a id="{{ widget.id }}">
                        {{widget.title}}
                    </a>
                    <ul>
                    {% for sub_link in widget.children %}
                        <li>
                            {{ sub_link.render()|safe }}
                        </li>
                    {% endfor %}
                </ul>
                {% endif %} 
            </li>
            """
        return render_template_string(self.template_text,widget=self)


def register_widget(block, widget='<div>Something...</div>', order=0):
    """
    Register widget to html block.
    :param block: location where widget will display in. available blocks are
        'style',
        'script',
        'sidebar',
        'sidebar-profile',
        'dashboard'
    :param order: position where widget will render.
    :param widget: a widget object that can render to html element.
    :type widget: Widget
    :return:
    """
    containers = app.config.get('WIDGET') or dict()  # type: dict
    container = containers.get(block) or list()  # type: list[Widget]
    if order:
        widget.order = order
    widget.module = app.config.get('LAST_MODULE', '')
    container.append(widget)

    containers[block] = container
    app.config['WIDGET'] = containers


def register_widget_text(block, text, id=None, order=0):
    if not id:
        id = str(random.random())
    w = Widget(text=text, id=id, order=order)
    register_widget(block, widget=w)


def render_widget(block):
    containers = app.config.get('WIDGET') or dict()  # type: dict
    container = containers.get(block) or list()  # type: list[Widget]
    wgs = []
    for w in sorted(container, key=lambda x: x.order):
        wgs.append(w.render())
    text = ''.join(wgs)
    return text


