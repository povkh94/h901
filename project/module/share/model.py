from edf.base.model import *
from edf.helper.sequence import randomstring

current._gid = lambda: randomstring()
current._key = lambda: String(10)


class ShortcutColumn(object):
    def PkColumn(self):
        return Column(Key(), primary_key=True, default=gid)

    def FkColumn(self, index=True):
        return Column(Key(), index=True)

    def CodeColumn(self, index=False):
        return Column(String(10))

    def TitleColumn(self):
        return Column(String(100))

    def NoteColumn(self):
        return Column(String(1000))

    def MetaColumn(self):
        return Column(String(4000))

    def TextColumn(self):
        return Column(UnicodeText)

    def DecimalColumn(self, s=18, p=4, default=0):
        return Column(Numeric(s, p), default=default)

    def IntegerColumn(self):
        return Column(Integer)

    def BooleanColumn(self, default=False):
        return Column(Boolean, default=default)

    def DateTimeColumn(self):
        return Column(DateTime)

    def BinaryColumn(self):
        return NotImplementedError('Not yet implement.')


c = ShortcutColumn()


class ParentMixin(object):
    @declared_attr
    def parent_id(self):
        return c.FkColumn()

    @declared_attr
    def path(self):
        return c.NoteColumn()

    @declared_attr
    def parent(self):
        return relationship(self.__name__,
                            primaryjoin='foreign({c}.parent_id) == remote({c}.id)'.format(c=self.__name__)
                            )
