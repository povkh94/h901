from flask_babel import lazy_gettext as _

from project.module.company.logic import company_logic
from .base_view import *

class CompanyForm(FlaskForm):
    ext__files = UploadField(_(u'Files'), multiple=True)

class HomeView(AdminSecureView):
    route_base = '/home'

    @route('/index', methods=['GET'])
    def home(self):

        return render_template(
            'home/index.html',
            title=_(u'Home Page'),
            view={},
            layout=session.get('layout', 'breadcrumb')
        )

    @route('/dashboard', methods=['GET'])
    def dashboard(self):
        company = company_logic.default.find(current.company_id())
        images, form = None, None
        if company:
            images = company.ext__photo or '-'
            form = CompanyForm()
            form.ext__files.data = company.ext__files
        layout = session.get('layout', 'breadcrumb')
        if layout == 'full':
            return redirect(url_for('.HomeView:home'))
        if images:
            images = images.split(";")
        return render_template(
            'home/dashboard.html',
            title=_(u'Dashboard'),
            company=company,
            images=images,
            form=form,
            view={},


        )


HomeView.register(app)
