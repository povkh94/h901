from flask_classy import route, get_interesting_members
from flask import render_template_string

import os
import logging
import json

from flask import current_app as app, url_for, redirect, request, session, \
    redirect, url_for, render_template, render_template_string, request

from wtforms import TextAreaField, TextField, PasswordField, \
    DateField, DateTimeField, FileField, HiddenField

from wtforms.validators import DataRequired, Regexp, InputRequired, Email
from flask_wtf import FlaskForm

Form = FlaskForm

from flask_babel import lazy_gettext as _
from flask_classy import FlaskView, route

from edf.helper.convert import *
import edf.helper.jsonx

from edf_admin.form.dynamicselectfield import DynamicSelectField
from edf_admin.form.select2field import Select2Field
from edf_admin.form.tokenfield import Select2TokenField
from edf_admin.form.uploadfield import UploadField, UploadImageField
from edf_admin.table import Table, CheckColumn, DecimalColumn, Column as DataColumn, LamdaColumn, RowNumberColumn
from edf_admin.table import DateColumn, DateTimeColumn, CheckBoxColumn, SelectingCheckBoxColumn, DataView, paginate, \
    ActionColumn, LamdaColumn, LinkColumn


class AdminView(FlaskView):
    pass


class AdminSecureView(FlaskView):
    pass


from edf.base.model import *
from edf.base.logic import *
from events import Events


class RemoveConfirmForm(FlaskForm):
    id = HiddenField()


class BlankForm(FlaskForm):
    pass


class FilterForm(FlaskForm):
    search = TextField("search", validators=[], render_kw={"placeholder": _("Search"), "class": "form-control no-print"})


class BlankTable(Table):
    pass


class CRUDView(AdminSecureView):
    def render_template(self, template='index.html', **kwargs):
        tpl = '%s/%s' % (self.v_template, template)

        kwargs.update(dict(view=self.v_data))
        if 'form' in self.v_data:
            kwargs.update(dict(form=self.v_data['form']))
        if 'table' in self.v_data:
            kwargs.update(dict(table=self.v_data['table']))

        #kwargs['layout'] = request.args.get('x-layout', 'breadcrumb')
        kwargs['layout'] = session.get('layout', request.args.get('x-layout', 'breadcrumb'))
        kwargs['request'] = request
        kwargs['sid'] = self.sid

        return render_template(tpl, **kwargs)

    def __init__(self, *args, **kwargs):
        import random
        self.sid = 's%s' % str(random.random())[2:]
        # register event
        self.events = Events()
        self.events.on_initializing += self._on_initializing
        self.events.on_initialized += self._on_initialized
        self.events.on_index_rendering = self._on_index_rendering
        self.events.on_filter_rendering = self._on_filter_rendering
        self.events.on_export_rendering = self._on_export_rendering
        self.events.on_add_rendering += self._on_add_rendering
        self.events.on_adding += self._on_adding
        self.events.on_added += self._on_added
        self.events.on_edit_rendering += self._on_edit_rendering
        self.events.on_editing += self._on_editing
        self.events.on_edited += self._on_edited
        self.events.on_remove_rendering += self._on_remove_rendering
        self.events.on_removing += self._on_removing
        self.events.on_removed += self._on_removed
        self.events.on_detail_rendering = self._on_detail_rendering

        # initialing value
        self.v_logic = LogicBase()
        self.v_class = object()
        self.v_route = None
        self.v_template = "crud"
        self.v_data = dict()
        self.v_forms = dict()
        self.v_tables = dict()
        self.v_form_add_class = None
        self.v_form_edit_class = None
        self.v_form_filter_class = FilterForm
        self.v_form_remove_class = RemoveConfirmForm
        self.v_form_detail_class = None
        self.v_table_index_class = None

        self.events.on_initializing()

        self.v_template = self.v_template or self.__class__.__name__.lower().replace('view', '')
        self.v_route = self.v_route or self.__class__.__name__.lower().replace('view', '')
        self.v_data = self.v_data or dict()
        self.v_form_edit_class = self.v_form_edit_class or self.v_form_add_class
        self.v_form_detail_class = self.v_form_detail_class or self.v_form_edit_class
        self.v_form_remove_class = self.v_form_remove_class or RemoveConfirmForm

        self.v_data['title'] = self.__class__.__name__.lower().replace('view', '')
        self.v_data['endpoints'] = {m[0]: self.__class__.__name__ + ':' + m[0] for m in
                                    get_interesting_members(FlaskView, self.__class__)}
        self.v_forms['filter'] = self.v_form_filter_class or FilterForm
        self.v_forms['add'] = self.v_form_add_class
        self.v_forms['edit'] = self.v_form_edit_class
        self.v_forms['remove'] = self.v_form_remove_class
        self.v_forms['detail'] = self.v_form_detail_class
        self.v_tables['index'] = self.v_table_index_class or BlankTable

        if not hasattr(self, 'v_redirect_fn'):
            self.v_redirect_fn = lambda: url_for(self.v_data['endpoints']['index'])

        self.events.on_initialized()

        return super(CRUDView, self).__init__(*args, **kwargs)

    def _get_form(self, name, **kwargs):
        return self.v_forms[name]

    def index(self):
        form = self.v_forms['filter']()
        table = self.v_tables['index']()

        # load filter from session
        filter_data = json.loadx(session.get('%s/filter' % self.v_route, '{}'))
        for k, v in filter_data.iteritems():
            if hasattr(form, k):
                getattr(form, k).data = v

        self.v_data['form'] = form
        self.v_data['table'] = table
        self.events.on_index_rendering()
        return self.render_template('index.html', filter_data=filter_data, d1=datetime.now())

    @route('/filter', methods=['GET', 'POST'])
    def filter(self):
        offset = request.args.get('offset', 0)
        limit = request.args.get('limit', 15)
        page = int(request.args.get('page', 1))
        per_page = int(request.args.get('per_page', 10))

        form = self.v_forms['filter']()
        filter_data = dict()
        # if peek to page then load filter from session
        if 'page' in request.args:
            filter_data = json.loadx(session.get('%s/filter' % self.v_route, '{}'))
        else:
            filter_data = form.data

        q = self.v_logic.search(**filter_data)
        if hasattr(q,'order_by'):
           q = q.order_by('updated_date desc')
        # pagging = DataView(q,limit=limit, offset=offset)
        # table = self.v_tables['index'](data=pagging.result)
        pagging = paginate(q, page=page, per_page=per_page)
        table = self.v_tables['index'](data=pagging.items, row_number_offset=pagging.per_page * (pagging.page - 1))

        # keep filter in session
        filter_data = json.dumps(filter_data)
        session['%s/filter' % self.v_route] = filter_data

        self.v_data['form'] = form
        self.v_data['table'] = table
        self.events.on_filter_rendering()
        return self.render_template('filter.html', pagging=pagging)

    @route('/add', methods=['GET', 'POST'])
    def add(self):
        self.v_data['modal'] = bool(request.args.get('x-ajax-modal'))
        self.v_data['obj'] = self.v_logic.new()
        form = self._get_form(name='add')(obj=self.v_data['obj'])

        self.v_data['form'] = form
        if form.validate_on_submit():
            obj = from_dict(form.data, to_cls=self.v_class, ignores=['id'])
            try:
                self.events.on_adding(obj)
                self.v_logic.add(obj)
                self.events.on_added(obj)
                return redirect(self.v_redirect_fn())
            except LogicError, e:
                db.rollback()
                if e.field in form:
                    form[e.field].errors.append(str(e))

        # fill default value
        if not form.is_submitted():
            # default value by user via filter session
            filter_data = json.loads(session.get('%s/filter' % self.v_route, '{}'))
            for k, v in filter_data.iteritems():
                if k in form:
                    form[k].data = v
            # default value by code via url
            for k, v in request.args.iteritems():
                if hasattr(form, k):
                    getattr(form, k).data = v

        self.events.on_add_rendering()
        return self.render_template('add.html')

    @route('/edit/<id>', methods=['GET', 'POST'])
    def edit(self, id=0):
        self.v_data['obj'] = self.v_logic.find(id)
        form = self._get_form(name='edit')(request.form, obj=self.v_data['obj'])
        if request.method == 'POST' and form.validate():
            self.v_data['obj'] = from_dict(form.data, to_cls=self.v_class, defaults={})
            try:
                self.events.on_editing(self.v_data['obj'])
                self.v_logic.update(self.v_data['obj'])
                self.events.on_edited(self.v_data['obj'])
                return redirect(self.v_redirect_fn())
            except LogicError, e:
                db.rollback()
                if e.field in form:
                    form[e.field].errors.append(str(e))
        self.v_data['form'] = form
        # self.v_data['obj']=obj
        self.events.on_edit_rendering()
        return self.render_template('edit.html')

    @route('/remove/<id>', methods=['GET', 'POST'])
    def remove(self, id=0):
        obj = self.v_logic.find(id)
        canremove = self.v_logic.canremove(id)
        form = self.v_forms['remove'](obj=obj)
        if request.method == 'POST' and form.validate():
            try:
                self.events.on_removing(obj)
                self.v_logic.remove(obj)
                self.events.on_removed(obj)
                return redirect(self.v_redirect_fn())
            except LogicError, e:
                db.rollback()
                form.id.errors.append(str(e))
        self.v_data['form'] = form
        self.v_data['obj'] = obj
        self.events.on_remove_rendering()
        return self.render_template('remove.html', canremove=canremove)

    @route('/detail/<id>')
    def detail(self, id=0):
        obj = self.v_logic.find(id)
        form = self.v_forms['detail'](request.form, obj=obj)
        self.v_data['form'] = form
        self.v_data['obj'] = obj
        self.events.on_detail_rendering()
        return self.render_template('detail.html')

    @route('/suggestion')
    def suggestion(self):
        term = request.args.get('term', '')
        limit = request.args.get('limit', 25)
        suggests = self.v_logic.search(search=term).limit(limit).all();
        results = [dict(
            id=s.id,
            text=unicode(s)
        ) for s in suggests]
        data = {'results': results}
        return json.dumps(data)

    def _on_initializing(self, *args, **kwargs):
        pass

    def _on_initialized(self, *args, **kwargs):
        pass

    def _on_index_rendering(self, *args, **kwargs):
        pass

    def _on_filter_rendering(self, *args, **kwargs):
        pass

    def _on_export_rendering(self, *args, **kwargs):
        pass

    def _on_add_rendering(self, *args, **kwargs):
        pass

    def _on_adding(self, *args, **kwargs):
        pass

    def _on_added(self, *args, **kwargs):
        pass

    def _on_edit_rendering(self, *args, **kwargs):
        pass

    def _on_editing(self, *args, **kwargs):
        pass

    def _on_edited(self, *args, **kwargs):
        pass

    def _on_remove_rendering(self, *args, **kwargs):
        pass

    def _on_removing(self, *args, **kwargs):
        pass

    def _on_removed(self, *args, **kwargs):
        pass

    def _on_detail_rendering(self, *args, **kwargs):
        pass
