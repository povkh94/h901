import warnings

from edf.base.logic import *

from project.module.payment.model import PaymentDetail


class EventMixin(object):
    _events = dict()

    @classmethod
    def on(cls, event, **kwargs):
        """
        The decorator for handle event of an object.

        @CustomerLogic.on('added')
        def extend_customer_add(**kwargs):
            obj = kwargs.get('obj')
            # do something...

        :param event: event name such added, updated, removed...
        :param kwargs: kw arguments for all event.
        :return: event handler.
        """

        def decorator(f):
            if event not in cls._events:
                cls._events[event] = list()
            handlers = cls._events.get(event)  # type:list
            handlers.append(f)
            return f

        return decorator

    @classmethod
    def _event(cls, event, **kwargs):
        if event not in cls._events:
            cls._events[event] = list()
        handlers = cls._events.get(event)  # type:list
        for handler in handlers:
            handler(**kwargs)


class LogicBase(EventMixin):
    def __init__(self, *args, **kw_args):
        if '__classname__' in kw_args:
            self.__classname__ = kw_args['__classname__']
        else:
            self.__classname__ = object

        if 'is_commit' not in kw_args:
            kw_args['is_commit'] = True
        self.is_commit = kw_args['is_commit']

    def db_commit(self, _commit=True):
        self._commit(_commit=_commit)
        warnings.warn(
            'The .db_commit() has been deprecated. Consider use .commit(_commit=True)',
            DeprecationWarning, stacklevel=2
        )

    def _commit(self, _commit=True):
        # flush is come first.
        if not _commit:
            return db.flush()
        if not self.is_commit:
            return db.flush()
        return db.commit()

    def actives(self):
        q = db.query(self.__classname__).filter_by(is_active=True)
        return q

    def all(self):
        result = self.actives().all()
        return result

    def new(self):
        result = self.__classname__()
        return result

    def find(self, id, update=False):
        if not id:
            return None
        result = db.query(self.__classname__) \
            .filter_by(id=id) \
            .first()
        return result

    def find_invoice_ids(self, payment_id):
        result = db.query(PaymentDetail).filter(PaymentDetail.payment_id == payment_id).all()
        if result:
            return [i.invoice_id for i in result]
        return []

    def add(self, obj, _commit=True):
        if not obj.id:
            obj.id = gid()
        obj.is_active = True
        db.add(obj)
        db.flush()
        self._event('added', obj=obj)
        self._commit(_commit=_commit)

    def update(self, obj, _commit):
        old = self.find(obj.id)
        copyupdate(obj, old)
        db.flush()
        self._event('updated', obj=obj)
        self._commit(_commit=_commit)

    def remove(self, obj, _commit=True):
        if hasattr(obj, 'is_active'):
            obj.is_active = False
        else:
            db.remove(obj)
        db.flush()
        self._event('removed', obj=obj)
        self._commit(_commit=_commit)

    def search(self, **kwargs):
        search = kwargs.get('search', '')
        q = self.actives()
        if search:
            if hasattr(self.__classname__, 'name') and hasattr(self.__classname__, 'code'):
                q = q.filter(
                    or_(func.lower(self.__classname__.code).like('%' + search.lower() + '%'),
                        func.lower(self.__classname__.name).like('%' + search.lower() + '%')
                        )
                )
            elif hasattr(self.__classname__, 'code'):
                q = q.filter(func.lower(self.__classname__.code).like('%' + search.lower() + '%'))
            elif hasattr(self.__classname__, 'name'):
                q = q.filter(func.lower(self.__classname__.name).like('%' + search.lower() + '%'))
        return q

    def canremove(self, id):
        return True

    def canedit(self, id):
        return True
