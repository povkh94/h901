from wtforms import widgets
from wtforms.fields import Field
from wtforms.validators import ValidationError
from wtforms.widgets import HTMLString, html_params
from wtforms.compat import text_type, string_types

from flask import current_app as app, request, url_for, send_from_directory

app.config['UPLOAD_PATH'] = app.config.get('UPLOAD_PATH') or app.root_path + '/static/images'
app.config['SHARE_IMAGES'] = app.config.get('SHARE_IMAGES') or app.root_path + '/static/images'


@app.route('/save_snap/<path:data>')
def save_snap(data):
    import uuid, json, os.path
    img_data = data.split(",")[-1]
    name = "%s.jpeg" % str(uuid.uuid4())
    path = "%s/captures/%s" % (app.config['SHARE_IMAGES'], name)
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    with open(path, "wb") as fh:
        fh.write(img_data.decode('base64'))
    result = "/snapshot/captures/%s" % (name)
    return json.dumps(result)


@app.route('/remove_snap/<path:filename>')
def remove_snap(filename):
    import os, json
    os.remove(os.path.join(app.config.get('SHARE_IMAGES'), filename))
    return json.dumps(True)


@app.route('/snapshot/<path:filename>')
def snapshot(filename):
    return send_from_directory(app.config.get('SHARE_IMAGES'), filename, as_attachment=False)


class CaptureWiget(object):
    html_params = staticmethod(html_params)

    def __init__(self):
        pass

    def __call__(self, field, **kwargs):
        if field.data == u'None':
            field.data = None

        html = """
<div class="capture-wrap"  id="{id}-wrap" {render_kw}>
    <input type="hidden" id="{id}" name="{id}" value="{value}" />
    <div id="{id}-viewer">
        <img class="img img-responsive" id="{id}-display" style="display:none;" />
    </div>
    <div id="{id}-camera"></div>
    <button type="button" class="btn btn-primary" id="{id}-take-photo">Take Photo</button>
    <button type="button" class="btn btn-primary" id="{id}-retake-photo" style="display:none;margin-top:5px;">Retake Photo</button>
</div>
<script type="text/javascript">
$(document).ready(function () {
    //Configure a few settings and attach camera
    Webcam.set({
        width: 295,
        height: 240,
        image_format: 'jpeg',
        jpeg_quality: 90
    });

    if ($("#{id}").val()) {
        $("#{id}-display").show();
        $("#{id}-display").attr("src", $("#{id}").val())
        $("#{id}-take-photo").hide();
        $("#{id}-camera").hide();
        $("#{id}-retake-photo").show();
    } else {
        Webcam.reset();
        Webcam.attach('#{id}-camera');
    }

	$("#{id}-take-photo").die("click").live("click", function (e) {
	    Webcam.snap(function (data_uri) {
            //delete old snap if have
            if($("#{id}").val()){
                var url = {delete_url} + $("#{id}").val().replace("/snapshot/","");
                $.ajax({
                    url: url,
                    type: 'GET',
                    data: '',
                    success: function(data) {
                        //$(this).remove();
                    },
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
            //Upload
            var url = {upload_url} + data_uri
            $.ajax({
                url: url,
                type: 'GET',
                data: '',
                success: function(data) {
                    data = jQuery.parseJSON(data);
                    // display results in page
	                $("#{id}-display").attr("src", data);
	                $("#{id}-display").show();
	                $("#{id}").val(data);
                },
                async: false,
                cache: false,
                contentType: false,
                processData: false
            });
	    });
	    $("#{id}-camera").hide();
	    $(this).hide();
	    $("#{id}-retake-photo").show();
        Webcam.reset();
	});

	$("#{id}-retake-photo").die("click").live("click", function (e) {
        $("#{id}-display").hide();
	    $("#{id}-take-photo").show();
	    $(this).hide();
	    $("#{id}-camera").show();
	    Webcam.reset();
	    Webcam.attach('#{id}-camera');
	});
});
</script>
""".replace('{id}', field.id) \
            .replace('{delete_url}', url_for('remove_snap', filename='')) \
            .replace('{upload_url}', url_for('save_snap', data='')) \
            .replace('{value}', (field.data or ''))
        html = html.replace('{render_kw}', html_params(**field.render_kw))
        return HTMLString(html)


class CaptureField(Field):
    widget = CaptureWiget()

    def __init__(self, label=None, validators=None, render_kw={}, **kwargs):
        super(CaptureField, self).__init__(label, validators, **kwargs)
        self.render_kw = render_kw or dict();

    def process_data(self, value):
        try:
            self.data = text_type(value)
        except (ValueError, TypeError):
            self.data = None

    def process_formdata(self, valuelist):
        if valuelist:
            self.data = text_type(valuelist[0])

    def pre_validate(self, form):
        pass
