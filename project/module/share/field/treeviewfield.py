from wtforms import widgets
from wtforms.fields import Field
from wtforms.validators import ValidationError
from wtforms.widgets import HTMLString,html_params
from wtforms.compat import text_type, string_types 
from flask import render_template_string

class TreeViewWiget(object):
    html_params = staticmethod(html_params)

    def __init__(self ): 
        pass

    def __call__(self, field, **kwargs): 
        if field.data==u'None':
            field.data=None

        html = """
{% macro render_tree_item(obj,get_child,get_pk,get_label,value) -%}
{% for parent in obj %}
        {% if parent[get_child] %}
            <li data-value='{% if disable_parent %}0{%else%}{{parent[get_pk]}}{%endif%}'>
                {{ _(parent[get_label]) }}
                <ol>
                    {{render_tree_item(parent[get_child],get_child,get_pk,get_label,value)}}
                </ol>
            </li>
        {% else %}
            <li data-value='{{ parent[get_pk] }}' {% if parent[get_pk] in value.split(",") %} data-checked="1" {% endif %}>{{ _(parent[get_label]) }}</li>
        {% endif %}
    {% endfor %}
{%- endmacro %}

<div class="tree-wrap default-checkbox"  id="{{id}}-wrap" {{render_kw}}>
    <input type="hidden" id="{{id}}" name="{{id}}" value="{{value}}" />
   {% if tree_obj %}
        <ol id="{{id}}-tree" data-name='{{id}}-tree'>
            {% if all_visible %}
                <li data-value='0'>{{ all_visible_text or _('ALL') }}
                    <ol>
                        {{render_tree_item(tree_obj,_(get_child),get_pk, _(get_label) ,_(value))}}
                    </ol>
                </li>
            {% else %}
                {{render_tree_item(tree_obj,_(get_child),get_pk,_(get_label),_(value) )}}
            {% endif %}
        </ol>
    {% endif %}
</div>
<script language="javascript">
    $(document).ready(function () {
        $('#{{id}}-tree').bonsai({
            expandAll: {% if expend_all %}true{% else %}false{%endif%},
            checkboxes: true, // depends on jquery.qubit plugin
            createInputs: 'checkbox' // takes values from data-name and data-value, and data-name is inherited
        });
        $("form").submit(function () {
            var trees = [];
            $('input[name="{{id}}-tree"]:checked').each(function () {
                trees.push($(this).val());
            });
            $("#{{id}}").val(trees.join());
        });
    });
</script>
"""
        #query = field.query().all()
        query = field.query
        if callable(query):
            query = query()
            if hasattr(query,'all'):
                query = query.all()

        html = render_template_string(HTMLString(html),
                                      id=field.id, 
                                      value=field.data or '',
                                      render_kw=html_params(**field.render_kw), 
                                      tree_obj=query,
                                      all_visible=field.all_visible,
                                      all_visible_text=field.all_visible_text,
                                      expend_all=field.expend_all,
                                      get_child=field.get_child,
                                      get_pk=field.get_pk,
                                      get_label=field.get_label,
                                      disable_parent=field.disable_parent)
        return HTMLString(html)
        

class TreeViewField(Field):
    widget = TreeViewWiget()
     
    def __init__(self, label=None,validators=None,render_kw={},query=None,get_child="childs",get_label="name",get_pk="id",all_visible=True,all_visible_text="ALL",expend_all=True,disable_parent=False,**kwargs):
        super(TreeViewField, self).__init__(label, validators, **kwargs)
        self.render_kw  = render_kw or dict()
        self.query = query
        self.all_visible = all_visible
        self.all_visible_text = all_visible_text
        self.expend_all = expend_all
        self.get_child = get_child
        self.get_label = get_label
        self.get_pk = get_pk
        self.disable_parent = disable_parent
        
    def process_data(self, value):
        try:
            self.data = text_type(value)
        except (ValueError, TypeError):
            self.data = None

    def process_formdata(self, valuelist): 
         if valuelist:
             self.data = text_type(valuelist[0]) 

    def pre_validate(self, form):
        pass
