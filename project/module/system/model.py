from project.module.share.model import *

class AutoNo(Base, TrackMixin, ExtMixin):
    __tablename__ = 'sys_sequence'
    __table_args__ = dict(extend_existing=True)
    id = Column(Key(), primary_key=True, default=gid)
    name = Column(String(50))
    format = Column(String(50))
    scope = Column(String(50))
    current = Column(Integer, default=0)
    dated = Column(DateTime, default=datetime.now, onupdate=datetime.now)



class Lookup(Base, TrackMixin, ExtMixin):
    __tablename__ = 'sys_lookup'
    id = Column(Key(), primary_key=True, default=gid)
    name = Column(String(50))
    note = Column(String(MAX))
    scope = Column(String(50))
    values = relationship('LookupValue', primaryjoin='foreign(Lookup.id) == remote(LookupValue.lookup_id)',
                          uselist=True, viewonly=True)


class LookupValue(Base, TrackMixin, ExtMixin):
    __tablename__ = 'sys_lookup_value'
    id = Column(Key(), primary_key=True, default=gid)
    name = Column(String(50))
    note = Column(String(MAX))
    value = Column(String(MAX))  # other value and view
    lookup_id = Column(Key(), index=True)
    is_system = Column(Boolean)
    scope = Column(String(50))
    lookup = relationship('Lookup', primaryjoin='foreign(LookupValue.lookup_id) == remote(Lookup.id)')


class Setting(Base, TrackMixin):
    __tablename__ = 'sys_setting'
    id = Column(Key(), primary_key=True, default=gid)
    name = Column(String(50))
    datatype = Column(String(50), default=u'text')
    value = Column(String(MAX))
    note = Column(String(MAX))
