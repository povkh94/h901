from edf.base.logic import *
from ..model import *


class SettingLogic(object):
    def __init__(self):
        pass

    def refresh(self):
        self._all = {s.name: s.value for s in db.query(Setting).all()}

    def get(self, name, default_value=None):
        s = db.query(Setting).filter(Setting.name == name).first()
        try:
            if s.datatype == 'bool':
                return str(s.value).lower() in ('1', 'yes', 'true')
            elif s.datatype == 'int':
                return int(s.value)
            elif s.datatype == 'json':
                return json.loads(s.value)
            else:
                return s.value
        except:
            return default_value

    def set(self, name, value):
        s = db.query(Setting).filter(Setting.name == name).first()
        if s:
            s.value = value
            db.commit()
        self.refresh()


default = SettingLogic()
