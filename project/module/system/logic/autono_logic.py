from edf.base.logic import *
from ..model import *

import random
import uuid

formaters = dict()
formaters['n'] = lambda v, *g: ('%0' + str(int(g[0])) + 'd') % v
formaters['rnd'] = lambda v, *g: str(random.random())[2:][:int(g[0])]
formaters['uid'] = lambda v, *g: str(uuid.uuid4()).replace('-', '')[:int(g[0])]
formaters['yy'] = lambda v, *g: datetime.now().strftime('%Y')[2:]
formaters['yyyy'] = lambda v, *g: datetime.now().strftime('%Y')
formaters['mm'] = lambda v, *g: datetime.now().strftime('%m')[2:]


def render(format, value):
    """
    format : INV{n:5}-{rnd:2}-{chk:2}-{uid:16}-{yyyy}{mm}
    """
    t = []
    for f in format.replace('}', '{').split('{'):
        if f:
            fa = (f + ':6').split(':')
            fn = formaters.get(fa[0], lambda v, *g: fa[0])
            args = fa[1].split(',')
            f = fn(value, *args)
            t.append(f)
    return ''.join(t)


def init_enum(enum):
    lookup = Lookup(id=enum.__val__, name=enum.__name__)
    lookup.ext__is_system = True
    lookup.ext__enable_add = True
    lookup.ext__enable_duplicate_value = True
    db.add(lookup)
    for i, (k, v) in enumerate(enum.get_members().items()):
        db.add(LookupValue(lookup_id=enum.__val__, is_system=True, name=k, value=v))
    db.commit()


class AutoNoLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = AutoNo

    def current_scope(self):
        return None

    def search(self, **kwargs):
        search = kwargs.get('search', '')
        scope = self.current_scope()
        q = (db.query(AutoNo)
             .filter(AutoNo.is_active)
             .filter(AutoNo.scope == scope)
             .filter(AutoNo.name.like('%' + search + '%'))
             )
        return q

    def find(self, id=0, name=''):
        obj = None
        if id:
            obj = db.query(AutoNo).filter(AutoNo.id == id).first()
        if name:
            obj = db.query(AutoNo).filter(AutoNo.name == name).first()
        return obj

    def add_new(self, name, format='{n:4}', scope=''):
        obj = AutoNo(name=name, format=format, scope=scope)
        db.add(obj)
        db.flush()
        return obj

    def next_no(self, id=0, name='', update=False):
        if update:
            db.begin(subtransactions=True)
        obj = self.find(id, name)
        if not obj:
            return ''
        code = self.render(obj)
        if update:
            obj.dated = datetime.now()
            obj.current = obj.current + 1
            db.commit()
        return code

    def render(self, obj):
        current = (obj.current or 0) + 1
        code = render(obj.format, current)
        return code


default = AutoNoLogic()
