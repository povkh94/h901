﻿from edf.base.logic import *
from ..model import *


class LookupLogic(LogicBase):
    def __init__(self):
        self.__classname__ = Lookup

    def get_values(self, lookup_id=0, lookup_name='',value='', lookupvalue_id=''):
        if lookup_id:
            if hasattr(lookup_id, '__val__'):
                lookup_id = lookup_id.__val__
            q = (db.query(LookupValue)
                 .filter(or_(LookupValue.lookup_id == lookup_id, LookupValue.lookup_id == 'none'))
                 .filter(LookupValue.is_active == True)
                 )

            return q

        if lookupvalue_id:
            q = (db.query(LookupValue)
                 .filter(LookupValue.id == lookupvalue_id))
            return q

        if lookup_name:
            lk = db.query(Lookup).filter(Lookup.name == lookup_name).first()
            if lk:
                q = (db.query(LookupValue)
                     .filter(LookupValue.lookup_id == lk.id)
                     .filter(LookupValue.is_active == True))
                return q
        if value:
            q = db.query(LookupValue).filter(LookupValue.value == value, LookupValue.is_active)
            return q


    def get_value(self, lookup_id, value_name):
        if hasattr(lookup_id, '__val__'):
            lookup_id = lookup_id.__val__
        v = (db.query(LookupValue)
             .filter(LookupValue.lookup_id == lookup_id)
             .filter(LookupValue.name == value_name)
             .filter(LookupValue.is_active == True)
             ).first()
        return v

    def add_enum(self, enum):
        lookup = Lookup(id=enum.__val__, name=enum.__name__)
        lookup.ext__is_system = True
        lookup.ext__enable_add = False
        lookup.ext__enable_duplicate_value = False
        db.add(lookup)
        for i, (k, v) in enumerate(enum.get_members().items()):
            db.add(LookupValue(lookup_id=enum.__val__, is_system=True, name=k, value=v))
        db.commit()

default = LookupLogic()
