__info__ = {
    "name": "system",
    "version" : "1.0.0",
    "title": "System Module",
    "summary": "Basic System Module for Sequence, Component, Setting etc.",
    "author": "RY Rith",
    "website": "http://project.com",
    "depends": ['share']
}


def activate(**kwargs):
    import model


def install(**kwargs):
    pass


def uninstall(**kwargs):
    pass
