from flask_babel import lazy_gettext as _
from ...share.view.base_view import *
from ..model import *
from ..logic import currency_logic


class CurrencyForm(FlaskForm):
    id = HiddenField()
    code = TextField(
        _('Code'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    name = TextField(
        _('Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    sign = TextField(
        _('Sign'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    round_digit = TextField(
        _('Rounding Digit'),
        render_kw={
            'data-val': 'true',
            'data-required': _('Input Required'),
            'required': 'required'
        }
    )

class CurrencyTable(Table):
    rowno = RowNumberColumn(_('No.'))
    code = DataColumn(_('Code'))
    name = DataColumn(_('Name'))
    sign = DataColumn(_('Sign'))
    round_digit = DecimalColumn(_('Rounding Digit'))
    action = ActionColumn('', endpoint='.CurrencyView')


class CurrencyView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Currency
        self.v_logic = currency_logic.default
        self.v_table_index_class = CurrencyTable
        self.v_form_add_class = CurrencyForm
        self.v_template = 'crud'

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _('Currency')


CurrencyView.register(app)
