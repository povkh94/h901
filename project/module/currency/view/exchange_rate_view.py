from flask_babel import lazy_gettext as _
from ...share.view.base_view import *
from ..model import *
from ..logic import currency_logic
from ..logic import exchange_rate_logic


class ExchangeRateForm(FlaskForm):
    id = HiddenField()
    currency_id = DynamicSelectField(
        _('Currency'),
        query_factory=currency_logic.default.search,
        get_pk='id',
        get_label='code',
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    based_currency_id = DynamicSelectField(
        _('Based'),
        query_factory=currency_logic.default.search,
        get_pk='id',
        get_label='code',
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    record_date = DateField(
        _('Record Date'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    exchange_rate = TextField(
        _('Exchange Rate'),
        render_kw={
            'data-val': 'true',
            'data-required': _('Input Required'),
            'required': 'required'
        }
    )

class ExchangeRateTable(Table):
    rowno = RowNumberColumn(_('No.'))
    record_date = DateColumn(_('Date'))
    currency_id = LamdaColumn(_('Currency'), get_value=lambda x:currency_logic.default.find(x.currency_id).code)
    exchange_rate = DecimalColumn(_('Exchange Rate'))
    based_currency_id = LamdaColumn(_('Based'), get_value=lambda x: currency_logic.default.find(x.based_currency_id).code)
    action = ActionColumn('', endpoint='.ExchangeRateView')


class ExchangeRateView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = ExchangeRate
        self.v_logic = exchange_rate_logic.default
        self.v_table_index_class = ExchangeRateTable
        self.v_form_add_class = ExchangeRateForm
        self.v_template = 'crud'

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _('Exchange Rate')


ExchangeRateView.register(app)
