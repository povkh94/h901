from ..share.model import *


class Currency(Base, TrackMixin):
    __tablename__ = 'acc_currency'
    id = c.PkColumn()
    code = c.CodeColumn()
    name = c.TitleColumn()
    sign = c.CodeColumn()
    round_digit = c.DecimalColumn()
    exchange_rate = c.DecimalColumn()


class ExchangeRate(Base, TrackMixin):
    __tablename__ = 'acc_currency_rate'
    id = c.PkColumn()
    currency_id = c.FkColumn()
    based_currency_id = c.FkColumn()
    exchange_rate = c.DecimalColumn()
    record_date = c.DecimalColumn()
