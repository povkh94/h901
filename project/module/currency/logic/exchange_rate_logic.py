from ..model import *
from edf.base.logic import *


class ExchangeRateLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = ExchangeRate


default = ExchangeRateLogic()
