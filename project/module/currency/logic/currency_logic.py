from ..model import *
from edf.base.logic import *


class CurrencyLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = Currency


default = CurrencyLogic()
