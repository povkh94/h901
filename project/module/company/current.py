from edf.base.model import current
from .logic import company_logic


@current.promote('company_id')
def current_company_id():
    user = current.user()
    if user:
        return user.ext__current_scope
    return None


@current.promote('company')
def current_company():
    cid = current.company_id()
    if cid:
        company = company_logic.default.find(id=cid)
        return company
    return None


@current.promote('scope')
def current_scope():
    return current.company_id()
