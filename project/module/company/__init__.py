__info__ = {
    "name": "company",
    "title": "Company Module",
    "summary": "System Module for Sequence, Component, Role, Permission, Setting etc.",
    "author": "RY Rith",
    "website": "http://project.com",
    "depends": ['share']
}


def activate(**kwargs):
    from .import current
    from project.module.company.hook import system_hook


def install(**kwargs):
    pass


def uninstall(**kwargs):
    pass
