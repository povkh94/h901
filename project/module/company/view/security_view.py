from ...share.view.base_view import *
from flask_babel import lazy_gettext as _

class SignupForm(Form):
    ext__company_name = TextField(
        _('Company Name'),
        render_kw={
            'required': 'required',
            'data-val': 'true',
            'data-val-required': _('Input Required')
        }
    )
    ext__company_address = TextAreaField(
        _('Address'), 
        render_kw={
            'required': 'required',
            "data-val": "true",
            "data-val-required": _("This field is required.")
        }
    )
    name = TextField(
        _('Name'),
        render_kw={
            'data-val': 'true',
            'required': 'required',
            'data-val-required': _('Input Required')
        }
    )
    full_name = TextField(
        _('Full Name'),
        render_kw={
            'required': 'required',
            'data-val': 'true',
            'data-val-required': _('Input Required')
        }
    )
    email = TextField(
        _('Email'),
        render_kw={
            'required': 'required',
            'data-val': 'true',
            'data-val-required': _('Input Required'),
            'data-val-remote': _('Email already exist'),
            'data-val-remote-additionalfields': 'email',
            'data-val-remote-url': 'verify_email'
        },
        validators=[Email()]
    )
    phone = TextField(
        _('Phone'),
        render_kw={
            'required': 'required',
            'data-val': 'true',
            'data-val-required': _('Input Required')
        }
    )
    password = PasswordField(
        _('Password'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _('Input Required')
        }
    )
    confirm_password = PasswordField(
        _('Confirm Password'),
        render_kw={
            'required': 'required',
            'data-val': 'true',
            'data-val-required': _('Input Required'),
            'data-val-remote': _('Password Incorrect'),
            'data-val-remote-additionalfields': 'password,confirm_password',
            'data-val-remote-url': 'confirm_password'
        }
    )
