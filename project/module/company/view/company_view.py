from edf_admin.form.dynamicselectfield import DynamicSelectField
from edf_admin.form.uploadfield import UploadImageField, UploadField
from edf_admin.table import Table
from flask import url_for, session
from flask_classy import route
from flask_wtf import FlaskForm
from wtforms import TextField, HiddenField, TextAreaField, SelectField
from flask_babel import lazy_gettext as _

from project.admin.application import app
from project.module.company import current
from project.module.company.logic import company_logic
from project.module.company.model import Company
from project.module.share.tinymcefield import TinyMCEField
from project.module.share.view.crud_view import CRUDView


class CompanyForm(FlaskForm):
    id = HiddenField()
    name = TextField(
        _("Company Name"),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    ext__title = TextField(
        _("Title"),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    phone = TextField(
        _("Phone"),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    contact_name = TextField(
        _("Contact Name"),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    # address = TextAreaField(
    #     _(u'Address'),
    #     render_kw={
    #         'rows': 5,
    #         'data-val': 'true',
    #         'clear_top': True,
    #         'is_half_width': 'False',
    #         'style': 'resize:none;'
    #     }
    # )
    address = TinyMCEField(
        _(u'Footer'),
        render_kw={
            'is_half_width': 'False',
            'data-val-required': _(u'Input Required')
        },
        height="200px"

    )

    #

    biology = TinyMCEField(
        _(u'Biology'),
        render_kw={
            'data-val': 'true',
            'is_half_width': 'False',
            'data-val-required': _(u'Input Required')
        },
        height="300px"

    )

    logo = UploadImageField(_(u'Photo'), multiple=False)
    ext__photo = UploadImageField(_(u'Photo'), multiple=True)

    ext__files = UploadField(_(u'Files'), multiple=True)
    ext__license_number = TextField(u'License Number')
    ext__access_key = TextField(u'Access Key')

    ext_data = HiddenField()

    ext__invoice_footer = TinyMCEField(
        _(u'Footer'),
        render_kw={
            'is_half_width': 'False',
            'data-val-required': _(u'Input Required')
        },
        height="200px"

    )
    ext__receipt_title = TextField(
        _("Title"),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    ext__receipt_footer = TinyMCEField(
        _(u'Footer'),
        render_kw={
            'is_half_width': 'False',
            'data-val-required': _(u'Input Required')
        },
        height="200px"

    )

    ext__farmer_seq = SelectField(
        _(u'Farmer Code'),
        choices=[('None', _(u'Non Auto')), ('auto', _(u'Auto'))]
    )
    ext__land_seq = SelectField(
        _(u'Land  Code'),
        choices=[('None', _(u'Non Auto')), ('auto', _(u'Auto'))]
    )


class CompanyTable(Table):
    pass


class CompanyView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Company
        self.v_logic = company_logic.default
        self.v_table_index_class = CompanyTable
        self.v_form_add_class = CompanyForm
        self.v_template = 'company'
        self.v_logic_order_by = ''

    def _on_index_rendering(self, *args, **kwargs):
        company = company_logic.default.find(id=current.current_company_id())
        self.v_data['form'] = CompanyForm()
        self.v_data['form'].id.data = company.id
        self.v_data['form'].name.data = company.name
        self.v_data['form'].ext__title.data = company.ext__title
        self.v_data['form'].ext__license_number.data = company.ext__license_number
        self.v_data['form'].ext__access_key.data = company.ext__access_key
        self.v_data['form'].phone.data = company.phone
        self.v_data['form'].contact_name.data = company.contact_name
        self.v_data['form'].address.data = company.address
        self.v_data['form'].logo.data = company.logo
        self.v_data['form'].ext__photo.data = company.ext__photo
        self.v_data['form'].biology.data = company.biology
        self.v_data['biology'] = company.biology
        self.v_data['title'] = _('company')
        self.v_data['form'].ext__invoice_footer.data = company.ext__invoice_footer
        self.v_data['form'].ext__receipt_title.data = company.ext__receipt_title
        self.v_data['form'].ext__receipt_footer.data = company.ext__receipt_footer
        self.v_data['form'].ext__farmer_seq.data = company.ext__farmer_seq
        self.v_data['form'].ext__land_seq.data = company.ext__land_seq
        self.v_data['invoice_footer'] = company.ext__invoice_footer
        self.v_data['receipt_footer'] = company.ext__receipt_footer

    def _on_edited(self, *args, **kwargs):
        self.v_redirect_fn = lambda: url_for('.HomeView:home')


CompanyView.register(app)
