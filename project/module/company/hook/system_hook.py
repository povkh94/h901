from edf.helper.dictobj import copyupdate

from project.module.system_security.logic import role_logic

try:
    from ...system_security.logic import user_logic
    from ...system_security.logic.user_logic import UserLogic, User, UserLink, Role
    from ...system_security.view import security_view
    from ..view.security_view import SignupForm as NewSignupForm
    from ..logic.company_logic import CompanyLogic, Company
    from edf.base.database import db

    # system_signup = UserLogic.signup
    user_logic.default.db_commit()

    @user_logic.default.on('added')
    def customer_signup_hook(obj,**kwargs):
        """
        :type self: UserLogic
        :type obj: User
        :rtype : User
        """
        # add company
        user = obj
        company_id = None
        if not user.ext__current_scope:
            company = Company()
            company.name = user.ext__company_name
            company.address = user.ext__company_address
            #company.phone = user.phone
            # company.email = user.email

            company_lg = CompanyLogic()
            company_lg.add(company)
            company_id = company.id
        else:
            company_id = user.ext__current_scope
        # add user link with company
        user_link = UserLink()
        user_link.user_id = user.id
        user_link.link_type = 'user-company'
        user_link.link_id = company_id
        db.add(user_link)
        db.flush()

        # add role for user
        role_id = None
        if not user.ext__roles or user.ext__roles =='1':
            default_admin_role = role_logic.default.find('1')
            admin = Role()
            copyupdate(default_admin_role, admin)
            admin.scope = company_id
            db.add(admin)
            db.flush()
            # assign role to user
            role_logic.default.assign_role(obj.id, admin.id)
            role_id = admin.id
        else:
            role_id = user.ext__roles

        user.ext__current_scope = company_id
        user.ext__roles = role_id
        db.commit()
        return user


    # override system_security view & logic
    security_view.SignupForm = NewSignupForm
    # UserLogic.signup = customer_signup_hook
    # user_logic.default = UserLogic()

except ImportError:
    pass
