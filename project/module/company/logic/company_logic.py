from edf.base.logic import *

from ..model import *
from ...system.logic import autono_logic
from ...system.logic import setting_logic
from ...system_security.logic import user_link_logic
from ...system_security.logic import user_logic


class CompanyLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = Company

    def default_ext(self):
        ext = dict()
        ext['facebook_page_id'] = ''
        ext['facebook_page_name'] = ''
        ext['facebook_page_token'] = ''
        ext['seq'] = setting_logic.default.get('default_sequence', dict())
        ext['default_currency_id'] = setting_logic.default.get('default_currency_id', 1)  # USD
        ext['calendar_id'] = setting_logic.default.get('calendar_id', 1)  # Cambodia Calendar
        return ext

    def search(self, **kwargs):
        search = kwargs.get('search') or ''
        user_id = current.user().id
        q = self.actives()
        if search:
            q = q.filter(func.lower(Company.name).like('%' + search.lower() + '%'))
        if user_id:
            available_company_ids = user_link_logic.default.get_avaiable_link_ids(link_type='user_company')
        q = q.filter(Company.id.in_(available_company_ids))  # in_() filter more than one value
        return q

    def add(self, obj):
        obj.ext_data
        obj.id = gid()
        if not obj.ext_data:
            obj.ext = self.default_ext()
        for k, v in obj.ext['seq'].iteritems():
            if v:
                seq = autono_logic.default.add_new(name=u'{s}-{t}'.format(s=obj.name, t=k),
                                                   scope=obj.id, format=v)
                obj.ext['seq'][k] = seq.id

        obj.ext_data = json.dumps(obj.ext)
        return super(CompanyLogic, self).add(obj)

    def current(self, user_id=0):
        user = current.user()
        if user_id:
            user = user_logic.default.find(user_id)
        cid = user.ext__current_scope #ext__current_company_id
        c = self.find(cid)
        return c

    def find_root_company_id(self, company_id):
        company = self.find(id=company_id)
        return company.parent_id if company and company.parent_id else company_id


default = CompanyLogic()
