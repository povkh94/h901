﻿from edf.base.logic import *
from ..model import *


class CompanyTypeLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = CompanyType


default = CompanyTypeLogic()
