from ..share.model import *
from edf.helper.sequence import randomstring


class CompanyType(Base, TrackMixin):
    __tablename__ = 'com_company_type'
    id = c.PkColumn()
    name = c.TitleColumn()


class Company(Base, TrackMixin, ExtMixin):
    __tablename__ = 'com_company'
    id = c.PkColumn()
    name = c.TitleColumn()
    phone = c.TitleColumn()
    contact_name = c.TitleColumn()
    biology = c.TextColumn()
    address = c.NoteColumn()
    logo = c.NoteColumn()
    parent_id = c.FkColumn()
    company_type_id = c.FkColumn()

    company_type = relationship('CompanyType', primaryjoin='foreign(Company.company_type_id) == remote(CompanyType.id)')
    parent = relationship('Company', primaryjoin='foreign(Company.parent_id) == remote(Company.id)')

