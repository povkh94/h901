from sqlalchemy import cast, extract, distinct, case, literal

from project.core.database import db
from project.module.agri_bill import enum
from project.module.currency.model import Currency
from project.module.payment.model import Billing, BillingStation, Invoice, Station, Tariff, InvoiceDetail, Block, \
    Location, Farmer, Land, BillingLand, BillingCycle
from edf.base.logic import *

from project.module.share.app_filter import to_riel


class ReportBillingLogic(LogicBase):
    def __init__(self):
        self.__classname__ = Billing

    def report_billing_by(self, **kwargs):
        d1 = kwargs.get('d1') or datetime.now().date()
        d2 = kwargs.get('d2') or datetime.now().date().replace(day=1, year=datetime.now().year + 1) - timedelta(days=1)
        season_id = kwargs.get('season_id') or ''
        q = (
            db.query(
                func.row_number().over(order_by=Billing.name).label(
                    '_row_number'),
                Billing.id.label('id'),
                Billing.name.label('name'),
                func.sum(Invoice.total_amount).label('total_cost'),
                func.sum(Invoice.paid_amount).label('paid_amount'),
                Invoice.currency_id.label('currency'),

            )
                .join(Invoice, Invoice.billing_id == Billing.id)
                .filter(Billing.is_active)
                .filter(Billing.start_date >= d1)
                .filter(Billing.start_date <= d2)
                .filter(Billing.billing_status_id == 'close')
                .filter(Invoice.is_active, Invoice.status_id != 'void')
                .group_by(Billing.id, Billing.name, Invoice.currency_id)
        )
        if season_id:
            q = q.filter(Billing.season_id == season_id)
        q = q.subquery()
        result = (
            db.query(
                q.c._row_number,
                q.c.id,
                q.c.name,
                q.c.total_cost,
                q.c.currency,
                func.sum(case([((q.c.total_cost - q.c.total_paid) <= 0, 0,)],
                              else_=(q.c.total_cost - q.c.total_paid))).label('ending_balance'),
            ).group_by(q.c._row_number, q.c.id, q.c.name, q.c.total_cost, q.c.currency)
        )
        return result

    # By block
    def report_billing(self, **kwargs):
        billing_id = kwargs.get('billing_id') or ''
        station_id = kwargs.get('station_id') or ''
        block_id = kwargs.get('block_id') or ''
        village_id = kwargs.get('village_id') or ''
        q = (
            db.query(
                func.row_number().over(order_by=(Station.code, Block.code), partition_by=Billing.name).label(
                    '_row_number'),
                Billing.id.label('billing_id'),
                Billing.name.label('name'),
                Billing.start_date.label('start_date'),
                Billing.end_date.label('end_date'),
                Station.code.label('station_code'),
                Station.name.label('station_name'),
                Block.name.label('block_name'),
                Block.code.label('block_code'),
                func.count(distinct(Invoice.farmer_id)).label('total_farmer'),
                func.sum(Invoice.total_lands).label('total_land'),
                func.sum(Invoice.total_area).label('total_area'),
                func.count(Invoice.id).label('total_invoice'),
                func.sum(Invoice.total_amount).label('total_cost'),
                func.count(distinct(case([
                    (Invoice.paid_amount != 0, Invoice.farmer_id)
                ], else_=literal(None)))).label('total_farmer_paid'),
                func.sum(case([
                    (and_(Invoice.paid_amount > Invoice.total_amount, Invoice.is_active), Invoice.total_amount)],
                    else_=Invoice.paid_amount)).label('total_paid'),
                func.sum(case([
                    (Invoice.paid_amount > Invoice.total_amount, Invoice.paid_amount - Invoice.total_amount)],
                    else_=0)).label('total_exceed_paid'),
                # func.sum(Invoice.paid_amount).label('total_paid'),
                Invoice.currency_id.label('currency'),
                Currency.sign.label('currency_sign'),

            ).join(Invoice, Invoice.billing_id == Billing.id)
                .join(Farmer, Farmer.id == Invoice.farmer_id)
                .join(Station, Station.id == Invoice.station_id)
                .join(Block, Block.id == Invoice.block_id)
                .join(Currency, Currency.id == Invoice.currency_id)
                .filter(Invoice.total_amount != 0)
                # .filter(Invoice.invoice_type != enum.InvoiceType.PENALTY)
                # .filter(Billing.is_active, Invoice.is_active, Invoice.status_id != 'void', Block.is_active)
                .filter(Billing.is_active, Invoice.is_active, Invoice.status_id != 'void')
                .filter(Billing.billing_status_id == 'close')
        )
        if billing_id:
            q = q.filter(Billing.id == billing_id)
        if station_id:
            q = q.filter(Station.id == station_id)
        if block_id:
            q = q.filter(Block.id == block_id)
        if village_id:
            q = q.filter(Invoice.village_id == village_id)
        q = (
            q.group_by(Billing.id, Billing.name, Station.code, Station.name, Block.name, Block.code,
                       Invoice.currency_id,
                       Billing.start_date, Billing.end_date, Currency.sign)
                .order_by(Billing.name, Station.code)
        )
        q = q.subquery()
        query = (db.query(
            q.c._row_number,
            q.c.billing_id,
            q.c.name,
            q.c.start_date,
            q.c.end_date,
            q.c.station_code,
            q.c.station_name,
            q.c.block_name,
            q.c.block_code,
            q.c.total_farmer,
            q.c.total_farmer_paid,
            q.c.total_land,
            q.c.total_area,
            q.c.total_invoice,
            q.c.total_exceed_paid,
            q.c.total_cost,
            q.c.total_paid,
            q.c.currency,
            q.c.currency_sign,
            func.sum(q.c.total_cost - q.c.total_paid).label('ending_balance'),
            # func.sum(
            #     case([((q.c.total_cost - q.c.total_paid) <= 0, 0,)], else_=(q.c.total_cost - q.c.total_paid))).label(
            #     'ending_balance'),
            func.sum((q.c.total_paid * 100) / q.c.total_cost).label('percent_paid'),
            func.sum((q.c.total_farmer_paid * 100) / q.c.total_farmer).label('percent_farmer_paid')

        ).group_by(q.c._row_number,
                   q.c.billing_id,
                   q.c.name,
                   q.c.start_date,
                   q.c.end_date,
                   q.c.station_code,
                   q.c.station_name,
                   q.c.block_name,
                   q.c.block_code,
                   q.c.total_farmer,
                   q.c.total_farmer_paid,
                   q.c.total_land,
                   q.c.total_area,
                   q.c.total_invoice,
                   q.c.total_cost,
                   q.c.total_paid,
                   q.c.total_exceed_paid,
                   q.c.currency,
                   q.c.currency_sign)
                 ).order_by(q.c.start_date.desc())
        return query

    # By Station
    def report_billing_by_station(self, **kwargs):
        d1 = kwargs.get('d1') or datetime.now().date()
        d2 = kwargs.get('d2') or datetime.now().date().replace(day=1, year=datetime.now().year + 1) - timedelta(
            days=1)
        billing_id = kwargs.get('billing_id') or ''
        station_id = kwargs.get('station_id') or ''
        block_id = kwargs.get('block_id') or ''
        village_id = kwargs.get('village_id') or ''
        q = (
            db.query(
                func.row_number().over(order_by=(Station.code), partition_by=Billing.name).label(
                    '_row_number'),
                Billing.id.label('billing_id'),
                Billing.name.label('name'),
                Billing.start_date.label('start_date'),
                Billing.end_date.label('end_date'),
                Station.code.label('station_code'),
                Station.name.label('station_name'),
                func.count(distinct(Invoice.farmer_id)).label('total_farmer'),
                func.sum(Invoice.total_lands).label('total_land'),
                func.sum(Invoice.total_area).label('total_area'),
                func.sum(Invoice.total_amount).label('total_cost'),
                func.count(Invoice.id).label('total_invoice'),
                func.count(distinct(case([
                    (Invoice.paid_amount != 0, Invoice.farmer_id)
                ], else_=literal(None)))).label('total_farmer_paid'),
                func.sum(case([
                    (Invoice.paid_amount > Invoice.total_amount, Invoice.total_amount)],
                    else_=Invoice.paid_amount)).label('total_paid'),
                func.sum(case([
                    (Invoice.paid_amount > Invoice.total_amount, Invoice.paid_amount - Invoice.total_amount)],
                    else_=0)).label('total_exceed_paid'),
                Invoice.currency_id.label('currency'),
                Currency.sign.label('currency_sign'),

            ).join(Invoice, Invoice.billing_id == Billing.id)
                .join(Farmer, Farmer.id == Invoice.farmer_id)
                .join(Station, Station.id == Invoice.station_id)
                .join(Block, Block.id == Invoice.block_id)
                .join(Currency, Currency.id == Invoice.currency_id)
                .filter(Invoice.total_amount != 0)
                # .filter(Invoice.invoice_type != enum.InvoiceType.PENALTY)
                .filter(
                Billing.is_active,
                Invoice.is_active,
             Invoice.status_id != 'void',
                Block.is_active)
                .filter(Billing.billing_status_id == 'close')

        )
        if billing_id:
            q = q.filter(Billing.id == billing_id)
        if station_id:
            q = q.filter(Station.id == station_id)
        if block_id:
            q = q.filter(Block.id == block_id)
        if village_id:
            q = q.filter(Invoice.village_id == village_id)
        q = (q.group_by(Billing.id, Billing.name, Station.code, Station.name,
                        Invoice.currency_id,
                        Billing.start_date, Billing.end_date, Currency.sign)
             .order_by(Billing.id, Billing.name, Station.code, Station.name,
                       Invoice.currency_id,
                       Billing.start_date, Billing.end_date, Currency.sign))
        q = q.subquery()
        query = (db.query(
            q.c._row_number,
            q.c.billing_id,
            q.c.name,
            q.c.start_date,
            q.c.end_date,
            q.c.station_code,
            q.c.station_name,
            q.c.total_farmer,
            q.c.total_farmer_paid,
            q.c.total_land,
            q.c.total_area,
            q.c.total_invoice,
            q.c.total_cost,
            q.c.total_paid,
            q.c.total_exceed_paid,
            q.c.currency,
            q.c.currency_sign,
            func.sum(q.c.total_cost - q.c.total_paid).label('ending_balance'),
            func.sum((q.c.total_paid * 100) / q.c.total_cost).label('percent_paid'),
            func.sum((q.c.total_farmer_paid * 100) / q.c.total_farmer).label('percent_farmer_paid'),

        ).group_by(q.c._row_number,
                   q.c.billing_id,
                   q.c.name,
                   q.c.start_date,
                   q.c.end_date,
                   q.c.station_code,
                   q.c.station_name,
                   q.c.total_farmer,
                   q.c.total_farmer_paid,
                   q.c.total_land,
                   q.c.total_area,
                   q.c.total_invoice,
                   q.c.total_cost,
                   q.c.total_paid,
                   q.c.total_exceed_paid,
                   q.c.currency,
                   q.c.currency_sign)
                 ).order_by(q.c.start_date.desc())
        return query

    def report_billing_by_tariff(self, **kwargs):
        billing_id = kwargs.get('billing_id') or ''
        station_id = kwargs.get('station_id') or ''
        block_id = kwargs.get('block_id') or ''
        village_id = kwargs.get('village_id') or ''
        q = (
            db.query(
                func.row_number().over(order_by=(Billing.name), partition_by=Billing.name).label(
                    '_row_number'),
                Billing.id.label('billing_id'),
                Billing.name.label('name'),
                Billing.start_date.label('start_date'),
                Billing.end_date.label('end_date'),
                Invoice.id.label('invoice_id'),
                Currency.id.label('currency'),
                Currency.sign.label('currency_sign'),
                func.sum(case([
                    (Invoice.paid_amount > Invoice.total_amount, Invoice.total_amount)],
                    else_=Invoice.paid_amount)).label('total_paid'),
                func.sum(case([
                    (Invoice.paid_amount > Invoice.total_amount, Invoice.paid_amount - Invoice.total_amount)],
                    else_=0)).label('total_exceed_paid'),
            ).join(Invoice, Invoice.billing_id == Billing.id)
                .join(Station, Station.id == Invoice.station_id)
                .join(Block, Block.id == Invoice.block_id)
                .join(Currency, Currency.id == Invoice.currency_id)
                .filter(Invoice.total_amount != 0)
                # .filter(Invoice.invoice_type != enum.InvoiceType.PENALTY)
                .filter(Invoice.status_id != enum.InvoiceStatus.void)
                .filter(Billing.is_active, Invoice.is_active)
                .filter(Billing.billing_status_id == 'close')

        )
        if billing_id:
            q = q.filter(Invoice.billing_id == billing_id)
        if station_id:
            q = q.filter(Station.id == station_id)
        if block_id:
            q = q.filter(Block.id == block_id)
        if village_id:
            q = q.filter(Invoice.village_id == village_id)

        q = q.group_by(Billing.id,
                       Billing.name,
                       Billing.start_date,
                       Billing.end_date,
                       Invoice.id,
                       Currency.id,
                       Currency.sign)

        q = q.subquery()

        query = (db.query(

            q.c.billing_id,
            q.c.name,
            q.c.start_date,
            q.c.end_date,
            q.c.currency,
            q.c.currency_sign,
            func.row_number().over(order_by=Tariff.name).label(
                '_row_number'),
            InvoiceDetail.tariff_id,
            Tariff.id,
            Tariff.name.label('tariff_name'),
            func.sum(InvoiceDetail.total_land).label('total_land'),
            func.sum(InvoiceDetail.quantity).label('total_area'),
            func.sum(InvoiceDetail.line_total).label('total_cost'),

        ).join(InvoiceDetail, InvoiceDetail.invoice_id == q.c.invoice_id)
            .join(Tariff, Tariff.id == InvoiceDetail.tariff_id)
            .group_by(
            q.c.billing_id,
            q.c.name,
            q.c.start_date,
            q.c.end_date,
            q.c.currency,
            q.c.currency_sign,
            InvoiceDetail.tariff_id,
            Tariff.id, Tariff.name)
        ).order_by(Tariff.name)
        return query

    def report_billing_by_village(self, **kwargs):
        billing_id = kwargs.get('billing_id') or ''
        station_id = kwargs.get('station_id') or ''
        block_id = kwargs.get('block_id') or ''
        village_id = kwargs.get('village_id') or ''
        q = (
            db.query(
                func.row_number().over(order_by=(Location.name), partition_by=Billing.name).label(
                    '_row_number'),
                Billing.id.label('billing_id'),
                Billing.name.label('name'),
                Billing.start_date.label('start_date'),
                Billing.end_date.label('end_date'),
                func.count(distinct(Invoice.farmer_id)).label('total_farmer'),
                func.sum(Invoice.total_lands).label('total_land'),
                func.sum(Invoice.total_area).label('total_area'),
                func.count(Invoice.id).label('total_invoice'),
                func.sum(Invoice.total_amount).label('total_cost'),
                Invoice.currency_id.label('currency'),
                Currency.sign.label('currency_sign'),
                Location.id.label('village_id'),
                Location.name.label('village_name'),
                Location.parent_id.label('commune_id'),
                func.count(distinct(case([
                    (Invoice.paid_amount != 0, Invoice.farmer_id)
                ], else_=literal(None)))).label('total_farmer_paid'),
                func.sum(case([
                    (Invoice.paid_amount > Invoice.total_amount, Invoice.paid_amount - Invoice.total_amount)],
                    else_=0)).label('total_exceed_paid'),
                func.sum(case([
                    (Invoice.paid_amount > Invoice.total_amount, Invoice.total_amount)],
                    else_=Invoice.paid_amount)).label('total_paid'),

            ).join(Invoice, Invoice.billing_id == Billing.id)
                .join(Farmer, Farmer.id == Invoice.farmer_id)
                .join(Station, Station.id == Invoice.station_id)
                .join(Block, Block.id == Invoice.block_id)
                .join(Currency, Currency.id == Invoice.currency_id)
                .join(Location, Location.id == Invoice.village_id)
                .filter(Invoice.total_amount != 0)
                # .filter(Invoice.invoice_type != enum.InvoiceType.PENALTY)
                .filter(Invoice.status_id != enum.InvoiceStatus.void)
                .filter(Billing.is_active, Invoice.is_active, Block.is_active)
                .filter(Billing.billing_status_id == 'close')

        )
        if billing_id:
            q = q.filter(Billing.id == billing_id)
        if station_id:
            q = q.filter(Station.id == station_id)
        if block_id:
            q = q.filter(Block.id == block_id)
        if village_id:
            q = q.filter(Invoice.village_id == village_id)
        q = (q.group_by(Billing.id, Billing.name, Location.id, Location.name, Location.parent_id,
                        Invoice.currency_id,
                        Billing.start_date, Billing.end_date, Currency.sign)
             .order_by(Billing.id, Billing.name, Location.id, Location.name, Location.parent_id,
                       Invoice.currency_id,
                       Billing.start_date, Billing.end_date, Currency.sign))
        q = q.subquery()
        query = (db.query(
            q.c._row_number,
            q.c.billing_id,
            q.c.name,
            q.c.start_date,
            q.c.end_date,
            q.c.total_farmer,
            q.c.total_land,
            q.c.total_area,
            q.c.total_invoice,
            q.c.total_cost,
            q.c.total_paid,
            q.c.currency,
            q.c.currency_sign,
            q.c.village_id,
            q.c.village_name,
            q.c.commune_id,
            q.c.total_farmer_paid,
            q.c.total_exceed_paid,
            func.sum(q.c.total_cost - q.c.total_paid).label('ending_balance'),
            # func.sum(case([((q.c.total_cost - q.c.total_paid) <= 0, 0,)], else_=(q.c.total_cost - q.c.total_paid))).label('ending_balance'),
            func.sum((q.c.total_paid * 100) / q.c.total_cost).label('percent_paid'),
            Location.name.label('commune_name'),
            func.sum((q.c.total_farmer_paid * 100) / q.c.total_farmer).label('percent_farmer_paid')
        ).join(Location, Location.id == q.c.commune_id)
                 .group_by(q.c._row_number,
                           q.c.billing_id,
                           q.c.name,
                           q.c.start_date,
                           q.c.end_date,
                           q.c.total_farmer,
                           q.c.total_land,
                           q.c.total_area,
                           q.c.total_invoice,
                           q.c.total_cost,
                           q.c.total_paid,
                           q.c.currency,
                           q.c.currency_sign,
                           q.c.village_id,
                           q.c.village_name,
                           q.c.commune_id,
                           Location.name,
                           q.c.total_farmer_paid,
                           q.c.total_exceed_paid
                           )
                 .order_by(q.c.start_date.desc(),
                           q.c.name,
                           Location.name)
                 )
        return query

    """"
      INVOICE PENALTY DETAIL
    """

    def report_billing_invoice_penalty(self, **kwargs):
        billing_id = kwargs.get('billing_id') or ''
        station_id = kwargs.get('station_id') or ''
        block_id = kwargs.get('block_id') or ''
        village_id = kwargs.get('village_id') or ''
        q = (
            db.query(
                func.row_number().over(order_by=(Station.code, Block.code), partition_by=Billing.name).label(
                    '_row_number'),
                Billing.id.label('billing_id'),
                Billing.name.label('name'),
                Billing.start_date.label('start_date'),
                Billing.end_date.label('end_date'),
                Station.code.label('station_code'),
                Station.name.label('station_name'),
                Block.name.label('block_name'),
                Block.code.label('block_code'),
                func.count(distinct(Invoice.farmer_id)).label('total_farmer'),
                func.count(Invoice.id).label('total_invoice'),
                func.sum(Invoice.total_amount).label('total_cost'),
                func.sum(Invoice.paid_amount).label('total_paid'),
                Invoice.currency_id.label('currency'),
                Currency.sign.label('currency_sign'),

            ).join(Invoice, Invoice.billing_id == Billing.id)
                .join(Station, Station.id == Invoice.station_id)
                .join(Block, Block.id == Invoice.block_id)
                .join(Currency, Currency.id == Invoice.currency_id)
                .filter(Invoice.total_amount != 0)
                .filter(Invoice.status_id != enum.InvoiceStatus.void)
                .filter(Invoice.invoice_type == enum.InvoiceType.PENALTY)
                .filter(Billing.is_active, Invoice.is_active, Block.is_active)
                .filter(Billing.billing_status_id == 'close')

        )
        if billing_id:
            q = q.filter(Billing.id == billing_id)
        if station_id:
            q = q.filter(Station.id == station_id)
        if block_id:
            q = q.filter(Block.id == block_id)
        if village_id:
            q = q.filter(Invoice.village_id == village_id)
        q = (
            q.group_by(Billing.id, Billing.name, Station.code, Station.name, Block.name, Block.code,
                       Invoice.currency_id,
                       Billing.start_date, Billing.end_date, Currency.sign)
                .order_by(Billing.name, Station.code)
        )
        q = q.subquery()
        query = (db.query(
            q.c._row_number,
            q.c.billing_id,
            q.c.name,
            q.c.start_date,
            q.c.end_date,
            q.c.station_code,
            q.c.station_name,
            q.c.block_name,
            q.c.block_code,
            q.c.total_farmer,
            q.c.total_invoice,
            q.c.total_cost,
            q.c.total_paid,
            q.c.currency,
            q.c.currency_sign,
            func.sum(q.c.total_cost - q.c.total_paid).label('ending_balance'),
            func.sum((q.c.total_paid * 100) / q.c.total_cost).label('percent_paid'),

        ).group_by(q.c._row_number,
                   q.c.billing_id,
                   q.c.name,
                   q.c.start_date,
                   q.c.end_date,
                   q.c.station_code,
                   q.c.station_name,
                   q.c.block_name,
                   q.c.block_code,
                   q.c.total_farmer,
                   q.c.total_invoice,
                   q.c.total_cost,
                   q.c.total_paid,
                   q.c.currency,
                   q.c.currency_sign).order_by(q.c.start_date.desc())
                 )
        return query

    """"
          INVOICE PENALTY DETAIL BY FARMERS
    """

    def report_billing_invoice_penalty_by_farmers(self, **kwargs):
        billing_id = kwargs.get('billing_id') or ''
        station_id = kwargs.get('station_id') or ''
        block_id = kwargs.get('block_id') or ''
        village_id = kwargs.get('village_id') or ''
        q = (
            db.query(
                func.row_number().over(order_by=(Farmer.name, Farmer.spouse_name, Farmer.code),
                                       partition_by=Billing.name).label(
                    '_row_number'),
                Billing.id.label('billing_id'),
                Billing.name.label('name'),
                Billing.start_date.label('start_date'),
                Billing.end_date.label('end_date'),
                Farmer.code.label('farmer_code'),
                Farmer.name.label('farmer_name'),
                Farmer.spouse_name.label('spouse_name'),
                Station.code.label('station_code'),
                Station.name.label('station_name'),
                Block.name.label('block_name'),
                Block.code.label('block_code'),
                func.count(Invoice.id).label('total_invoice'),
                func.sum(Invoice.total_amount).label('total_cost'),
                func.sum(Invoice.paid_amount).label('total_paid'),
                Invoice.currency_id.label('currency'),
                Currency.sign.label('currency_sign'),

            ).join(Invoice, Invoice.billing_id == Billing.id)
                .join(Farmer, Farmer.id == Invoice.farmer_id)
                .join(Station, Station.id == Invoice.station_id)
                .join(Block, Block.id == Invoice.block_id)
                .join(Currency, Currency.id == Invoice.currency_id)
                .filter(Invoice.total_amount != 0)
                .filter(Invoice.invoice_type == enum.InvoiceType.PENALTY)
                .filter(Invoice.status_id != enum.InvoiceStatus.void)
                .filter(Billing.is_active, Invoice.is_active, Block.is_active)
                .filter(Billing.billing_status_id == 'close')

        )
        if billing_id:
            q = q.filter(Billing.id == billing_id)
        if station_id:
            q = q.filter(Station.id == station_id)
        if block_id:
            q = q.filter(Block.id == block_id)
        if village_id:
            q = q.filter(Invoice.village_id == village_id)
        q = (
            q.group_by(Billing.id, Billing.name, Farmer.code, Farmer.name, Farmer.spouse_name, Station.code,
                       Station.name, Block.name,
                       Block.code,
                       Invoice.currency_id,
                       Billing.start_date, Billing.end_date, Currency.sign)
                .order_by(Billing.name, Farmer.code, Farmer.name, Farmer.spouse_name)
        )
        q = q.subquery()
        query = (db.query(
            q.c._row_number,
            q.c.billing_id,
            q.c.name,
            q.c.start_date,
            q.c.end_date,
            q.c.farmer_code,
            q.c.farmer_name,
            q.c.spouse_name,
            q.c.station_code,
            q.c.station_name,
            q.c.block_name,
            q.c.block_code,
            q.c.total_invoice,
            q.c.total_cost,
            q.c.total_paid,
            q.c.currency,
            q.c.currency_sign,
            func.sum(q.c.total_cost - q.c.total_paid).label('ending_balance'),
            func.sum((q.c.total_paid * 100) / q.c.total_cost).label('percent_paid'),

        ).group_by(q.c._row_number,
                   q.c.billing_id,
                   q.c.name,
                   q.c.start_date,
                   q.c.end_date,
                   q.c.farmer_code,
                   q.c.farmer_name,
                   q.c.spouse_name,
                   q.c.station_code,
                   q.c.station_name,
                   q.c.block_name,
                   q.c.block_code,
                   q.c.total_invoice,
                   q.c.total_cost,
                   q.c.total_paid,
                   q.c.currency,
                   q.c.currency_sign).order_by(q.c.start_date.desc())
                 )

        return query

    def report_by_farmer(self, **kwargs):
        """
            dept report
        """
        billing_id = kwargs.get('billing_id') or ''
        station_id = kwargs.get('station_id') or ''
        block_id = kwargs.get('block_id') or ''
        village_id = kwargs.get('village_id') or ''
        q1 = (
            db.query(
                func.row_number().over(order_by=(Farmer.name, Farmer.spouse_name, Farmer.code),
                                       partition_by=Billing.name).label(
                    '_row_number'),
                Billing.id.label('billing_id'),
                Billing.name.label('name'),
                Billing.start_date.label('start_date'),
                Billing.end_date.label('end_date'),
                Invoice.farmer_id.label('farmer_id'),
                Station.name.label('station_name'),
                Block.name.label('block_name'),
                Block.code.label('block_code'),
                Farmer.name.label('farmer_name'),
                Farmer.spouse_name.label('spouse_name'),
                Farmer.code.label('farmer_code'),
                func.count(Invoice.id).label('total_invoice'),
                func.sum(Invoice.total_amount).label('total_amount'),
                func.sum(Invoice.paid_amount).label('paid_amount'),
                func.sum(Invoice.total_amount - Invoice.paid_amount).label('ending_balance'),
                Invoice.currency_id.label('currency_id'),
                Currency.id.label('currency'),
                Currency.sign.label('sign')
            )
                .join(Invoice, Invoice.billing_id == Billing.id)
                .join(Station, Station.id == Invoice.station_id)
                .join(Block, Block.id == Invoice.block_id)
                .join(Farmer, Farmer.id == Invoice.farmer_id)
                .join(Currency, Currency.id == Invoice.currency_id)
                # .filter(Billing.is_active, Invoice.is_active, Invoice.status_id != enum.InvoiceStatus.void, Station.is_active,
                #         Block.is_active)
                .filter(Billing.is_active, Invoice.is_active, Invoice.status_id != enum.InvoiceStatus.void,
                        Station.is_active,)
        )

        if billing_id:
            q1 = q1.filter(Invoice.billing_id == billing_id)
        if station_id:
            q1 = q1.filter(Invoice.station_id == station_id)
        if block_id:
            q1 = q1.filter(Invoice.block_id == block_id)
        if village_id:
            q1 = q1.filter(Invoice.village_id == village_id)

        q1 = q1.group_by(
            Farmer.name,
            Farmer.spouse_name,
            Farmer.code,
            Billing.id,
            Billing.name,
            Billing.start_date,
            Billing.end_date,
            Invoice.farmer_id,
            Station.name,
            Block.name,
            Block.code,
            Invoice.currency_id,
            Currency.id,
            Currency.sign)
        q1 = q1.subquery()

        q2 = (
            db.query(
                q1.c._row_number,
                q1.c.billing_id,
                q1.c.name,
                q1.c.start_date,
                q1.c.end_date,
                q1.c.station_name,
                q1.c.block_name,
                q1.c.block_code,
                q1.c.farmer_id,
                q1.c.farmer_code,
                q1.c.farmer_name,
                q1.c.spouse_name,
                q1.c.total_invoice,
                q1.c.total_amount,
                q1.c.paid_amount,
                case([(q1.c.ending_balance <= 0, 0,)], else_=q1.c.ending_balance).label('ending_balance'),
                q1.c.currency_id,
                q1.c.currency,
                q1.c.sign
            )
                .filter(q1.c.paid_amount < q1.c.total_amount)
                .group_by(
                q1.c.farmer_name,
                q1.c.spouse_name,
                q1.c.farmer_code,
                q1.c._row_number,
                q1.c.farmer_id,
                q1.c.billing_id,
                q1.c.name,
                q1.c.start_date,
                q1.c.end_date,
                q1.c.station_name,
                q1.c.block_name,
                q1.c.block_code,
                q1.c.total_invoice,
                q1.c.total_amount,
                q1.c.paid_amount,
                q1.c.ending_balance,
                q1.c.currency_id,
                q1.c.currency,
                q1.c.sign).order_by(q1.c.start_date.desc())

        )

        return q2

    def report_exceed_by_farmer(self, **kwargs):
        """
            exceed report
        """
        billing_id = kwargs.get('billing_id') or ''
        station_id = kwargs.get('station_id') or ''
        block_id = kwargs.get('block_id') or ''
        village_id = kwargs.get('village_id') or ''
        q1 = (
            db.query(
                func.row_number().over(order_by=(Farmer.name, Farmer.spouse_name, Farmer.code),
                                       partition_by=Billing.name).label(
                    '_row_number'),
                Billing.id.label('billing_id'),
                Billing.name.label('name'),
                Billing.start_date.label('start_date'),
                Billing.end_date.label('end_date'),
                Invoice.farmer_id.label('farmer_id'),
                Station.name.label('station_name'),
                Block.name.label('block_name'),
                Block.code.label('block_code'),
                Farmer.name.label('farmer_name'),
                Farmer.spouse_name.label('spouse_name'),
                Farmer.code.label('farmer_code'),
                func.count(Invoice.id).label('total_invoice'),
                func.sum(Invoice.total_amount).label('total_amount'),
                func.sum(Invoice.paid_amount).label('paid_amount'),
                func.sum(Invoice.total_amount - Invoice.paid_amount).label('ending_balance'),
                Invoice.currency_id.label('currency_id'),
                Currency.id.label('currency'),
                Currency.sign.label('sign')
            )
                .join(Invoice, Invoice.billing_id == Billing.id)
                .join(Station, Station.id == Invoice.station_id)
                .join(Block, Block.id == Invoice.block_id)
                .join(Farmer, Farmer.id == Invoice.farmer_id)
                .join(Currency, Currency.id == Invoice.currency_id)
                .filter(Billing.is_active, Invoice.is_active, Invoice.status_id != enum.InvoiceStatus.void, Station.is_active, Block.is_active)

        )

        if billing_id:
            q1 = q1.filter(Invoice.billing_id == billing_id)
        if station_id:
            q1 = q1.filter(Invoice.station_id == station_id)
        if block_id:
            q1 = q1.filter(Invoice.block_id == block_id)
        if village_id:
            q1 = q1.filter(Invoice.village_id == village_id)

        q1 = q1.group_by(Billing.id,
                         Billing.name,
                         Billing.start_date,
                         Billing.end_date,
                         Invoice.farmer_id,
                         Station.name,
                         Block.name,
                         Block.code,
                         Farmer.name,
                         Farmer.spouse_name,
                         Farmer.code,
                         Invoice.currency_id,
                         Currency.id,
                         Currency.sign)
        q1 = q1.order_by(Billing.id,
                         Billing.name,
                         Billing.start_date,
                         Billing.end_date,
                         Invoice.farmer_id,
                         Station.name,
                         Block.name,
                         Block.code,
                         Farmer.name,
                         Farmer.spouse_name,
                         Farmer.code,
                         Invoice.currency_id,
                         Currency.id,
                         Currency.sign)

        q1 = q1.subquery()

        q2 = (
            db.query(
                q1.c._row_number,
                q1.c.billing_id,
                q1.c.name,
                q1.c.start_date,
                q1.c.end_date,
                q1.c.station_name,
                q1.c.block_name,
                q1.c.block_code,
                q1.c.farmer_id,
                q1.c.farmer_code,
                q1.c.spouse_name,
                q1.c.farmer_name,
                q1.c.total_invoice,
                q1.c.total_amount,
                q1.c.paid_amount,
                case([(q1.c.ending_balance <= 0, 0,)], else_=q1.c.ending_balance).label('ending_balance'),
                case([(q1.c.ending_balance < 0, q1.c.ending_balance,)], else_=0).label('exceed_amount'),
                q1.c.currency_id,
                q1.c.currency,
                q1.c.sign
            )
                .filter(q1.c.paid_amount > q1.c.total_amount)
                .group_by(q1.c._row_number,
                          q1.c.billing_id,
                          q1.c.name,
                          q1.c.start_date,
                          q1.c.end_date,
                          q1.c.farmer_id,
                          q1.c.farmer_code,
                          q1.c.station_name,
                          q1.c.block_name,
                          q1.c.block_code,
                          q1.c.farmer_name,
                          q1.c.spouse_name,
                          q1.c.total_invoice,
                          q1.c.total_amount,
                          q1.c.paid_amount,
                          q1.c.ending_balance,
                          q1.c.currency_id,
                          q1.c.currency,
                          q1.c.sign)

        )

        return q2

    def report_land_by_tariff(self, **kwargs):
        billing_id = kwargs.get('billing_id') or ''
        station_id = kwargs.get('station_id') or ''
        block_id = kwargs.get('block_id') or ''
        # village_id = kwargs.get('village_id') or ''
        q = (
            db.query(
                func.row_number().over(order_by=(Billing.name), partition_by=Billing.name).label(
                    '_row_number'),
                Billing.id.label('billing_id'),
                Billing.name.label('name'),
                Billing.start_date.label('start_date'),
                Billing.end_date.label('end_date'),
                Billing.is_active.label('is_active'),
                BillingLand.billing_id,
                func.count(BillingLand.land_id).label('total_land'),
                func.sum(BillingLand.area).label('total_area'),
                func.count(distinct(BillingLand.invoice_farmer_id)).label('total_farmer'),
                Tariff.id,
                Tariff.name.label('tariff_name'),

            ).join(BillingLand, BillingLand.billing_id == Billing.id)
                .join(Station, Station.id == BillingLand.station_id)
                .join(Block, Block.id == BillingLand.block_id)
                .join(Tariff, Tariff.id == BillingLand.tariff_id)
                .join(BillingCycle, BillingCycle.id == Billing.billing_cycle_id)
                .filter(BillingCycle.is_active)
                .filter(Billing.billing_status_id == 'close')
                .filter(Billing.is_active)
                .filter(BillingLand.is_active)
        )
        if billing_id:
            q = q.filter(BillingLand.billing_id == billing_id)
        if station_id:
            q = q.filter(Station.id == station_id)
        if block_id:
            q = q.filter(Block.id == block_id)
        q = q.group_by(Billing.id,
                       Billing.name,
                       Billing.start_date,
                       Billing.end_date,
                       Billing.is_active,
                       BillingLand.billing_id,
                       Tariff.id,
                       Tariff.name
                       ).order_by(Billing.start_date.desc())

        return q

    def report_preview_invoices(self, **kwargs):
        billing_id = kwargs.get('billing_id') or ''
        station_id = kwargs.get('station_id') or ''
        block_id = kwargs.get('block_id') or ''
        # village_id = kwargs.get('village_id') or ''
        q = (
            db.query(
                func.row_number().over(order_by=(Billing.name), partition_by=Billing.name).label(
                    '_row_number'),
                Billing.id.label('billing_id'),
                Billing.name.label('name'),
                Billing.start_date.label('start_date'),
                Billing.end_date.label('end_date'),
                BillingLand.billing_id,
                func.count(BillingLand.land_id).label('total_land'),
                func.sum(BillingLand.area).label('total_area'),
                func.count(distinct(BillingLand.invoice_farmer_id)).label('total_farmer'),
                Tariff.id,
                Tariff.name.label('tariff_name')
            ).join(BillingLand, BillingLand.billing_id == Billing.id)
                .join(Station, Station.id == BillingLand.station_id)
                .join(Block, Block.id == BillingLand.block_id)
                .join(Tariff, Tariff.id == BillingLand.tariff_id)
                .filter(Billing.billing_status_id == 'close')
                .filter(Billing.is_active)
                .filter(BillingLand.is_active)
        )
        if billing_id:
            q = q.filter(BillingLand.billing_id == billing_id)
        if station_id:
            q = q.filter(Station.id == station_id)
        if block_id:
            q = q.filter(Block.id == block_id)
        q = q.group_by(Billing.id,
                       Billing.name,
                       Billing.start_date,
                       Billing.end_date,
                       BillingLand.billing_id,
                       Tariff.id,
                       Tariff.name
                       )

        return q


default = ReportBillingLogic()
