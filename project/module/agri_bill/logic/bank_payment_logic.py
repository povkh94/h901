from sqlalchemy import case

from project.module.agri_bill.logic import farmer_logic, invoice_logic
from project.module.payment.logic import payment_logic
from ...share.view.base_view import *
from ...agri_bill.model import *
from ...payment.model import Payment
from project.module.currency.model import Currency
from project.module.agri_bill import enum
from bank_gateway_client_logic import BankGatewayClientLogic
from sqlalchemy_paginator import Paginator
from project.admin import config as conf
import uuid
from flask_babel import lazy_gettext as _

import sys

reload(sys)
sys.setdefaultencoding('utf8')


def push_update(message='', **kwargs):
    import time
    time.sleep(0.5)


# prefix_code = '900'
# access_key = '0af0b9b3-c0fc-462f-a756-976379a9d39c'


class BankPaymentLogic(LogicBase):
    def __init__(self):
        self.__classname__ = BankPaymentLog

    def search(self, **kwargs):
        search = kwargs.get('search', '')
        q = (db.query(BankPaymentLog.start_date,
                      BankPaymentLog.created_by,
                      BankPaymentLog.file_name,
                      BankPaymentLog.id,
                      func.count(BankPaymentDetail.id).label('total_record'),
                      func.row_number().over(order_by=desc(BankPaymentLog.start_date)).label('_row_number')
                      )
             .join(BankPaymentDetail, BankPaymentDetail.bank_payment_log_id == BankPaymentLog.id)
             .filter(BankPaymentLog.is_active,
                     BankPaymentLog.action_type == enum.BankPaymentActionType.GET_PAYMENT,
                     )
             .group_by(BankPaymentLog.start_date,
                       BankPaymentLog.created_by,
                       BankPaymentLog.file_name,
                       BankPaymentLog.id)
             .order_by(desc(BankPaymentLog.start_date))
             )
        if search:
            pass
        return q

    def get_pending_download_count(self):
        bank_gateway_client = BankGatewayClientLogic(conf.GATEWAY_API_URI,
                                                     current.company().ext__access_key,
                                                     current.user().name)
        return bank_gateway_client.get_pending_payment_count()

    def get_remaining_amount_count(self):
        result = (db.query(BankPaymentDetail)
                  .filter(BankPaymentDetail.is_active,
                          BankPaymentDetail.result == enum.BankPaymentResult.CORRECT,
                          BankPaymentDetail.pay_amount > BankPaymentDetail.paid_amount)
                  .count()
                  )
        return result

    def get_payment_detail(self, **kwargs):
        log_id = kwargs.get('log_id', '')
        result = kwargs.get('result', '')
        search = kwargs.get('search', '')
        if not log_id:
            return None

        q = (db.query(BankPaymentDetail)
             .filter(BankPaymentDetail.is_active,
                     BankPaymentDetail.bank_payment_log_id == log_id)
             )
        q = q.join(Farmer, Farmer.id == BankPaymentDetail.customer_id)
        if search:
            search = search.strip()
            q = q.filter(or_(
                func.lower(BankPaymentDetail.customer_code).like('%' + search.lower() + '%'),
                func.lower(Farmer.name).like('%' + search.lower() + '%'),
                func.lower(Farmer.code).like('%' + search.lower() + '%'),
                func.lower(BankPaymentDetail.pay_amount).like('%' + search.lower() + '%')
            )
            )
        if result:
            q = q.filter(BankPaymentDetail.result == result)

        return q

    def get_remaining_amount(self):
        result = (db.query(BankPaymentDetail)
                  .filter(BankPaymentDetail.is_active,
                          BankPaymentDetail.result == enum.BankPaymentResult.CORRECT,
                          BankPaymentDetail.pay_amount > BankPaymentDetail.paid_amount)
                  )
        return result

    def get_pending_customers(self, d1, d2=datetime.now(), force_all=False):
        if force_all:
            customer_ids = db.query(Farmer.id).filter(
                Farmer.is_active,
                Farmer.active == 'active'
            )
        else:
            farmer = db.query(Farmer.id.label('customer_id')).filter(Farmer.is_active,
                                                                     Farmer.active == 'active',
                                                                     Farmer.updated_date.between(d1, d2))
            invoice = db.query(Invoice.farmer_id.label('customer_id')).filter(Invoice.is_active,
                                                                              Invoice.updated_date.between(d1, d2))
            payment = db.query(Payment.customer_id.label('customer_id')).filter(Payment.is_active,
                                                                                Payment.updated_date.between(d1, d2))
            customer_ids = farmer.union_all(invoice).union_all(payment)
            #  void duplicated customer_id
            customer_ids = list(set([customer.customer_id for customer in customer_ids]))

        q = (db.query(Farmer.code.label('CUSTOMER_ID'),
                      Farmer.code.label('CUSTOMER_CODE'),
                      Farmer.name.label('CUSTOMER_NAME'),
                      func.isnull(Currency.code, 'KHR').label('CURRENCY'),
                      func.isnull(
                          func.sum(case(whens=((
                                                   and_(Invoice.total_amount - Invoice.paid_amount > 0,
                                                        Invoice.is_active,
                                                        Invoice.status_id != 'void'), 1),), else_=0)), 0).label(
                          'TOTAL_RECORD'),
                      func.isnull(func.sum(case(whens=((
                                                           or_(Invoice.total_amount - Invoice.paid_amount < 0,
                                                               Invoice.is_active == False,
                                                               Invoice.status_id == 'void'),
                                                           0),), else_=Invoice.total_amount - Invoice.paid_amount)),
                          0).label('TOTAL_AMOUNT'),
                      Farmer.id.label('METER_CODE'),
                      func.max(func.isnull(Invoice.due_date, '1990-01-01')).label('DUE_DATE')
                      )
             .select_from(Farmer)
             .outerjoin(Invoice, Farmer.id == Invoice.farmer_id)
             .outerjoin(Currency, Currency.id == Invoice.currency_id)
             # .filter(Invoice.is_active, Invoice.status_id != 'void')
             .filter(Farmer.id.in_(customer_ids))
             .group_by(Farmer.code, Farmer.name, Currency.code, Farmer.id)
             )
        return q

    def get_last_log(self):
        last_log = (db.query(BankPaymentLog).
                    filter(BankPaymentLog.is_active,
                           BankPaymentLog.action_type == enum.BankPaymentActionType.UPDATE_CUSTOMER
                           )
                    .order_by(desc(BankPaymentLog.created_date))
                    .first()
                    )
        return last_log

    def update_customer_data(self, **kwargs):
        force_all = False if not kwargs.get('force_all') or kwargs.get('force_all') == "false" else True
        # push_update(message=_(u'Start upload data to bank'), **kwargs)
        last_date_action = to_datetime("2000-01-01")
        bank_gateway_client = BankGatewayClientLogic(conf.GATEWAY_API_URI, current.company().ext__access_key,
                                                     current.user().name)
        last_log = self.get_last_log()  # type: BankPaymentLog
        if last_log:
            if not last_log.is_completed and last_log.data_file:
                #  Last log didn't complete the action send it to gateway again
                bank_gateway_client.update_customer_to_server(last_log.id, last_log.data_file)

            last_date_action = last_log.start_date

        pending_customers = self.get_pending_customers(last_date_action, datetime.now(), force_all)
        if not pending_customers.count():
            #  No data to send
            # push_update(message=_(u'No farmer for upload to bank!'), finish=True, **kwargs)
            return dict(message='<strong>%s</strong> %s' % (_('Info!'), _('No farmer for upload to bank!')),
                        message_class='alert-info')
        #  crate log in have pending customer
        log_id = str(uuid.uuid4())
        log = BankPaymentLog()
        log.id = log_id
        log.action_type = enum.BankPaymentActionType.UPDATE_CUSTOMER
        log.is_completed = False
        log.start_date = datetime.now()
        log.end_date = datetime.now()
        log.total_record = pending_customers.count()
        log.total_amount = sum([r.TOTAL_AMOUNT for r in pending_customers])
        log.file_name = datetime.now().strftime("%Y%m%d%H%M%S") + '.csv'
        # db.add(log)
        # db.commit()
        #  upload customer to gateway 1 time limit 500 records
        csv_data = ""
        csv = ''
        i = 1
        for r in pending_customers:
            try:
                if i % 500 == 0 or i >= pending_customers.count():
                    bank_gateway_client.update_customer_to_server(log_id, csv)
                    csv_data += csv
                    csv = ''

                csv += u"{customer_id},{customer_code},{customer_name},{currency},{total_record},{total_amount},{due_date},{new_code},{meter_code},{status_id}\r\n".format(
                    customer_id=r.CUSTOMER_ID,
                    customer_code=r.CUSTOMER_CODE,
                    customer_name=r.CUSTOMER_NAME,
                    currency=r.CURRENCY,
                    total_record=r.TOTAL_RECORD,
                    total_amount=r.TOTAL_AMOUNT,
                    due_date=r.DUE_DATE.strftime("%Y-%m-%d"),
                    new_code=current.company().ext__license_number + '-' + r.CUSTOMER_CODE,
                    meter_code=r.METER_CODE,
                    status_id=2
                )
                i += 1


            except Exception as e:
                print r
                print e.message

        if csv:
            bank_gateway_client.update_customer_to_server(log_id, csv)
            csv_data += csv

        log.data_file = csv_data
        log.is_completed = True
        log.end_date = datetime.now()
        db.add(log)
        db.commit()
        return dict(message='<strong>%s</strong> %s' % (_('Success!'), _('Data already uploaded to server!')),
                    message_class='alert-success')

    def update_customer(self, force_all):
        """
        description update customer to bank geteway not using polling
        :param force_all:
        :return:
        """
        last_date_action = to_datetime("2000-01-01")
        bank_gateway_client = BankGatewayClientLogic(conf.GATEWAY_API_URI, current.company().ext__access_key,
                                                     current.user().name)
        last_log = self.get_last_log()  # type: BankPaymentLog
        if last_log:
            if not last_log.is_completed and last_log.data_file:
                #  Last log didn't complete the action send it to gateway again
                bank_gateway_client.update_customer_to_server(last_log.id, last_log.data_file)

            last_date_action = last_log.start_date

        pending_customers = self.get_pending_customers(last_date_action, datetime.now(), force_all)
        if not pending_customers.count():
            #  No data to send
            return dict(message='<strong>%s</strong> %s' % (_('Info!'), _('No farmer for upload to bank!')),
                        message_class='alert-info')
        #  crate log in have pending customer
        log_id = str(uuid.uuid4())
        log = BankPaymentLog()
        log.id = log_id
        log.action_type = enum.BankPaymentActionType.UPDATE_CUSTOMER
        log.is_completed = False
        log.start_date = datetime.now()
        log.end_date = datetime.now()
        log.total_record = pending_customers.count()
        log.total_amount = sum([r.TOTAL_AMOUNT for r in pending_customers])
        log.file_name = datetime.now().strftime("%Y%m%d%H%M%S") + '.csv'
        db.add(log)
        db.commit()
        #  upload customer to gateway 1 time limit 500 records
        csv_data = ""
        csv = ''
        i = 1
        for r in pending_customers:
            if i % 500 == 0 or i >= pending_customers.count():
                bank_gateway_client.update_customer_to_server(log_id, csv)
                csv_data += csv
                csv = ''

            csv += u"{customer_id},{customer_code},{customer_name},{currency},{total_record},{total_amount},{due_date},{new_code},{meter_code},{status_id}\r\n".format(
                customer_id=r.CUSTOMER_ID,
                customer_code=r.CUSTOMER_CODE,
                customer_name=r.CUSTOMER_NAME,
                currency=r.CURRENCY,
                total_record=r.TOTAL_RECORD,
                total_amount=r.TOTAL_AMOUNT,
                due_date=r.DUE_DATE.strftime("%Y-%m-%d"),
                new_code=current.company().ext__license_number + '-' + r.CUSTOMER_CODE,
                meter_code=r.METER_CODE,
                status_id=2
            )
            i += 1

        log.data_file = csv_data
        log.is_completed = True
        log.end_date = datetime.now()
        db.commit()
        # return dict(message='<strong>%s</strong> %s' % (_('Success!'), _('Data already uploaded to server!')),
        #             message_class='alert-success')

    def parse_to_bank_payment_detail(self, str_payment):
        payment = str_payment.split(',')
        obj = BankPaymentDetail()
        obj.bank = payment[0]
        obj.branch = payment[1]
        obj.customer_code = payment[2]
        obj.currency = payment[3]
        obj.pay_amount = to_decimal(payment[4])
        obj.pay_date = to_datetime(payment[5] + ' ' + payment[6])
        obj.note = payment[7]
        obj.cashier = payment[8]
        obj.payment_method = payment[9]
        obj.payment_type = payment[10]
        obj.bank_payment_id = payment[11]
        return obj

    def get_payment(self):
        try:
            bank_gateway_client = BankGatewayClientLogic(conf.GATEWAY_API_URI,
                                                         current.company().ext__access_key,
                                                         current.user().name)

            data = bank_gateway_client.get_pending_payment()
            if data:
                log_id = uuid.uuid4()
                log = BankPaymentLog()
                log.id = log_id
                log.action_type = enum.BankPaymentActionType.GET_PAYMENT
                log.is_completed = False
                log.start_date = datetime.now()
                log.end_date = datetime.now()
                log.total_record = 0
                log.total_amount = 0
                log.file_name = datetime.now().strftime("%Y%m%d%H%M%S") + '.csv'
                log.data_file = ''
                db.add(log)
                db.commit()
            else:
                #  No data to download
                return dict(message='<strong>%s</strong> %s' % (_('Info!'), _('There is not new payment to download!')),
                            message_class='alert-info')

            while data and bank_gateway_client.get_pending_payment_count() != 0:
                payments = list(filter(None, data.split('\r\n')))
                bank_payment_details = list()
                detail = None
                for payment in payments:
                    try:
                        detail = self.parse_to_bank_payment_detail(payment)
                        detail.bank_payment_log_id = log_id
                        detail.result = enum.BankPaymentResult.CORRECT
                        error = list()
                        customer = db.query(Farmer).filter(Farmer.is_active,
                                                           Farmer.code == detail.customer_code.split('-')[-1]).first()
                        if customer:
                            detail.customer_id = customer.id
                        else:
                            error.append('Customer')

                        currency = db.query(Currency).filter(Currency.is_active,
                                                             Currency.code == detail.currency).first()
                        if currency:
                            detail.currency_id = currency.id
                        else:
                            error.append('Currency')
                        if len(error):
                            detail.result = enum.BankPaymentResult.INCORRECT
                            detail.result_note = "Invalid {error}".format("".join(error))
                        else:
                            old_payment = (db.query(BankPaymentDetail)
                                           .filter(BankPaymentDetail.is_active,
                                                   BankPaymentDetail.customer_id == detail.customer_id,
                                                   BankPaymentDetail.currency_id == detail.currency_id,
                                                   BankPaymentDetail.pay_amount == detail.pay_amount,
                                                   BankPaymentDetail.pay_date == detail.pay_date,
                                                   BankPaymentDetail.result != enum.BankPaymentResult.INCORRECT)
                                           ).first()
                            if old_payment:
                                detail.result = enum.BankPaymentResult.DUPLICATED
                            else:
                                detail.result = enum.BankPaymentResult.CORRECT

                    except Exception as e:
                        print payment
                    #  add to bank payment detail
                    if detail.result == enum.BankPaymentResult.CORRECT:
                        bank_payment_details.append(detail)
                        import logging, json
                        logging.error(json.dumps(bank_payment_details))
                    db.add(detail)
                    db.commit()
                #  update log
                log.end_date = datetime.now()
                log.data_file += data
                db.commit()
                # process payment
                self.update_payment(bank_payment_details)
                #  update record to gateway
                string_data = ""
                for pd in bank_payment_details:
                    string_data += "{id},{pay_amount}\r\n".format(id=pd.bank_payment_id, pay_amount=pd.pay_amount)
                #  update payment to server
                bank_gateway_client.update_payment_data(string_data)
                #  get new pending payment
                data = bank_gateway_client.get_pending_payment()
            #  update customer data to gateway
            self.update_customer(force_all=False)
            return dict(
                message='<strong>%s</strong> %s <a style=\"text-decoration: underline;\" href=\"/bankpayment/detail/%s\" class=\"alert-link\">%s</a>' % (
                    _('Success!'), _('Data already downloaded and settled!'), log_id, _('show downloaded payment')),
                message_class='alert-success')
        except Exception as e:
            return dict(
                message='<strong>%s</strong> %s ' % (
                    _('Failed!'), e.message),
                message_class='alert-danger')

    def update_payment(self, bank_payment_details):

        ''''
            1.find invoices by customer id
            2.create new json obj append to ext of payment
            @dict : { customer name:value , details:{[]} }
        '''

        for detail in bank_payment_details:
            if detail.pay_amount <= detail.paid_amount:
                continue
            invoices = invoice_logic.default.find_invoice_by_farmer_id(detail.customer_id)
            details = []
            total_amount = 0
            total_paid = 0
            if invoices:
                pay_amount = detail.pay_amount
                for invoice in invoices:
                    end_balance = invoice.total_amount - invoice.paid_amount
                    if pay_amount >= end_balance:
                        paid_amount = end_balance
                        pay_amount -= end_balance
                        # end_balance = 0
                    else:
                        paid_amount = pay_amount
                    total_amount += (invoice.total_amount - invoice.paid_amount)
                    total_paid += paid_amount
                    ending_balance = (invoice.total_amount - invoice.paid_amount) - paid_amount
                    ext_data = {'invoice_id': invoice.id, 'paid_amount': paid_amount}
                    details.append(
                        {
                            'code': invoice.code,
                            'pay_amount': paid_amount,
                            'ending_balance': ending_balance,
                            'ext_data': ext_data,
                            'balance': end_balance,
                            'total_amount': invoice.total_amount,
                            'paid_amount': invoice.paid_amount})
                customer = farmer_logic.default.find(id=detail.customer_id)
                payment = Payment()
                payment.customer_id = detail.customer_id
                payment.currency_id = detail.currency_id
                payment.customer_name = customer.name + '-' + customer.spouse_name if customer.spouse_name else ''
                if total_amount == total_paid:
                    payment.pay_amount = total_paid
                    payment.ending_balance = 0
                    payment.partial_payment = False
                elif total_amount > total_paid:
                    payment.pay_amount = total_paid
                    payment.ending_balance = payment.pay_amount - total_paid
                    payment.partial_payment = True
                payment.pay_date = detail.pay_date
                payment.ext__customer_name = customer.name
                payment.ext__details = details
                payment.payment_method = enum.payment_method.E_Money
                payment_logic.default.add(payment)
                detail.paid_amount += payment.pay_amount
                db.add(detail)
                db.commit()

    def get_banks(self):
        q = db.query(func.cast(BankPaymentDetail.bank, String(50))).distinct()
        result = []
        for r in q:
            bd = BankPaymentDetail()
            bd.bank = r[0]
            result.append(bd)
        return result


default = BankPaymentLogic()
