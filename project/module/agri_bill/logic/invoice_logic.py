from sqlalchemy import distinct, update, func, case
from flask_babel import gettext as _
from project.module.agri_bill import enum
from project.module.agri_bill.convert import round_riel
from project.module.agri_bill.export import export_pdf
from project.module.agri_bill.logic import block_logic, location_logic
from project.module.system.logic import autono_logic
from ...share.view.base_view import *
from ...agri_bill.model import *
from ...company.model import Company


def push_update(message='', **kwargs):
    import time
    time.sleep(0.5)


class InvoiceLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = Invoice

    def new(self):
        seq_id = current.company().ext.get('seq', dict(invoice='0')).get('invoice-code', '0')
        obj = self.__classname__()
        if seq_id:
            obj.code = autono_logic.default.next_no(id=seq_id)
        return obj

    def find(self, id='', code=''):
        # q = self.actives
        q = db.query(Invoice).filter(
            or_(
                Invoice.is_active, Invoice.status_id == 'void'
            )
        )
        if id:
            q = q.filter(self.__classname__.id == id)
        if code:
            q = q.filter(self.__classname__.code == code)
        return q.first()

    def search(self, **kwargs):
        search = kwargs.get('search') or ''
        billing_id = kwargs.get('billing_id') or None
        block_id = kwargs.get('block_id') or None
        village_id = kwargs.get('village_id') or None
        invoice_status_id = kwargs.get('invoice_status_id') or None
        invoice_type = kwargs.get('invoice_type') or None
        # q = self.actives()
        q = (db.query(Invoice)
             .join(Farmer, Farmer.id == Invoice.farmer_id)
             .join(Billing, Billing.id == Invoice.billing_id))
        if search:
            search = search.strip()
            q = q.filter(or_(
                func.lower(self.__classname__.code).like('%' + search.lower() + '%'),
                func.lower(Farmer.name).like('%' + search.lower() + '%'),
                func.lower(Farmer.code).like('%' + search.lower() + '%'),
                func.lower(self.__classname__.total_amount).like('%' + search.lower() + '%')
            )
            )
        if billing_id:
            q = q.filter(Invoice.billing_id == billing_id)

        if block_id:
            q = q.filter(Invoice.block_id == block_id)

        if village_id:
            q = q.filter(Farmer.village_id == village_id)

        if invoice_status_id:
            if invoice_status_id != 'void':
                q = q.filter(Invoice.status_id == invoice_status_id)
            else:
                return q.filter(Invoice.is_active == 0, Invoice.status_id == "void",
                                Billing.billing_status_id == 'close')

        if invoice_type:
            q = q.filter(Invoice.invoice_type == invoice_type)

        q = q.filter(Billing.billing_status_id == 'close', Invoice.is_active)
        q = q.order_by(Farmer.name)
        return q

    def find_invoice_detail(self, invoice_id):
        result = (
            db.query(
                BillingLand.area,
                BillingLand.unit_cost,
                BillingLand.total_cost,
                Station.name.label('station_name'),
                Block.name.label('block_name'),
                Land.code.label('land_code'),
                Invoice.code.label('invoice_code'),
                Invoice.contract_number.label('contract_number')
            )
                .join(InvoiceDetail, InvoiceDetail.id == BillingLand.invoice_detail_id)
                .join(Station, Station.id == BillingLand.station_id)
                .join(Block, Block.id == BillingLand.block_id)
                .join(Land, Land.id == BillingLand.land_id)
                .join(Invoice, Invoice.id == InvoiceDetail.invoice_id)
                .filter(InvoiceDetail.invoice_id == invoice_id)
                .order_by(Block.name)
                .all()
        )
        return result

    def preview(self, id='', billing_id='', village_id=''):

        if id:
            invoice = (db.query(Invoice).filter(Invoice.id == id).first())
            return invoice

        if billing_id != '' and village_id != '':
            result = (
                db.query(
                    BillingLand.billing_id,
                    Invoice.village_id,
                    Location.name.label('village_name'),
                    Billing.name.label('billing_name'),
                    func.count(distinct(Invoice.id)).label('total_invoices'),
                    func.count(distinct(Invoice.farmer_id)).label('total_farmers'),
                    Location.name.label('village_name'),
                    func.count(BillingLand.land_id).label('total_land'),
                    func.sum(BillingLand.area).label('total_areas'),
                    func.sum(BillingLand.total_cost).label('total_amounts'),
                )
                    .join(InvoiceDetail, InvoiceDetail.id == BillingLand.invoice_detail_id)
                    .join(Invoice, Invoice.id == InvoiceDetail.invoice_id)
                    .join(Location, Location.id == Invoice.village_id)
                    .join(Billing, Billing.id == BillingLand.billing_id)

                    .filter(BillingLand.billing_id == billing_id)
                    .group_by(BillingLand.billing_id, Invoice.village_id, Location.name, Billing.name).first()
            )
        return result

    def preview_to_pdf(self, billing_id='', block_id='', village_id=''):

        q = db.query(Invoice).filter(Invoice.is_active)

        if billing_id:
            q = q.filter(Invoice.billing_id == billing_id)

        if block_id != '__None':
            q = q.filter(Invoice.block_id == block_id)

        if village_id != '__None':
            q = q.filter(Invoice.village_id == village_id)
        q = q.join(Farmer, Farmer.id == Invoice.farmer_id)
        q = q.order_by(Farmer.delivery_code, Farmer.code)
        return q.all()

    # find invoice by farmer_id for making payment
    def find_invoice_by_farmer_id(self, farmer_id):
        q = db.query(Invoice).filter(
            and_(Invoice.farmer_id == farmer_id, Invoice.status_id != 'paid', Invoice.is_active))
        q = q.order_by(Invoice.created_date)
        result = q.all()
        return result

    def check_invoice_status(self, id='', billing_id=''):
        if billing_id:
            q = (db.query(Invoice)
                 .filter(Invoice.is_active,
                         Invoice.billing_id == billing_id,
                         or_(Invoice.status_id == 'paid', Invoice.status_id == 'partial'))
                 .count()
                 )
        if id:
            q = (db.query(Invoice)
                 .filter(Invoice.is_active,
                         Invoice.id == id,
                         or_(Invoice.status_id == 'paid', Invoice.status_id == 'partial'))
                 .count()
                 )
        if q == 0:
            return False
        else:
            return True

    def remove(self, obj, _commit=True):
        if hasattr(obj, 'is_active'):
            obj.is_active = False
            obj.status_id = 'void'
            invoice_details = db.query(InvoiceDetail).filter(InvoiceDetail.invoice_id == obj.id).all()
            invoice_detail_ids = []
            mapping_invoice_detail = []
            for invoice_detail in invoice_details:
                invoice_detail_ids.append(invoice_detail.id)
                mapping_invoice_detail.append({'id': invoice_detail.id, 'is_active': 0})

            billing_lands = db.query(BillingLand).filter(BillingLand.invoice_detail_id.in_(invoice_detail_ids)).all()
            mapping_billing_land = []
            for billing_land in billing_lands:
                mapping_billing_land.append({'id': billing_land.id, 'is_active': 0})
            db.bulk_update_mappings(InvoiceDetail, mapping_invoice_detail)
            db.bulk_update_mappings(BillingLand, mapping_billing_land)
        db.flush()
        db.commit()

    def update(self, obj, _commit=True):
        invoice = db.query(Invoice).filter(Invoice.id == obj.id).first()
        if obj.ext__total_amount and obj.ext__total_amount != 'NaN':
            invoice.total_amount = obj.ext__total_amount
        if obj.ext__total_area and obj.ext__total_area != 'NaN':
            invoice.total_area = obj.ext__total_area
        invoice.note = obj.note
        invoice.ext = obj.ext
        invoice.status_id = obj.status_id
        if obj.is_active == 'true':
            invoice.is_active = True
        db.add(invoice)
        db.commit()

    def find_invoice_suggetion(self, **kwargs):
        farmer_id = kwargs.get('farmer_id') or ''
        invoice_code = kwargs.get('invoice_code') or ''
        billing_id = kwargs.get('billing_id')
        invoice_ids = kwargs.get('invoice_ids')
        if billing_id:
            q = db.query(Invoice.id).filter(Invoice.billing_id == billing_id,
                                            Invoice.status_id.in_(['paid', 'partial']),
                                            Invoice.is_active)
            return q
        if farmer_id:
            q = db.query(Invoice).filter(Invoice.farmer_id == farmer_id, Invoice.status_id != 'paid', Invoice.is_active)
        if invoice_code:
            q = db.query(Invoice).filter(Invoice.code == invoice_code, Invoice.status_id != 'paid', Invoice.is_active)
        if invoice_ids:
            q = db.query(Invoice).filter(Invoice.id.in_(invoice_ids))
        q = q.order_by(Invoice.issue_date, Invoice.code)
        return q.all()

    def add(self, obj, _commit=True):
        current._gid = lambda: randomstring(8, 'ABCDEFGHIJKLMNOPQRSTUVWYZabcdefghijklmnopqrstuvwyz0123456789')
        billing = self.search(billing_id=obj.billing_id).first()
        price = obj.ext__unit_cost
        if not price:
            price = 0
        else:
            price = float(price)
        # if not price:
        q1 = (db.query(
            Land.id,
            Land.block_id,
            Land.farmer_id,
            case([
                (Land.rent_id == None, Land.farmer_id),
                (Land.rent_id == '', Land.farmer_id)],
                else_=Land.rent_id).label('rent_id'),
            Land.tariff_id.label('tariff_id'),
            Land.area,
            Tariff.price.label('unit_cost'),
            func.sum(price).label('price')
        )
              .join(Tariff, Tariff.id == Land.tariff_id)
              .filter(Land.id.in_(obj.ext__lands), Land.is_active).group_by(
            Land.id,
            Land.block_id,
            Land.farmer_id,
            Land.tariff_id,
            Land.rent_id,
            Land.area,
            Tariff.price
        ).subquery())
        q2 = (
            db.query(
                q1.c.id,
                q1.c.block_id,
                q1.c.farmer_id,
                q1.c.rent_id,
                q1.c.tariff_id,
                q1.c.unit_cost,
                q1.c.area,
                q1.c.price,
                Block.station_id,
                Block.name.label('block_name'),
                Farmer.village_id.label('village_id'),
                case([
                    (q1.c.price != 0, func.sum(q1.c.area * q1.c.price))
                ], else_=func.sum(q1.c.area * q1.c.unit_cost)).label('line_total'),
                case([
                    (q1.c.price != 0, q1.c.area)
                ], else_=q1.c.unit_cost).label('unit_costs')
            )
                .join(Block, Block.id == q1.c.block_id)
                .join(Farmer, Farmer.id == q1.c.rent_id)
                .group_by(
                q1.c.id,
                q1.c.block_id,
                q1.c.farmer_id,
                q1.c.rent_id,
                q1.c.tariff_id,
                q1.c.area,
                q1.c.unit_cost,
                q1.c.price,
                Block.station_id,
                Block.name,
                Farmer.village_id).subquery()
        )

        q_invoice = (
            db.query(
                q2.c.station_id,
                q2.c.block_id,
                q2.c.farmer_id,
                q2.c.rent_id,
                q2.c.village_id,
                # q2.c.unit_costs,
                func.count(q2.c.id).label('total_lands'),
                func.sum(q2.c.area).label('total_area'),
                func.sum(q2.c.line_total).label('total_amount'),

            ).group_by(
                q2.c.station_id,
                q2.c.block_id,
                q2.c.farmer_id,
                q2.c.rent_id,
                q2.c.village_id
                # q2.c.unit_costs
            ).all()
        )

        q_invoice_detail = (
            db.query(
                q2.c.station_id,
                q2.c.block_id.label('item_id'),
                q2.c.block_name.label('item_name'),
                q2.c.farmer_id,
                q2.c.rent_id,
                q2.c.tariff_id,
                q2.c.unit_costs,
                func.count(q2.c.id).label('total_land'),
                func.sum(q2.c.area).label('quantity'),
                func.sum(q2.c.line_total).label('line_total')
            ).group_by(
                q2.c.station_id,
                q2.c.block_id,
                q2.c.block_name,
                q2.c.farmer_id,
                q2.c.rent_id,
                q2.c.tariff_id,
                q2.c.unit_costs
            ).all()
        )

        q_billing_land = (
            db.query(
                q1.c.id,
                q1.c.block_id,
                q1.c.farmer_id,
                q1.c.rent_id,
                q1.c.tariff_id,
                q1.c.area,
                q1.c.unit_cost,
                q1.c.price,
                Block.station_id,
                case([
                    (q1.c.price != 0, q1.c.price)
                ], else_=q1.c.unit_cost).label('unit_costs'),
                # func.sum(q1.c.area * float(obj.ext__unit_cost)).label('total_cost')
                case([
                    (q1.c.price != 0, func.sum(q1.c.area * q1.c.price))
                ], else_=func.sum(q1.c.area * q1.c.unit_cost)).label('total_cost')
            )
                .join(Block, Block.id == q1.c.block_id)
                .group_by(
                q1.c.id,
                q1.c.block_id,
                q1.c.farmer_id,
                q1.c.rent_id,
                q1.c.tariff_id,
                q1.c.area,
                q1.c.unit_cost,
                q1.c.price,
                Block.station_id
            ).all()
        )
        invoice_id_map = dict()
        invoices = []
        for itm in q_invoice:
            seq_id = current.company().ext.get('seq', dict(invoice='')).get('invoice-code', '')
            if seq_id:
                obj.code = autono_logic.default.next_no(id=seq_id, update=True)
            invoice = Invoice()
            invoice.id = gid()
            invoice_id_map[(itm.station_id, itm.block_id, itm.farmer_id, itm.rent_id)] = invoice.id
            invoice.code = obj.code
            invoice.billing_id = obj.billing_id
            invoice.company_id = billing.company_id
            invoice.block_id = itm.block_id
            invoice.station_id = itm.station_id
            invoice.farmer_id = itm.rent_id
            invoice.owner_farmer_id = itm.farmer_id
            invoice.village_id = itm.village_id
            invoice.total_lands = itm.total_lands
            invoice.total_area = itm.total_area
            invoice.total_amount = round_riel(itm.total_amount)
            invoice.paid_amount = 0.00
            invoice.currency_id = 'KHR'
            invoice.contract_number = None
            invoice.status_id = 'pending'
            invoice.invoice_type = enum.InvoiceType.IRRIGATE
            invoice.issue_date = billing.issue_date
            invoice.due_date = billing.due_date
            invoice.is_active = '1'
            invoice.note = obj.note
            invoices.append(invoice)
        db.flush()

        invoice_detail_id_map = dict()
        invoice_details = []
        for itm in q_invoice_detail:
            invoice_detail = InvoiceDetail()
            invoice_detail.id = gid()
            invoice_detail_id_map[
                (itm.station_id, itm.item_id, itm.farmer_id, itm.rent_id, itm.tariff_id)] = invoice_detail.id
            invoice_detail.invoice_id = invoice_id_map[(itm.station_id, itm.item_id, itm.farmer_id, itm.rent_id)]
            invoice_detail.item_id = itm.item_id
            invoice_detail.item_name = itm.item_name
            invoice_detail.tariff_id = itm.tariff_id
            invoice_detail.price = itm.unit_costs
            invoice_detail.block_id = itm.item_id
            invoice_detail.is_active = 1
            invoice_detail.line_total = round_riel(itm.line_total)
            invoice_detail.quantity = itm.quantity
            invoice_detail.total_land = itm.total_land
            invoice_details.append(invoice_detail)
        db.flush()

        billing_lands = []
        for itm in q_billing_land:
            billing_land = BillingLand()
            billing_land.id = gid()
            billing_land.invoice_detail_id = invoice_detail_id_map[
                (itm.station_id, itm.block_id, itm.farmer_id, itm.rent_id, itm.tariff_id)]
            billing_land.tariff_id = itm.tariff_id
            billing_land.billing_id = billing.billing_id
            billing_land.station_id = itm.station_id
            billing_land.block_id = itm.block_id
            billing_land.land_id = itm.id
            billing_land.unit_cost = itm.unit_costs
            billing_land.area = itm.area
            billing_land.total_cost = itm.total_cost
            billing_land.invoice_farmer_id = itm.rent_id,
            billing_land.owner_farmer_id = itm.farmer_id
            billing_lands.append(billing_land)
        db.flush()
        db.bulk_save_objects(invoices)
        db.bulk_save_objects(invoice_details)
        db.bulk_save_objects(billing_lands)
        db.commit()

    def find_template(self, is_active=''):
        if is_active:
            return db.query(Setting).filter(Setting.datatype == 'template', Setting.is_active).first()
        return db.query(Setting).filter(Setting.datatype == 'template').all()

    def update_template(self, id):
        if id:
            row = db.query(Setting).filter(Setting.datatype == 'template', Setting.is_active).first()
            row.is_active = 0
            db.add(row)
            db.flush()
            template = db.query(Setting).filter(Setting.id == id).first()
            template.is_active = 1
            db.add(template)
            db.commit()
            return 1
        return 0

    def update_invoice_payment(self, invoice):
        """
        update invoices with bank
        @param invoice
        """
        obj = db.query(Invoice).filter(Invoice.id == invoice.id).first()
        if obj.total_amount == invoice.paid_amount:
            obj.status_id = 'paid'
        elif 0 < invoice.paid_amount < obj.total_amount:
            obj.status_id = 'partial'
        else:
            obj.status_id = 'pending'
        db.add(obj)
        db.flush()
        return obj

    def find_invoices(self, billing_id, farmer_id):
        date = datetime.today()

        result = (db.query(Invoice)
                  .outerjoin(InvoicePenalty, InvoicePenalty.invoice_id == Invoice.id) \
                  .filter(
            Invoice.billing_id == billing_id,
            Invoice.farmer_id == farmer_id,
            Invoice.issue_date < date,
            Invoice.is_active,
            Invoice.status_id != enum.InvoiceStatus.void,
            InvoicePenalty.invoice_id == None
        ).filter(or_(Invoice.invoice_type != enum.InvoiceType.PENALTY, Invoice.invoice_type == None,
                     Invoice.invoice_type == ''))
                  .filter(or_(Invoice.status_id == 'pending', Invoice.status_id == 'partial'))
                  .filter(InvoicePenalty.invoice_id == None)
                  .all())
        return result

    def export_data_to_pdf(self, **kwargs):

        push_update(message=_('Processing'), **kwargs)
        param = json.loads(kwargs.get('param'))
        obj = self.preview_to_pdf(param['billing_id'], param['block_id'], param['village_id'])
        template = self.find_template(is_active=True)
        push_update(message=_('Finding template'), **kwargs)
        template = 'invoice/pdf_templates/' + template.value
        paths = []
        if len(obj) > 100:
            from PyPDF2 import PdfFileMerger

            merger = PdfFileMerger()
            lists = self.chunks(obj, 50)
            for list in lists:
                push_update(message=_("Generating File"), **kwargs)
                data = render_template(template, objects=list)

                path = self.export_pdf(data=data)
                merger.append(path['path'] + '\\' + path['name'])
                paths.append(path['path'] + '\\' + path['name'])
            push_update(message=_("Downloading File"), **kwargs)
            merger.write(path['path'] + '\\' + path['name'].split('.pdf')[0] + '_result.pdf')
            merger.close()
            url = request.url_root[:-1] + url_for('download_file',
                                                  filename=path['name'].split('.pdf')[0] + '_result.pdf')
            push_update(message=url, finish=True, **kwargs)

        else:
            push_update(message=_("Generating File"), **kwargs)
            data = render_template(template, objects=obj)
            push_update(message=_("Downloading File"), **kwargs)
            path = self.export_pdf(data)
            url = request.url_root[:-1] + url_for('download_file',
                                                  filename=path['name'])
            push_update(message=url, finish=True, **kwargs)

        # remove paths
        if paths:
            for path in paths:
                import os
                os.remove(path)

    def export_pdf(self, data):
        import os
        current_dir = os.path.dirname(os.path.realpath(__file__))
        target_dir = os.sep.join(current_dir.split(os.sep)[0:-2])
        css = os.sep.join([target_dir, 'share', 'static', 'styles', 'export.css'])
        output_path = os.path.join(app.config.get('SHARE_IMAGES'))
        # output_path = 'D:\\app'
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        # export pdf

        path = export_pdf(input=data, css=css, output_path=output_path)
        return path

    def chunks(self, l, n):
        """Yield successive n-sized chunks from l."""
        lists = []
        for i in range(0, len(l), n):
            lists.append(l[i:i + n])
        return lists

    def get_invoice_due_date(self, bill_id):
        invoice = db.query(Invoice.billing_id, Invoice.due_date).filter(Invoice.billing_id == bill_id).group_by(
            Invoice.billing_id, Invoice.due_date).order_by(desc(Invoice.due_date)).first()
        return invoice

    def update_invoice_due_date(self, billing_id, due_date):
        try:
            invoices = db.query(Invoice).filter(Invoice.is_active,
                                                Invoice.billing_id == billing_id, Invoice.is_active).all()
            mapping = []
            for invoice in invoices:
                mapping.append({'id': invoice.id, 'due_date': due_date})

            if mapping:
                db.bulk_update_mappings(Invoice, mapping)
                db.commit()
                return billing_id
        except Exception, e:
            db.rollback()
            return

    def get_receipts(self, invoice_id):
        from project.module.payment.model import PaymentDetail, Payment
        q = db.query(distinct(PaymentDetail.payment_id)).filter(PaymentDetail.invoice_id == invoice_id,
                                                                PaymentDetail.is_active)
        payments = db.query(Payment).filter(Payment.id.in_(q)).order_by(desc(Payment.created_date)).all()
        payment_method = {
            '10011': _('CASH'),
            '10012': _('SmartLuy'),
            '10013': _('E_Money')
        }
        data = []
        if payments:
            from project.module.payment.view.payment_view import lambda_total_amount
            for payment in payments:
                payment.payment_method = payment_method.get(payment.payment_method)
                total_amount = lambda_total_amount(payment)
                obj = dict(
                    id=payment.id,
                    code=payment.code,
                    total_amount=total_amount,
                    payment_method=payment.payment_method,
                    currency=payment.currency.sign,
                    pay_amount=payment.pay_amount,
                    farmer=payment.farmer_name,
                    pay_date=to_date(payment.pay_date),
                    ending_balance=payment.ending_balance
                )

                data.append(obj)
        if data:
            return json.dumps(data)
        return []


default = InvoiceLogic()
