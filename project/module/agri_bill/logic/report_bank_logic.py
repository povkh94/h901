import sqlalchemy
from sqlalchemy import cast, extract, distinct, case
from project.core.database import db
from project.module.agri_bill import enum
from project.module.currency.model import Currency
from project.module.payment.model import Payment, PaymentDetail, Invoice, Block, BankPaymentDetail, Farmer
from edf.base.logic import *


class ReportBankLogic(LogicBase):
    def __init__(self):
        self.__classname__ = Payment

    def report_daily_bank_payment(self, **kwargs):
        d1 = kwargs.get('d1') or datetime.now().date()
        d2 = kwargs.get('d2') or datetime.now().date().replace(day=1, year=datetime.now().year + 1) - timedelta(days=1)
        bank = kwargs.get('bank') or ''
        status = kwargs.get('status') or 'all'
        d2 = to_datetime(d2) + timedelta(days=1)
        q = (db.query(
            func.row_number().over(order_by=(BankPaymentDetail.pay_date)).label(
                '_row_number'),
            BankPaymentDetail.pay_date.label('date'),
            BankPaymentDetail.currency.label('currency'),
            func.cast(BankPaymentDetail.bank, String(50)).label('bank_name'),
            func.cast(BankPaymentDetail.branch, String(50)).label('bank_branch'),
            Currency.sign.label('currency_sign'),
            BankPaymentDetail.customer_id.label('customer'),
            Farmer.name.label('customer_name'),
            Farmer.spouse_name.label('spouse_name'),
            Farmer.code.label('code'),
            BankPaymentDetail.pay_amount.label('total_amount'),
            BankPaymentDetail.paid_amount.label('total_paid_amount'),
            func.sum(case([
                (BankPaymentDetail.pay_amount > BankPaymentDetail.paid_amount, BankPaymentDetail.pay_amount - BankPaymentDetail.paid_amount)],
                else_=0)).label('ending_balance'),
            # func.sum(BankPaymentDetail.pay_amount - BankPaymentDetail.paid_amount).label('ending_balance')
        )
             .join(Currency, Currency.id == BankPaymentDetail.currency)
             .join(Farmer, Farmer.id == BankPaymentDetail.customer_id)
             .filter(BankPaymentDetail.is_active,  BankPaymentDetail.result == enum.BankPaymentResult.CORRECT)
             .filter(BankPaymentDetail.pay_date >= d1, BankPaymentDetail.pay_date < d2))
        if status == 'pending':
           q = q.filter(BankPaymentDetail.paid_amount == 0)
        elif status == 'partial':
            q = q.filter(BankPaymentDetail.paid_amount < BankPaymentDetail.pay_amount, BankPaymentDetail.paid_amount!=0)
        elif status == 'paid':
            q = q.filter(BankPaymentDetail.pay_amount-BankPaymentDetail.paid_amount == 0)
        else:
            q = q

        if bank:
            q = q.filter(func.cast(BankPaymentDetail.bank, String(50)) == bank)
        q = q.group_by(
                        BankPaymentDetail.pay_date,
                        BankPaymentDetail.currency,
                        func.cast(BankPaymentDetail.bank, String(50)),
                        func.cast(BankPaymentDetail.branch, String(50)),
                        Currency.sign,
                        BankPaymentDetail.customer_id,
                        Farmer.name,
                        Farmer.spouse_name,
                        Farmer.code,
                        BankPaymentDetail.pay_amount,
                        BankPaymentDetail.paid_amount
            )

        return q



    def summary_detail_by_date(self, **kwargs):
        """
            summary bank payment receive by date
        """
        d1 = kwargs.get('d1') or datetime.now().date()
        d2 = kwargs.get('d2') or datetime.now().date().replace(day=1, year=datetime.now().year + 1) - timedelta(days=1)
        d2 = to_datetime(d2) + timedelta(days=1)
        bank = kwargs.get('bank') or ''
        q = (db.query(
            func.row_number().over(order_by=(extract('year', BankPaymentDetail.pay_date))).label(
                '_row_number'),
            (cast(extract('year', BankPaymentDetail.pay_date), String) + '-' + cast(extract('month', BankPaymentDetail.pay_date), String) + '-' + cast(extract('day', BankPaymentDetail.pay_date), String)).label('date'),
            BankPaymentDetail.currency.label('currency'),
            func.cast(BankPaymentDetail.bank, String(50)).label('bank_name'),
            func.cast(BankPaymentDetail.branch, String(50)).label('bank_branch'),
            Currency.sign.label('currency_sign'),
            func.count(BankPaymentDetail.customer_id).label('total_customers'),
            func.sum(BankPaymentDetail.paid_amount).label('total_paid_amount'),
        )
             .join(Currency, Currency.id == BankPaymentDetail.currency)
             .filter(BankPaymentDetail.is_active, BankPaymentDetail.result == enum.BankPaymentResult.CORRECT)
             .filter(BankPaymentDetail.pay_date >= d1, BankPaymentDetail.pay_date < d2))
        if bank:
            q = q.filter(func.cast(BankPaymentDetail.bank, String(50)) == bank)
        q = q.group_by(
            extract('year', BankPaymentDetail.pay_date),
            extract('month', BankPaymentDetail.pay_date),
            extract('day', BankPaymentDetail.pay_date),
            BankPaymentDetail.currency,
            Currency.sign,
            func.cast(BankPaymentDetail.bank, String(50)),
            func.cast(BankPaymentDetail.branch, String(50))
        )
        return q


    def summary_detail_by_bank(self, **kwargs):
        """
            summary bank payment receive by bank
        """
        d1 = kwargs.get('d1') or datetime.now().date()
        d2 = kwargs.get('d2') or datetime.now().date().replace(day=1, year=datetime.now().year + 1) - timedelta(days=1)
        d2 = to_datetime(d2) + timedelta(days=1)
        bank = kwargs.get('bank') or ''
        q = (db.query(
            func.row_number().over(order_by=(func.cast(BankPaymentDetail.bank, String(50)))).label(
                '_row_number'),
            BankPaymentDetail.currency.label('currency'),
            func.cast(BankPaymentDetail.bank, String(50)).label('bank_name'),
            func.cast(BankPaymentDetail.branch, String(50)).label('bank_branch'),
            Currency.sign.label('currency_sign'),
            func.count(BankPaymentDetail.customer_id).label('total_customers'),
            func.sum(BankPaymentDetail.paid_amount).label('total_paid_amount'),
        )
             .join(Currency, Currency.id == BankPaymentDetail.currency)
             .filter(BankPaymentDetail.is_active, BankPaymentDetail.result == enum.BankPaymentResult.CORRECT)
             .filter(BankPaymentDetail.pay_date >= d1, BankPaymentDetail.pay_date < d2))
        if bank:
            q = q.filter(func.cast(BankPaymentDetail.bank, String(50)) == bank)
        q = q.group_by(
            BankPaymentDetail.currency,
            Currency.sign,
            func.cast(BankPaymentDetail.bank, String(50)),
            func.cast(BankPaymentDetail.branch, String(50))
        )
        return q
default = ReportBankLogic()
