from sqlalchemy import distinct, update, bindparam
from flask_babel import gettext as _

from project.module.agri_bill import enum
from project.module.agri_bill.convert import round_riel
from project.module.agri_bill.logic import land_logic, invoice_logic, billing_cycle_logic, verify_billing_logic
from project.module.system.logic import autono_logic
from ...share.view.base_view import *
from ...agri_bill.model import *

from sqlalchemy import case


def push_update(message='', **kwargs):
    import time
    time.sleep(0.5)


class BillingLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = Billing

    def new(self, seq_id=0):
        seq_id = current.company().ext.get('seq', dict(invoice='0')).get('invoice-code', '0')
        obj = self.__classname__()
        if seq_id:
            obj.code = autono_logic.default.next_no(id=seq_id)
        obj.start_date = datetime.now()
        obj.end_date = obj.start_date + timedelta(days=30)
        return obj

    # find billing_station by billing for update uni cost and total cost
    def find_billing_station(self, **kwargs):
        billing_id = kwargs.get('id')
        new_data = kwargs.get('billing_stations')
        billing_stations = db.query(BillingStation).filter(BillingStation.billing_id == billing_id)
        unit_cost_map = dict()
        total_cost_map = dict()
        for item in new_data:
            unit_cost_map[item['station_id'], item['tariff_id']] = item['unit_cost']
            total_cost_map[item['station_id'], item['tariff_id']] = item['total_cost']
        for billing_station in billing_stations:
            billing_station.unit_cost = unit_cost_map[billing_station.station_id, billing_station.tariff_id]
            billing_station.total_cost = total_cost_map[billing_station.station_id, billing_station.tariff_id]
            db.add(billing_station)
        return db.flush()

    def update(self, obj, _commit=True):
        billing = self.find(obj.id)
        not_update = invoice_logic.default.check_invoice_status(billing_id=obj.id)
        if not not_update:
            billing.billing_cycle_id = obj.billing_cycle_id
            billing_station = self.find_billing_station(id=obj.id, billing_stations=obj.ext__billing_station)
            del obj.ext['billing_station']
            obj.ext = obj.ext

            # delete billing , invoice , invoice_detail and create new billing process
            if obj.billing_status_id == 'close' and billing.billing_cycle_id != obj.billing_cycle_id:
                invoices = db.query(Invoice).filter(Invoice.billing_id == obj.id)
                db.query(BillingLand).filter(BillingLand.billing_id == obj.id).delete()
                invoice_ids = []
                for invoice in invoices.all():
                    invoice_ids.append(invoice.id)
                invoice_detail = db.query(InvoiceDetail).filter(InvoiceDetail.invoice_id.in_(invoice_ids))
                invoice_detail.delete(synchronize_session=False)
                invoices = invoices.delete()
                param = {
                    'id': obj.id,
                    'issue_date': obj.start_date,
                    'due_date': obj.end_date
                }
                import json
                self.update_process_billing(param=json.dumps(param))
        billing.season_id = obj.season_id
        billing.name = obj.name
        billing.start_date = obj.start_date
        billing.end_date = obj.end_date
        db.add(billing)
        db.flush()
        db.commit()

    def add(self, obj):
        from ..logic import billing_cycle_logic
        current._gid = lambda: randomstring(8, 'ABCDEFGHIJKLMNOPQRSTUVWYZabcdefghijklmnopqrstuvwyz0123456789')
        billing_cycle = billing_cycle_logic.default.find(obj.billing_cycle_id)
        obj.season_id = billing_cycle.season_id
        obj.company_id = current.company_id()
        obj.contract_template_id = 'billing'
        db.add(obj)
        db.flush()
        for item in obj.ext__billing_station:
            itm_new = from_dict(item, to_cls=BillingStation, ignores=['id'], defaults={'billing_id': obj.id})
            db.add(itm_new)
            db.flush()
        del obj.ext['billing_station']
        obj.ext = obj.ext
        block_ids = billing_cycle_logic.default.get_existing_block(id=obj.billing_cycle_id)
        verify_obj = self.get_verify_billing(block_ids)

        verify_billing = []
        for item in verify_obj:
            verify = Verify()
            verify.id = gid()
            verify.billing_id = obj.id
            verify.station_id = item.station_id
            verify.block_id = item.block_id
            verify.farmer_id = item.farmer_id
            verify.status = enum.VerifyStatus.unverified
            verify.contract_number = str(obj.end_date.strftime(
                "%y")) + "-" + billing_cycle.ext__code + "-" + item.block_code + "-" + item.farmer_code  # contract number follow by year-block_code-farmer-code
            verify_billing.append(verify)
            db.flush()
        db.bulk_save_objects(verify_billing)
        db.commit()

    def remove(self, obj, _commit=True):
        if hasattr(obj, 'is_active'):
            obj.is_active = False
        else:
            db.remove(obj)
        if obj.billing_status_id == 'close':
            billing_lands = db.query(BillingLand).filter(BillingLand.billing_id == obj.id).all()
            mapping_billing_land = []
            for billing_land in billing_lands:
                mapping_billing_land.append({'id': billing_land.id, 'is_active': 0})
            invoices = db.query(Invoice).filter(Invoice.billing_id == obj.id).all()
            invoice_ids = []
            mapping_invoice = []
            for invoice in invoices:
                invoice_ids.append(invoice.id)
                mapping_invoice.append({'id': invoice.id, 'is_active': 0})
            invoice_details = db.query(InvoiceDetail).filter(InvoiceDetail.invoice_id.in_(invoice_ids))
            mapping_invoice_detail = []
            for invoice_detail in invoice_details:
                mapping_invoice_detail.append({'id': invoice_detail.id, 'is_active': 0})

            db.bulk_update_mappings(BillingLand, mapping_billing_land)
            db.bulk_update_mappings(Invoice, mapping_invoice)
            db.bulk_update_mappings(InvoiceDetail, mapping_invoice_detail)
        db.flush()
        db.commit()

    def get_billing_cycle_id(self, id):
        q = db.query(
            Billing.id,
            Billing.name,
            Billing.start_date,
            Billing.end_date,
            Billing.billing_cycle_id,
            BillingCycle.name.label('billing_cycle')
        ) \
            .join(BillingCycle, BillingCycle.id == Billing.billing_cycle_id) \
            .filter(Billing.id == id)
        return q.first()

    # update when change change land property
    def update_billing_station(self, obj):
        unit_cost_map = dict()
        total_cost_map = dict()
        billing_station = obj.billing_station
        billing_statation_ids = []
        if not billing_station and obj.ext__billing_station:
            billing_station = json.loads(obj.ext__billing_station)
        for itm in billing_station:
            if isinstance(itm, dict):
                itm = DictObject(itm)
            unit_cost_map[(itm.station_id, itm.tariff_id)] = itm.unit_cost
            total_cost_map[(itm.station_id, itm.tariff_id)] = itm.total_cost
            billing_statation_ids.append(itm.id)
            # db.query(BillingStation).filter(BillingStation.id == itm.id).delete()

        # block_ids = billing_cycle_logic.default.get_existing_block(id=obj.billing_cycle_id)
        # find block in verify billing
        block_ids = db.query(Verify.block_id).filter(Verify.billing_id == obj.id).group_by(Verify.block_id).all()
        block_ids = [o[0] for o in block_ids]
        sum_land_area = land_logic.default.sum_land_area(block_ids) or []
        billing_stations = []
        for item in sum_land_area:
            billing_station = BillingStation()
            billing_station.billing_id = obj.id
            billing_station.station_id = item['station_id']
            billing_station.total_farmer = item['total_farmer']
            billing_station.total_land = item['total_land']
            billing_station.total_area = item['total_area']
            billing_station.tariff_id = item['tariff_id']
            # add object to ext_data tha have key station_name
            billing_station.ext__station_name = item['station_name']
            billing_station.ext__tariff_name = item['tariff_name']
            billing_station.ext = billing_station.ext
            try:
                billing_station.unit_cost = unit_cost_map[(item['station_id'], item['tariff_id'])]
                billing_station.total_cost = billing_station.unit_cost * item['total_area']
                # billing_station.total_cost = total_cost_map[(item['station_id'], item['tariff_id'])]
            except:
                billing_station.unit_cost = 0
                billing_station.total_cost = 0

            if billing_station.unit_cost == 0:
                unit_cost = db.query(Tariff).filter(Tariff.id == item['tariff_id']).first()
                billing_station.unit_cost = unit_cost.price
                billing_station.total_cost = unit_cost.price * item['total_area']

            billing_stations.append(billing_station)

        db.bulk_save_objects(billing_stations)

        data = json.dumps(billing_stations)
        obj.ext = ({'billing_station': data})
        obj.ext_data = json.dumps(obj.ext)
        db.add(obj)
        db.commit()

        if billing_statation_ids:
            db.query(BillingStation).filter(BillingStation.id.in_(billing_statation_ids)).delete(
                synchronize_session=False)
            db.commit()
        try:
            # db.expunge_all()
            # db.close()
            pass
        except:
            pass
        obj = self.find(obj.id)
        return obj

    # process billing
    def process_billing(self, id, issue_date, due_date):
        status = verify_billing_logic.default.check_status(billing_id=id)
        if status == 'unverified':
            billing_status = self.billing_status(id=id, status='start')
            view = {'title': status}
        else:
            current._gid = lambda: randomstring(8, 'ABCDEFGHIJKLMNOPQRSTUVWYZabcdefghijklmnopqrstuvwyz0123456789')
            q_verify_block = db.query(Verify.block_id).filter(Verify.billing_id == id).group_by(Verify.block_id)
            # query
            q1 = (
                db.query
                    (
                    BillingStation.billing_id.label('billing_id'),
                    BillingStation.station_id.label('station_id'),
                    BillingStation.unit_cost.label('unit_cost'),
                    BillingStation.tariff_id.label('tariff_id'),
                    Block.id.label('block_id'),
                    Block.name.label('block_name'),
                    Land.id.label('land_id'),
                    Land.area.label('area'),
                    case(
                        [(or_(Land.rent_id == None, Land.rent_id == ''), Land.farmer_id)], else_=Land.rent_id).label(
                        'invoice_farmer_id'),
                    Land.status_id.label('status_id'),
                    Land.farmer_id.label('owner_farmer_id')
                )
                    .join(Station, Station.id == BillingStation.station_id)
                    .join(Block, Block.station_id == Station.id)
                    .join(Billing, Billing.id == BillingStation.billing_id)
                    .join(Land, Land.block_id == Block.id)
                    .join(Tariff, Tariff.id == Land.tariff_id)
                    .join(Farmer, Farmer.id == Land.farmer_id)
                    .filter(BillingStation.billing_id == id)
                    .filter(BillingStation.tariff_id == Tariff.id)
                    .filter(Block.id.in_(q_verify_block))
                    .filter(Land.tariff_id == Tariff.id)
                    .filter(Land.status_id == 'irrigate')
                    .filter(Land.active == 'active')
                    .filter(Farmer.is_active)
                    .filter(Farmer.active == 'active')
                    .filter(Land.is_active).subquery()
            )
            query = (db.query(
                q1.c.billing_id,
                q1.c.station_id,
                q1.c.unit_cost,
                q1.c.tariff_id,
                q1.c.block_id,
                q1.c.block_name,
                q1.c.land_id,
                q1.c.area,
                q1.c.invoice_farmer_id,
                q1.c.status_id,
                q1.c.owner_farmer_id,
                Farmer.id.label('farmer_id'),
                Farmer.name.label("farmer_name"),
                Farmer.village_id.label('village_id')
            ).join(Farmer, Farmer.id == q1.c.invoice_farmer_id).subquery())

            # q2=q_invoice_detail
            q2 = (
                db.query(
                    query.c.station_id,
                    query.c.invoice_farmer_id,
                    query.c.owner_farmer_id,
                    query.c.village_id,
                    query.c.block_id,
                    query.c.block_name,
                    query.c.unit_cost,
                    query.c.tariff_id,
                    query.c.billing_id,
                    func.count(query.c.land_id).label('total_land'),
                    func.sum(query.c.area).label('quantity'),
                    func.sum(query.c.area * query.c.unit_cost).label('line_total')
                ).group_by(query.c.station_id,
                           query.c.invoice_farmer_id,
                           query.c.owner_farmer_id,
                           query.c.village_id,
                           query.c.block_id, query.c.block_name,
                           query.c.unit_cost,
                           query.c.tariff_id,
                           query.c.billing_id
                           ).subquery()
            )
            # q3 = q_invoice
            q3 = (db.query(
                q2.c.station_id,
                q2.c.block_id,
                q2.c.village_id,
                q2.c.invoice_farmer_id,
                q2.c.owner_farmer_id,
                func.sum(q2.c.total_land).label('total_lands'),
                func.sum(q2.c.quantity).label('total_area'),
                func.sum(q2.c.line_total).label('total_amount')
            ).group_by(
                q2.c.station_id,
                q2.c.block_id,
                q2.c.village_id,
                q2.c.invoice_farmer_id,
                q2.c.owner_farmer_id
            ).all())

            invoices = []
            invoice_ids_map = dict()
            for item in q3:
                seq_id = current.company().ext.get('seq', dict(invoice='')).get('invoice-code', '')
                invoice = Invoice()
                invoice.id = gid()
                if seq_id:
                    invoice.code = autono_logic.default.next_no(id=seq_id, update=True)
                invoice.billing_id = id
                invoice.company_id = current.company_id()
                invoice.station_id = item.station_id
                invoice.block_id = item.block_id
                invoice.farmer_id = item.invoice_farmer_id
                invoice.owner_farmer_id = item.owner_farmer_id
                invoice_ids_map[
                    (item.station_id, item.block_id, item.invoice_farmer_id, item.owner_farmer_id)] = invoice.id
                invoice.village_id = item.village_id
                invoice.total_lands = item.total_lands
                invoice.total_area = item.total_area
                invoice.total_amount = round_riel(item.total_amount)
                invoice.paid_amount = 0
                invoice.currency_id = 'KHR'
                invoice.status_id = 'pending'
                invoice.invoice_type = enum.InvoiceType.IRRIGATE
                invoice.issue_date = issue_date
                invoice.due_date = due_date
                contract_number = verify_billing_logic.default.get_contract_number(id, item.block_id,
                                                                                   item.owner_farmer_id)
                try:
                    invoice.contract_number = contract_number[0]
                except Exception as e:
                    invoice.contract_number = None
                invoices.append(invoice)
            db.bulk_save_objects(invoices)

            invoice_details = []
            invoice_detail_id_map = dict()
            for item in db.query(q2).all():
                invoice_detail = InvoiceDetail()
                invoice_detail.id = gid()
                invoice_detail_id_map[
                    (item.station_id, item.block_id, item.invoice_farmer_id, item.owner_farmer_id,
                     item.tariff_id)] = invoice_detail.id
                invoice_detail.invoice_id = invoice_ids_map[
                    (item.station_id, item.block_id, item.invoice_farmer_id, item.owner_farmer_id)]
                invoice_detail.block_id = item.block_id
                invoice_detail.item_id = item.block_id
                invoice_detail.item_name = item.block_name
                invoice_detail.tariff_id = item.tariff_id
                invoice_detail.total_land = item.total_land
                invoice_detail.price = item.unit_cost
                invoice_detail.quantity = item.quantity
                invoice_detail.line_total = round_riel(item.line_total)
                invoice_details.append(invoice_detail)
            db.bulk_save_objects(invoice_details)

            billing_lands = []
            for item in db.query(query).all():
                billing_land = BillingLand()
                billing_land.billing_id = item.billing_id
                billing_land.land_id = item.land_id
                billing_land.block_id = item.block_id
                billing_land.station_id = item.station_id
                billing_land.invoice_farmer_id = item.invoice_farmer_id
                billing_land.tariff_id = item.tariff_id
                billing_land.owner_farmer_id = item.owner_farmer_id
                billing_land.invoice_detail_id = invoice_detail_id_map[
                    (item.station_id, item.block_id, item.invoice_farmer_id, item.owner_farmer_id, item.tariff_id)]
                billing_land.area = item.area
                billing_land.unit_cost = item.unit_cost
                if item.status_id == 'irrigate':
                    billing_land.total_cost = item.area * item.unit_cost
                else:
                    billing_land.total_cost = 0
                billing_lands.append(billing_land)
            db.bulk_save_objects(billing_lands)

            # update verify billing
            lands = verify_billing_logic.default.find_lands(billing_id=id)
            verify_ids_mapping = dict()
            for o in lands:
                try:
                    if not verify_ids_mapping[(str(o.verify_billing_id))]:
                        verify_ids_mapping[(str(o.verify_billing_id))] = []
                except:
                    verify_ids_mapping[(str(o.verify_billing_id))] = []

                verify_ids_mapping[(str(o.verify_billing_id))] += [
                    dict(
                        land_id=o.land_id,
                        land_code=o.land_code,
                        land_area=o.area,
                        land_status_id=o.land_status_id,
                        farmer_id=o.farmer_id,
                        farmer_code=o.farmer_code,
                        farmer_spouse_name=o.spouse_name,
                        farmer_name=o.farmer_name,
                        block_code=o.block_code,
                        block_name=o.block_name,
                        tariff_name=o.tariff_name,
                        tariff_id=o.tariff_id,
                        tariff_price=o.tariff_price,
                        rent_name=o.rent_name,
                        rent_code=o.rent_code,
                        rent_spouse_name=o.rent_spouse_name
                    )
                ]
            verify_billings = db.query(Verify).filter(Verify.billing_id == id).all()
            verify_mapping = []
            for verify_billing in verify_billings:
                ext_data = None
                try:
                    ext_data = verify_ids_mapping[(verify_billing.id)]
                except:
                    ext_data = None
                    verify_mapping.append(
                        dict(id=str(verify_billing.id), is_active=0)
                    )
                if ext_data:
                    verify_mapping.append(
                        dict(id=str(verify_billing.id), ext_data=json.dumps({'lands': ext_data}))
                    )
            db.bulk_update_mappings(Verify, verify_mapping)
            db.commit()
            return id

    def update_process_billing(self, **kwargs):
        param = json.loads(kwargs.get('param'))
        id = param['id']
        status = verify_billing_logic.default.check_status(billing_id=id)
        if status == 'unverified':
            billing_status = self.billing_status(id=id, status='start')
        else:
            current._gid = lambda: randomstring(8, 'ABCDEFGHIJKLMNOPQRSTUVWYZabcdefghijklmnopqrstuvwyz0123456789')
            # query
            q1 = (
                db.query
                    (
                    BillingStation.billing_id.label('billing_id'),
                    BillingStation.station_id.label('station_id'),
                    BillingStation.unit_cost.label('unit_cost'),
                    BillingStation.tariff_id.label('tariff_id'),
                    Block.id.label('block_id'),
                    Block.name.label('block_name'),
                    Land.id.label('land_id'),
                    Land.area.label('area'),
                    case(
                        [(or_(Land.rent_id == None, Land.rent_id == ''), Land.farmer_id)], else_=Land.rent_id).label(
                        'invoice_farmer_id'),
                    Land.status_id.label('status_id'),
                    Land.farmer_id.label('owner_farmer_id')
                )
                    .join(Station, Station.id == BillingStation.station_id)
                    .join(Block, Block.station_id == Station.id)
                    .join(Billing, Billing.id == BillingStation.billing_id)
                    .join(BillingCycleBlock, BillingCycleBlock.billing_cycle_id == Billing.billing_cycle_id)
                    .join(Land, Land.block_id == Block.id)
                    .join(Tariff, Tariff.id == Land.tariff_id)
                    .filter(BillingStation.billing_id == id)
                    .filter(BillingCycleBlock.block_id == Block.id)
                    .filter(BillingStation.tariff_id == Tariff.id)
                    .filter(Land.tariff_id == Tariff.id)
                    .filter(Land.status_id == 'irrigate')
                    .filter(Land.is_active).subquery()
            )
            query = (db.query(
                q1.c.billing_id,
                q1.c.station_id,
                q1.c.unit_cost,
                q1.c.tariff_id,
                q1.c.block_id,
                q1.c.block_name,
                q1.c.land_id,
                q1.c.area,
                q1.c.invoice_farmer_id,
                q1.c.status_id,
                q1.c.owner_farmer_id,
                Farmer.id.label('farmer_id'),
                Farmer.name.label("farmer_name"),
                Farmer.village_id.label('village_id')
            ).join(Farmer, Farmer.id == q1.c.invoice_farmer_id).subquery())

            # q2=q_invoice_detail
            q2 = (
                db.query(
                    query.c.station_id,
                    query.c.invoice_farmer_id,
                    query.c.owner_farmer_id,
                    query.c.village_id,
                    query.c.block_id,
                    query.c.block_name,
                    query.c.unit_cost,
                    query.c.tariff_id,
                    query.c.billing_id,
                    func.count(query.c.land_id).label('total_land'),
                    func.sum(query.c.area).label('quantity'),
                    func.sum(query.c.area * query.c.unit_cost).label('line_total')
                ).group_by(query.c.station_id,
                           query.c.invoice_farmer_id,
                           query.c.owner_farmer_id,
                           query.c.village_id,
                           query.c.block_id, query.c.block_name,
                           query.c.unit_cost,
                           query.c.tariff_id,
                           query.c.billing_id
                           ).subquery()
            )
            # q3 = q_invoice
            q3 = (db.query(
                q2.c.station_id,
                q2.c.block_id,
                q2.c.village_id,
                q2.c.invoice_farmer_id,
                q2.c.owner_farmer_id,
                func.sum(q2.c.total_land).label('total_lands'),
                func.sum(q2.c.quantity).label('total_area'),
                func.sum(q2.c.line_total).label('total_amount')
            ).group_by(
                q2.c.station_id,
                q2.c.block_id,
                q2.c.village_id,
                q2.c.invoice_farmer_id,
                q2.c.owner_farmer_id
            ).all())
            invoices = []
            invoice_ids_map = dict()
            for item in q3:
                seq_id = current.company().ext.get('seq', dict(invoice='')).get('invoice-code', '')
                invoice = Invoice()
                invoice.id = gid()
                if seq_id:
                    invoice.code = autono_logic.default.next_no(id=seq_id, update=True)
                invoice.billing_id = id
                invoice.company_id = current.company_id()
                invoice.station_id = item.station_id
                invoice.block_id = item.block_id
                invoice.farmer_id = item.invoice_farmer_id
                invoice.owner_farmer_id = item.owner_farmer_id
                invoice_ids_map[
                    (item.station_id, item.block_id, item.invoice_farmer_id, item.owner_farmer_id)] = invoice.id
                invoice.village_id = item.village_id
                invoice.total_lands = item.total_lands
                invoice.total_area = item.total_area
                invoice.total_amount = round_riel(item.total_amount)
                invoice.paid_amount = 0
                invoice.currency_id = 'KHR'
                invoice.status_id = 'pending'
                invoice.invoice_type = enum.InvoiceType.IRRIGATE
                invoice.issue_date = param['issue_date']
                invoice.due_date = param['due_date']
                contract_number = verify_billing_logic.default.get_contract_number(id, item.block_id,
                                                                                   item.owner_farmer_id)
                invoice.contract_number = contract_number
                invoices.append(invoice)
            db.bulk_save_objects(invoices)

            invoice_details = []
            invoice_detail_id_map = dict()
            for item in db.query(q2).all():
                invoice_detail = InvoiceDetail()
                invoice_detail.id = gid()
                invoice_detail_id_map[
                    (item.station_id, item.block_id, item.invoice_farmer_id, item.owner_farmer_id,
                     item.tariff_id)] = invoice_detail.id
                invoice_detail.invoice_id = invoice_ids_map[
                    (item.station_id, item.block_id, item.invoice_farmer_id, item.owner_farmer_id)]
                invoice_detail.block_id = item.block_id
                invoice_detail.item_id = item.block_id
                invoice_detail.item_name = item.block_name
                invoice_detail.tariff_id = item.tariff_id
                invoice_detail.total_land = item.total_land
                invoice_detail.price = item.unit_cost
                invoice_detail.quantity = item.quantity
                invoice_detail.line_total = round_riel(item.line_total)
                invoice_details.append(invoice_detail)
            db.bulk_save_objects(invoice_details)

            billing_lands = []
            for item in db.query(query).all():
                billing_land = BillingLand()
                billing_land.billing_id = item.billing_id
                billing_land.land_id = item.land_id
                billing_land.block_id = item.block_id
                billing_land.station_id = item.station_id
                billing_land.invoice_farmer_id = item.invoice_farmer_id
                billing_land.tariff_id = item.tariff_id
                billing_land.owner_farmer_id = item.owner_farmer_id
                billing_land.invoice_detail_id = invoice_detail_id_map[
                    (item.station_id, item.block_id, item.invoice_farmer_id, item.owner_farmer_id, item.tariff_id)]
                billing_land.area = item.area
                billing_land.unit_cost = item.unit_cost
                if item.status_id == 'irrigate':
                    billing_land.total_cost = item.area * item.unit_cost
                else:
                    billing_land.total_cost = 0
                billing_lands.append(billing_land)
            db.bulk_save_objects(billing_lands)

            verify_billings = db.query(Verify).filter(Verify.billing_id == id).all()
            for verify_billing in verify_billings:
                verify_billing.is_active = 0
                db.add(verify_billing)
                db.flush()

    def count_invoice(self, billing_id):
        count_invoice = (
            db.query(
                Invoice.billing_id.label('billing_id'),
                Invoice.issue_date.label('issue_date'),
                Invoice.due_date.label('due_date'),
                func.count(distinct(Invoice.id)).label('total_invoices'),
                func.count(distinct(Invoice.farmer_id)).label('total_farmers'),
                func.sum(Invoice.total_lands).label('total_lands'),
                func.sum(Invoice.total_area).label('total_areas'),
                func.sum(Invoice.total_amount).label('total_amounts'),
            )
                .filter(Invoice.billing_id == billing_id)
                .group_by(Invoice.billing_id, Invoice.issue_date, Invoice.due_date).first()
        )

        billing = db.query(Billing).filter(Billing.id == count_invoice.billing_id).first()
        billing.billing_status_id = 'close'
        db.add(billing)
        db.commit()
        return count_invoice

    # find all invoice group by billing and block
    def invoice_by_block(self, billing_id):
        result = (
            db.query(
                Invoice.billing_id,
                Invoice.block_id.label('block_id'),
                func.count(distinct(Invoice.id)).label('total_invoices'),
                Block.name.label('block_name'),
                Block.code,
                Station.code,
                func.sum(Invoice.total_lands).label('total_lands'),
                func.sum(Invoice.total_area).label('total_areas'),
                func.sum(Invoice.total_amount).label('total_amounts'),
            )
                .join(Block, Block.id == Invoice.block_id)
                .join(Station, Station.id == Invoice.station_id)
                .filter(Invoice.billing_id == billing_id)
                .order_by(Station.code, Block.code)
                .group_by(Invoice.billing_id, Invoice.block_id, Block.name, Station.code, Block.code).all()
        )
        return result

        # find station block and farmer by billing_cycle_block
        # in billing_cycle from billing

        # find all invoice group by billing and village

    # find all invoice group by billing and station
    def invoice_by_station(self, billing_id):
        result = (
            db.query(
                Invoice.billing_id,
                Invoice.block_id.label('block_id'),
                func.count(distinct(Invoice.id)).label('total_invoices'),
                Station.name.label('station_name'),
                Station.code,
                func.sum(Invoice.total_lands).label('total_lands'),
                func.sum(Invoice.total_area).label('total_areas'),
                func.sum(Invoice.total_amount).label('total_amounts'),
            )
                .join(Station, Station.id == Invoice.station_id)
                .filter(Invoice.billing_id == billing_id)
                .order_by(Station.code)
                .group_by(Invoice.billing_id, Station.code).all()
        )
        return result

    # find station block and farmer by billing_cycle_block
    # in billing_cycle from billing
    # find all invoice group by billing and village
    def invoice_by_village(self, billing_id):
        result = (
            db.query(
                Invoice.billing_id,
                Invoice.village_id,
                func.count(distinct(Invoice.id)).label('total_invoices'),
                Location.name.label('village_name'),
                func.sum(Invoice.total_lands).label('total_lands'),
                func.sum(Invoice.total_area).label('total_areas'),
                func.sum(Invoice.total_amount).label('total_amounts'),
            )
                .join(Location, Location.id == Invoice.village_id)
                .filter(Invoice.billing_id == billing_id)
                .group_by(Invoice.billing_id, Invoice.village_id, Location.name).all()
        )
        return result

        # find station block and farmer by billing_cycle_block
        # in billing_cycle from billing

    # def get_verify_billing(self, block_ids):
    #     q = (
    #         db.query
    #             (
    #             Block.station_id.label('station_id'),
    #             Block.id.label('block_id'),
    #             Land.farmer_id.label('farmer_id')
    #         ).join(Land, Land.block_id == Block.id)
    #             .filter(Land.block_id.in_(block_ids))
    #             .filter(Land.is_active)
    #             .filter(Land.active == 'active')
    #             .group_by(Block.station_id, Block.id, Land.farmer_id)
    #     )
    #     result = q.all()
    #     return result

    def get_verify_billing(self, block_ids):
        q1 = (
            db.query
                (
                Block.station_id.label('station_id'),
                Block.id.label('block_id'),
                Block.code.label('block_code'),
                Land.farmer_id.label('farmer_id'),
                Farmer.code.label('farmer_code')
            ).join(Land, Land.block_id == Block.id)
                .join(Farmer, Farmer.id == Land.farmer_id)
                .filter(Farmer.active == 'active', Land.active == 'active')
                .filter(Land.block_id.in_(block_ids))
                .group_by(Block.station_id, Block.id, Block.code, Land.farmer_id, Farmer.code)
        )
        q2 = (
            db.query
                (
                Block.station_id.label('station_id'),
                Block.id.label('block_id'),
                Block.code.label('block_code'),
                Land.rent_id.label('farmer_id'),
                Farmer.code.label('farmer_code')
            ).join(Land, Land.block_id == Block.id)
                .join(Farmer, Farmer.id == Land.rent_id)
                .filter(Farmer.active == 'active', Land.active == 'active')
                .filter(Land.block_id.in_(block_ids))
                .group_by(Block.station_id, Block.id, Block.code, Land.rent_id, Farmer.code)
        )
        q3 = q1.union(q2).subquery()
        q3 = db.query(
            q3.c.station_id,
            q3.c.block_id,
            q3.c.block_code,
            q3.c.farmer_id,
            q3.c.farmer_code
        ).distinct()
        result = q3.all()
        return result

    def billing_status(self, id, status):
        billing = self.find(id)
        if status == 'unverified':
            billing.billing_status_id = status
        else:
            billing.billing_status_id = status
        db.add(billing)
        db.commit()

    # find land that non irrigated in billing
    def find_non_irrigated(self, billing_id):
        q = (db.query(
            BillingLand.billing_id,
            Station.name.label('station_name'),
            Block.name.label('block_name'),
            func.count(BillingLand.land_id).label('total_lands'),
            func.sum(BillingLand.area).label('total_areas')
        )
             .join(Station, Station.id == BillingLand.station_id)
             .join(Block, Block.id == BillingLand.block_id)
             .filter(BillingLand.billing_id == billing_id)
             .filter(BillingLand.total_cost == 0)
             .group_by(BillingLand.billing_id, Station.name, Block.name))

        return q.all()

    def find_billing(self):
        q = db.query(Billing).filter(Billing.billing_status_id == 'close', Billing.is_active)
        return q.order_by(desc(Billing.created_date))

    def update_billing(self, obj, _commit=True):
        billing = self.find(obj.id)
        # billing.billing_cycle_id = obj.billing_cycle_id
        billing.name = obj.name
        billing.start_date = obj.start_date
        billing.end_date = obj.end_date
        db.add(billing)
        db.commit()
        return obj.id

    def view_billing_land_by(self, block_id, farmer_id):
        q = (db.query(
            BillingLand)
             .filter(BillingLand.block_id == block_id)
             .filter(BillingLand.owner_farmer_id == farmer_id)
             .filter(BillingLand.is_active)).all()
        q = (db.query(Land).filter(Land.id.in_()))

        # .order_by(func.right('000000' + Land.code, 6)))
        return q


default = BillingLogic()
