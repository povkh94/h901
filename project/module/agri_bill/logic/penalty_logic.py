from project.module.agri_bill.model import Tariff, Penalty
from edf.base.logic import *


class PenaltyLogic(LogicBase):

    def __init__(self, *args, **kw_args):
        self.__classname__ = Penalty

    def new(self):
        penalty = Penalty()
        return penalty

    def find_penalty_type(self):
        return db.query(Penalty).first()


default = PenaltyLogic()




