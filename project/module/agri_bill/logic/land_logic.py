from ..model import *
from flask_babel import gettext as _
from edf.base.logic import *
from project.module.currency.model import Currency
from sqlalchemy import cast, literal, case


class LandLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = Land

    def new(self):
        """
        :rtype : Land
        """
        land = Land()
        return land

    def search(self, **kwargs):
        search = kwargs.get('search') or ''
        block_id = kwargs.get('block_id') or None
        tariff_id = kwargs.get('tariff_id') or None
        active = kwargs.get('active') or 'active'
        status_id = kwargs.get('status_id') or None

        user = current.user()
        q = db.query(Land).filter(Land.is_active)
        if search:
            search = search.strip()
            farmers = (
                db.query(Farmer)
                    .filter(
                    or_(
                        func.lower(Farmer.code).like('%' + search.lower() + '%'),
                        func.lower(Farmer.spouse_name).like('%' + search.lower() + '%'),
                        func.lower(Farmer.name).like('%' + search.lower() + '%'))
                ).all()
            )
            if farmers:
                farmer_ids = []
                rent_id = []
                for farmer in farmers:
                    farmer_ids.append(farmer.id)
                q = q.filter(or_(Land.farmer_id.in_(farmer_ids), Land.rent_id.in_(farmer_ids), Land.code == search))
            else:
                q = q.filter(
                    or_(
                        func.lower(Land.code).like('%' + search.lower() + '%'),
                    )
                )

        if user.ext__block_ids:
            block_ids = user.ext__block_ids.split(';')
            q = q.filter(Land.block_id.in_(block_ids))
        if block_id and not user.ext__block_ids:
            q = q.filter(Land.block_id == block_id)
        if tariff_id:
            q = q.filter(Land.tariff_id == tariff_id)
        if status_id:
            q = q.filter(Land.status_id == status_id)
        if active:
            q = q.filter(Land.active == active)
        q = q.join(Farmer, Farmer.id == Land.farmer_id) \
            .join(Tariff, Tariff.id == Land.tariff_id).filter(
            Farmer.is_active,
            Tariff.is_active,
            Farmer.active == 'active').order_by(asc(Farmer.name), Farmer.code)
        q = q.order_by(func.right('000000' + Land.code, 6))
        return q

    def find_lands(self, **kwargs):
        search = kwargs.get('search') or ''
        q = self.actives()
        if search:
            #     q = q.filter(
            #         or_(
            #             func.lower(Land.code).like('%' + search.lower() + '%'),
            #         )
            #     )
            q = q.filter(Land.code == search.lower())
        return q

    def search_tariff(self):
        q = db.query(Tariff).filter(Tariff.is_active)
        return q

    def sum_land_area(self, block_ids='', query='', land_ids=''):
        # type: (object, object, object) -> object
        if block_ids:
            # farmer
            q1 = (db.query(
                Land.farmer_id.label('farmer_id')
            )
                  .join(Farmer, Farmer.id == Land.farmer_id)
                  .filter(Land.active == 'active')
                  .filter(Land.is_active)
                  .filter(Land.status_id == 'irrigate')
                  .filter(or_(Land.rent_id == Land.farmer_id, Land.rent_id == None, Land.rent_id == '',
                              Land.rent_id == '__None'))
                  .filter(Land.block_id.in_(block_ids))
                  .filter(Farmer.active == 'active', Farmer.is_active)
                  )
            # rent
            q2 = (db.query(
                Land.rent_id.label('farmer_id')
            )
                  .join(Farmer, Farmer.id == Land.rent_id)
                  .filter(Land.active == 'active')
                  .filter(Land.status_id == 'irrigate')
                  .filter(Land.is_active)
                  .filter(and_(Land.farmer_id != Land.rent_id, Land.farmer_id != None, Land.farmer_id != '',
                               Land.farmer_id != '__None'))
                  .filter(Land.block_id.in_(block_ids))
                  .filter(Farmer.active == 'active', Farmer.is_active)
                  ).distinct()
            q3 = q1.union(q2).subquery()
            q3 = db.query(q3.c.farmer_id).distinct().subquery()

            # sum owner land
            q_owner = (db.query(
                Tariff.id.label('tariff_id'),
                Tariff.name.label('tariff_name'),
                Tariff.price.label('unit_cost'),
                Farmer.id.label('farmer_id'),
                Station.id.label('station_id'),
                Station.name.label('station_name'),
                func.sum(Land.area).label('area'),
                func.count(Land.id).label('land')
            )
                       .join(Land, Land.tariff_id == Tariff.id)
                       .join(Farmer, Farmer.id == Land.farmer_id)
                       .join(Block, Block.id == Land.block_id)
                       .join(Station, Station.id == Block.station_id)
                       .filter(Land.is_active, Land.active == 'active', Land.status_id == 'irrigate')
                       .filter(
                or_(Land.rent_id == Land.farmer_id, Land.rent_id == '', Land.rent_id == None, Land.rent_id == '__None'))
                       .filter(Land.farmer_id.in_(q3))
                       .filter(Farmer.is_active, Farmer.active == 'active')
                       .filter(Tariff.is_active, Block.is_active, Station.is_active)
                        .filter(Land.block_id.in_(block_ids))
                       .group_by(Tariff.id, Tariff.name, Tariff.price, Farmer.id, Station.id, Station.name)
                       )
            # test = db.query(q_owner.c.tariff_id).distinct()
            OwnFarmer = aliased(Farmer, name='parent_location')
            q_rent = (db.query(
                Tariff.id.label('tariff_id'),
                Tariff.name.label('tariff_name'),
                Tariff.price.label('unit_cost'),
                Farmer.id.label('farmer_id'),
                Station.id.label('station_id'),
                Station.name.label('station_name'),
                func.sum(Land.area).label('area'),
                func.count(Land.id).label('land')
            )
                      .join(Land, Land.tariff_id == Tariff.id)
                      .join(Farmer, Farmer.id == Land.rent_id)
                      .join(OwnFarmer,OwnFarmer.id == Land.farmer_id)
                      .join(Block, Block.id == Land.block_id)
                      .join(Station, Station.id == Block.station_id)
                      .filter(Land.is_active, Land.active == 'active', Land.status_id == 'irrigate')
                      .filter(Land.rent_id.in_(q3))
                      .filter(Land.farmer_id != Land.rent_id, Land.farmer_id != '', Land.farmer_id != None,
                              Land.farmer_id != '__None')
                      .filter(Land.block_id.in_(block_ids))
                      .filter(Farmer.is_active, Farmer.active == 'active')
                      .filter(OwnFarmer.is_active, OwnFarmer.active == 'active')
                      .filter(Tariff.is_active, Block.is_active, Station.is_active)
                      .group_by(Tariff.id, Tariff.name, Tariff.price, Farmer.id, Station.id, Station.name)
                      )

            # test = db.query(q_rent.c.tariff_id).distinct()
            q = q_owner.union(q_rent).subquery()

            q2 = db.query(
                q.c.tariff_id,
                q.c.tariff_name,
                q.c.unit_cost,
                q.c.station_id,
                q.c.station_name,
                q.c.farmer_id.label('farmer_id'),
                func.sum(q.c.area).label('area'),
                func.sum(q.c.land).label('land')
            ).group_by(
                q.c.tariff_id,
                q.c.tariff_name,
                q.c.unit_cost,
                q.c.farmer_id,
                q.c.station_id, q.c.station_name).subquery()

            query = db.query(
                q2.c.tariff_id,
                q2.c.tariff_name,
                q2.c.unit_cost,
                q2.c.station_id,
                q2.c.station_name,
                func.sum(q2.c.area).label('total_area'),
                func.sum(q2.c.land).label('total_land'),
                func.count(q2.c.farmer_id).label('total_farmer')
            ).group_by(q2.c.tariff_id,
                       q2.c.tariff_name,
                       q2.c.unit_cost,
                       q2.c.station_id,
                       q2.c.station_name
                       )
            result = query.all()

            # q = (
            #     db.query
            #         (
            #         func.sum(Land.area).label('total_area'),
            #         func.count(Land.id).label('total_land'),
            #         func.count(Land.farmer_id.distinct()).label('total_farmer'),
            #         Station.id.label('station_id'),
            #         Station.name.label('station_name'),
            #         Land.tariff_id.label('tariff_id'),
            #         Tariff.name.label('tariff_name'),
            #         Tariff.price.label('unit_cost')
            #     )
            #         .join(Block, Block.id == Land.block_id)
            #         .join(Station, Station.id == Block.station_id)
            #         .join(Tariff, Tariff.id == Land.tariff_id)
            #         .join(Farmer, Farmer.id == Land.farmer_id)
            #         .filter(Block.is_active)
            #         .filter(Farmer.is_active)
            #         .filter(Land.block_id.in_(block_ids))
            #         .filter(Tariff.is_active)
            #         .filter(Farmer.is_active)
            #         .filter(Farmer.active == 'active')
            #         .filter(Land.tariff_id == Tariff.id)
            #         .filter(Land.active == 'active')
            #         .filter(Land.status_id == 'irrigate', Land.is_active)
            #         .filter(Farmer.active == 'active')
            #         .filter(Land.farmer_id != '', Land.farmer_id != None)
            #
            #         .group_by(Station.id, Station.name, Land.tariff_id, Tariff.name, Tariff.price))

            result = [r._asdict() for r in result]
            return result
        if query:
            query = query.subquery()
            q = db.query(
                func.sum(query.c.area).label('total_area')).filter(query.c.status_id == 'irrigate').group_by(
                query.c.block_id,
                query.c.farmer_id
            ).first()
            return q
        if land_ids:
            q = (
                db.query(
                    func.sum(Land.area).label('total_area'))
                    .filter(Land.id.in_(land_ids))
            )
            total_area = q.first().total_area
            return json.dumps(total_area)

    def view_land_by(self, block_id, farmer_id):
        q1 = (db.query(
            Land)
              .filter(Land.block_id == block_id)
              .filter(or_(
            and_(Land.farmer_id == farmer_id,
                 or_(Land.rent_id == farmer_id, Land.rent_id == '', Land.rent_id == None, Land.rent_id == '__None')),

            and_(Land.rent_id == farmer_id, Land.farmer_id != farmer_id)
        )
        ).join(Farmer, Farmer.id == Land.farmer_id)
              .filter(Land.is_active, Farmer.is_active)
              .filter(Land.active == 'active')
              .filter(Farmer.active == 'active')
              .order_by(func.right('000000' + Land.code, 6))
              )
        return q1

    def update_land(self, id, **kwargs):
        rent_id = kwargs.get('rent_id') or None
        status_id = kwargs.get('status_id') or None
        tariff_id = kwargs.get('tariff_id') or None
        area = kwargs.get('area') or None
        block_id = kwargs.get('block_id') or None
        farmer_id = kwargs.get('farmer_id') or None
        note = kwargs.get('note') or None
        land = db.query(Land).filter(Land.id == id).first()

        if rent_id and rent_id != '__None':
            land.rent_id = rent_id
        elif rent_id == '__None':
            land.rent_id = None
        if status_id:
            land.status_id = status_id
        if tariff_id:
            land.tariff_id = tariff_id
        if area:
            land.area = area
        if block_id:
            land.block_id = block_id
        if farmer_id:
            land.farmer_id = farmer_id
        land.note = note
        db.add(land)
        db.commit()
        return 'updated'

    def find_block_id_in_land(self, id=''):
        if id:
            result = db.query(self.__classname__).filter(and_(Land.block_id == id, Land.active == 'active')).first()
            if result:
                return True
            else:
                return False

    def find(self, id='', code='', update=False, farmer_id='', rent_id=''):
        if id:
            result = db.query(self.__classname__).filter(Land.id == id).first()
            return result
        if code:
            result = db.query(Land).filter(Land.code == code).first()
            return result
        if farmer_id:
            result = (
                db.query(Land).join(
                    Farmer, Farmer.id == Land.farmer_id
                )
                    .filter(Land.farmer_id == farmer_id)
                    .filter(Land.is_active)
                    .filter(Land.active == 'active')
                    .filter(Farmer.is_active, Farmer.active == 'active')
                    .order_by(func.right('000000' + Land.code, 6))
                    .all()
            )
            return result
        if rent_id:
            result = (
                db.query(Land).join(Farmer, Farmer.id == Land.farmer_id)
                    .filter(Land.rent_id == rent_id)
                    .filter(Land.farmer_id != rent_id)
                    .filter(Land.is_active)
                    .filter(Land.active == 'active')
                    .order_by(func.right('000000' + Land.code, 6))
                    .all()
            )
            return result

    def update(self, obj, _commit=True):
        id = obj.id
        old = self.find(obj.id)
        if obj.active == "disactive":
            invoice = db.query(Invoice) \
                .join(InvoiceDetail, InvoiceDetail.invoice_id == Invoice.id) \
                .join(BillingLand, BillingLand.invoice_detail_id == InvoiceDetail.id) \
                .filter(Invoice.is_active) \
                .filter(InvoiceDetail.is_active) \
                .filter(BillingLand.is_active) \
                .filter(Invoice.status_id.in_(['pending', 'partial'])) \
                .filter(BillingLand.land_id == id).first()

            if invoice:
                return False

        if not obj.active:
            obj.active = getattr(old, 'active', 'active')
        copyupdate(obj, old)
        db.flush()
        self.trigger(self.EVENT_UPDATED, obj=obj)
        db.commit()
        return obj

    def remove(self, obj, _commit=True):
        if hasattr(obj, 'active') and obj.active == 'active':
            obj.active = 'disactive'
        else:
            # avoid error: Instance '<Object>' is not persisted
            obj = self.find(obj.id)
            obj.is_active = False
        db.flush()
        self.trigger(self.EVENT_REMOVED, obj=obj)
        self._commit(_commit=_commit)

    def update_all_lands(self, ids, data, _commit=True):
        farmer_id = data['farmer_id'] or None
        rent_id = data['rent_id'] or None
        status_id = data['status_id'] or None
        tariff_id = data['tariff_id'] or None
        block_id = data['block_id'] or None
        active = data['active'] or None
        note = data['note'] or None
        mapping_land = []
        lands = db.query(Land).filter(Land.id.in_(ids)).all()
        for land in lands:
            mapping_land.append({
                'id': land.id,
                'farmer_id': farmer_id if farmer_id else land.farmer_id,
                'rent_id': rent_id if rent_id else land.rent_id,
                'status_id': status_id if status_id else land.status_id,
                'tariff_id': tariff_id if tariff_id else land.tariff_id,
                'block_id': block_id if block_id else land.block_id,
                'active': active if active else land.active,
                'note': note if note else land.note
            })

        db.bulk_update_mappings(Land, mapping_land)
        db.flush()
        db.commit()
        return _(u"update successfully!")

    def is_remove_all(self, ids, is_remove=False):
        try:
            mapping_land = []
            if is_remove == 'false':
                for id in ids:
                    invoice = db.query(Invoice) \
                        .join(InvoiceDetail, InvoiceDetail.invoice_id == Invoice.id) \
                        .join(BillingLand, BillingLand.invoice_detail_id == InvoiceDetail.id) \
                        .filter(Invoice.is_active) \
                        .filter(InvoiceDetail.is_active) \
                        .filter(BillingLand.is_active) \
                        .filter(Invoice.status_id.in_(['pending', 'partial'])) \
                        .filter(BillingLand.land_id == id).first()
                    if invoice:
                        return "false"
                    else:
                        land = self.find(id)
                        mapping_land.append({'id': land.id, 'active': 'disactive'})
                        db.bulk_update_mappings(Land, mapping_land)
                db.commit()
                return "success"
            else:
                for id in ids:
                    invoice = db.query(Invoice) \
                        .join(InvoiceDetail, InvoiceDetail.invoice_id == Invoice.id) \
                        .join(BillingLand, BillingLand.invoice_detail_id == InvoiceDetail.id) \
                        .filter(Invoice.is_active) \
                        .filter(InvoiceDetail.is_active) \
                        .filter(BillingLand.is_active) \
                        .filter(Invoice.status_id.in_(['pending', 'partial'])) \
                        .filter(BillingLand.land_id == id).first()
                    if invoice:
                        return "false"
                    else:
                        land = self.find(id)
                        mapping_land.append({'id': land.id, 'is_active': 0})
                        db.bulk_update_mappings(Land, mapping_land)
                db.commit()
                return "success"

            #         land = self.find(id)
            #         if land:
            #             mapping_land.append({'id': land.id, 'is_active': 0})
            #     db.bulk_update_mappings(Land, mapping_land)
            # db.commit()
            # return "success"
        except:
            # raise ValueError('Hello')
            return "false"

    def check_land_have_invoice(self, id):
        invoice = db.query(Invoice) \
            .join(InvoiceDetail, InvoiceDetail.invoice_id == Invoice.id) \
            .join(BillingLand, BillingLand.invoice_detail_id == InvoiceDetail.id) \
            .filter(Invoice.is_active) \
            .filter(InvoiceDetail.is_active) \
            .filter(BillingLand.is_active) \
            .filter(Invoice.status_id.in_(['pending', 'partial'])) \
            .filter(BillingLand.land_id == id).first()

        if invoice:
            return False
        return True

    def find_invoice_by_land(self, id):
        # q = (db.query(
        #     Invoice.total_amount,
        #     Invoice.paid_amount,
        #     LookupValue.value,
        #     Invoice.currency,
        #     Currency.sign,
        #     LookupValue.name.label('status_value')
        # )
        #         .join(Currency, Currency.id == Invoice.currency_id)
        #         .join(InvoiceDetail, InvoiceDetail.invoice_id == Invoice.id)
        #         .join(BillingLand, BillingLand.invoice_detail_id == InvoiceDetail.id)
        #         .join(LookupValue, LookupValue.value == Invoice.invoice_type)
        #         .filter(Invoice.is_active, BillingLand.land_id == id, InvoiceDetail.is_active))
        invoice = db.query(
            BillingLand.id,
            InvoiceDetail.invoice_id
        ).join(InvoiceDetail, InvoiceDetail.id == BillingLand.invoice_detail_id) \
            .filter(
            BillingLand.land_id == id,
            BillingLand.is_active,
            InvoiceDetail.is_active
        ).all()
        invoice_ids = [itm[1] for itm in invoice]

        q = db.query(Invoice).filter(Invoice.id.in_(invoice_ids))
        return q.all()

    def find_land_code(self, code):
        result = db.query(Land).filter(Land.code == code).first()
        return result

    def add(self, obj):
        code = obj.code
        old = self.find_land_code(code)
        if old:
            raise ValueError("{0} ".format(obj.code) + _(u'Land code already exist'))
        super(LandLogic, self).add(obj)
        return True


default = LandLogic()
