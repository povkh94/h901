from sqlalchemy import cast, extract, distinct

from project.core.database import db
from project.module.currency.model import Currency
from project.module.payment.model import Payment, PaymentDetail, Invoice, Block, LookupValue, Farmer, Billing
from edf.base.logic import *


class ReportPaymentLogic(LogicBase):
    def __init__(self):
        self.__classname__ = Payment

    def report_payment_by_billing(self, **kwargs):
        payment_method = kwargs.get('payment_method') or ''
        billing_id = kwargs.get('billing_id') or ''

        q = (db.query(
            func.row_number().over(order_by=(Payment.payment_method)).label(
                '_row_number'),
            Payment.currency_id.label('currency'),
            Payment.payment_method.label('payment_method'),
            LookupValue.value.label('lookup_value'),
            LookupValue.name.label('lookup_value_eng'),
            Currency.sign.label('currency_sign'),
            func.sum(PaymentDetail.pay_amount).label('pay_amount'),
        )
             .join(LookupValue, LookupValue.id == Payment.payment_method)
             .join(PaymentDetail, PaymentDetail.payment_id == Payment.id)
             .join(Invoice, Invoice.id == PaymentDetail.invoice_id)
             .join(Currency, Currency.id == Payment.currency_id)
             .filter(Payment.is_active)
             )

        if payment_method:
            q = q.filter(Payment.payment_method == payment_method)
        if billing_id:
            q = q.filter(Invoice.billing_id == billing_id)
        q = q.group_by(Payment.currency_id,Payment.payment_method, LookupValue.value, LookupValue.name, Currency.sign)

        return q
    def report_payment_detail(self, **kwargs):
        payment_method = kwargs.get('payment_method') or ''
        # old one
        # q = (db.query(
        #     func.row_number().over(order_by=(Payment.payment_method)).label(
        #         '_row_number'),
        #     Payment.currency_id.label('currency'),
        #     Payment.payment_method.label('payment_method'),
        #     LookupValue.value.label('lookup_value'),
        #     LookupValue.name.label('lookup_value_eng'),
        #     Currency.sign.label('currency_sign'),
        #     func.sum(PaymentDetail.pay_amount).label('pay_amount'),
        # )
        #      .join(PaymentDetail, PaymentDetail.payment_id == Payment.id)
        #      .join(LookupValue, LookupValue.id == Payment.payment_method)
        #      .join(Currency, Currency.id == Payment.currency_id)
        #      .filter(Payment.is_active, PaymentDetail.is_active)
        #      )

        q = (db.query(
            func.row_number().over(order_by=(Payment.payment_method)).label(
                '_row_number'),
            Payment.currency_id.label('currency'),
            Payment.payment_method.label('payment_method'),
            LookupValue.value.label('lookup_value'),
            LookupValue.name.label('lookup_value_eng'),
            Currency.sign.label('currency_sign'),
            func.sum(Payment.pay_amount).label('pay_amount'),
        )
             .join(LookupValue, LookupValue.id == Payment.payment_method)
             .join(Currency, Currency.id == Payment.currency_id)
             .filter(Payment.is_active)
             )

        if payment_method:
            q = q.filter(Payment.payment_method == payment_method)
        q = q.group_by(Payment.currency_id, Payment.payment_method, LookupValue.value, LookupValue.name, Currency.sign)

        return q

    def report_daily_payment(self, **kwargs):
        d1 = kwargs.get('d1') or datetime.today()
        d2 = kwargs.get('d2') or datetime.now().date().replace(day=1, year=datetime.now().year + 1) - timedelta(days=1)
        if d2:
            d2 = to_datetime(d2) + timedelta(days=1)

        payment_method = kwargs.get('payment_method') or ''

        qu = (
            db.query(
                PaymentDetail.payment_id.label('payment_id'),
                func.count(PaymentDetail.invoice_id).label('total_invoices')
            )
                .filter(PaymentDetail.is_active)
                .group_by(PaymentDetail.payment_id).subquery()
        )

        q = (db.query(
            func.row_number().over(order_by=(Payment.payment_method, Payment.pay_date)).label(
                '_row_number'),
            Payment.pay_date.label('date'),
            Payment.currency_id.label('currency'),
            Payment.payment_method.label('payment_method'),
            LookupValue.value.label('lookup_value'),
            LookupValue.name.label('lookup_value_eng'),
            Currency.sign.label('currency_sign'),
            func.sum(qu.c.total_invoices).label('total_invoice'),
            func.sum(Payment.pay_amount).label('pay_amount'),
            func.count(distinct(Payment.customer_id)).label('total_customers')

        ).join(Currency, Currency.id == Payment.currency_id)
             .join(LookupValue, LookupValue.id == Payment.payment_method)
             .filter(Payment.id == qu.c.payment_id)
             .filter(Payment.is_active)
             .filter(Payment.pay_date >= d1)
             .filter(Payment.pay_date < d2))
        if payment_method:
            q = q.filter(Payment.payment_method == payment_method)
        q = q.group_by(Payment.pay_date, Payment.currency_id, Payment.payment_method, LookupValue.value,
                       LookupValue.name, Currency.sign)
        # q = (db.query(
        #     func.row_number().over(order_by=(Payment.pay_date)).label(
        #         '_row_number'),
        #     Payment.pay_date.label('date'),
        #     Payment.currency_id.label('currency'),
        #     Payment.payment_method.label('payment_method'),
        #     Currency.sign.label('currency_sign'),
        #     func.count(distinct(Payment.customer_id)).label('total_customers'),
        #     func.count(PaymentDetail.invoice_id).label('total_invoice'),
        #     func.sum(Payment.pay_amount).label('pay_amount'),
        # )
        #      .join(PaymentDetail, PaymentDetail.payment_id == Payment.id)
        #      .join(Currency, Currency.id == Payment.currency_id)
        #      .filter(Payment.is_active, PaymentDetail.is_active)
        #      .filter(Payment.pay_date >= d1)
        #      .filter(Payment.pay_date < d2)
        #      .filter(Payment.payment_method == payment_method)
        #      .group_by(Payment.pay_date, Payment.currency_id, Payment.payment_method, Currency.sign))

        return q

    def report_monthly_payment(self, **kwargs):
        d1 = kwargs.get('d1') or datetime.now().date()
        d2 = kwargs.get('d2') or datetime.now().date().replace(day=1, year=datetime.now().year + 1) - timedelta(days=1)
        if d2:
            d2 = to_datetime(d2)
            d2 = d2.replace(day=1, month=d2.month + 1 if d2.month < 12 else 1,
                            year=d2.year if d2.month <= 11 else d2.year + 1)

        payment_method = kwargs.get('payment_method') or ''

        qu = (
            db.query(
                PaymentDetail.payment_id.label('payment_id'),
                func.count(PaymentDetail.invoice_id).label('total_invoices')
            )
                .filter(PaymentDetail.is_active)
                .group_by(PaymentDetail.payment_id).subquery()
        )

        q = (db.query(
            func.row_number().over(order_by=(Payment.payment_method, extract('year', Payment.pay_date),
                                             extract('month', Payment.pay_date))).label(
                '_row_number'),
            (cast(extract('year', Payment.pay_date), String) + '-' + cast(extract('month', Payment.pay_date),
                                                                          String)).label('date'),
            Payment.currency_id.label('currency'),
            Payment.payment_method.label('payment_method'),
            Currency.sign.label('currency_sign'),
            LookupValue.value.label('lookup_value'),
            LookupValue.name.label('lookup_value_eng'),
            func.count(distinct(Payment.customer_id)).label('total_customers'),
            func.sum(Payment.pay_amount).label('pay_amount'),
            func.sum(qu.c.total_invoices).label('total_invoices')
        )
             .join(Currency, Currency.id == Payment.currency_id)
             .join(LookupValue, LookupValue.id == Payment.payment_method)
             .filter(Payment.id == qu.c.payment_id)
             .filter(Payment.is_active,
                     Payment.pay_date >= d1,
                     Payment.pay_date < d2))
        if payment_method:
            q = q.filter(
                Payment.payment_method == payment_method)
        q = (q.group_by(
            extract('year', Payment.pay_date),
            extract('month', Payment.pay_date),
            Payment.currency_id, Payment.payment_method, Currency.sign, LookupValue.value, LookupValue.name
        )
            .order_by(
            Payment.payment_method,
            extract('year', Payment.pay_date),
            extract('month', Payment.pay_date))
        )

        return q

    def report_year_payment(self, **kwargs):
        d1 = kwargs.get('d1') or datetime.now().date()
        d2 = kwargs.get('d2') or datetime.now().date().replace(day=1, year=datetime.now().year + 1)

        d1 = to_datetime(d1).replace(month=1, day=1)

        if d2:
            d2 = to_datetime(d2)
            d2 = d2.replace(day=1, month=1,
                            year=d2.year + 1)
        payment_method = kwargs.get('payment_method') or ''

        qu = (
            db.query(
                PaymentDetail.payment_id.label('payment_id'),
                func.count(PaymentDetail.invoice_id).label('total_invoices')
            )
                .filter(PaymentDetail.is_active)
                .group_by(PaymentDetail.payment_id).subquery()
        )

        q = (db.query(
            func.row_number().over(order_by=(Payment.payment_method, extract('year', Payment.pay_date))).label(
                '_row_number'),
            (cast(extract('year', Payment.pay_date), String)).label('date'),
            Payment.currency_id.label('currency'),
            Payment.payment_method.label('payment_method'),
            Currency.sign.label('currency_sign'),
            LookupValue.value.label('lookup_value'),
            LookupValue.name.label('lookup_value_eng'),
            func.count(distinct(Payment.customer_id)).label('total_customers'),
            func.sum(qu.c.total_invoices).label('total_invoices'),
            func.sum(Payment.pay_amount).label('pay_amount')
        )
             .join(Currency, Currency.id == Payment.currency_id)
             .join(LookupValue, LookupValue.id == Payment.payment_method)
             .filter(Payment.id == qu.c.payment_id)
             .filter(Payment.is_active,
                     Payment.pay_date >= d1,
                     Payment.pay_date < d2))
        if payment_method:
            q = q.filter(Payment.payment_method == payment_method)

        q = (q.group_by(
            extract('year', Payment.pay_date),
            Payment.currency_id, Payment.payment_method, Currency.sign, LookupValue.value, LookupValue.name
        )
             .order_by(Payment.payment_method,
                       extract('year', Payment.pay_date),
                       )

             )
        return q

    def report_by_customer(self, **kwargs):
        d1 = kwargs.get('d1') or datetime.today()
        d2 = kwargs.get('d2') or datetime.now().date().replace(day=1, year=datetime.now().year + 1) - timedelta(days=1)
        if d2:
            d2 = to_datetime(d2) + timedelta(days=1)
        payment_method = kwargs.get('payment_method') or ''
        qu = (
            db.query(
                PaymentDetail.payment_id.label('payment_id'),
                func.count(distinct(PaymentDetail.invoice_id)).label('total_invoices'),
                func.sum(Invoice.total_area).label('total_area'),
            )
                .join(Invoice, Invoice.id == PaymentDetail.invoice_id)
                .filter(PaymentDetail.is_active)
                .group_by(PaymentDetail.payment_id).subquery()
        )

        q = (db.query(
            func.row_number().over(order_by=(Payment.payment_method, Farmer.name, Farmer.spouse_name, Farmer.code)).label(
                '_row_number'),
            Payment.customer_id,
            Payment.customer_name,
            Payment.payment_method,
            # Block.name.label('block_name'),
            LookupValue.value.label('lookup_value'),
            LookupValue.name.label('lookup_value_eng'),
            func.sum(Payment.pay_amount).label('pay_amount'),
            func.sum(qu.c.total_invoices).label('total_invoices'),
            func.sum(qu.c.total_area).label('total_area'),
            Payment.currency_id.label('currency'),
            Currency.sign.label('currency_sign'),
            Farmer.name.label('farmer_name'),
            Farmer.spouse_name.label('spouse_name'),
            Farmer.code.label('farmer_code')
        )
             # .join(PaymentDetail, PaymentDetail.payment_id == Payment.id)
             # .join(Invoice, Invoice.id == PaymentDetail.invoice_id)
             # .join(Block, Block.id == Invoice.block_id)
             .join(Currency, Currency.id == Payment.currency_id)
             .join(LookupValue, LookupValue.id == Payment.payment_method)
             .join(Farmer, Farmer.id == Payment.customer_id)
             .filter(Payment.id == qu.c.payment_id)
             .filter(Payment.is_active)
             .filter(Payment.pay_date >= d1)
             .filter(Payment.pay_date < d2))
        if payment_method:
            q = q.filter(Payment.payment_method == payment_method)
        q = q.group_by(Farmer.name,
                       Farmer.spouse_name, Farmer.code, Payment.customer_id, Payment.customer_name, Payment.payment_method,
                       Payment.currency_id, Currency.sign, LookupValue.value, LookupValue.name)
        # q = q.order_by(Farmer.name,
        #                Farmer.spouse_name, Farmer.code, Payment.customer_id, Payment.customer_name, Payment.payment_method,
        #                Payment.currency_id, Currency.sign, LookupValue.value, LookupValue.name)
        return q


default = ReportPaymentLogic()
