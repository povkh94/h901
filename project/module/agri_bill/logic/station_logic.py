from ..model import *
from edf.base.logic import *
from sqlalchemy import cast, literal, case


class StationLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = Station

    def new(self):
        """
        :rtype : Station
        """
        station = Station()
        return station

    def search(self, **kwargs):
        search = kwargs.get('search') or ''
        company_id = kwargs.get('company_id') or current.company_id()
        q = self.actives()
        if search:
            q = q.filter(or_(func.lower(self.__classname__.name).like('%' + search.lower() + '%')))
        if company_id:
            q = q.filter(or_(self.__classname__.company_id == company_id, self.__classname__.company_id == None))

        return q.order_by(self.__classname__.name)



default = StationLogic()
