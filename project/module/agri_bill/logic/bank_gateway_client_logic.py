import getip
import socket
from lxml import etree
from zeep import Client


class BankGatewayClientLogic(object):
    def __init__(self, wsdl_uri, access_key, user):
        self._wsdl_uri = wsdl_uri
        self._access_key = access_key
        self._user = user

    def client(self):
        client = Client(self._wsdl_uri)
        client.service.TestConnection()

        access_key = etree.Element("AccessKey")
        access_key.text = self._access_key
        user = etree.Element("user")
        user.text = self._user
        host = etree.Element("host")
        host.text = socket.gethostname()
        ip = etree.Element("ip")
        ip.text = ''
        client.set_default_soapheaders([access_key, user, host, ip])

        return client

    def update_customer_to_server(self, log_id, csv):
        client = self.client()
        client.service.UpdateCustomerData(log_id, csv)

    def get_pending_payment_count(self):
        client = self.client()
        return client.service.GetPendingPaymentCount()

    def get_pending_payment(self):
        client = self.client()
        return client.service.GetPaymentData()

    def update_payment_data(self, data):
        client = self.client()
        return client.service.UpdatePaymentData(data)

#default = BankGatewayClientLogic()
