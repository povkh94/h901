from ...share.view.base_view import *
from ...agri_bill.model import *
from ...company.model import Company
import json


class BillingCycleLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = BillingCycle

    def new(self):
        billingCycle = BillingCycle()
        billingCycle.company_id = current.company_id
        return billingCycle

    def search(self, **kwargs):
        search = kwargs.get('search', '')
        company_id = kwargs.get('company_id', None)

        q = self.actives()
        if search:
            q = q.filter(or_
                         (func.lower(self.__classname__.name).like('%' + search.lower() + '%')))
        if company_id:
            q = q.filter(Company.company_id == company_id)

        return q

    def add(self, obj):
        if not obj.id:
            obj.id = gid()
        obj.is_active = True
        db.add(obj)
        db.flush()

        for block_id in obj.ext__blocks.split(','):
            if block_id != '0':
                cycle_block = BillingCycleBlock()
                cycle_block.billing_cycle_id = obj.id
                cycle_block.block_id = block_id
                db.add(cycle_block)
                db.flush()
        db.commit()

    def find(self, id=0, billing_id=0):
        if id:
            return db.query(self.__classname__).filter_by(id=id,is_active=True).first()
        if billing_id:
            return db.query(self.__classname__).filter_by(billing_id=billing_id,is_active=True).first()

    def get_existing_block(self, season_id='', id=''):
        q = (db.query(BillingCycleBlock.block_id)
             .join(BillingCycle, BillingCycle.id == BillingCycleBlock.billing_cycle_id)
             .filter(BillingCycleBlock.is_active,
                     BillingCycle.is_active))
        if season_id:
            q = q.filter(BillingCycle.season_id == season_id)
        if id:
            q = q.filter(BillingCycleBlock.billing_cycle_id == id)
        return q.all()

    def filter_land(self,id,station_id):
        q = (
                db.query
                (
                    BillingCycleBlock.block_id,
                    Block.station_id,
                    Land
                )
                    .join(Block,Block.id == BillingCycleBlock.block_id)
                    .join(Land,Land.block_id == BillingCycleBlock.block_id)
                    .filter(BillingCycleBlock.billing_cycle_id == id)
                    .filter(Block.station_id == station_id)
            )
        result = q.all()

        return result

    def update(self, obj):
        billing_cycle = self.find(obj.id)
        billing_cycle.id = obj.id
        billing_cycle.company_id = obj.company_id
        billing_cycle.season_id = obj.season_id
        billing_cycle.name = obj.name
        billing_cycle.note = obj.note
        billing_cycle.ext__code = obj.ext__code
        billing_cycle.ext__blocks = obj.ext__blocks
        db.add(billing_cycle)
        db.flush()
        #delete billing cycle block
        db.query(BillingCycleBlock).filter(BillingCycleBlock.billing_cycle_id == billing_cycle.id).delete()
        for block_id in billing_cycle.ext__blocks.split(','):
            if block_id !='0':
                billing_cycle_block = BillingCycleBlock()
                billing_cycle_block.billing_cycle_id = billing_cycle.id
                billing_cycle_block.block_id = block_id
                db.add(billing_cycle_block)
                db.flush()
        db.commit()

default = BillingCycleLogic()
