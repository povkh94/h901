﻿from operator import or_

from project.core.database import db
from project.module.company import current
from project.module.system_security.model import Component
from ...share.view.base_view import *

class ComponentLogic(LogicBase):
    def __init__(self):
        self.__classname__ = Component

    def add(self, obj):
        obj.scope = current.company().id
        return super(ComponentLogic, self).add(obj)

    def search(self, **kwargs): 
        search = kwargs.get('search','')
        component_type = int(kwargs.get('component_type', 0))
        app_target = kwargs.get('app_target',0)
        scope = current.company().id
        parent_id = kwargs.get('parent_id',0)
        q = (db.query(Component)
               .filter(Component.is_active==True) 
               .filter(or_(Component.scope == scope, Component.scope == None, Component.scope == '', Component.scope == '0'))
               .filter(Component.name.like('%'+search+'%')))
        if component_type:
            q = q.filter(Component.component_type == component_type)
        if app_target:
            q = q.filter(Component.app_target == app_target)
        
        if parent_id:
            q = q.filter(Component.parent_id == parent_id)
        else:
            q = q.filter(or_(Component.parent_id == None,
                             Component.parent_id == '',
                             Component.parent_id == '0'))

        #q = q.filter(or_(Component.parent_id == parent_id, 
        #                 Component.parent_id == None, 
        #                 Component.parent_id == '', 
        #                 Component.parent_id == 0))

        #if scope:
        #    q = q.filter(or_(Component.scope == scope, Component.scope == None, Component.scope == '', Component.scope == '0'))
        
        #if scope and component_type == enum.component_type.report_group:
        if scope and component_type == 5:
            q = q.filter(Component.scope == scope)

        return q;

    def find(self, id=0,name='',update = False):
        if id:
            return db.query(Component).filter(Component.id==id).first()
        if name:
            return db.query(Component).filter(Component.name==name).first()

    def register(self,c):
        scope = current.company().id;
        f = (db.query(Component)
               .filter(Component.name==c.name)
               .filter(or_(Component.scope==scope,Component.scope==None,Component.scope==''))
               .first())
        if not f:
            c.position = c.position or 0
            c.is_system = True
            self.add(c) 


default = ComponentLogic()
