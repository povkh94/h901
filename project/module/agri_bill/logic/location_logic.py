# - *- coding: utf- 8 - *-
from ...share.view.base_view import *
from ...agri_bill.model import *


class LocationLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = Location

    def search(self, **kwargs):
        search = kwargs.get('search') or None
        type = kwargs.get('type') or None
        q = self.actives()
        if search:
            search = search.strip()
            q = q.filter(
                    or_(
                        func.lower(Location.id).like('%' + search.lower() + '%'),
                        func.lower(Location.name).like('%' + search.lower() + '%'),
                        func.lower(Location.latin_name).like('%' + search.lower() + '%'),

                    )
                )
        if type:
            q = q.filter(Location.type == type)
        return q

    def all(self, type, parent_id=''):
        q = db.query(Location).filter(Location.is_active, Location.type == type)
        if parent_id:
            q = q.filter(Location.parent_id == parent_id)
        result = q
        return result


    def get_villages(self, type):
        if type:
            ParentLocation = aliased(Location, name='parent_location')
            # q = db.query(Location.id, ParentLocation,
            #              func.concat(ParentLocation.name, ' - ', Location.name).label('filter_name')).outerjoin(
            #     (ParentLocation, Location.parent_id == ParentLocation.id)
            # ) \
            #     .filter(Location.is_active, Location.type == 4)
            q = db.query(Location.id, ParentLocation,
                         (ParentLocation.name + ' - ' + Location.name).label('filter_name')).outerjoin(
                (ParentLocation, Location.parent_id == ParentLocation.id)
            ) \
                .filter(Location.is_active, Location.type == '4')
        result = q.all()
        return result

    def get_location(self, farmer_id):
        village_id = db.query(Farmer.village_id).filter(Farmer.id == farmer_id).first()
        Commune = aliased(Location, name='commnue')
        District = aliased(Location, name='district')
        Province = aliased(Location, name='province')
        q = (db.query(
            Location.id,
            Location.name.label('village'),
            Commune.name.label('commune'),
            District.name.label('district'),
            Province.name.label('province'))
             .outerjoin((Commune, Location.parent_id == Commune.id))
             .outerjoin((District, Commune.parent_id == District.id))
             .outerjoin((Province, District.parent_id == Province.id))
             .filter(Location.is_active, Location.id == village_id))

        return q.first()

    def update(self, obj, _commit=True, **kwargs):
        location = self.find(obj.id)
        location.code = obj.code
        location.name = obj.name
        location.latin_name = obj.latin_name
        location.type = obj.type
        location.parent_id = obj.parent_id
        obj.ext = obj.ext
        db.add(location)
        db.commit()

    def add(self, obj, _commit=True):
        location = Location()
        location.code = obj.code
        location.name = obj.name
        location.latin_name = obj.latin_name
        location.type = obj.type
        location.parent_id = obj.parent_id
        location.is_active = '1'
        if obj.type == '1':
            location.prefix = u'ខេត្ត'
            location.latin_prefix = 'Province'
        elif obj.type == '2':
            location.prefix = u'ស្រុក'
            location.latin_prefix = 'District'
        elif obj.type=='3':
            location.prefix = u'ឃុំ'
            location.latin_prefix = 'Commune'
        else:
            location.prefix = u'ភូមិ'
            location.latin_prefix = 'Village'
        db.add(obj)
        db.commit()


default = LocationLogic()
