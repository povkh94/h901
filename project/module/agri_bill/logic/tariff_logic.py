from project.module.agri_bill.model import Tariff
from edf.base.logic import *


class TariffLogic(LogicBase):

    def __init__(self, *args, **kw_args):
        self.__classname__ = Tariff

    def search(self, **kwargs):
        search = kwargs.get('search', '')
        q = self.actives()
        if search:
            if hasattr(self.__classname__, 'name') and hasattr(self.__classname__, 'code'):
                q = q.filter(
                    or_(func.lower(self.__classname__.code).like('%' + search.lower() + '%'),
                        func.lower(self.__classname__.name).like('%' + search.lower() + '%')
                        )
                )
            elif hasattr(self.__classname__, 'code'):
                q = q.filter(func.lower(self.__classname__.code).like('%' + search.lower() + '%'))
            elif hasattr(self.__classname__, 'name'):
                q = q.filter(func.lower(self.__classname__.name).like('%' + search.lower() + '%'))
        # q = q.order_by(Tariff.name)
        return q

    def new(self):
        tariff = Tariff()
        return tariff

    def getAll(self):
        return db.query(Tariff).filter(Tariff.is_active==True).all()


default = TariffLogic()
