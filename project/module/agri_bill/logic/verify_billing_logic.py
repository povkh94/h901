from flask_babel import gettext as _

from project.module.agri_bill import enum
from ...share.view.base_view import *
from ...agri_bill.model import *
from sqlalchemy import case, literal_column


def push_update(message='', **kwargs):
    import time
    time.sleep(0.5)


class VerifyBillingLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = Verify

    def new(self, seq_id=0):
        obj = self.__classname__()
        return obj

    def search(self, **kwargs):
        search = kwargs.get('search', '')
        block_id = kwargs.get('block_id', '')
        verify_id = kwargs.get('verify_id', '')
        billing_id = kwargs.get('billing_id', '')

        # find land of owner
        q1 = (db.query(
            Verify.billing_id.label('billing_id'),
            Verify.station_id.label('station_id'),
            Verify.farmer_id.label('farmer_id'),
            Verify.block_id.label('block_id'),
            Verify.status.label('status'),
            Block.code.label('block_code'),
            Block.name.label('block_name'),
            Farmer.name.label('farmer_name'),
            Farmer.code.label('farmer_code'),
            Farmer.spouse_name.label('spouse_name'),
            Land.id.label('land_id'),
            Land.code.label('land_code'),
            Land.area.label('area'),
            Land.status_id.label('land_status_id')
        )
              .join(Land, Land.farmer_id == Verify.farmer_id)
              .join(Block, Block.id == Land.block_id)
              .join(Tariff, Tariff.id == Land.tariff_id)
              .join(Farmer, Farmer.id == Land.farmer_id)
              .filter(Verify.billing_id == billing_id)
              .filter(Land.active == 'active')
              .filter(Land.is_active, Tariff.is_active, Block.is_active)
              .filter(and_(Land.farmer_id == Verify.farmer_id,
                           or_(Land.rent_id == Verify.farmer_id, Land.rent_id == None, Land.rent_id == '',
                               Land.rent_id == '__None')))
              .filter(Block.id == Verify.block_id)
              .filter(Station.id == Verify.station_id)
              .filter(Farmer.active == 'active', Farmer.id == Verify.farmer_id)
              )
        OwnFarmer = aliased(Farmer, name='parent_location')
        q2 = (db.query(
            Verify.billing_id.label('billing_id'),
            Verify.station_id.label('station_id'),
            Verify.farmer_id.label('farmer_id'),
            Verify.block_id.label('block_id'),
            Verify.status.label('status'),
            Block.code.label('block_code'),
            Block.name.label('block_name'),
            Farmer.name.label('farmer_name'),
            Farmer.code.label('farmer_code'),
            Farmer.spouse_name.label('spouse_name'),
            Land.id.label('land_id'),
            Land.code.label('land_code'),
            Land.area.label('area'),
            Land.status_id.label('land_status_id')
        )
              .join(Land, Land.rent_id == Verify.farmer_id)
              .join(Farmer, Farmer.id == Land.rent_id)
              .join(OwnFarmer,OwnFarmer.id == Land.farmer_id)
              .join(Block, Block.id == Land.block_id)
              .join(Tariff, Tariff.id == Land.tariff_id)
              .filter(Verify.billing_id == billing_id)
              .filter(Land.active == 'active')
              .filter(Land.is_active)
              .filter(and_(Land.rent_id == Verify.farmer_id,
                           or_(Land.farmer_id != Verify.farmer_id, Land.rent_id != None, Land.rent_id != '',
                               Land.rent_id != '__None')))
              .filter(
            Land.farmer_id != None, Land.farmer_id != '', Land.farmer_id != '__None'
        )
              .filter(Block.id == Verify.block_id)
              .filter(Station.id == Verify.station_id)
              .filter(Farmer.active == 'active', Farmer.id == Verify.farmer_id)
              .filter(OwnFarmer.is_active, OwnFarmer.active == 'active')
              )

        if search:
            q1 = q1.filter(or_(
                func.lower(Farmer.name).like('%' + search.lower() + '%'),
                func.lower(Farmer.code).like('%' + search.lower() + '%'),
                func.lower(Farmer.spouse_name).like('%' + search.lower() + '%'),
                func.lower(Block.name).like('%' + search.lower() + '%'),
                func.lower(Block.code).like('%' + search.lower() + '%')
            ))
            q2 = q2.filter(or_(
                func.lower(Farmer.name).like('%' + search.lower() + '%'),
                func.lower(Farmer.code).like('%' + search.lower() + '%'),
                func.lower(Farmer.spouse_name).like('%' + search.lower() + '%'),
                func.lower(Block.name).like('%' + search.lower() + '%'),
                func.lower(Block.code).like('%' + search.lower() + '%')
            ))
        if block_id:
            q1 = q1.filter(Block.id == block_id)
            q2 = q2.filter(Block.id == block_id)

        if verify_id == enum.VerifyStatus.unverified or verify_id == enum.VerifyStatus.verified:
            q1 = q1.filter(Verify.status == verify_id)
            q2 = q2.filter(Verify.status == verify_id)

        q3 = q1.union(q2).subquery()
        q3 = db.query(
            func.row_number().over(partition_by=q3.c.block_id, order_by=(q3.c.block_code, q3.c.farmer_code)).label(
                '_row_number'),
            q3.c.billing_id,
            q3.c.station_id,
            q3.c.farmer_id,
            q3.c.block_id,
            q3.c.block_code,
            q3.c.block_name,
            q3.c.farmer_name,
            q3.c.farmer_code,
            q3.c.spouse_name,
            q3.c.status,
            func.count(
                case([(q3.c.land_status_id == enum.LandStatus.irrigate, q3.c.area)],else_=literal_column("NULL"))
            ).label('total_land'),
            # func.count(q3.c.land_id).label('total_land'),
            func.sum(q3.c.area).label('total_area'),
            func.sum(
                case([(q3.c.land_status_id == enum.LandStatus.irrigate, q3.c.area)],
                     else_=literal_column("NULL"))).label(
                'total_area_irrigate')
        ).group_by(q3.c.status, q3.c.block_id, q3.c.station_id, q3.c.farmer_id, q3.c.block_code, q3.c.block_name,
                   q3.c.farmer_name, q3.c.farmer_code, q3.c.spouse_name, q3.c.billing_id).order_by(q3.c.block_code)
        return q3

    def find_lands(self, billing_id):
        # find land of owner
        Rent = aliased(Farmer)
        q1 = (db.query(
            Verify.id.label('verify_billing_id'),
            Verify.billing_id.label('billing_id'),
            Verify.station_id.label('station_id'),
            Verify.farmer_id.label('farmer_id'),
            Verify.block_id.label('block_id'),
            Verify.status.label('status'),
            Block.code.label('block_code'),
            Block.name.label('block_name'),
            Farmer.name.label('farmer_name'),
            Farmer.code.label('farmer_code'),
            Farmer.spouse_name.label('spouse_name'),
            Rent.name.label('rent_name'),
            Rent.code.label('rent_code'),
            Rent.spouse_name.label('rent_spouse_name'),
            Land.id.label('land_id'),
            Land.code.label('land_code'),
            Land.area.label('area'),
            Land.rent_id.label('rent_id'),
            Land.status_id.label('land_status_id'),
            Tariff.id.label('tariff_id'),
            Tariff.name.label('tariff_name'),
            Tariff.price.label('tariff_price')
        )
            .join(Land, Land.farmer_id == Verify.farmer_id)
            .join(Block, Block.id == Land.block_id)
            .join(Tariff, Tariff.id == Land.tariff_id)
            .join(Farmer, Farmer.id == Land.farmer_id)
            .join(Rent, Rent.id == Land.rent_id)
            .filter(Verify.billing_id == billing_id)
            .filter(Land.active != 'disactive')
            .filter(Land.is_active, Tariff.is_active, Block.is_active)
            .filter(Block.id == Verify.block_id)
            .filter(Station.id == Verify.station_id)
            .filter(
            or_(Land.rent_id == '', Land.rent_id == None, Land.rent_id == Land.farmer_id, Land.rent_id == '__None'))
        )
        q2 = (db.query(
            Verify.id.label('verify_billing_id'),
            Verify.billing_id.label('billing_id'),
            Verify.station_id.label('station_id'),
            Verify.farmer_id.label('farmer_id'),
            Verify.block_id.label('block_id'),
            Verify.status.label('status'),
            Block.code.label('block_code'),
            Block.name.label('block_name'),
            Farmer.name.label('farmer_name'),
            Farmer.code.label('farmer_code'),
            Farmer.spouse_name.label('spouse_name'),
            Rent.name.label('rent_name'),
            Rent.code.label('rent_code'),
            Rent.spouse_name.label('rent_spouse_name'),
            Land.id.label('land_id'),
            Land.code.label('land_code'),
            Land.area.label('area'),
            Land.rent_id.label('rent_id'),
            Land.status_id.label('land_status_id'),
            Tariff.id.label('tariff_id'),
            Tariff.name.label('tariff_name'),
            Tariff.price.label('tariff_price')

        )
              .join(Land, Land.rent_id == Verify.farmer_id)
              .join(Farmer, Farmer.id == Land.farmer_id)
              .join(Rent, Rent.id == Land.rent_id)
              .join(Block, Block.id == Land.block_id)
              .join(Tariff, Tariff.id == Land.tariff_id)
              .filter(Verify.billing_id == billing_id)
              .filter(Land.active != 'disactive')
              .filter(Land.is_active)
              .filter(Block.id == Verify.block_id)
              .filter(Station.id == Verify.station_id)
              .filter(Land.rent_id == Verify.farmer_id)
              .filter(Land.rent_id != Land.farmer_id)
              )
        q = q1.union(q2)
        return q.all()

    # get all billing to verify
    def get_verify_billing(self, id):
        from sqlalchemy import literal_column
        q = (db.query(
            func.row_number().over(order_by=Verify.block_id).label('_row_number'),
            Verify.billing_id.label('billing_id'),
            Verify.station_id.label('station_id'),
            Verify.farmer_id.label('farmer_id'),
            Verify.block_id.label('block_id'),
            Verify.status,
            Block.code.label('block_code'),
            Block.name.label('block_name'),
            Farmer.name.label('farmer_name'),
            Farmer.code.label('farmer_code'),
            func.count(Land.id).label('total_land'),
        )
             .join(Billing, Billing.id == Verify.billing_id)
             .join(Block, Block.id == Verify.block_id)
             .join(Farmer, Farmer.id == Verify.farmer_id)
             .join(Land, Land.block_id == Verify.block_id)
             .filter(Verify.billing_id == id)
             .filter(Verify.farmer_id == Land.farmer_id)
             .filter(Block.id != 'none')
             .group_by(Verify.billing_id, Verify.station_id, Verify.block_id, Verify.status, Block.code, Block.name,
                       Verify.farmer_id, Farmer.name, Farmer.code))

        return q.all()

    def update_status(self, id='', block_id='', farmer_id='', status=''):
        if id and block_id and farmer_id:
            verify_billing = (db.query(Verify)
                              .filter(Verify.billing_id == id)
                              .filter(Verify.block_id == block_id)
                              .filter(Verify.farmer_id == farmer_id).first())
            verify_billing.status = status
            db.add(verify_billing)
            db.commit()
        return "success"

    def update_verify_billing_land(self, land, billing_id):
        """
        if not exist billing verify (farmer id and block_id)
        """
        from project.module.agri_bill.logic import billing_logic
        billing = billing_logic.default.find(id=billing_id)
        if billing:
            verify = db.query(Verify).filter(Verify.billing_id == billing_id, Verify.farmer_id == land.farmer_id,
                                             Verify.block_id == land.block_id, Verify.is_active).first()
            if not verify:
                from project.module.agri_bill.logic import block_logic
                block = block_logic.default.find(id=land.block_id)
                if not block.code:
                    block.code = ''
                station_id = ''
                if hasattr(block, 'station') and hasattr(block.station, 'id'):
                    station_id = block.station.id
                billing_cycle_code = ''
                if hasattr(billing, 'billing_cycle') and hasattr(billing.billing_cycle, 'id'):
                    billing_cycle_code = billing.billing_cycle.ext__code
                verify = Verify()
                verify.id = gid()
                verify.billing_id = billing_id
                verify.station_id = station_id
                verify.block_id = land.block_id
                verify.farmer_id = land.farmer_id
                verify.status = enum.VerifyStatus.unverified
                if not land.farmer:
                    land.farmer.code = ''
                verify.contract_number = str(billing.end_date.strftime(
                    "%y")) + "-" + billing_cycle_code + "-" + block.code + "-" + land.farmer.code  # contract number follow by year-block_code-farmer-code
                db.add(verify)
                db.commit()

    def verify_all(self, billing_id):
        if billing_id:
            status = enum.VerifyStatus.verified
            verify_billing = (db.query(Verify).filter(Verify.billing_id == billing_id).all())
            for verify in verify_billing:
                verify.status = status
                db.add(verify)
                db.flush()
            db.commit()
        return "success"

    # get verify billing by billing_id , block_id , farmer_id
    def get_verify_billing_by(self, id, block_id, farmer_id):
        q = (db.query(Verify)
             .filter(Verify.billing_id == id)
             .filter(Verify.block_id == block_id)
             .filter(Verify.farmer_id == farmer_id))
        q = q.first()
        return q

    def check_status(self, billing_id):
        row_num = db.query(Verify).filter(Verify.billing_id == billing_id,
                                          Verify.status == enum.VerifyStatus.unverified).count()
        if row_num > 0:
            return enum.VerifyStatus.unverified
        else:
            return enum.VerifyStatus.verified

    def find(self, **kwargs):
        contract_number = kwargs.get('contract_number') or ''
        id = kwargs.get('id') or ''
        billing_id = kwargs.get('billing_id') or ''
        q = db.query(self.__classname__).filter(self.__classname__.is_active)
        if id:
            q = q.filter(self.__classname__.id == id)
        if contract_number:
            q = q.filter(self.__classname__.contract_number == contract_number)
        if billing_id:
            q = q.filter(self.__classname__.billing_id == billing_id)
        return q.first()

    def get_contract_number(self, billing_id, block_id, farmer_id):
        result = (db.query(Verify.contract_number)
                  .filter(Verify.billing_id == billing_id)
                  .filter(Verify.block_id == block_id)
                  .filter(Verify.farmer_id == farmer_id)
                  .filter(Verify.is_active).first())
        return result or ''

    def preview_to_pdf(self, billing_id='', block_id=''):
        q = db.query(Verify).filter(Verify.is_active)
        if billing_id:
            q = q.filter(Verify.billing_id == billing_id)
        if block_id != '__None':
            q = q.filter(Verify.block_id == block_id)

        return q.all()

    def export_constract_to_pdf(self, **kwargs):
        push_update(message=_("Processing"), **kwargs)
        param = json.loads(kwargs.get('param'))
        obj = self.preview_to_pdf(param['billing_id'], param['block_id'])
        from project.module.agri_bill.logic import block_logic
        block = block_logic.default.find(id=param['block_id'])
        contents = []
        paths = []
        for item in obj:
            content = self.export_content_contracts(item)
            contents.append(content)
        if len(contents) > 100:
            from PyPDF2 import PdfFileMerger

            merger = PdfFileMerger()
            lists = self.chunks(contents, 50)
            for list in lists:
                push_update(message=_("Generating File"), **kwargs)
                data = render_template("verify/tasuong_contract.html", contents=list)
                path = self.export_pdf(data)
                merger.append(path['path'] + '\\' + path['name'])
                paths.append(path['path'] + '\\' + path['name'])
            push_update(message=_("Downloading File"), **kwargs)
            try:
                merger.write(path['path'] + '\\' + path['name'].split('.pdf')[0] + '_' + block.name + '.pdf')
                merger.close()
            except Exception as e:
                print e.message
            url = request.url_root[:-1] + url_for('download_file',
                                                  filename=path['name'].split('.pdf')[0] + '_' + block.name + '.pdf')



        else:
            push_update(message=_("Generating File"), **kwargs)
            data = render_template("verify/tasuong_contract.html", contents=contents)
            push_update(message=_("Downloading File"), **kwargs)
            path = self.export_pdf(data)
            url = request.url_root[:-1] + url_for('download_file', filename=path['name'])

        # remove paths
        if paths:
            for path in paths:
                import os
                os.remove(path)
        if url:
            push_update(message=url, finish=True, **kwargs)
        push_update(message=_('Printing Fail!'), finish=True, **kwargs)

    def chunks(self, l, n):
        """Yield successive n-sized chunks from l."""
        lists = []
        for i in range(0, len(l), n):
            lists.append(l[i:i + n])
        return lists

    def export_pdf(self, data):
        import os
        current_dir = os.path.dirname(os.path.realpath(__file__))
        target_dir = os.sep.join(current_dir.split(os.sep)[0:-2])
        output_path = os.path.join(app.config.get('SHARE_IMAGES'))
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        # export pdf
        from project.module.agri_bill.export import export_pdf
        path = export_pdf(data, css='', output_path=output_path)
        # url = request.url_root[:-1] + url_for('download_file', filename=export.get('name'))
        return path

    def export_content_contracts(self, obj):
        from project.module.agri_bill.logic import billing_logic
        billing = billing_logic.default.find(obj.billing_id)
        from project.module.agri_bill.logic import contract_template_logic
        content_template = contract_template_logic.default.find_content_template(billing.contract_template_id)
        billing = billing.name.split('-')
        from project.module.agri_bill.logic import block_logic
        block = block_logic.default.find(obj.block_id)
        from project.module.agri_bill.logic import farmer_logic
        farmer = farmer_logic.default.find(obj.farmer_id)
        sex_kh = farmer_logic.default.get_marital_status(farmer.sex)
        farmer.sex_kh = sex_kh
        from project.module.agri_bill.logic import location_logic
        location = location_logic.default.get_location(farmer.id)
        from project.module.agri_bill.logic import land_logic
        land = land_logic.default.view_land_by(block_id=obj.block_id, farmer_id=obj.farmer_id)
        total_area = land_logic.default.sum_land_area(query=land)
        data = DataView(query=land)
        if content_template.id == '2':
            # land.table = LandVerifyTabel_Ha(data=data.result)
            # total_area = to_ha(total_area[0])
            pass
        else:
            from project.module.agri_bill.view.verify_billing_view import LandVerifyTabel
            land.table = LandVerifyTabel(data=data.result)
            total_area = total_area[0]
        before = '<div style="page-break-before: always;"></div>'
        try:
            content = render_template_string(content_template.content_template,
                                             contract_number=obj.contract_number,
                                             billing=billing,
                                             block=block,
                                             farmer=farmer,
                                             location=location,
                                             land=land,
                                             total_area=total_area,
                                             before=before
                                             )
            return content
        except:
            return ''

    def add_new_agreement_by_farmer_ids(self, billing_id, farmer_ids):

        try:
            q = (db.query(Land.farmer_id.label('farmer_id'), Block.id.label('block_id'),
                          Block.code.label('block_code'), Station.id.label('station_id'),
                          Farmer.code.label('farmer_code'))
                .join(Block, Block.id == Land.block_id)
                .join(Station, Station.id == Block.station_id)
                .join(Farmer, Farmer.id == Land.farmer_id)
                .filter(Land.active == 'active')
                .filter(Land.is_active, Land.farmer_id.in_(farmer_ids))
                .group_by(
                Land.farmer_id,
                Block.id,
                Block.code,
                Station.id,
                Farmer.code
            ))
            result = [i._asdict() for i in q.all()]

            verify_billing = []
            from project.module.agri_bill.logic import billing_logic
            from project.module.agri_bill.logic import billing_cycle_logic
            obj = billing_logic.default.find(billing_id)
            billing_cycle = billing_cycle_logic.default.find(obj.billing_cycle_id)
            for item in result:
                verify = Verify()
                verify.id = gid()
                verify.billing_id = billing_id
                verify.station_id = item.get('station_id')
                verify.block_id = item.get('block_id')
                verify.farmer_id = item.get('farmer_id')
                verify.status = enum.VerifyStatus.verified
                verify.contract_number = str(obj.end_date.strftime(
                    "%y")) + "-" + billing_cycle.ext__code + "-" + item.get('block_code') + "-" + item.get(
                    'farmer_code')  # contract number follow by year-block_code-farmer-code
                verify_billing.append(verify)
                db.flush()
            db.bulk_save_objects(verify_billing)
            db.commit()
            return verify_billing
        except Exception as e:
            pass

    def add_new_agreement_by_block_ids(self, billing_id, block_ids):
        from project.module.agri_bill.logic import billing_logic
        block = db.query(Verify).filter(Verify.billing_id == billing_id).filter(Verify.block_id.in_(block_ids)).all()
        if block:
            return ''

        verify_obj = billing_logic.default.get_verify_billing(block_ids=block_ids)
        obj = billing_logic.default.find(id=billing_id)
        if not obj.ext__add_blocks:
            obj.ext__add_blocks = []
        obj.ext__add_blocks += block_ids
        db.add(obj)
        db.flush()
        from project.module.agri_bill.logic import billing_cycle_logic
        billing_cycle = billing_cycle_logic.default.find(obj.billing_cycle_id)
        verify_billing = []
        for item in verify_obj:
            verify = Verify()
            verify.id = gid()
            verify.billing_id = obj.id
            verify.station_id = item.station_id
            verify.block_id = item.block_id
            verify.farmer_id = item.farmer_id
            verify.status = enum.VerifyStatus.unverified
            verify.contract_number = str(obj.end_date.strftime(
                "%y")) + "-" + billing_cycle.ext__code + "-" + item.block_code + "-" + item.farmer_code  # contract number follow by year-block_code-farmer-code
            verify_billing.append(verify)
            db.flush()
        db.bulk_save_objects(verify_billing)
        db.commit()

    def find_total_land(self, **kwargs):
        billing_id = kwargs.get('billing_id')
        block_id = kwargs.get('block_id')
        search = kwargs.get('search')
        q = (db.query(
            func.row_number().over(order_by=Verify.block_id).label('_row_number'),
            Verify.billing_id.label('billing_id'),
            Verify.id.label('id'),
            Verify.ext_data,
            Block.name.label('block_name'),
            Farmer.name.label('farmer_name'),
            Farmer.code.label('farmer_code'),
            Farmer.spouse_name.label('spouse_name'),
            Block.id.label('block_id'),
            Farmer.id.label('farmer_id')

        )
             .join(Block, Block.id == Verify.block_id)
             .join(Farmer, Farmer.id == Verify.farmer_id)
             .filter(Verify.billing_id == billing_id)
             .filter(Verify.is_active)
             )
        if block_id and block_id != '__None' and block_id != 'None':
            q = q.filter(Verify.block_id == block_id)
        if search:
            search = search.strip()
            q = q.filter(
                or_(
                    func.lower(Farmer.code).like('%' + search.lower() + '%'),
                    func.lower(Farmer.name).like('%' + search.lower() + '%'),
                    func.lower(Farmer.spouse_name).like('%' + search.lower() + '%'),
                    func.lower(Block.name).like('%' + search.lower() + '%'),
                    func.lower(Block.code).like('%' + search.lower() + '%')
                )
            )
        q = q.order_by(Block.code, Farmer.name)
        return q


default = VerifyBillingLogic()
