from project.module.currency.model import Currency
from project.module.system.logic import autono_logic
from ...share.view.base_view import *
from ...agri_bill.model import *
from flask_babel import gettext as _


class FarmerLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = Farmer

    def search(self, **kwargs):
        search = kwargs.get('search') or ''
        active = kwargs.get('is_active') or kwargs.get('active') or None
        marital_status = kwargs.get('marital_status') or None
        village_id = kwargs.get('village_id') or None

        q = self.actives()
        if search:
            search = search.strip()
            q = q.filter(
                or_(
                    func.lower(Farmer.code).like('%' + search.lower() + '%'),
                    func.lower(Farmer.name).like('%' + search.lower() + '%'),
                    func.lower(Farmer.spouse_name).like('%' + search.lower() + '%'),
                )
            )
        if active:
            q = q.filter(Farmer.active == active)
        if marital_status:
            q = q.filter(Farmer.marital_status == marital_status.lower())
        if village_id:
            q = q.filter(Farmer.village_id == village_id)

        return q.order_by(func.right('000000' + Farmer.code, 6))

    def new(self):
        # obj = super(CustomerLogic, self).new()
        #         # obj.code = autono_logic.default.next_no(name='customer-code')
        #         # return obj
        company = current.company()
        seq_id = None
        if company.ext.get('farmer_seq') == 'auto':
            seq_id = company.ext.get('seq', dict(farmer='0')).get('farmer-code', '0')
        obj = self.__classname__()
        if seq_id:
            obj.code = autono_logic.default.next_no(id=seq_id)
        return obj

    def add(self, obj):
        # code = func.right('000000' + obj.code, 6)
        code = str(obj.code).zfill(6)
        if len(code)>6:
            raise ValueError("{0}".format(code) + _(u'Farmer code must equal 6 digit'))
        obj.code = code
        old = self.find(code=code)
        if old:
            if old.active == 'active':
                raise ValueError("{0} ".format(obj.code) + _(u'Farmer code already exist'))

        company = current.company()
        # seq_id = None
        if company.ext.get('farmer_seq') == 'auto':
            seq_id = company.ext.get('seq', dict(farmer='0')).get('farmer-code', '0')
        # if seq_id:
        #     obj.code = autono_logic.default.next_no(id=seq_id, update=True)
        if obj.village_id == None:
            obj.village_id = 'VNA'
        obj = super(FarmerLogic, self).add(obj)
        return obj

    def find(self, id='', code=''):
        q = self.actives()
        if id:
            return q.filter(self.__classname__.id == id).first()
        if code:
            return q.filter(self.__classname__.code == code).first()

    def update(self, obj, _commit=True):
        id = obj.id
        old = self.find(obj.id)
        if obj.active =="disactive":
            land = db.query(Land).filter(or_(Land.farmer_id == id, Land.rent_id == id)) \
                .filter(Land.is_active).first()
            bill = db.query(Invoice).filter(Invoice.farmer_id == id) \
                .filter(Invoice.status_id.in_(['pending', 'partial'])) \
                .filter(Invoice.is_active).first()
            if land:
                return False
            elif bill:
                return False

        # if obj.active == 'active':
        #     old.active = 1
        # if obj.is_active == 'disactive':
        #     old.is_active = 0

        if not obj.active:
            obj.active = getattr(old, 'active', 'active')
        copyupdate(obj, old)
        db.flush()
        self.trigger(self.EVENT_UPDATED, obj=obj)
        db.commit()
        return obj

    def get_marital_status(self, lookup_value_id):
        q = db.query(LookupValue).filter(LookupValue.is_active, LookupValue.id == lookup_value_id).first()
        if not q:
            q = db.query(LookupValue).filter(LookupValue.is_active, LookupValue.value == lookup_value_id).first()
        return q

    def find_invoice_by_farmer(self, id):
        q = (
            db.query(
                func.count(Invoice.id).label('total_invoice'),
                func.sum(Invoice.total_amount).label('total_amount'),
                func.sum(Invoice.paid_amount).label('total_paid_amount'),
                Invoice.status_id.label('value'),
                Invoice.currency_id.label('currency'),
                Currency.sign.label('sign'),
            )
                .join(Currency, Currency.id == Invoice.currency_id)
                .filter(Invoice.is_active, Invoice.farmer_id == id)
                .group_by(Invoice.status_id, Invoice.currency_id, Currency.sign).subquery())
        result = (db.query(
            q.c.total_invoice,
            q.c.total_amount,
            q.c.total_paid_amount,
            func.sum(q.c.total_amount - q.c.total_paid_amount).label('ending_balance'),
            q.c.value,
            q.c.currency,
            q.c.sign,
            LookupValue.name.label('status_value')
        ).join(LookupValue, LookupValue.value == q.c.value).group_by(
            q.c.total_invoice,
            q.c.total_amount,
            q.c.total_paid_amount,
            q.c.value,
            q.c.currency,
            q.c.sign,
            LookupValue.name
        ).all())
        return result

    # find farmer invoice
    def invoice_payment_suggestion(self, **kwargs):
        search = kwargs.get('search') or ''
        q = self.search(search=search)
        q = q.join(Invoice, Invoice.farmer_id == Farmer.id)
        q = q.order_by(Invoice.created_date)
        q = q.filter(Invoice.status_id != 'paid', Invoice.status_id != 'void', Invoice.is_active)
        return q

    def remove(self, obj, _commit=True):
        if hasattr(obj, 'active') and obj.active == 'active':
            obj.active = 'disactive'
        else:
            # avoid error: Instance '<Object>' is not persisted
            obj = self.find(obj.id)
            obj.is_active = False
        db.flush()
        self.trigger(self.EVENT_REMOVED, obj=obj)
        self._commit(_commit=_commit)

    def is_remove_all(self, ids, is_remove=False):
        try:
            mapping_farmer = []
            if is_remove == 'false':
                for id in ids:
                    farmer = self.find(id)
                    if farmer:
                        mapping_farmer.append({'id': farmer.id, 'active': 'disactive'})
                        db.bulk_update_mappings(Farmer, mapping_farmer)
                        db.commit()
                return "success"
            else:
                for id in ids:
                    land = db.query(Land).filter(or_(Land.farmer_id == id, Land.rent_id == id)) \
                        .filter(Land.is_active).first()
                    bill = db.query(Invoice).filter(Invoice.farmer_id == id) \
                        .filter(Invoice.status_id.in_(['pending', 'partial'])) \
                        .filter(Invoice.is_active).first()
                    if land:
                        return "false"
                    elif bill:
                        return "false"
                    else:
                        farmer = self.find(id)
                        if farmer:
                            mapping_farmer.append({'id': farmer.id, 'is_active': 0})
                            db.bulk_update_mappings(Farmer, mapping_farmer)
                            db.commit()
                return "success"

            #     for id in ids:
            #         farmer = self.find(id)
            #         if farmer:
            #             mapping_farmer.append({'id': farmer.id, 'is_active': 0})
            #     db.bulk_update_mappings(Farmer, mapping_farmer)
            # db.commit()
            # return "success"
        except:
            return "false"

    def update_all_farmers(self, ids, data, _commit=True):
        marital_status = data['marital_status'] or None
        village_id = data['village_id'] or None
        active = data['active'] or None
        sex = data['sex'] or None

        mapping_farmer = []
        farmers = db.query(Farmer).filter(Farmer.id.in_(ids)).all()
        for farmer in farmers:
            mapping_farmer.append({
                'id': farmer.id,
                'marital_status': marital_status if marital_status else farmer.marital_status,
                'village_id': village_id if village_id else farmer.village_id,
                'active': active if active else farmer.active,
                'sex': sex if sex else farmer.sex
            })
        db.bulk_update_mappings(Farmer, mapping_farmer)
        db.flush()
        db.commit()
        return _(u"update successfully!")

    def check_exist_farmer_in_land(self, id):
        land = db.query(Land).filter(or_(Land.farmer_id == id, Land.rent_id == id))\
            .filter(Land.is_active).first()
        bill = db.query(Invoice).filter(Invoice.farmer_id==id)\
            .filter(Invoice.status_id.in_(['pending','partial']))\
            .filter(Invoice.is_active).first()
        if land:
            return False
        elif bill:
            return False
        return True

    def find_last_farmer_code(self):
        company = current.company()
        if company.ext.get('farmer_seq') == 'auto':
            obj = db.query(Farmer.code).filter(Farmer.active == 'active', Farmer.is_active).order_by(Farmer.code.desc()).first()
            code = int(obj.code) + 1
            return str(code).zfill(6)
        else:
            return ''

default = FarmerLogic()
