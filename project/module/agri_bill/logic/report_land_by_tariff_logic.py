from ..model import *
from flask_babel import gettext as _
from edf.base.logic import *
from sqlalchemy import cast, literal, case


class LandLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = Land

    def new(self):
        """
        :rtype : Land
        """
        land = Land()
        return land

    def search(self, **kwargs):
        search = kwargs.get('search') or ''
        block_id = kwargs.get('block_id') or None
        tariff_id = kwargs.get('tariff_id') or None
        active = kwargs.get('active') or None
        status_id = kwargs.get('status_id') or None
        q = db.query(Land).filter(Land.is_active)
        if search:
            search = search.strip()
            farmers = (
                db.query(Farmer)
                    .filter(
                    or_(
                        func.lower(Farmer.code).like('%' + search.lower() + '%'),
                        func.lower(Farmer.spouse_name).like('%' + search.lower() + '%'),
                        func.lower(Farmer.name).like('%' + search.lower() + '%'))
                ).all()
            )
            if farmers:
                farmer_ids = []
                rent_id = []
                for farmer in farmers:
                    farmer_ids.append(farmer.id)
                q = q.filter(or_(Land.farmer_id.in_(farmer_ids), Land.rent_id.in_(farmer_ids), Land.code == search))
            else:
                q = q.filter(
                    or_(
                        func.lower(Land.code).like('%' + search.lower() + '%'),
                    )
                )
        if block_id:
            q = q.filter(Land.block_id == block_id)
        if tariff_id:
            q = q.filter(Land.tariff_id == tariff_id)
        if status_id:
            q = q.filter(Land.status_id == status_id)
        if active:
            q = q.filter(Land.active == active)
        q = q.join(Farmer, Farmer.id == Land.farmer_id).filter(Farmer.is_active).order_by(asc(Farmer.name), Farmer.code)
        q = q.order_by(func.right('000000' + Land.code, 6))
        return q

    def find_lands(self, **kwargs):
        search = kwargs.get('search') or ''
        q = self.actives()
        if search:
            #     q = q.filter(
            #         or_(
            #             func.lower(Land.code).like('%' + search.lower() + '%'),
            #         )
            #     )
            q = q.filter(Land.code == search.lower())
        return q

    def search_tariff(self):
        q = db.query(Tariff).filter(Tariff.is_active)
        return q

    def sum_land_area(self, block_ids='', query='', land_ids=''):
        # type: (object, object, object) -> object

        if block_ids:
            q = (
                db.query
                    (
                    func.sum(Land.area).label('total_area'),
                    func.count(Land.id).label('total_land'),
                    func.count(Land.farmer_id.distinct()).label('total_farmer'),
                    Station.id.label('station_id'),
                    Station.name.label('station_name'),
                    Land.tariff_id.label('tariff_id'),
                    Tariff.name.label('tariff_name'),
                    Tariff.price.label('unit_cost')
                )
                    .join(Block, Block.id == Land.block_id)
                    .join(Station, Station.id == Block.station_id)
                    .join(Tariff, Tariff.id == Land.tariff_id)
                    .join(Farmer, Farmer.id == Land.farmer_id)
                    .join()
                    .filter(Farmer.is_active)
                    .filter(Land.block_id.in_(block_ids))
                    .filter(Tariff.is_active)
                    .filter(Land.tariff_id == Tariff.id)
                    .filter(Land.active == 'active')
                    .filter(Land.status_id == 'irrigate', Land.is_active)
                    .group_by(Station.id, Station.name, Land.tariff_id, Tariff.name, Tariff.price))

            result = [r._asdict() for r in q.all()]
            return result
        if query:
            query = query.subquery()
            q = db.query(
                func.sum(query.c.area).label('total_area')).filter(query.c.status_id == 'irrigate').group_by(
                query.c.block_id,
                query.c.farmer_id
            ).first()
            return q
        if land_ids:
            q = (
                db.query(
                    func.sum(Land.area).label('total_area'))
                    .filter(Land.id.in_(land_ids))
            )
            total_area = q.first().total_area
            return json.dumps(total_area)

    def view_land_by(self, block_id, farmer_id):
        q = (db.query(
            Land)
             .filter(Land.block_id == block_id)
             .filter(Land.farmer_id == farmer_id)
             .filter(Land.is_active)
             .filter(Land.active == 'active')
             .order_by(func.right('000000' + Land.code, 6)))
        return q

    def update_land(self, id, **kwargs):
        rent_id = kwargs.get('rent_id') or None
        status_id = kwargs.get('status_id') or None
        tariff_id = kwargs.get('tariff_id') or None
        area = kwargs.get('area') or None
        block_id = kwargs.get('block_id') or None
        farmer_id = kwargs.get('farmer_id') or None
        land = db.query(Land).filter(Land.id == id).first()

        if rent_id:
            land.rent_id = rent_id
        elif rent_id == '__None':
            land.rent_id = None
        if status_id:
            land.status_id = status_id
        if tariff_id:
            land.tariff_id = tariff_id
        if area:
            land.area = area
        if block_id:
            land.block_id = block_id
        if farmer_id:
            land.farmer_id = farmer_id
        db.add(land)
        db.commit()
        return 'updated'

    def find(self, id='', code='', update=False, farmer_id='', rent_id=''):
        if id:
            result = db.query(self.__classname__).filter(Land.id == id).first()
            return result
        if code:
            result = db.query(Land).filter(Land.code == code).first()
            return result
        if farmer_id:
            result = (
                db.query(Land)
                    .filter(Land.farmer_id == farmer_id)
                    .order_by(func.right('000000' + Land.code, 6))
                    .all()
            )
            return result
        if rent_id:
            result = (
                db.query(Land)
                    .filter(Land.rent_id == rent_id)
                    .order_by(func.right('000000' + Land.code, 6))
                    .all()
            )
            return result

    def update(self, obj, _commit=True):
        old = self.find(obj.id)
        if not obj.active:
            obj.active = getattr(old, 'active', 'active')
        copyupdate(obj, old)
        db.flush()
        self.trigger(self.EVENT_UPDATED, obj=obj)
        db.commit()
        return obj

    def remove(self, obj, _commit=True):
        if hasattr(obj, 'active') and obj.active == 'active':
            obj.active = 'disactive'
        else:
            # avoid error: Instance '<Object>' is not persisted
            obj = self.find(obj.id)
            obj.is_active = False
        db.flush()
        self.trigger(self.EVENT_REMOVED, obj=obj)
        self._commit(_commit=_commit)

    def update_all_lands(self, ids, data, _commit=True):
        farmer_id = data['farmer_id'] or None
        rent_id = data['rent_id'] or None
        status_id = data['status_id'] or None
        tariff_id = data['tariff_id'] or None
        block_id = data['block_id'] or None
        active = data['active'] or None
        note = data['note'] or None
        mapping_land = []
        lands = db.query(Land).filter(Land.id.in_(ids)).all()
        for land in lands:
            mapping_land.append({
                'id': land.id,
                'farmer_id': farmer_id if farmer_id else land.farmer_id,
                'rent_id': rent_id if rent_id else land.rent_id,
                'status_id': status_id if status_id else land.status_id,
                'tariff_id': tariff_id if tariff_id else land.tariff_id,
                'block_id': block_id if block_id else land.block_id,
                'active': active if active else land.active,
                'note': note if note else land.note
            })

        db.bulk_update_mappings(Land, mapping_land)
        db.flush()
        db.commit()
        return _(u"update successfully!")

    def is_remove_all(self, ids, is_remove=False):
        try:
            mapping_land = []
            if is_remove == 'false':
                for id in ids:
                    land = self.find(id)
                    if land:
                        mapping_land.append({'id': land.id, 'active': 'disactive'})
                db.bulk_update_mappings(Land, mapping_land)
            else:
                for id in ids:
                    land = self.find(id)
                    if land:
                        mapping_land.append({'id': land.id, 'is_active': 0})
                db.bulk_update_mappings(Land, mapping_land)
            db.commit()
            return "success"
        except:
            return False

    def report_by_billing(self, **kwargs):
        billing_id = kwargs.get('billing_id')
        if not billing_id:
            billing = db.query(Billing).filter(Billing.billing_status_id == 'close', Billing.is_active).order_by(
                desc(Billing.created_date)).first()
            billing_id = billing.id
        block_id = kwargs.get('block_id')
        tariff_id = kwargs.get('tariff_id')
        Rent = aliased(Farmer, name='rent')
        q = (db.query(
            func.row_number().over(order_by=(func.right('000000' + Land.code, 6))).label(
                '_row_number'),
            Land.code.label('code'),
            BillingLand.billing_id.label('billing_id'),
            BillingLand.land_id,
            BillingLand.area.label('area'),
            Tariff.name.label('tariff'),
            Block.code.label('block_code'),
            Block.name.label('block_name'),
            Farmer.name.label('farmer_name'),
            Farmer.spouse_name.label('spouse_name'),
            Farmer.code.label('farmer_code'),
            Rent.name.label('rent_name'),
            Rent.spouse_name.label('rent_spouse'),
            Rent.code.label(('rent_code'))
        )
             .join((BillingLand, BillingLand.land_id == Land.id))
             .join((Tariff, Tariff.id == BillingLand.tariff_id))
             .join((Block, Block.id == BillingLand.block_id))
             .join((Farmer, Farmer.id == BillingLand.owner_farmer_id))
             .join((Rent, Rent.id == BillingLand.invoice_farmer_id))
             .filter(BillingLand.is_active)
             )
        if billing_id:
            q = q.filter(BillingLand.billing_id == billing_id)
        if block_id:
            q = q.filter(BillingLand.block_id == block_id)
        if tariff_id:
            q = q.filter(BillingLand.tariff_id == tariff_id)

        q = q.order_by(func.right('000000' + Land.code, 6))
        return q


default = LandLogic()
