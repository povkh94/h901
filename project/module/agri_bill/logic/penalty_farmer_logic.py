from project.module.agri_bill import enum
from project.module.agri_bill.convert import round_riel
from project.module.agri_bill.logic import invoice_logic
from project.module.currency.model import Currency
from project.module.system.logic import autono_logic
from ...share.view.base_view import *
from ...agri_bill.model import *
from sqlalchemy import case


class PenaltyFarmerLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = Invoice

    def new(self, seq_id=0):
        obj = self.__classname__()
        return obj

    def search(self, **kwargs):
        search = kwargs.get('search') or ''
        billing_id = kwargs.get('billing_id') or None
        date = datetime.today()
        q = (
            db.query(
                func.row_number().over(order_by=(Invoice.billing_id)).label(
                    '_row_number'),

                Farmer.name.label('name'),
                Farmer.code.label('code'),
                Farmer.spouse_name.label('spouse_name'),
                Invoice.farmer_id.label('farmer_id'),
                Invoice.billing_id.label('billing_id'),
                Invoice.currency_id,
                Currency.sign.label('currency'),
                func.count(Invoice.id).label('total_invoices'),
                func.sum(Invoice.total_amount - Invoice.paid_amount).label('total_amount')
            )
                .join(Farmer, Farmer.id == Invoice.farmer_id)
                .join(Billing, Billing.id == Invoice.billing_id)
                .join(Currency, Currency.id == Invoice.currency_id)
                .outerjoin(InvoicePenalty, InvoicePenalty.invoice_id == Invoice.id)
                .filter(Billing.is_active, Invoice.is_active)
                .filter(Invoice.due_date < date)
                .filter(or_(Invoice.invoice_type != enum.InvoiceType.PENALTY, Invoice.invoice_type == None,
                            Invoice.invoice_type == ''))
                .filter(or_(Invoice.status_id == 'pending', Invoice.status_id == 'partial'))
                .filter(InvoicePenalty.invoice_id == None)

        )
        if search:
            search = search.strip()
            q = q.filter(or_(
                func.lower(self.__classname__.code).like('%' + search.lower() + '%'),
                func.lower(Farmer.name).like('%' + search.lower() + '%'),
                func.lower(Farmer.spouse_name).like('%' + search.lower() + '%'),
                func.lower(Farmer.code).like('%' + search.lower() + '%')
            )
            )

        if billing_id:
            q = q.filter(Billing.id == billing_id)
        q = q.group_by(
                       Farmer.name,
                       Farmer.code,
                       Farmer.spouse_name,
                       Invoice.farmer_id,
                       Invoice.billing_id,
                       Invoice.currency_id,
                       Currency.sign)
        q = q.order_by(
            Farmer.name,
            Farmer.code,
            Farmer.spouse_name
        )
        return q

    def process(self, value, penalty_type, issue_date, due_date, objects):
        bulk_invoices = []
        bulk_invoices_penalty = []
        invoice_penalty_id_map = dict()
        objects = json.loads(objects)
        for obj in objects:
            invoices = invoice_logic.default.find_invoices(obj['billing_id'], obj['farmer_id'])
            for item in invoices:
                seq_id = current.company().ext.get('seq', dict(invoice='')).get('invoice-code', '')
                if seq_id:
                    code = autono_logic.default.next_no(id=seq_id, update=True)
                invoice = Invoice()
                invoice.id = gid()
                invoice_penalty_id_map[(item.id, item.farmer_id)] = invoice.id
                invoice.code = code
                invoice.billing_id = item.billing_id
                invoice.farmer_id = item.farmer_id
                invoice.owner_farmer_id = item.owner_farmer_id
                invoice.issue_date = issue_date
                invoice.due_date = due_date
                invoice.is_active = True
                invoice.station_id = item.station_id
                invoice.status_id = 'pending'
                invoice.block_id = item.block_id
                invoice.village_id = item.village_id
                invoice.currency_id = item.currency_id
                invoice.total_area = item.total_area
                invoice.total_lands = item.total_lands
                if penalty_type == enum.PenaltyType.PERCENT:
                    total_amount = (item.total_amount * value) / 100
                else:
                    total_amount = value
                invoice.total_amount = round_riel(total_amount)
                invoice.paid_amount = 0
                invoice.invoice_type = enum.InvoiceType.PENALTY
                invoice_penalty = InvoicePenalty()
                invoice_penalty.id = gid()
                invoice_penalty.invoice_id = item.id
                invoice_penalty.penalty_invoice_id = invoice_penalty_id_map[(item.id, item.farmer_id)]
                invoice_penalty.currency_id = item.currency_id
                invoice_penalty.is_active = True
                invoice_penalty.amount = round_riel(total_amount)
                bulk_invoices.append(invoice)
                bulk_invoices_penalty.append(invoice_penalty)
        db.bulk_save_objects(bulk_invoices)
        db.bulk_save_objects(bulk_invoices_penalty)
        db.flush()
        db.commit()
        return objects[0]['billing_id']


default = PenaltyFarmerLogic()
