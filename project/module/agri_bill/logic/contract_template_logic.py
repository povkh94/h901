from sqlalchemy import distinct, update

from project.module.system.logic import autono_logic
from ...share.view.base_view import *
from ...agri_bill.model import *

from sqlalchemy import case


class ContractTemplateLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = ContractTemplate

    def search(self, **kwargs):
        search = kwargs.get('search') or None
        q = db.query(ContractTemplate)
        if search:
            search = search.strip()
            q = (q.filter(func.lower(self.__classname__.name).like('%' + search.lower() + '%')))
        return q

    def add(self, obj, _commit=True):
        obj.is_active = 0
        db.add(obj)
        db.flush()
        if obj.contract_template_id == b'billing':
            verify_billing = db.query(Verify)
            for item in verify_billing:
                item.contract_template_id = obj.id
                db.flush()
        db.commit()


    def find_content_template(self, contract_template_id='', is_active=''):
        if contract_template_id:
            return db.query(ContractTemplate).filter(ContractTemplate.contract_template_id == contract_template_id,
                                                     ContractTemplate.is_active).first()
        if is_active:
            return db.query(ContractTemplate).all()

    def update_template(self, id):
        if id:
            row = db.query(ContractTemplate).filter(ContractTemplate.is_active).first()
            row.is_active = 0
            db.add(row)
            db.flush()
            contract = db.query(ContractTemplate).filter(ContractTemplate.id == id).first()
            contract.is_active = 1
            db.add(contract)
            db.commit()
            return 1
        return 0

default = ContractTemplateLogic()
