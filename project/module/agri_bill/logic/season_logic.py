from project.module.agri_bill.model import Season
from edf.base.logic import *


class SeasonLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = Season

    def new(self):
        """
        :rtype : Season
        """
        season = Season()
        season.company_id = current.company_id()
        return season

    def all(self):
        q = db.query(Season).filter(Season.is_active)
        return q


default = SeasonLogic()
