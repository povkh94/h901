from ..model import *
from edf.base.logic import *
from project.module.agri_bill.logic import land_logic
from sqlalchemy import cast, literal, case


class BlockLogic(LogicBase):
    def __init__(self, *args, **kw_args):
        self.__classname__ = Block

    def new(self):
        """
        :rtype : Block
        """
        block = Block()
        block.company_id = current.company_id()
        return block

    def search(self, **kwargs):
        search = kwargs.get('search') or ''
        station_id = kwargs.get('station_id') or None

        q = self.actives()
        if search:
            q = q.filter(or_
                         (func.lower(self.__classname__.name).like('%' + search.lower() + '%')))
        if station_id:
            q = q.filter(Block.station_id == station_id)

        return q.order_by(func.right('000000'+Block.code,6))

    def filter(self, **kwargs):
        user = current.user()
        block_ids = None
        if user.ext__block_ids:
            block_ids = user.ext__block_ids.split(';')
        q = (db.query(Block.id, (Station.name + '-' + Block.name).label('name'))
             .join(Station, Station.id == Block.station_id)
             # .filter(Block.is_active)
             .filter(Station.is_active)
             )
        if block_ids:
            q = q.filter(Block.id.in_(block_ids))
        q = q.order_by(func.right('000000'+Station.code,6), func.right('000000'+Block.code,6))
        return q

    def filter_block_active(self, **kwargs):
        q = (db.query(Block.id, (Station.name + '-' + Block.name).label('name'))
             .join(Station, Station.id == Block.station_id)
             .filter(Block.is_active)
             .filter(Station.is_active)
             .order_by(func.right('000000'+Station.code,6), func.right('000000'+Block.code,6))
             )
        return q

    def find(self, id=0,billing_cycle_id=0):
        if id:
            q = db.query(self.__classname__).filter_by(id=id).first()
            return q
        if billing_cycle_id:
            q = db.query(self.__classname__)

    def find_block_have_land(self,ids=0):
        land = land_logic.default.find_block_id_in_land(id=ids)
        if land:
            return True
        else:
            return False

    def remove_block(self, id=0, _commit=True):
        block = self.find(id=id)
        block.is_active = False
        db.commit()
        return True




default = BlockLogic()
