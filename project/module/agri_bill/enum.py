from edf.base.model import Enum


class component_type(Enum):
    _val_ = 102
    menu_group = 1
    menu = 2
    report_center = 3
    report_group = 5
    report = 6
    report_template = 7
    form = 8
    field = 9


class BankPaymentActionType(Enum):
    _val_ = '1002'
    UPDATE_CUSTOMER = '10021'
    UPDATE_PAYMENT = '10022'
    GET_PAYMENT = '10023'


class BankPaymentResult(Enum):
    __val__ = '1003'
    INCORRECT = '10031'
    CORRECT = '10032'
    DUPLICATED = '10033'


class InvoiceType(Enum):
    __val__ = '1004'
    IRRIGATE = '10041'
    PENALTY = '10042'


class pay_status(Enum):
    __val__ = '1005'
    ALL = 'all'
    PENDING = 'pending'
    PARTIAL = 'partial'
    PAID = 'paid'


class payment_method(Enum):
    __val__ = '1001'
    CASH = '10011'
    SmartLuy = '10012'
    E_Money = '10013'


class PenaltyType(Enum):
    __val__ = '1006'
    PERCENT = '10061'
    CASH = '10062'


class VerifyStatus(Enum):
    all = 'all_verify'
    unverified = 'unverified'
    verified = 'verified'


class InvoiceStatus(Enum):
    void = 'void'


class LandStatus(Enum):
    irrigate = 'irrigate'
    n_irrigate = 'n_irrigate'
