from edf_admin.table import Table
from flask import request
from flask_babel import lazy_gettext as _
from flask_classy import route
from project.module.agri_bill.model import *
from project.module.share.view.base_view import *
from project.module.agri_bill.model import Location
from project.module.agri_bill.logic import location_logic



class ProvinceForm(FlaskForm):
    id = HiddenField()
    parent_id = HiddenField()
    type = HiddenField()
    code = TextField(
        _(u'Province Code'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )
    name = TextField(
        _(u'Province Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )
    latin_name = TextField(
        _(u'Province Latin Name'),
        render_kw={
            'data-val': 'true',
            'autocomplete':'off'
        }
    )


class ProvinceFilterForm(FlaskForm):
      type = HiddenField()

class ProviceTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    code = LinkColumn(_(u'Province Code'), get_url=lambda row: url_for('.ProvinceView:detail', id=row.id),
                      get_value=lambda row: row.code)
    name = DataColumn(_(u'Province Name'))
    latin_name = DataColumn(_(u'Province Latin Name'))
    action = ActionColumn('', endpoint='.ProvinceView')


class ProvinceView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Location
        self.v_logic = location_logic.default
        self.v_table_index_class = ProviceTable
        self.v_form_add_class = ProvinceForm
        self.v_form_filter_class = ProvinceFilterForm
        self.v_template = 'province'
        self.v_breadcrum.append({'url': "/setting/index", 'title': _('Setting')})
        self.v_logic_order_by = 'code'

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _(u'Province')

    def _on_index_rendering(self, **kwargs):
        self.v_data['form'].type.data = '1'

ProvinceView.register(app)
