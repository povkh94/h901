﻿from datetime import datetime

from edf_admin.table import Group
from flask_babel import lazy_gettext as _

from project.module.agri_bill import enum
from project.module.agri_bill.table import AmountColumn, AreaColumn
from project.module.share.app_filter import format_integer, format_currency, to_riel
from project.module.system.logic import lookup_logic

from project.module.agri_bill.convert import format_value
from ...share.view.base_view import *
from wtforms import SelectField, DateField, HiddenField
from project.module.agri_bill.logic import report_payment_logic, season_logic, farmer_logic, report_billing_logic, \
    billing_logic, station_logic, location_logic, block_logic, report_bank_logic, bank_payment_logic, land_logic, report_land_by_tariff_logic
from project.module.share.reports import SystemReportCenter, SimpleReport, DataColumn, ReportGroup

SystemReportCenter.center.register_group(ReportGroup(name='general-group', title=_('General Reports')))
SystemReportCenter.center.register_group(ReportGroup(name='payment-group', title=_('Payment Reports')))
SystemReportCenter.center.register_group(ReportGroup(name='billing-group', title=_('Crop Based Bill Payment Reports')))
SystemReportCenter.center.register_group(
    ReportGroup(name='billing-invoice-penalty-group', title=_('Invoice Penalty Report')))
SystemReportCenter.center.register_group(ReportGroup(name='debt-group', title=_('Debt Reports')))
SystemReportCenter.center.register_group(ReportGroup(name='bank-group', title=_(u'Bank Reports')))

'''
   Payment Report
'''

months = [u'មករា', u'កុម្ភៈ', u'មីនា', u'មេសា', u'ឧសភា', u'មិថុនា', u'កក្កដា', u'សីហា', u'កញ្ញា', u'តុលា', u'វិចិក្កា',
          u'ធ្នូ']


# Payment Daily Report
def convert_to_mon(row):
    from project.module.agri_bill import convert
    if hasattr(row, 'date'):
        if hasattr(row.date, 'strftime'):
            d = row.date.strftime('%d')
            m = to_int(row.date.strftime('%m'))
            y = row.date.strftime('%Y')
        else:
            date = row.date.split('-')
            y = date[0]
            m = to_int(date[1])
            d = date[2]
        return y + "-" + months[m - 1] + '-' + d

    if hasattr(row, 'start_date') and hasattr(row, 'end_date'):
        start_date = row.start_date
        end_date = row.end_date
        start_month = start_date.strftime('%m')
        end_month = end_date.strftime('%m')
        year = end_date.strftime('%y')
        return convert.to_month_name(to_int(start_month)) + '-' + convert.to_month_name(to_int(end_month)) + '-' + year


"""
REPORT PAYMENT DETAIL
"""


class ReportRecievedPaymentDetailTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    pay_amount = AmountColumn(_(u'Amount'))
    currency_sign = DataColumn('')


ReportRecievedPaymentDetailTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string(
        '', **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="1">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ data | total("pay_amount", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
),
    Group(
        name='payment_method',
        render_header=lambda **kw: render_template_string(
            '<tr class="group-footer-header"><th colspan="3">{{ _(row.lookup_value) if request.cookies.get("language")=="km" else _(row.lookup_value_eng)  }}</th></tr>',
            **kw),
        render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer" > \
                                                         <th colspan="1">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ row.pay_amount | format_value() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
    )
]


class ReportRecievedPaymentDetailFilterForm(FlaskForm):
    company_id = HiddenField()
    # payment_method = DynamicSelectField(
    #     _(u'Payment Method'),
    #     query_factory=lookup_logic.default.get_values(enum.payment_method.__val__),
    #     get_pk="id",
    #     get_label='value',
    #     render_kw={
    #         'data-val': 'true',
    #         'data-val-required': _(u'Input Required'),
    #         'col-class': 'col-md-12'
    #     }
    # )

    payment_method = SelectField(
        _(u'Payment Method'),
        choices=[("", _(u'All Payment Method'))] + [(v, _(k)) for k, v in enum.payment_method.get_members().items()],
        render_kw={
            'col-class': 'col-md-12'
        }
    )


SystemReportCenter.register(SimpleReport(
    name='payment-detail',
    title=_("Payment Detail"),
    parent='payment-group',
    get_result=lambda **k: report_payment_logic.default.report_payment_detail(**k),
    get_table=lambda **k: ReportRecievedPaymentDetailTable(**k),
    get_filter_form=lambda **k: ReportRecievedPaymentDetailFilterForm(**k)
)
)

""""
REPORT DAILY RECEIVED  PAYMENT
"""


class ReportDailyRecievedTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    date = LamdaColumn(_(u'Date'), get_value=convert_to_mon)
    total_customers = LamdaColumn(_(u'Total Customers'), get_value=lambda row: format_integer(row.total_customers),
                                  render_kw={
                                      'class': 'decimal'
                                  })
    total_invoice = AmountColumn(_(u'Total Invoices'))
    pay_amount = AmountColumn(_(u'Amount'))
    currency_sign = DataColumn('')


ReportDailyRecievedTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string('', **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="2">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ data | total("total_customers", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoice", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("pay_amount", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
), Group(
    name='payment_method',
    render_header=lambda **kw: render_template_string(
        '<tr class="group-footer-header"><th colspan="5">{{ _(row.lookup_value) if request.cookies.get("language")=="km" else _(row.lookup_value_eng)  }}</th></tr>',
        **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer"> \
                                                         <th colspan="2">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ data | total("total_customers", {"payment_method":row.payment_method}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoice", {"payment_method":row.payment_method}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("pay_amount", {"payment_method":row.payment_method}) | format_value() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
)]


class ReportDailyRecievedFilterForm(FlaskForm):
    d1 = DateField(
        _(u'From Date'),
        default=datetime.now().date().replace(day=1),
        render_kw={
            # 'class': 'form-control input-tip date',
            # 'col-class': 'col-md-12'
            'class': 'form-control input-tip date',
            'col-class': 'col-md-12',
            'role': 'date',
            'data-val': 'true',
            'data-val-required': u'Input Required',
            'addon': 'fa-calendar'
        }
    )
    d2 = DateField(
        _(u'To Date'),
        default=datetime.now().date() + timedelta(days=1),
        render_kw={
            'class': 'form-control input-tip date',
            'col-class': 'col-md-12',
            'role': 'date',
            'data-val': 'true',
            'data-val-required': u'Input Required',
            'addon': 'fa-calendar'
        }
    )

    company_id = HiddenField()
    payment_method = SelectField(
        _(u'Payment Method'),
        choices=[("", _(u'All Payment Method'))] + [(v, _(k)) for k, v in enum.payment_method.get_members().items()],
        render_kw={
            'col-class': 'col-md-12'
        }
    )


SystemReportCenter.register(SimpleReport(
    name='daily-payment',
    title=_("Daily Payment"),
    parent='payment-group',
    get_result=lambda **k: report_payment_logic.default.report_daily_payment(**k),
    get_table=lambda **k: ReportDailyRecievedTable(**k),
    get_filter_form=lambda **k: ReportDailyRecievedFilterForm(**k)
)
)


# Monthly Payment Report

class ReportMonthlyRecievedTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    date = LamdaColumn(_('Pay Date'), get_value=lambda r: (
            r.date.split('-')[0] + '-' + months[int(r.date.split('-')[-1]) - 1]) if r.date and '-' in r.date else '-')
    total_customers = LamdaColumn(_(u'Total Customers'), get_value=lambda row: format_integer(row.total_customers),
                                  render_kw={
                                      'class': 'decimal'
                                  })
    total_invoices = AmountColumn(_(u"Total Invoices"))
    pay_amount = AmountColumn(_(u'Amount'))
    currency_sign = DataColumn('')


ReportMonthlyRecievedTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string('', **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="2">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ data | total("total_customers", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoices", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("pay_amount", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
),
    Group(
        name='payment_method',
        render_header=lambda **kw: render_template_string(
            '<tr class="group-footer-header"><th colspan="6">{{ _(row.lookup_value) if request.cookies.get("language")=="km" else _(row.lookup_value_eng)  }}</th></tr>',
            **kw),
        render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer"> \
                                                         <th colspan="2">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ data | total("total_customers", {"payment_method":row.payment_method}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoices", {"payment_method":row.payment_method}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("pay_amount", {"payment_method":row.payment_method}) | format_value() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
    )
]


class ReportMonthlyRecievedFilterForm(FlaskForm):
    d1 = DateField(
        _(u'From Date'),
        default=datetime.now().date().replace(day=1),
        render_kw={
            'class': 'form-control input-tip date date-month',
            'col-class': 'col-md-12',
            'role': 'date-month',
            'data-val': 'true',
            'data-val-required': u'Input Required',
            'addon': 'fa-calendar'
        }
    )

    d2 = DateField(
        _(u'To Date'),
        default=datetime.now().date().replace(day=1, year=datetime.now().year + 1) - timedelta(days=1),
        render_kw={
            'class': 'form-control input-tip date date-month',
            'col-class': 'col-md-12',
            'role': 'date-month',
            'data-val': 'true',
            'data-val-required': u'Input Required',
            'addon': 'fa-calendar'
        }

    )

    company_id = HiddenField()

    payment_method = SelectField(
        _(u'Payment Method'),
        choices=[("", _(u'All Payment Method'))] + [(v, _(k)) for k, v in enum.payment_method.get_members().items()],
        render_kw={
            'col-class': 'col-md-12'
        }
    )


SystemReportCenter.register(SimpleReport(
    name='monthly-payment',
    title=_('Monthly Payment'),
    parent='payment-group',
    get_result=lambda **k: report_payment_logic.default.report_monthly_payment(**k),
    get_table=lambda **k: ReportMonthlyRecievedTable(**k),
    get_filter_form=lambda **k: ReportMonthlyRecievedFilterForm(**k)
)
)


# Yearly Payment Report
class ReportYearlyRecievedTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    date = DataColumn(_(u"Pay Date"))
    total_customers = LamdaColumn(_(u'Total Customers'), get_value=lambda row: format_integer(row.total_customers),
                                  render_kw={
                                      'class': 'decimal'
                                  })
    total_invoices = AmountColumn(_(u"Total Invoices"))
    pay_amount = AmountColumn(_(u'Amount'))
    currency_sign = DataColumn('')


ReportYearlyRecievedTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string('', **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="2">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ data | total("total_customers", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoices", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("pay_amount", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
),
    Group(
        name='payment_method',
        render_header=lambda **kw: render_template_string(
            '<tr class="group-footer-header"><th colspan="6">{{ _(row.lookup_value) if request.cookies.get("language")=="km" else _(row.lookup_value_eng)  }}</th></tr>',
            **kw),
        render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer"> \
                                                         <th colspan="2">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ data | total("total_customers", {"payment_method":row.payment_method}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoices", {"payment_method":row.payment_method}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("pay_amount", {"payment_method":row.payment_method}) | format_value() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
    )
]


class ReportYearlyRecievedFilterForm(FlaskForm):
    d1 = DateField(
        _(u'From Date'),
        default=datetime.now().date().replace(day=1, month=1),
        render_kw={
            'class': 'form-control input-tip date date-year',
            # 'col-class': 'col-md-12'
            'col-class': 'col-md-12',
            'role': 'date-year',
            'data-val': 'true',
            'data-val-required': u'Input Required',
            'addon': 'fa-calendar'
        }
    )

    d2 = DateField(
        _(u'To Date'),
        default=datetime.now().date().replace(day=1, year=datetime.now().year + 1),
        render_kw={
            'class': 'form-control input-tip date date-year',
            'col-class': 'col-md-12',
            'role': 'date-year',
            'data-val': 'true',
            'data-val-required': u'Input Required',
            'addon': 'fa-calendar'
        }
    )

    company_id = HiddenField()
    payment_method = SelectField(
        _(u'Payment Method'),
        choices=[("", _(u'All Payment Method'))] + [(v, _(k)) for k, v in enum.payment_method.get_members().items()],
        render_kw={
            'col-class': 'col-md-12'
        }
    )


SystemReportCenter.register(SimpleReport(
    name='yearly-payment',
    title=_('Yearly Payment'),
    parent='payment-group',
    get_result=lambda **k: report_payment_logic.default.report_year_payment(**k),
    get_table=lambda **k: ReportYearlyRecievedTable(**k),
    get_filter_form=lambda **k: ReportYearlyRecievedFilterForm(**k))
)


def lambda_farmer(row):
    try:
        if row.spouse_name:
            return u'{name} - {spouse_name} - {code}'.format(name=row.farmer_name, spouse_name=row.spouse_name,
                                                             code=row.farmer_code)
        else:
            return u'{name} - {code}'.format(name=row.farmer_name, code=row.farmer_code)
    except Exception as e:
        return ''


# Customer Payment Report
class ReportPaymentByCustomerRecievedTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    farmer_name = LamdaColumn(_(u'Customer Name'), get_value=lambda row: lambda_farmer(row))
    total_invoices = AmountColumn(_(u'Total Invoices'))
    total_area = DecimalColumn(_(u'Total Area'))
    pay_amount = AmountColumn(_(u'Amount'))
    currency_sign = DataColumn('')


ReportPaymentByCustomerRecievedTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string('', **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:blue;color:white"> \
                                                         <th colspan="2">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ data | total("total_invoices", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_area", {"currency":row.currency}) | format_currency() }}</th> \
                                                         <th class="decimal">{{ data | total("pay_amount", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
),
    Group(
        name='payment_method',
        render_header=lambda **kw: render_template_string(
            '<tr class="group-footer-header"><th colspan="6">{{ _(row.lookup_value) if request.cookies.get("language")=="km" else _(row.lookup_value_eng)  }}</th></tr>',
            **kw),
        render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer"> \
                                                         <th colspan="2">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ data | total("total_invoices", {"payment_method":row.payment_method}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_area", {"payment_method":row.payment_method}) | format_currency() }}</th> \
                                                         <th class="decimal">{{ data | total("pay_amount", {"payment_method":row.payment_method}) | format_value() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
    )
]


class ReportCusomterRecievedFilterForm(FlaskForm):
    d1 = DateField(
        _(u'From Date'),
        default=datetime.now().date().replace(day=1),
        render_kw={
            'class': 'form-control input-tip date',
            #        'col-class': 'col-md-12'
            'col-class': 'col-md-12',
            'role': 'date',
            'data-val': 'true',
            'data-val-required': u'Input Required',
            'addon': 'fa-calendar'
        }
    )
    d2 = DateField(
        _(u'To Date'),
        default=datetime.now().date().replace(day=1,
                                              month=datetime.now().month + 1 if datetime.now().month < 12 else 1,
                                              year=datetime.now().year if datetime.now().month <= 11 else datetime.now().year + 1) - timedelta(
            days=1),
        render_kw={
            'class': 'form-control input-tip date',
            #        'col-class': 'col-md-12'
            'col-class': 'col-md-12',
            'role': 'date',
            'data-val': 'true',
            'data-val-required': u'Input Required',
            'addon': 'fa-calendar'
        }
    )

    company_id = HiddenField()
    payment_method = SelectField(
        _(u'Payment Method'),
        choices=[("", _(u'All Payment Method'))] + [(v, _(k)) for k, v in enum.payment_method.get_members().items()],
        render_kw={
            'col-class': 'col-md-12'
        }
    )


SystemReportCenter.register(SimpleReport(
    name='customers payment',
    title=_('Customers Payment'),
    parent='payment-group',
    get_result=lambda **k: report_payment_logic.default.report_by_customer(**k),
    get_table=lambda **k: ReportPaymentByCustomerRecievedTable(**k),
    get_filter_form=lambda **k: ReportCusomterRecievedFilterForm(**k))
)

""""
Report Payment By Billing
"""
class ReportPaymentByBillingTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    pay_amount = AmountColumn(_(u'Amount'))
    currency_sign = DataColumn('')


ReportPaymentByBillingTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string(
        '', **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="1">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ data | total("pay_amount", {"currency":row.currency}) | to_riel() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
),
    Group(
        name='payment_method',
        render_header=lambda **kw: render_template_string(
            '<tr class="group-footer-header"><th colspan="3">{{ _(row.lookup_value) if request.cookies.get("language")=="km" else _(row.lookup_value_eng)  }}</th></tr>',
            **kw),
        render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer" > \
                                                         <th colspan="1">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ row.pay_amount | to_riel() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
    )
]


class ReportPaymentByBillingFilterForm(FlaskForm):
    company_id = HiddenField()

    billing_id = DynamicSelectField(
            _(u'Billing Name'),
            query_factory=billing_logic.default.find_billing,
            get_pk="id",
            get_label="name",
            allow_blank=True,
            blank_text=_(u'All Billing'),
            render_kw={
                'data-val': 'true',
                'data-val-required': _(u'Input Required'),
                'col-class': 'col-md-12'
            }
        )

    payment_method = SelectField(
        _(u'Payment Method'),
        choices=[("", _(u'All Payment Method'))] + [(v, _(k)) for k, v in enum.payment_method.get_members().items()],
        render_kw={
            'col-class': 'col-md-12'
        }
    )


SystemReportCenter.register(SimpleReport(
    name='payment-by-billing',
    title=_("Payment By Billings"),
    parent='payment-group',
    get_result=lambda **k: report_payment_logic.default.report_payment_by_billing(**k),
    get_table=lambda **k: ReportPaymentByBillingTable(**k),
    get_filter_form=lambda **k: ReportPaymentByBillingFilterForm(**k)
)
)


'''
   Billing Report
'''


# Billing Report by  block
def paid_percent(row):
    # if row.percent_paid > 0 and row.percent_paid < 0.5:
    #     #     return str(round(row.percent_paid, 2)) + " %"
    #     # elif row.percent_paid > 0.5:
    #     #     return str(int(round(row.percent_paid))) + " %"
    if row.percent_paid > 0:
        return '{0:,.2F}'.format(row.percent_paid) + " %"
    else:
        return "-"


def farmer_paid_percent(row):
    if row.percent_farmer_paid > 0:
        return '{0:,.2F}'.format(row.percent_farmer_paid) + " %"
    # elif row.percent_farmer_paid > 0.5:
    #     return str(int(round(row.percent_farmer_paid,2))) + " %"
    else:
        return "-"


def unpaid_percent(row):
    if row.percent_paid > 0:
        return '{0:,.2F}'.format(100 - row.percent_paid) + " %"
        # unpaid = 100 - round(row.percent_paid, 2)
        # return str(unpaid) + " %"
    # elif row.percent_paid > 0.5:
    #     unpaid = 100 - int(round(row.percent_paid, 2))
    #     return str(unpaid) + " %"
    else:
        return str(100) + ' %'


class ReportBillingByRecievedTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    block_name = DataColumn(_(u'Block'))
    total_farmer = LamdaColumn(_(u'Total Customers'), get_value=lambda row: format_integer(row.total_farmer),
                               render_kw={
                                   'class': 'decimal'
                               })
    total_land = AmountColumn(_(u'Total Lands'))
    total_area = AreaColumn(_(u'Total Area'))
    total_invoice = AmountColumn(_(u'Total Invoices'))
    total_cost = AmountColumn(_(u'Total Cost'))
    # total_cost = LamdaColumn(_(u'Total Cost'), get_value=lambda row: to_riel(row.total_cost))
    total_farmer_paid = AmountColumn(_(u'Total Farmer Paid'))
    percent_farmer_paid = LamdaColumn(_(u"Percent"), render_kw={"class": 'decimal'}, get_value=farmer_paid_percent)
    total_paid = AmountColumn(_(u'Paid Amount'))
    percent_paid = LamdaColumn(_(u"Percent"), render_kw={"class": 'decimal'}, get_value=paid_percent)
    total_exceed_paid = AmountColumn(_(u'Exceed Amount'))
    ending_balance = AmountColumn(_(u"Ending Balance"))
    percent_unpaid = LamdaColumn(_(u'Percent'), get_value=unpaid_percent, render_kw={"class": 'decimal'})
    currency_sign = DataColumn('')

    # def render_pre_rows(self):
    #     return '<tr><th colspan="100%" id="date"></th></tr>'


ReportBillingByRecievedTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string(
        '',
        **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="2">{{ _("Total") }} ({{row.currency_sign}})</th> \
                                                         <th class="decimal">{{ data | total("total_farmer", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_land", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_area", {"currency":row.currency}) | to_ha() }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoice", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_cost", {"currency":row.currency}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total("total_farmer_paid", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_farmer","total_farmer_paid","", {"currency":row.currency}) }} %</th> \
                                                         <th class="decimal">{{ data | total("total_paid", {"currency":row.currency}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","", {"currency":row.currency}) }} %</th> \
                                                         <th class="decimal">{{ data | total("total_exceed_paid", {"currency":row.currency}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total("ending_balance", {"currency":row.currency}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","ending_balance", {"currency":row.currency})  }} %</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
), Group(
    name='name',
    render_header=lambda **kw: render_template_string(
        '<tr class="group-footer-header"><th colspan="15">{{ row.name }}</th></tr>',
        **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer" > \
                                                         <th colspan="2">{{ _("Total") }}</th> \
                                                         <th class="decimal">{{ data | total("total_farmer", {"name":row.name}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_land", {"name":row.name}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_area", {"name":row.name}) | to_ha() }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoice", {"name":row.name}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_cost", {"name":row.name}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total("total_farmer_paid", {"name":row.name}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_farmer","total_farmer_paid","", {"name":row.name}) }} %</th> \
                                                         <th class="decimal">{{ data | total("total_paid", {"name":row.name}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","", {"name":row.name})}} %</th> \
                                                         <th class="decimal">{{ data | total("total_exceed_paid", {"name":row.name}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total("ending_balance", {"name":row.name}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","ending_balance", {"name":row.name}) }} %</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
)
]


class ReportBillingRecievedFilterForm(FlaskForm):
    billing_id = DynamicSelectField(
        _(u'Billing Name'),
        query_factory=billing_logic.default.find_billing,
        get_pk="id",
        get_label="name",
        allow_blank=True,
        blank_text=_(u'All Billing'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    station_id = DynamicSelectField(
        _(u'Station'),
        query_factory=station_logic.default.search,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All Stations'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.filter,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    village_id = DynamicSelectField(
        _(u'Village'),
        # query_factory=location_logic.default.all,
        query=location_logic.default.get_villages(type=4),
        get_pk='id',
        get_label='filter_name',
        allow_blank=True,
        blank_text=_(u'All Villages'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    company_id = HiddenField()


SystemReportCenter.register(SimpleReport(
    name='billing report by block ',
    title=_('Billing Report By Block'),
    parent='billing-group',
    get_result=lambda **k: report_billing_logic.default.report_billing(**k),
    get_table=lambda **k: ReportBillingByRecievedTable(**k),
    get_filter_form=lambda **k: ReportBillingRecievedFilterForm(**k),
    index_template='reports/default2/index.html')
)


# By Station
class ReportBillingByStationRecievedTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    station_name = DataColumn(_(u'Station'))
    total_farmer = AmountColumn(_(u'Total Farmers'))
    total_land = AmountColumn(_(u'Total Lands'))
    total_area = AreaColumn(_(u'Total Area'))
    total_invoice = AmountColumn(_(u'Total Invoices'))
    total_cost = AmountColumn(_(u'Total Cost'))
    total_farmer_paid = AmountColumn(_(u'Total Farmer Paid'))
    percent_farmer_paid = LamdaColumn(_(u"Percent"), render_kw={"class": 'decimal'}, get_value=farmer_paid_percent)
    total_paid = AmountColumn(_(u'Paid Amount'))
    percent_paid = LamdaColumn(_(u"Percent"), render_kw={"class": 'decimal'}, get_value=paid_percent)
    total_exceed_paid = AmountColumn(_(u'Exceed Amount'))
    ending_balance = AmountColumn(_(u"Ending Balance"))
    percent_unpaid = LamdaColumn(_(u'Percent'), get_value=unpaid_percent, render_kw={"class": 'decimal'})
    currency_sign = DataColumn('')


ReportBillingByStationRecievedTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string(
        '<tr class="hide"><th id="billing_date">{{ row.start_date | to_month_name() }} {{_("To")}} {{row.end_date | to_month_name()}}</th></tr>',
        **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="2">{{ _("Total") }} ({{row.currency_sign}})</th> \
                                                         <th class="hide decimal" id ="start_date">{{ row.start_date }}</th> \
                                                         <th class="decimal">{{ data | total("total_farmer", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_land", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_area", {"currency":row.currency}) | to_ha() }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoice", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_cost", {"currency":row.currency}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total("total_farmer_paid", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_farmer","total_farmer_paid","", {"currency":row.currency}) }} %</th> \
                                                         <th class="decimal">{{ data | total("total_paid", {"currency":row.currency}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","", {"currency":row.currency}) }} %</th> \
                                                         <th class="decimal">{{ data | total("total_exceed_paid", {"currency":row.currency}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total("ending_balance", {"currency":row.currency}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","ending_balance", {"currency":row.currency})  }} %</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
), Group(
    name='name',
    render_header=lambda **kw: render_template_string(
        '<tr class="group-footer-header"><th colspan="15">{{ row.name }}</th></tr>',
        **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer" > \
                                                         <th colspan="2">{{ _("Total") }}</th> \
                                                         <th class="decimal">{{ data | total("total_farmer", {"name":row.name}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_land", {"name":row.name}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_area", {"name":row.name}) | to_ha() }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoice", {"name":row.name}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_cost", {"name":row.name}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total("total_farmer_paid", {"name":row.name}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_farmer","total_farmer_paid","", {"name":row.name})}} %</th> \
                                                         <th class="decimal">{{ data | total("total_paid", {"name":row.name}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","", {"name":row.name})}} %</th> \
                                                         <th class="decimal">{{ data | total("total_exceed_paid", {"name":row.name}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total("ending_balance", {"name":row.name}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","ending_balance", {"name":row.name}) }} %</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
)
]


class ReportBillingRecievedFilterForm(FlaskForm):
    billing_id = DynamicSelectField(
        _(u'Billing Name'),
        query_factory=billing_logic.default.find_billing,
        get_pk="id",
        get_label="name",
        allow_blank=True,
        blank_text=_(u'All Billing'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    station_id = DynamicSelectField(
        _(u'Station'),
        query_factory=station_logic.default.search,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All Stations'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.filter,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    village_id = DynamicSelectField(
        _(u'Village'),
        # query_factory=location_logic.default.all,
        query=location_logic.default.get_villages(type=4),
        get_pk='id',
        get_label='filter_name',
        allow_blank=True,
        blank_text=_(u'All Villages'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    company_id = HiddenField()


SystemReportCenter.register(SimpleReport(
    name='billing report by station ',
    title=_('Billing Report By Station'),
    parent='billing-group',
    get_result=lambda **k: report_billing_logic.default.report_billing_by_station(**k),
    get_table=lambda **k: ReportBillingByStationRecievedTable(**k),
    get_filter_form=lambda **k: ReportBillingRecievedFilterForm(**k),
    index_template='reports/default2/index.html')
)


# By Village
class ReportBillingByVillageRecievedTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    village_name = DataColumn(_(u'Village'))
    total_farmer = AmountColumn(_(u'Total Farmers'))
    total_land = AmountColumn(_(u'Total Lands'))
    total_area = AreaColumn(_(u'Total Area'))
    total_invoice = AmountColumn(_(u'Total Invoices'))
    total_cost = AmountColumn(_(u'Total Cost'))
    total_farmer_paid = AmountColumn(_(u'Total Farmer Paid'))
    percent_farmer_paid = LamdaColumn(_(u"Percent"), render_kw={"class": 'decimal'}, get_value=farmer_paid_percent)
    total_paid = AmountColumn(_(u'Paid Amount'))
    percent_paid = LamdaColumn(_(u"Percent"), render_kw={"class": 'decimal'}, get_value=paid_percent)
    total_exceed_paid = AmountColumn(_(u'Exceed Amount'))
    ending_balance = AmountColumn(_(u"Ending Balance"))
    percent_unpaid = LamdaColumn(_(u'Percent'), get_value=unpaid_percent, render_kw={"class": 'decimal'})
    currency_sign = DataColumn('')


ReportBillingByVillageRecievedTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string(
        '<tr class="hide"><th id="billing_date">{{ row.start_date | to_month_name() }} {{_("To")}} {{row.end_date | to_month_name()}}</th></tr>',
        **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="2">{{ _("Total") }} ({{row.currency_sign}})</th> \
                                                         <th class="decimal">{{ data | total("total_farmer", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_land", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_area", {"currency":row.currency}) | to_ha() }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoice", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_cost", {"currency":row.currency}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total("total_farmer_paid", {"name":row.currency}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_farmer","total_farmer_paid","", {"name":row.currency})}} %</th> \
                                                         <th class="decimal">{{ data | total("total_paid", {"currency":row.currency}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","", {"currency":row.currency}) }} %</th> \
                                                         <th class="decimal">{{ data | total("total_exceed_paid", {"currency":row.currency}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total("ending_balance", {"currency":row.currency}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","ending_balance", {"currency":row.currency})  }} %</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
), Group(
    name='name',
    render_header=lambda **kw: render_template_string(
        '<tr class="group-footer-header"><th colspan="15">{{ row.name }}</th></tr>',
        **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer"> \
                                                         <th colspan="2">{{ _("Total") }}</th> \
                                                         <th class="decimal">{{ data | total("total_farmer", {"name":row.name}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_land", {"name":row.name}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_area", {"name":row.name}) | to_ha() }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoice", {"name":row.name}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_cost", {"name":row.name}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total("total_farmer_paid", {"name":row.name}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_farmer","total_farmer_paid","", {"name":row.name})}} %</th> \
                                                         <th class="decimal">{{ data | total("total_paid", {"name":row.name}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","", {"name":row.name})}} %</th> \
                                                         <th class="decimal">{{ data | total("total_exceed_paid", {"name":row.name}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total("ending_balance", {"name":row.name}) | to_riel() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","ending_balance", {"name":row.name}) }} %</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
)]

SystemReportCenter.register(SimpleReport(
    name='billing report by village ',
    title=_('Billing Report By Village'),
    parent='billing-group',
    get_result=lambda **k: report_billing_logic.default.report_billing_by_village(**k),
    get_table=lambda **k: ReportBillingByVillageRecievedTable(**k),
    get_filter_form=lambda **k: ReportBillingRecievedFilterForm(**k),
    index_template='reports/default2/index.html')
)

# Billing by tariff
# class ReportBillingByTariffRecievedTable(Table):
#     rowno = RowNumberColumn(_(u'No.'))
#     tariff_name = DataColumn(_(u"Tariff"))
#     total_land = AmountColumn(_(u'Total Lands'))
#     total_area = AreaColumn(_(u'Total Area'))
#     total_cost = AmountColumn(_(u'Total Cost'))
#     currency_sign = DataColumn(u'')
#
#
# ReportBillingByTariffRecievedTable.groups = [Group(
#     name='currency',
#     render_header=lambda **kw: render_template_string('', **kw),
#     render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
#                                                          <th colspan="2">{{_("Total")}} ({{row.currency_sign}})</th> \
#                                                          <th class="decimal">{{ data | total("total_land", {"currency":row.currency}) | format_integer() }}</th> \
#                                                          <th class="decimal">{{ data | total("total_area", {"currency":row.currency}) | to_ha() }}</th> \
#                                                          <th class="decimal">{{ data | total("total_cost", {"currency":row.currency}) | format_value() }}</th> \
#                                                          <th>{{ row.currency_sign }}</th> \
#                                                          </tr>', **kw)
# ), Group(
#     name='name',
#     render_header=lambda **kw: render_template_string(
#         '<tr class="group-footer-header"><th colspan="12">{{ row.name }}</th></tr>',
#         **kw),
#     render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer" > \
#                                                          <th colspan="2">{{ _("Total") }}</th> \
#                                                          <th class="decimal">{{ data | total("total_land", {"name":row.name}) | format_integer() }}</th> \
#                                                          <th class="decimal">{{ data | total("total_area", {"name":row.name}) | to_ha() }}</th> \
#                                                          <th class="decimal">{{ data | total("total_cost", {"name":row.name}) | format_value() }}</th> \
#                                                          <th>{{ row.currency_sign }}</th>\
#                                                          </tr>', **kw)
# )]
#
#
# class ReportBillingByTariffRecievedFilterForm(FlaskForm):
#     billing_id = DynamicSelectField(
#         _(u'Billing Name'),
#         query_factory=billing_logic.default.find_billing,
#         get_pk="id",
#         get_label="name",
#         allow_blank=True,
#         blank_text=_(u'All Billing'),
#         render_kw={
#             'data-val': 'true',
#             'data-val-required': _(u'Input Required')
#         }
#     )
#     station_id = DynamicSelectField(
#         _(u'Station'),
#         query_factory=station_logic.default.search,
#         get_pk='id',
#         get_label='name',
#         allow_blank=True,
#         blank_text=_(u'All Stations'),
#         render_kw={
#             'data-val': 'true',
#             'data-val-required': _(u'Input Required'),
#             'required': 'required'
#         }
#     )
#     block_id = DynamicSelectField(
#         _(u'Block'),
#         query_factory=block_logic.default.filter,
#         get_pk='id',
#         get_label='name',
#         allow_blank=True,
#         blank_text=_('All Blocks'),
#         render_kw={
#             'data-val': 'true',
#             'data-val-required': _(u'Input Required'),
#             'required': 'required'
#         }
#     )
#
#     village_id = DynamicSelectField(
#         _(u'Village'),
#         # query_factory=location_logic.default.all,
#         query=location_logic.default.get_villages(type=4),
#         get_pk='id',
#         get_label='filter_name',
#         allow_blank=True,
#         blank_text=_(u'All Villages'),
#         render_kw={
#             'data-val': 'true',
#             'data-val-required': _(u'Input Required'),
#             'required': 'required'
#         }
#     )
#

# SystemReportCenter.register(SimpleReport(
#     name='billing-by-tariff',
#     title=_('Billing Report By Tarrif'),
#     parent='billing-group',
#     get_result=lambda **k: report_billing_logic.default.report_billing_by_tariff(**k),
#     get_table=lambda **k: ReportBillingByTariffRecievedTable(**k),
#     get_filter_form=lambda **k: ReportBillingByTariffRecievedFilterForm(**k),
#     index_template='reports/default2/index.html')
# )

""""
Billing invoice Penalty
"""


class ReportBillingPenaltyRecievedTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    block_name = DataColumn(_(u'Block'))
    total_farmer = AmountColumn(_(u'Total Farmers'))
    total_invoice = AmountColumn(_(u'Total Invoices'))
    total_cost = AmountColumn(_(u'Total Cost'))
    total_paid = AmountColumn(_(u'Paid Amount'))
    percent_paid = LamdaColumn(_(u"Percent"), render_kw={"class": 'decimal'}, get_value=paid_percent)
    ending_balance = AmountColumn(_(u"Ending Balance"))
    percent_unpaid = LamdaColumn(_(u'Percent'), get_value=unpaid_percent, render_kw={"class": 'decimal'})
    currency_sign = DataColumn('')


ReportBillingPenaltyRecievedTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string(
        '',
        **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="2">{{ _("Total") }} ({{row.currency_sign}})</th> \
                                                         <th class="decimal">{{ data | total("total_farmer", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoice", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_cost", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total("total_paid", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","", {"currency":row.currency}) }} %</th> \
                                                         <th class="decimal">{{ data | total("ending_balance", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","ending_balance", {"currency":row.currency})  }} %</th> \
                                                        </tr>', **kw)
), Group(
    name='name',
    render_header=lambda **kw: render_template_string(
        '<tr class="group-footer-header"><th colspan="12">{{ row.name }}</th></tr>',
        **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer" > \
                                                         <th colspan="2">{{ _("Total") }}</th> \
                                                         <th class="decimal">{{ data | total("total_farmer", {"name":row.name}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoice", {"name":row.name}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_cost", {"name":row.name}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total("total_paid", {"name":row.name}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","", {"name":row.name})}} %</th> \
                                                         <th class="decimal">{{ data | total("ending_balance", {"name":row.name}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","ending_balance", {"name":row.name}) }} %</th> \
                                                         </tr>', **kw)
)
]


class ReportBillingPenaltyRecievedFilterForm(FlaskForm):
    billing_id = DynamicSelectField(
        _(u'Billing Name'),
        query_factory=billing_logic.default.find_billing,
        get_pk="id",
        get_label="name",
        allow_blank=True,
        blank_text=_(u'All Billing'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    station_id = DynamicSelectField(
        _(u'Station'),
        query_factory=station_logic.default.search,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All Stations'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.filter,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    village_id = DynamicSelectField(
        _(u'Village'),
        # query_factory=location_logic.default.all,
        query=location_logic.default.get_villages(type=4),
        get_pk='id',
        get_label='filter_name',
        allow_blank=True,
        blank_text=_(u'All Villages'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    company_id = HiddenField()


SystemReportCenter.register(SimpleReport(
    name='billing report Invoice Penalty ',
    title=_('Billing Report Invoice Penalty'),
    parent='billing-invoice-penalty-group',
    get_result=lambda **k: report_billing_logic.default.report_billing_invoice_penalty(**k),
    get_table=lambda **k: ReportBillingPenaltyRecievedTable(**k),
    get_filter_form=lambda **k: ReportBillingPenaltyRecievedFilterForm(**k),
    index_template='reports/default2/index.html')
)

""""
BILLING INVOICE PENALTY BY FARMER
"""


class ReportBillingPenaltyByFarmerRecievedTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    farmer = LamdaColumn(_(u'Farmer'), get_value=lambda row: lambda_farmer(row))
    total_invoice = AmountColumn(_(u'Total Invoices'))
    total_cost = AmountColumn(_(u'Total Cost'))
    total_paid = AmountColumn(_(u'Paid Amount'))
    percent_paid = LamdaColumn(_(u"Percent"), render_kw={"class": 'decimal'}, get_value=paid_percent)
    ending_balance = AmountColumn(_(u"Ending Balance"))
    percent_unpaid = LamdaColumn(_(u'Percent'), get_value=unpaid_percent, render_kw={"class": 'decimal'})
    currency_sign = DataColumn('')


ReportBillingPenaltyByFarmerRecievedTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string(
        '',
        **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="2">{{ _("Total") }} ({{row.currency_sign}})</th>                                                        <th class="decimal">{{ data | total("total_invoice", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_cost", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total("total_paid", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","", {"currency":row.currency}) }} %</th> \
                                                         <th class="decimal">{{ data | total("ending_balance", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","ending_balance", {"currency":row.currency})  }} %</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
), Group(
    name='name',
    render_header=lambda **kw: render_template_string(
        '<tr class="group-footer-header"><th colspan="9">{{ row.name }}</th></tr>',
        **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer" > \
                                                         <th colspan="2">{{ _("Total") }}</th> \
                                                         <th class="decimal">{{ data | total("total_invoice", {"name":row.name}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_cost", {"name":row.name}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total("total_paid", {"name":row.name}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","", {"name":row.name})}} %</th> \
                                                         <th class="decimal">{{ data | total("ending_balance", {"name":row.name}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total_percent("total_cost","total_paid","ending_balance", {"name":row.name}) }} %</th> \
                                                         </tr>', **kw)
)
]


class ReportBillingPenaltyByFarmerRecievedFilterForm(FlaskForm):
    billing_id = DynamicSelectField(
        _(u'Billing Name'),
        query_factory=billing_logic.default.find_billing,
        get_pk="id",
        get_label="name",
        allow_blank=True,
        blank_text=_(u'All Billing'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    station_id = DynamicSelectField(
        _(u'Station'),
        query_factory=station_logic.default.search,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All Stations'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.filter,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    village_id = DynamicSelectField(
        _(u'Village'),
        # query_factory=location_logic.default.all,
        query=location_logic.default.get_villages(type=4),
        get_pk='id',
        get_label='filter_name',
        allow_blank=True,
        blank_text=_(u'All Villages'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    company_id = HiddenField()


SystemReportCenter.register(SimpleReport(
    name='billing report invoice penalty by farmer',
    title=_('Billing Report Invoice Penalty By Farmer Name'),
    parent='billing-invoice-penalty-group',
    get_result=lambda **k: report_billing_logic.default.report_billing_invoice_penalty_by_farmers(**k),
    get_table=lambda **k: ReportBillingPenaltyByFarmerRecievedTable(**k),
    get_filter_form=lambda **k: ReportBillingPenaltyByFarmerRecievedFilterForm(**k),
    index_template='reports/default2/index.html')
)

"""
DEBT REPORT
"""


class ReportDebtTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    farmer = LamdaColumn(_(u'Farmer'), get_value=lambda row: farmer_name(row))
    total_invoice = DecimalColumn(_(u'Total Invoices'))
    total_amount = DecimalColumn(_(u'Total Amount'))
    paid_amount = AmountColumn(_(u'Paid'))
    ending_balance = AmountColumn(_(u'Ending Balance'))
    # exceed_amount = LamdaColumn(_(u'Exceed Amount'),
    #                             get_value=lambda row: format_value(row.exceed_amount) if row.exceed_amount < 0 else "-",
    #                             render_kw={'class':'decimal'})
    sign = DataColumn('')

    # def render_pre_rows(self):
    #     return '<tr><th colspan="100%" id="date"></th></tr>'


class ReportDebtFilterForm(FlaskForm):
    billing_id = DynamicSelectField(
        _(u'Billing Name'),
        query_factory=billing_logic.default.find_billing,
        get_pk="id",
        get_label="name",
        allow_blank=True,
        blank_text=_(u'All Billing'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    station_id = DynamicSelectField(
        _(u'Station'),
        query_factory=station_logic.default.search,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All Stations'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.filter,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    village_id = DynamicSelectField(
        _(u'Village'),
        # query_factory=location_logic.default.all,
        query=location_logic.default.get_villages(type=4),
        get_pk='id',
        get_label='filter_name',
        allow_blank=True,
        blank_text=_(u'All Villages'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )


ReportDebtTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string(
        '',
        **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="2">{{ _("Total") }} ({{ row.sign }})</th> \
                                                         <th class="decimal">{{ data | total("total_invoice", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_amount", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total("paid_amount", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total("ending_balance", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th>{{ row.sign}}</th>\
                                                         </tr>', **kw)
)
    , Group(
        name='name',
        render_header=lambda **kw: render_template_string(
            '<tr class="group-footer-header"><th colspan="7">{{ row.name }}</th></tr>',
            **kw),
        render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer"> \
                                                             <th colspan="2">{{ _("Total") }}</th> \
                                                             <th class="decimal">{{ data | total("total_invoice", {"name":row.name}) | format_integer() }}</th> \
                                                             <th class="decimal">{{ data | total("total_amount", {"name":row.name}) | format_value() }}</th>\
                                                             <th class="decimal">{{ data | total("paid_amount", {"name":row.name}) | format_value() }}</th>\
                                                             <th class="decimal">{{ data | total("ending_balance", {"name":row.name}) | format_value() }}</th>\
                                                             <th>{{ row.sign}}</th>\
                                                             </tr>', **kw)
    )
]

SystemReportCenter.register(SimpleReport(
    name='debt-by-farmer',
    title=_('Debt Report By Farmer'),
    parent='debt-group',
    get_result=lambda **k: report_billing_logic.default.report_by_farmer(**k),
    get_table=lambda **k: ReportDebtTable(**k),
    get_filter_form=lambda **k: ReportDebtFilterForm(**k),
    index_template='reports/default2/index.html')
)

"""
EXCEED REPORT
"""


class ReportExceedTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    farmer = LamdaColumn(_(u'Farmer'), get_value=lambda row: row.farmer_code + ' - ' + row.farmer_name)
    total_invoice = DecimalColumn(_(u'Total Invoices'))
    total_amount = DecimalColumn(_(u'Total Amount'))
    paid_amount = AmountColumn(_(u'Paid'))
    ending_balance = AmountColumn(_(u'Ending Balance'))
    exceed_amount = LamdaColumn(_(u'Exceed Amount'),
                                get_value=lambda row: format_value(row.exceed_amount) if row.exceed_amount < 0 else "-",
                                render_kw={'class': 'decimal'})
    sign = DataColumn('')

    # def render_pre_rows(self):
    #     return '<tr><th colspan="100%" id="date"></th></tr>'


class ReportExceedFilterForm(FlaskForm):
    billing_id = DynamicSelectField(
        _(u'Billing Name'),
        query_factory=billing_logic.default.find_billing,
        get_pk="id",
        get_label="name",
        allow_blank=True,
        blank_text=_(u'All Billing'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    station_id = DynamicSelectField(
        _(u'Station'),
        query_factory=station_logic.default.search,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All Stations'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.filter,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    village_id = DynamicSelectField(
        _(u'Village'),
        # query_factory=location_logic.default.all,
        query=location_logic.default.get_villages(type=4),
        get_pk='id',
        get_label='filter_name',
        allow_blank=True,
        blank_text=_(u'All Villages'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )


ReportExceedTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string(
        '',
        **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="2">{{ _("Total") }} ({{ row.sign }})</th> \
                                                         <th class="decimal">{{ data | total("total_invoice", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_amount", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total("paid_amount", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total("ending_balance", {"currency":row.currency}) | format_value() }}</th> \
                                                         <th class="decimal">{{ data | total("exceed_amount", {"name":row.name}) | format_value() }}</th>\
                                                         <th>{{ row.sign}}</th>\
                                                         </tr>', **kw)
)
    , Group(
        name='name',
        render_header=lambda **kw: render_template_string(
            '<tr class="group-footer-header"><th colspan="8">{{ row.name }}</th></tr>',
            **kw),
        render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer"> \
                                                             <th colspan="2">{{ _("Total") }}</th> \
                                                             <th class="decimal">{{ data | total("total_invoice", {"name":row.name}) | format_integer() }}</th> \
                                                             <th class="decimal">{{ data | total("total_amount", {"name":row.name}) | format_value() }}</th>\
                                                             <th class="decimal">{{ data | total("paid_amount", {"name":row.name}) | format_value() }}</th>\
                                                             <th class="decimal">{{ data | total("ending_balance", {"name":row.name}) | format_value() }}</th>\
                                                             <th class="decimal">{{ data | total("exceed_amount", {"name":row.name}) | format_value() }}</th>\
                                                             <th>{{ row.sign}}</th>\
                                                             </tr>', **kw)
    )
]

SystemReportCenter.register(SimpleReport(
    name='exceed-by-farmer',
    title=_('Exceed Report By Farmer'),
    parent='debt-group',
    get_result=lambda **k: report_billing_logic.default.report_exceed_by_farmer(**k),
    get_table=lambda **k: ReportExceedTable(**k),
    get_filter_form=lambda **k: ReportExceedFilterForm(**k),
    index_template='reports/default2/index.html')
)

"""
    Bank Daily reports
"""


def farmer_name(row):
    try:
        if row.spouse_name:
            return u'{name} - {spouse_name} - {code}'.format(name=getattr(row, 'customer_name', row.farmer_name),
                                                             spouse_name=row.spouse_name,
                                                             code=getattr(row, 'code', row.farmer_code))
        else:
            return u'{name} - {code}'.format(name=getattr(row, 'customer_name', row.farmer_name),
                                             code=getattr(row, 'code', row.farmer_code))
    except Exception as e:
        return ''


class ReportBankRecievedTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    date = LamdaColumn(_(u'Date'), get_value=convert_to_mon)
    customer = LamdaColumn(_(u'Customer'), get_value=lambda row: farmer_name(row))
    bank_name = DataColumn(_(u'Bank Name'))
    bank_branch = DataColumn(_('Branch'))
    total_amount = LamdaColumn(_(u'Total Amount'), get_value=lambda row: format_value(row.total_amount),
                               render_kw={'class': 'decimal'})
    total_paid_amount = LamdaColumn(_('Total Paid Amount'), get_value=lambda row: format_value(
        row.total_paid_amount) if row.total_paid_amount != 0 else '-', render_kw={'class': 'decimal'})
    ending_balance = LamdaColumn(_('Ending Balance'), get_value=lambda row: format_value(
        row.ending_balance) if row.ending_balance != 0 else '-', render_kw={'class': 'decimal'})
    currency_sign = DataColumn('')


ReportBankRecievedTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string('', **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="5">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ data | total("total_amount", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_paid_amount", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("ending_balance", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
)]


class ReportBankRecievedFilterForm(FlaskForm):
    d1 = DateField(
        _(u'From Date'),
        default=datetime.now().date().replace(day=1),
        render_kw={'class': 'form-control input-tip date',
                   'col-class': 'col-md-3'}
    )
    d2 = DateField(
        _(u'To Date'),
        default=datetime.now().date().replace(day=1,
                                              month=datetime.now().month + 1 if datetime.now().month < 12 else 1,
                                              year=datetime.now().year if datetime.now().month <= 11 else datetime.now().year + 1) - timedelta(
            days=1),
        render_kw={'class': 'form-control input-tip date',
                   'col-class': 'col-md-3'}
    )

    bank = DynamicSelectField(
        _(u'Bank'),
        query_factory=bank_payment_logic.default.get_banks,
        get_pk='bank',
        get_label='bank',
        allow_blank=True,
        blank_text=_("All Banks"),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    status = SelectField(
        _(u'Status'),
        choices=[(v, _(k)) for k, v in enum.pay_status.get_members().items()],
        render_kw={
            'class': 'form-control',
            'required': 'required'
        }
    )

    company_id = HiddenField()


SystemReportCenter.register(SimpleReport(
    name='bank-daily-payment',
    title=_("Bank Daily Payment"),
    parent='bank-group',
    get_result=lambda **k: report_bank_logic.default.report_daily_bank_payment(**k),
    get_table=lambda **k: ReportBankRecievedTable(**k),
    get_filter_form=lambda **k: ReportBankRecievedFilterForm(**k),
    index_template='reports/default2/index.html')
)

"""
  BANK SUMMARY BY DATE
"""


class ReportBankSummaryDetailByDateRecievedTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    date = LamdaColumn(_(u'Date'), get_value=convert_to_mon)
    bank_name = DataColumn(_(u'Bank Name'))
    bank_branch = DataColumn(_(u'Branch'))
    total_customers = LamdaColumn(_(u'Total Customers'), get_value=lambda row: format_value(row.total_customers),
                                  render_kw={'class': 'decimal'})
    total_paid_amount = LamdaColumn(_(u'Total Paid Amount'), get_value=lambda row: format_value(
        row.total_paid_amount) if row.total_paid_amount != 0 else '-', render_kw={'class': 'decimal'})
    currency_sign = DataColumn('')


ReportBankSummaryDetailByDateRecievedTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string('', **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="4">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ data | total("total_customers", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_paid_amount", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
)]


class ReportBankSummaryByDateFilterForm(FlaskForm):
    d1 = DateField(
        _(u'From Date'),
        default=datetime.now().date().replace(day=1),
        render_kw={
            'class': 'form-control input-tip date',
            'role': 'date',
            'data-val': 'true',
            'data-val-required': u'Input Required',
            'addon': 'fa-calendar'
        }
    )
    d2 = DateField(
        _(u'To Date'),
        default=datetime.now().date().replace(day=1,
                                              month=datetime.now().month + 1 if datetime.now().month < 12 else 1,
                                              year=datetime.now().year if datetime.now().month <= 11 else datetime.now().year + 1) - timedelta(
            days=1),
        render_kw={
            'class': 'form-control input-tip date',
            'col-class': 'col-md-3',
            'role': 'date',
            'data-val': 'true',
            'data-val-required': u'Input Required',
            'addon': 'fa-calendar'
        }
    )

    bank = DynamicSelectField(
        _(u'Bank'),
        query_factory=bank_payment_logic.default.get_banks,
        get_pk='bank',
        get_label='bank',
        allow_blank=True,
        blank_text=_("All Banks"),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )


SystemReportCenter.register(SimpleReport(
    name='summary-bank-payment-by-date',
    title=_("Summary Bank Payment By Date"),
    parent='bank-group',
    get_result=lambda **k: report_bank_logic.default.summary_detail_by_date(**k),
    get_table=lambda **k: ReportBankSummaryDetailByDateRecievedTable(**k),
    get_filter_form=lambda **k: ReportBankSummaryByDateFilterForm(**k),
    index_template='reports/default2/index.html')
)

"""
  BANK SUMMARY BY BANK
"""


class ReportBankSummaryDetailByBankRecievedTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    bank_name = DataColumn(_(u'Bank Name'))
    bank_branch = DataColumn(_('Branch'))
    total_customers = LamdaColumn(_(u'Total Customers'), get_value=lambda row: format_value(row.total_customers),
                                  render_kw={'class': 'decimal'})
    total_paid_amount = LamdaColumn(_(u'Total Paid Amount'), get_value=lambda row: format_value(
        row.total_paid_amount) if row.total_paid_amount != 0 else '-', render_kw={'class': 'decimal'})
    currency_sign = DataColumn('')


ReportBankSummaryDetailByBankRecievedTable.groups = [Group(
    name='currency',
    render_header=lambda **kw: render_template_string('', **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="3">{{ _("Total") }} ({{ row.currency_sign }})</th> \
                                                         <th class="decimal">{{ data | total("total_customers", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th class="decimal">{{ data | total("total_paid_amount", {"currency":row.currency}) | format_integer() }}</th> \
                                                         <th>{{ row.currency_sign }}</th> \
                                                         </tr>', **kw)
)]


class ReportBankSummaryByBankFilterForm(FlaskForm):
    d1 = DateField(
        _(u'From Date'),
        default=datetime.now().date().replace(day=1),
        render_kw={'class': 'form-control input-tip date',
                   'col-class': 'col-md-3'}
    )
    d2 = DateField(
        _(u'To Date'),
        default=datetime.now().date().replace(day=1,
                                              month=datetime.now().month + 1 if datetime.now().month < 12 else 1,
                                              year=datetime.now().year if datetime.now().month <= 11 else datetime.now().year + 1) - timedelta(
            days=1),
        render_kw={'class': 'form-control input-tip date',
                   'col-class': 'col-md-3'}
    )

    bank = DynamicSelectField(
        _(u'Bank'),
        query_factory=bank_payment_logic.default.get_banks,
        get_pk='bank',
        get_label='bank',
        allow_blank=True,
        blank_text=_("All Banks"),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )


SystemReportCenter.register(SimpleReport(
    name='summary-bank-payment-by-bank',
    title=_("Summary Bank Payment By Bank"),
    parent='bank-group',
    get_result=lambda **k: report_bank_logic.default.summary_detail_by_bank(**k),
    get_table=lambda **k: ReportBankSummaryDetailByBankRecievedTable(**k),
    get_filter_form=lambda **k: ReportBankSummaryByBankFilterForm(**k),
    index_template='reports/default2/index.html')
)

"""
GENERAL REPORT
"""


class ReportLandTariffTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    tariff_name = DataColumn(_(u"Tariff"))
    total_farmer = DataColumn(_(u'Total Farmers'))
    total_land = LamdaColumn(_(u'Total Lands'), get_value=lambda row: format_integer(row.total_land))
    total_area = LamdaColumn(_(u'Total Area'), get_value=lambda row: format_currency(row.total_area))


ReportLandTariffTable.groups = [Group(
    name='is_active',
    render_header=lambda **kw: render_template_string('', **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th colspan="2">{{ _("Total") }}</th> \
                                                         <th>{{ data | total("total_farmer",{"is_active":row.is_active}) | format_integer() }}</th> \
                                                         <th>{{ data | total("total_land",{"is_active":row.is_active}) | format_integer()}}</th> \
                                                         <th>{{ data | total("total_area",{"is_active":row.is_active}) | format_currency() }}</th> \
                                                         </tr>', **kw))
    , Group(
        name='name',
        render_header=lambda **kw: render_template_string(
            '<tr class="group-footer-header"><th colspan="12">{{ row.name }}</th></tr>',
            **kw),
        render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer"> \
                                                         <th colspan="2">{{ _("Total") }}</th> \
                                                         <th>{{ data | total("total_farmer", {"name":row.name}) | format_integer() }}</th> \
                                                         <th>{{ data | total("total_land", {"name":row.name}) | format_integer()}}</th> \
                                                         <th>{{ data | total("total_area", {"name":row.name}) | format_currency() }}</th> \
                                                         </tr>', **kw)
    )]


class ReportLandTariffFilterForm(FlaskForm):
    billing_id = DynamicSelectField(
        _(u'Billing Name'),
        query_factory=billing_logic.default.find_billing,
        get_pk="id",
        get_label="name",
        allow_blank=True,
        blank_text=_(u'All Billing'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    station_id = DynamicSelectField(
        _(u'Station'),
        query_factory=station_logic.default.search,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All Stations'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.filter,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )


SystemReportCenter.register(SimpleReport(
    name='tariff-report',
    title=_('Report Land by Tariff'),
    parent='general-group',
    get_result=lambda **k: report_billing_logic.default.report_land_by_tariff(**k),
    get_table=lambda **k: ReportLandTariffTable(**k),
    get_filter_form=lambda **k: ReportLandTariffFilterForm(**k),
    index_template='reports/default2/index.html')
)

"""
LAND REPORT
"""


class ReportLandTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    code = DataColumn(_(u'Land Code'))
    block = LamdaColumn(_(u'Block'), get_value=lambda row: row.block.name if row.block else '')
    farmer = LamdaColumn(_(u'Owner'), get_value=lambda row: row.farmer.farmer if row.farmer else '-')
    rent = LamdaColumn(_(u'Rent For'), get_value=lambda row: row.rent.farmer if row.rent and hasattr(row.rent,
                                                                                                     'name') and row.rent.is_active else row.farmer.farmer)
    area = DataColumn(_(u'Area(Are)'), render_kw={'data-t': 'n', 'data-a-h': 'left'})
    tariff = LamdaColumn(_(u'Tariff'),
                         get_value=lambda row: row.tariff.name if row.tariff and hasattr(row.tariff,
                                                                                         'name') and row.tariff.is_active else _(
                             u'NONE'))
    land_status_value = LamdaColumn(_(u'Land Status'), get_value=lambda row: getattr(row.land_status_value,'name', _(u'NONE')))
    note = DataColumn(_(u'Note'))

ReportLandTable.groups = [Group(
    name='is_active',
    render_header=lambda **kw: render_template_string(
        '',
        **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                         <th>{{ _("Total") }}</th> \
                                                         <th>{{ data | count("code", {"is_active":row.is_active}) | format_integer() }}</th> \
                                                         <th></th> \
                                                         <th>{{ data | count("farmer", {"is_active":row.is_active}) | format_integer() }}</th> \
                                                         <th>{{ data | count("rent", {"is_active":row.is_active}) | format_integer() }}</th> \
                                                         <th>{{ data | total("area", {"is_active":row.is_active}) }}</th> \
                                                         <th></th> \
                                                         <th></th> \
                                                         <th></th> \
                                                         </tr>', **kw)
)]


class ReportLandFilterForm(FlaskForm):
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.filter,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    from ..logic import tariff_logic
    tariff_id = DynamicSelectField(
        _(u'Tariff'),
        query_factory=tariff_logic.default.search,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_("All Tariff"),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    status_id = DynamicSelectField(
        _(u'Land Status'),
        allow_blank=True,
        blank_text=_(u"All Land Status"),
        query=lookup_logic.default.get_values('l_status'),
        get_pk="id",
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )

    active = DynamicSelectField(
        _(u'Active'),
        query=lookup_logic.default.get_values('activate'),
        get_pk="value",
        default='active',
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )


SystemReportCenter.register(SimpleReport(
    name='land-report',
    title=_('Land'),
    parent='general-group',
    get_result=lambda **k: land_logic.default.search(**k),
    get_table=lambda **k: ReportLandTable(**k),
    get_filter_form=lambda **k: ReportLandFilterForm(**k),
    index_template='reports/default2/index.html')
)


"""
LAND BY BILLING CYCLE
"""
def format_name_rent(row):
    if row.rent_spouse:
        return (row.rent_name + '-' + row.rent_spouse + '-' + row.rent_code)
    else:
        return (row.rent_name + '-' + row.rent_code)

def format_name_farmer(row):
    if row.spouse_name:
        return (row.farmer_name + '-' + row.spouse_name + '-' + row.farmer_code)
    else:
        return (row.farmer_name + '-' + row.farmer_code)

class ReportLandByBillingTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    code = DataColumn(_(u'Land Code'))
    block = LamdaColumn(_(u'Block'), get_value=lambda row: row.block_code)
    farmer = LamdaColumn(_(u'Owner'), get_value=lambda row:format_name_farmer(row))
    rent = LamdaColumn(_(u'Rent'), get_value=lambda row:format_name_rent(row))
    tariff = LamdaColumn(_(u'Tariff'),
                         get_value=lambda row: row.tariff)
    area = DataColumn(_(u'Area(Are)'), render_kw={'data-t': 'n', 'data-a-h': 'left'})

ReportLandByBillingTable.groups = [Group(
    name='billing_id',
    render_header=lambda **kw: render_template_string('', **kw),
    render_footer=lambda **kw: render_template_string('<tr class="group-footer2" style="background-color:#448AFF;color:white"> \
                                                        <th>{{ _("Total") }}</th> \
                                                        <th>{{ data | count("code",{"billing_id":row.billing_id}) }}</th>\
                                                        <th></th>\
                                                        <th>{{ data | count("farmer_code",{"billing_id":row.billing_id}) }}</th>\
                                                        <th>{{ data | count("rent_code",{"billing_id":row.billing_id}) }}</th>\
                                                        <th></th>\
                                                        <th>{{ data | total("area",{"billing_id":row.billing_id}) }}</th>'
                                                        '</tr>', **kw))
    # ,
    # Group(
    # name='',
    # render_header=lambda **kw: render_template_string(
    #     '',
    #     **kw),
    # render_footer=lambda **kw: render_template_string('<tr class="group-footer2 sub-footer" style="background-color:#448AFF;color:white"> \
    #                                                      <th colspan="2">{{ _("Total") }}</th> \
    #                                                      <th>{{ data | total("farmer") | format_integer() }}</th> \
    #                                                      </tr>', **kw)
]


class ReportByBillingFilterForm(FlaskForm):
    billing_id = DynamicSelectField(
        _(u'Billing Name'),
        query_factory=billing_logic.default.find_billing,
        get_pk="id",
        get_label="name",
        allow_blank=False,
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.filter,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    from ..logic import tariff_logic
    tariff_id = DynamicSelectField(
        _(u'Tariff'),
        query_factory=tariff_logic.default.search,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_("All Tariff"),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )




SystemReportCenter.register(SimpleReport(
    # name='land-report',
    name='report-by-land-billing-cycle',
    title=_('Land By Billing Cycle'),
    parent='general-group',
    get_result=lambda **k: report_land_by_tariff_logic.default.report_by_billing(**k),
    get_table=lambda **k: ReportLandByBillingTable(**k),
    get_filter_form=lambda **k: ReportByBillingFilterForm(**k),
    index_template='reports/default2/index.html')
)




"""
FARMER REPORT
"""


class ReportFarmerTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    code = DataColumn(_(u'Farmer Code'))
    name = DataColumn(_(u'Name'))
    sex = LamdaColumn(_(u'Sex'), get_value=lambda r: farmer_logic.default.get_marital_status(r.sex).name
    if r.marital_status and farmer_logic.default.get_marital_status(r.sex)
    else '-')
    marital_status = LamdaColumn(_(u'Marital'),
                                 get_value=lambda r: farmer_logic.default.get_marital_status(r.marital_status).name
                                 if r.marital_status and farmer_logic.default.get_marital_status(r.marital_status)
                                 else '-'
                                 )
    spouse_name = DataColumn(_(u'Spouse Name'))
    phone = DataColumn(_(u'Phone'))
    village = LamdaColumn(_(u'Village'), get_value=lambda row: row.village)


ReportFarmerTable.groups = [Group(
    name='',
    render_header=lambda **kw: render_template_string(
        '',
        **kw),
    render_footer=lambda **kw: render_template_string('', **kw)
)]


class ReportFarmerFilterForm(FlaskForm):
    village_id = DynamicSelectField(
        _(u'Village'),
        query=location_logic.default.get_villages(type=4),
        get_pk='id',
        get_label='filter_name',
        allow_blank=True,
        blank_text=_(u'All Villages'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    marital_status = DynamicSelectField(
        _(u'Marital'),
        query=lookup_logic.default.get_values('mar_status'),
        get_pk="value",
        allow_blank=True,
        blank_text=_('Marital'),
        render_kw={
            'data-val': 'true'
        }
    )

    is_active = DynamicSelectField(
        _(u'Active'),
        query=lookup_logic.default.get_values('activate'),
        get_pk="value",
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )


SystemReportCenter.register(SimpleReport(
    name='farmer-report',
    title=_('Farmer'),
    parent='general-group',
    get_result=lambda **k: farmer_logic.default.search(**k),
    get_table=lambda **k: ReportFarmerTable(**k),
    get_filter_form=lambda **k: ReportFarmerFilterForm(**k),
    index_template='reports/default2/index.html')
)
