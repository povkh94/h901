from flask_babel import lazy_gettext as _
from ...share.view.base_view import *
from ...agri_bill.model import *
from ..logic import location_logic

class LocationForm(FlaskForm):
    province_id = DynamicSelectField(
        _(u'Province'),
        query=location_logic.default.all(type='1'),
    )
    district_id = DynamicSelectField(
        _(u'District'),
        query=location_logic.default.all(type='2'),
    )
    commune_id = DynamicSelectField(
        _(u'Commune'),
        query=location_logic.default.all(type='3'),
    )
    village_id = DynamicSelectField(
        _(u'Village'),
        query=location_logic.default.all(type='4'),
    )


class LocationTable(Table):
    rowno = RowNumberColumn(_(u'No.'))

class LocationView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Location
        self.v_logic = location_logic.default
        self.v_table_index_class = LocationTable
        self.v_form_filter_class = ''
        self.v_template = 'location'
        self.v_form_add_class = LocationForm
        self.v_data['title'] = "Location"
        self.v_logic_order_by = ''

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _('Location')



    @route('/options/<type>/<parent_id>', methods=['GET', 'POST'])
    def options(self, type, parent_id):
        selected_id = request.args.get('selected_id', '')

        locations = self.v_logic.all(type=type, parent_id=parent_id)
        options = []
        options.append('<option value="__None"></option>')
        for location in locations:
            if selected_id == location.id:
                options.append('<option selected value="%s">%s</option>' % (location.id, location.name))
            else:
                options.append('<option value="%s">%s</option>' % (location.id, location.name))
        result = ''.join(options)
        return result

    @route('/suggestion')
    def location_suggestion(self):
        term = request.args.get('term') or ''
        type = request.args.get('type') or 0
        limit = request.args.get('limit') or 10
        suggests = location_logic.default.location_suggestion(search=term,type=type)
        suggests = suggests.limit(limit)
        results = [dict(
            id=s.id,
            text=s.name,
            obj=s
        ) for s in suggests]
        data = {'results': results}
        return json.dumps(data)

LocationView.register(app)
