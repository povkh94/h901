from flask_babel import lazy_gettext as _

from project.module.agri_bill.convert import to_ha
from project.module.agri_bill.table import AreaColumn

from project.module.agri_bill.logic import contract_template_logic, tariff_logic

from project.module.agri_bill.export import export_pdf
from .billing_view import ProcessForm
from ...share.view.base_view import *
from ...agri_bill.model import *
from ..logic import billing_logic, block_logic, land_logic, verify_billing_logic
from project.module.system.logic import lookup_logic
from project.module.agri_bill.view import land_view

from flask import render_template_string
from project.module.agri_bill.logic import farmer_logic, location_logic
from random import randint


def lamda_hidden(row):
    class FormHidden(FlaskForm):
        id = HiddenField("Id")

    rnd = randint(100, 200)
    field_name = "land_id_%s" % rnd
    setattr(FormHidden, field_name, HiddenField())
    form = FormHidden()
    getattr(form, field_name).data = row.id
    return render_template_string('{{ form.land_id_%s }}' % rnd, form=form)


def lamda_select2(row):
    class FormDummy(FlaskForm):
        id = HiddenField("Id")

    rnd = randint(100, 2000)
    field_name = 'rent_id_%s' % rnd
    setattr(FormDummy, field_name,
            Select2Field(
                label=_(u'Rent For'),
                remote_url='/farmer/suggestion',
                allow_blank=True,
                blank_text=_(u'Not Rent'),
                min_input_length=0,
                text_factory=lambda x: farmer_logic.default.find(x),
                multiple=False
            ))
    form = FormDummy()
    getattr(form, field_name).data = row.rent_id if row.rent_id else row.farmer_id
    return render_template_string('{{ form.rent_id_%s }}' % rnd, form=form)
    # return getattr(form, field_name).__call__()
    # select = LamdaColumn(_("select"),get_value= lambda_abc)


def lamda_select2_farmer(row):
    class FormDummy(FlaskForm):
        id = HiddenField("Id")

    rnd = randint(100, 2000)
    field_name = 'farmer_id_%s' % rnd
    setattr(FormDummy, field_name,
            Select2Field(
                label=_(u'Owner'),
                remote_url='/farmer/suggestion',
                allow_blank=True,
                blank_text=_(u'Owner'),
                min_input_length=0,
                text_factory=lambda x: farmer_logic.default.find(x),
                multiple=False
            ))
    form = FormDummy()
    getattr(form, field_name).data = row.farmer_id
    return render_template_string('{{ form.farmer_id_%s }}' % rnd, form=form)


def lamda_land_area(row):
    class FormLandArea(FlaskForm):
        id = HiddenField("Id")

    rnd = randint(100, 200)
    field_name = 'land_area_%s' % rnd
    setattr(FormLandArea, field_name,
            TextField(
                _(u'Area(Are)'),
                render_kw={
                    'data-val': 'true',
                    'data-val-required': _(u'Input Required'),
                    'required': 'required',
                    'class': 'form-control'
                }
            ))
    form = FormLandArea()
    getattr(form, field_name).data = '{0:.2F}'.format(row.area)
    return render_template_string('{{ form.land_area_%s }}' % rnd, form=form)


def lamda_tariff(row):
    class FormTariff(FlaskForm):
        id = HiddenField("Id")

    rnd = randint(100, 200)
    from ..logic import tariff_logic
    field_name = 'tariff_id_%s' % rnd
    setattr(FormTariff, field_name,
            DynamicSelectField(
                _(u'Tariff'),
                query_factory=tariff_logic.default.search,
                get_pk='id',
                get_label='name',
                allow_blank=True,
                render_kw={
                    'data-val': 'true',
                    'data-val-required': _(u'Input Required'),
                    'required': 'required',
                    'class': 'form-control'
                }
            ))
    form = FormTariff()
    getattr(form, field_name).data = row.tariff_id
    return render_template_string('{{ form.tariff_id_%s }}' % rnd, form=form)


def lamda_land_status(row):
    class FormStatus(FlaskForm):
        id = HiddenField("Id")

    rnd = randint(100, 200)
    from ..logic import tariff_logic
    field_name = 'land_status_%s' % rnd
    setattr(FormStatus, field_name,
            DynamicSelectField(
                _(u'Status'),
                query=lookup_logic.default.get_values('l_status'),
                get_pk="id",
                render_kw={
                    'data-val': 'true',
                    'data-val-required': _(u'Input Required'),
                    'required': 'required',
                    'class': 'form-control'
                }
            ))
    form = FormStatus()
    getattr(form, field_name).data = row.status_id
    return render_template_string('{{ form.land_status_%s }}' % rnd, form=form)


def lamda_block(row):
    class FormBlock(FlaskForm):
        id = HiddenField("Id")

    rnd = randint(100, 2000000)
    field_name = 'block_id_%s' % rnd
    setattr(FormBlock, field_name,
            DynamicSelectField(
                _(u'Block'),
                query_factory=block_logic.default.search,
                get_pk='id',
                get_label='name',
                render_kw={
                    'class': 'form-control',
                    'data-val': 'true',
                    'data-val-required': _(u'Input Required'),
                    'required': 'required'
                }
            )
            )
    form = FormBlock()
    getattr(form, field_name).data = row.block_id
    return render_template_string('{{ form.block_id_%s }}' % rnd, form=form)


class LandTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    code = DataColumn(_(u'Land Code'))
    land_id = LamdaColumn(u' ', get_value=lamda_hidden)
    # block = DataColumn(_(u'Block'))
    block = LamdaColumn(_(u'Block'), get_value=lamda_block)
    # farmer = LamdaColumn(_(u'Owner'), get_value=lambda row: row.farmer.name)
    farmer = LamdaColumn(_(u'Owner'), get_value=lamda_select2_farmer)
    rent = LamdaColumn(_(u'Rent For'), get_value=lamda_select2)
    area = LamdaColumn(_(u'Area'), get_value=lamda_land_area)
    land_status_value = LamdaColumn(_(u'Land Status'), get_value=lamda_land_status)
    tariff = LamdaColumn(_(u'Tariff'), get_value=lamda_tariff)


class LandVerifyTabel(Table):
    rowno = RowNumberColumn(_(u'No.'))
    code = DataColumn(_(u'Land Code'))
    block = LamdaColumn(_(u'Block'), get_value=lambda row: row.block.name)
    area = DecimalColumn(_(u'Area (Are)'))
    farmer = LamdaColumn(_(u'Owner'), get_value=lambda row: row.farmer)
    rent = LamdaColumn(_(u'Rent For'),
                       get_value=lambda row: u"{name}".format(name=u'' if not row.rent_id else row.rent))
    note = DataColumn(_(u'Note'))
    land_status = LamdaColumn(_(u'Irrigate/Non irrigate'),
                              get_value=lambda
                                  row: u'<input land_id="{land_id}" select_tariff="{tariff_id}" type="checkbox" {checked}/>'.format(
                                  tariff_id=row.tariff_id if hasattr(row, 'tariff') and row.tariff_id else '',
                                  land_id=row.id,
                                  checked="checked" if row.status_id == 'irrigate' else ''))


class LandVerifiedTabel(Table):
    rowno = RowNumberColumn(_(u'No.'))
    land_code = DataColumn(_(u'Land Code'))
    block_name = LamdaColumn(_(u'Block'), get_value=lambda row: row.block_name)
    land_area = DecimalColumn(_(u'Area (Are)'))
    farmer = LamdaColumn(_(u'Owner'), get_value=lambda row: farmer_format2(row))
    rent = LamdaColumn(_(u'Rent'), get_value=lambda row: farmer_format3(row))
    note = DataColumn(_(u'Note'))
    land_status = LamdaColumn(_(u'Irrigate/Non irrigate'),
                              get_value=lambda
                                  row: u'<input land_id="{land_id}" select_tariff="{tariff_id}" type="checkbox" {checked}/>'.format(
                                  tariff_id=row.tariff_id,
                                  land_id=row.land_id,
                                  checked="checked" if row.land_status_id == 'irrigate' else ''))


def farmer_format(row):
    if row.spouse_name and str(row.spouse_name).strip():
        return (row.farmer_name + ' - ' + row.spouse_name + '-' + row.farmer_code)
    else:
        return (row.farmer_name + ' - ' + row.farmer_code)


def farmer_format2(row):
    if row.farmer_spouse_name and str(row.farmer_spouse_name).strip():
        return (row.farmer_name + ' - ' + row.farmer_spouse_name + '-' + row.farmer_code)
    else:
        return (row.farmer_name + ' - ' + row.farmer_code)


def farmer_format3(row):
    if row.rent_spouse_name and str(row.rent_spouse_name).strip():
        return (row.rent_name + ' - ' + row.rent_spouse_name + '-' + row.rent_code)
    else:
        return (row.rent_name + ' - ' + row.rent_code)


class VerifyBillingTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    block_name = DataColumn(_(u'Block'))
    farmer_name = LamdaColumn(_(u'Farmer'),
                              get_value=lambda
                                  row: farmer_format(row))

    total_land = LamdaColumn(_(u'Total Land Irrigate'), get_value=lambda row: row.total_land, render_kw={
        'data-t': 'n',
        'data-a-h': 'left'
    })
    total_area = DecimalColumn(_(u'Total Area(Are)'), render_kw={
        'data-t': 'n',
        'data-a-h': 'left'
    })
    total_area_irrigate = DecimalColumn(_(u'Total Area(Are) Irrigate'), render_kw={
        'data-t': 'n',
        'data-a-h': 'left'
    })

    confirm = LamdaColumn(
        _(u'Agreement'),
        get_value=lambda row: u' <div class="checkbox">'
                              u'<label style="font-size:1.3em;" >'
                              u'<input type="checkbox" data-block-id="{block_id}" data-billing-id="{billing_id}" data-farmer-id="{farmer_id}" {status}/>'
                              u'<span class="cr">' u'<i class="cr-icon glyphicon glyphicon-ok"></i></span>'
                              u'</label>'
                              u'</div>'.format(
            status='checked' if row.status == 'verified' else '',
            block_id=row.block_id,
            billing_id=row.billing_id,
            farmer_id=row.farmer_id,

        ), render_kw={'width': '10%'}
    )
    action = LamdaColumn(
        get_value=lambda
            row: u'<a href="{url}" class="action btn btn-default btn-sm edit" style="height: 30px;">'
                 u'<span class="glyphicon glyphicon-print" style="font-family:Glyphicons Halflings !important;"></span>'
                 u'</a>'.format(
            url=url_for(".VerifyBillingView:agreement", billing_id=row.billing_id, block_id=row.block_id,
                        farmer_id=row.farmer_id,
                        back_url=url_for('.VerifyBillingView:verify_all', billing_id=row.billing_id))
        )
    )
    action = LamdaColumn(
        get_value=lambda
            row: u'<div class="btn-group btn-group-sm pull-right" style="display:flex;">'
                 u'<a href="{url}" class="action btn btn-default btn-sm edit" style="height: 30px;">'
                 u'<span class="glyphicon glyphicon-print" style="font-family:Glyphicons Halflings !important;"></span>'
                 u'</a>'
                 u'<a href="{url_edit}" class="action btn btn-default btn-sm edit" style="height: 30px;">'
                 u'<span class="glyphicon glyphicon-pencil" style="font-family:Glyphicons Halflings !important;"></span>'
                 u'</a></div>'
            .format(
            url=url_for(".VerifyBillingView:agreement", billing_id=row.billing_id, block_id=row.block_id,
                        farmer_id=row.farmer_id,
                        back_url=url_for('.VerifyBillingView:verify_all', billing_id=row.billing_id)),
            url_edit=url_for(".VerifyBillingView:view_land_by",
                             block_id=row.block_id,
                             farmer_id=row.farmer_id,
                             billing_id=row.billing_id,
                             back_url=url_for(".VerifyBillingView:index", billing_id=row.billing_id))
        )
    )


class VerifyLandDataTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    block_name = DataColumn(_(u'Block'))
    farmer_name = LamdaColumn(_(u'Farmer'),
                              get_value=lambda
                                  row: farmer_format(row))

    total_land = LamdaColumn(_(u'Total Land Irrigate'), get_value=lambda row: row.total_land, render_kw={
        'data-t': 'n',
        'data-a-h': 'left'
    })
    total_area = DecimalColumn(_(u'Total Area(Are)'), render_kw={
        'data-t': 'n',
        'data-a-h': 'left'
    })
    total_area_irrigate = DecimalColumn(_(u'Total Area(Are) Irrigate'), render_kw={
        'data-t': 'n',
        'data-a-h': 'left'
    })

    action = LamdaColumn(
        get_value=lambda
            row: u'<a href="{url}" class="action btn btn-default btn-sm edit" style="height: 30px;">'
                 u'<span class="glyphicon glyphicon-print" style="font-family:Glyphicons Halflings !important;"></span>'
                 u'</a>'.format(
            url=url_for(".VerifyBillingView:re_gen_agreement", billing_verify_id=row.id,
                        back_url=url_for('.VerifyBillingView:land_data', billing_id=row.billing_id))
        )
    )


class AddNew(FlaskForm):
    farmer_id = Select2Field(
        label=_(u'Farmer'),
        remote_url='/farmer/suggestion',
        min_input_length=0,
        text_factory=lambda x: farmer_logic.default.find(x),
        multiple=True,
        max_selection_length=10000,
    )
    billing_id = HiddenField()


class AddNewBlock(FlaskForm):
    block_id = Select2Field(
        label=_(u'Block'),
        remote_url='/block/suggestion',
        min_input_length=0,
        text_factory=lambda x: block_logic.default.find(x),
        multiple=True,
        max_selection_length=10000,
    )
    billing_id = HiddenField()


class VerifyFilterForm(FilterForm):
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.filter,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    verify_id = DynamicSelectField(
        _(u'Verify'),
        query=lookup_logic.default.get_values('v_status'),
        get_pk="id",
        get_label="name",
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    billing_id = HiddenField()


class VerifiedFilterForm(FilterForm):
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.filter,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    billing_id = HiddenField()


class VerifyBillingView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Verify
        self.v_logic = verify_billing_logic.default
        self.v_table_index_class = VerifyBillingTable
        self.v_form_filter_class = VerifyFilterForm
        self.v_template = 'verify'
        self.v_logic_order_by = ''

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _('Verify Billing')

    def _on_index_rendering(self, *args, **kwargs):
        billing_id = request.args.get('billing_id')
        billing = billing_logic.default.find(billing_id)
        billing_name = billing.name
        self.v_data['billing_name'] = billing_name
        self.v_data['billing_id'] = billing_id
        back_url = request.args.get('back_url')
        if back_url:
            self.v_data['back_url'] = back_url
        else:
            self.v_data['back_url'] = url_for(".BillingView:detail", id=billing_id)
        form = self.v_data['form']
        form.billing_id.data = billing_id
        form.verify_id.data = 'all_verify'

        self.v_data['addnew_form'] = AddNew()
        self.v_data['addnewblock_form'] = AddNewBlock()
        self.v_data['addnew_form'].billing_id.data, self.v_data[
            'addnewblock_form'].billing_id.data = billing_id, billing_id

        self.v_data['process_form'] = ProcessForm()
        self.v_data['process_form'].billing_id.data = billing_id
        self.v_data['process_form'].issue_date.data = datetime.now()
        self.v_data['process_form'].due_date.data = datetime.now() + timedelta(days=15)
        self.v_data['action_url'] = url_for('.BillingView:process_billing')
        self.v_data['obj'] = billing

    # update verify_billing
    @route("/update_status/<id>/<block_id>/<farmer_id>", methods=['get', 'post'])
    def update_status(self, id=None, block_id=None, farmer_id=None):
        status = request.get_data()
        if id and block_id and farmer_id:
            update = verify_billing_logic.default.update_status(id, block_id, farmer_id, status)
            # check if check itself on confirm checkbox if return verified update billing status
            verify_status = verify_billing_logic.default.check_status(billing_id=id)
            if verify_status == 'verified':
                billing_status = billing_logic.default.billing_status(id=id, status=verify_status)
        return update

    @route("/verify_all", methods=['get', 'post'])
    def verify_all(self):
        update = None
        billing_id = request.args.get('billing_id', '')
        back_url = request.args.get('back_url', '')
        if back_url:
            if billing_id:
                update = verify_billing_logic.default.verify_all(billing_id=billing_id)
            if update:
                billing = billing_logic.default.billing_status(id=billing_id, status='verified')
            return redirect(url_for(".VerifyBillingView:index", billing_id=billing_id, back_url=back_url))
        else:
            return redirect(url_for(".VerifyBillingView:index", billing_id=billing_id))

    @route('/view_land_by/<block_id>/<farmer_id>')
    def view_land_by(self, block_id, farmer_id):
        back_url = request.args.get('back_url')
        billing_id = request.args.get('billing_id')
        billing = billing_logic.default.find(billing_id)
        obj = land_logic.default.view_land_by(block_id, farmer_id)
        page = int(request.args.get('page', 1))
        per_page = int(request.args.get('per_page', 10))
        import math
        total = obj.count()
        num_page = math.ceil(total / float(per_page))
        next_num = page + 1
        if next_num > num_page:
            next_num = 0
        if per_page:
            obj = obj.limit(per_page)
        if page:
            obj = obj.offset((page - 1) * per_page)

        pagging = {"page": page, "next_num": next_num, "total": total, "per_page": per_page,
                   "pages": '{0:,.0f}'.format(num_page)}
        data = DataView(query=obj)
        table = LandTable(data=data.result)

        from flask import session
        return render_template('verify/verify.html',
                               data=data,
                               table=table,
                               pagging=pagging,
                               block_id=block_id,
                               farmer_id=farmer_id,
                               back_url=back_url,
                               billing=billing,
                               billing_id=billing_id,
                               layout=session.get('layout', 'breadcrumb'),
                               view={})

    @route('/agreement')
    def agreement(self):
        from flask import request, render_template
        billing_id = request.args['billing_id']
        back_url = request.args.get('back_url')
        block_id = request.args['block_id']
        farmer_id = request.args['farmer_id']
        billing = billing_logic.default.find(billing_id)
        verify_billing = verify_billing_logic.default.get_verify_billing_by(billing_id, block_id, farmer_id)
        return render_template("verify/contract_preview.html", back_url=back_url,
                               billing_name=billing.name,obj=verify_billing,
                               contract_content_url=url_for('.VerifyBillingView:contract_content',id=verify_billing.id)
                               )

    @route('/contract-content')
    def contract_content(self):
        from flask import request, render_template, render_template_string
        from ..logic import contract_template_logic
        verify_billing_id = request.args['id']
        verify_billing = verify_billing_logic.default.find(id=verify_billing_id)
        billing = billing_logic.default.find(verify_billing.billing_id)
        content_template = contract_template_logic.default.find_content_template(billing.contract_template_id)
        billing = billing.name.split('-')
        block = block_logic.default.find(verify_billing.block_id)
        farmer = farmer_logic.default.find(verify_billing.farmer_id)
        sex_kh = farmer_logic.default.get_marital_status(farmer.sex)
        farmer.sex_kh = sex_kh
        location = location_logic.default.get_location(farmer.id)
        land = land_logic.default.view_land_by(block_id=verify_billing.block_id, farmer_id=verify_billing.farmer_id)
        land.contract_tem_id = content_template.id
        data = DataView(query=land)
        tariffs = tariff_logic.default.search().all()
        total_area = 0
        for item in land.all():
            if item.status_id == 'irrigate':
                total_area += item.area
            try:
                if not item.tariff:
                    setattr(item, 'tariff_id', '4')  # default tariff
                for tariff in tariffs:
                    setattr(LandVerifyTabel, tariff.id,
                            LamdaColumn(tariff.name, render_kw={"tariff_id": tariff.id, 'width': '10%'},
                                        get_value=lambda
                                            x: u'<input type="checkbox" tariff_id={tariff_id}>'.format(
                                            tariff_id=x.tariff_id)))
            except Exception as e:
                pass
        land.table = LandVerifyTabel(data=data.result)
        before = '<div style="page-break-before: always;"></div>'
        try:
            content = render_template_string(content_template.content_template,
                                             contract_number=verify_billing.contract_number,
                                             billing=billing,
                                             block=block,
                                             farmer=farmer,
                                             location=location,
                                             land=land,
                                             total_area=total_area,
                                             before=before)
            return render_template("verify/contract_content.html", content=content)
        except:
            return ''

    @route('/re-agreement')
    def re_gen_agreement(self):
        from flask import request, render_template
        back_url = request.args.get('back_url')
        verify_billing_id = request.args.get('billing_verify_id')
        obj = verify_billing_logic.default.find(id=verify_billing_id)
        return render_template("verify/contract_preview.html", back_url=back_url,
                               obj=obj,
                               land_data=_('Land Data'),
                               contract_content_url=url_for('.VerifyBillingView:re_contract_content',
                                                            id=obj.id)
                               )

    @route('/re-contract-content')
    def re_contract_content(self):
        """
            obj: Verify
        """
        from flask import request, render_template, render_template_string
        from ..logic import contract_template_logic
        id = request.args.get('id')
        obj = verify_billing_logic.default.find(id=id)
        content_template = contract_template_logic.default.find_content_template(obj.billing.contract_template_id)
        billing = obj.billing.name.split('-')
        location = location_logic.default.get_location(obj.farmer.id)
        total_area = 0
        lands = obj.ext__lands
        tariffs = tariff_logic.default.search().all()
        new_lands = []
        if lands:
            for land in lands:
                o = ExtDictObj(land)
                if o.land_status_id == 'irrigate':
                    total_area += o.land_area

                for tariff in tariffs:
                    if tariff.id == o.tariff_id:
                        setattr(LandVerifiedTabel, o.tariff_id,
                                LamdaColumn(o.tariff_name, render_kw={"tariff_id": o.tariff_id, 'width': '10%'},
                                            get_value=lambda
                                                x: u'<input type="checkbox" tariff_id={tariff_id}>'.format(
                                                tariff_id=o.tariff_id)))

                    else:
                        setattr(LandVerifiedTabel, tariff.id,
                                LamdaColumn(tariff.name, render_kw={"tariff_id": tariff.id, 'width': '10%'},
                                            get_value=lambda
                                                x: u'<input type="checkbox" tariff_id={tariff_id}>'.format(
                                                tariff_id=tariff.id)))

                new_lands.append(o)

            def convert(value):
                try:
                    v = int(value)
                except:
                    v = 0
                return v

            newlist = sorted(new_lands, key=lambda x: '{0:4d}'.format(convert(x.land_code)))
            table = LandVerifiedTabel(data=newlist)
            before = '<div style="page-break-before: always;"></div>'
            try:
                content = render_template_string(content_template.content_template,
                                                 contract_number=obj.contract_number,
                                                 billing=billing,
                                                 block=obj.block,
                                                 farmer=obj.farmer,
                                                 location=location,
                                                 land=dict(table=table),
                                                 total_area=total_area,
                                                 before=before
                                                 )
                return render_template("verify/contract_content.html", content=content)
            except:
                return ''

    @route('/add-new', methods=['GET', 'POST'])
    def addNew(self):
        form = AddNew()
        if form.validate_on_submit():
            farmer_ids = form.farmer_id.data
            billing_id = form.billing_id.data
            farmer_ids = [id for id in farmer_ids.split(';')]
            if farmer_ids:
                objects = verify_billing_logic.default.add_new_agreement_by_farmer_ids(billing_id, farmer_ids)
            return redirect(url_for('VerifyBillingView:index', billing_id=billing_id))

    @route('/add-new-block', methods=['GET', 'POST'])
    def addNewBlock(self):
        form = AddNewBlock()
        if form.validate_on_submit():
            block_ids = form.block_id.data
            billing_id = form.billing_id.data
            block_ids = [id for id in block_ids.split(';')]
            if block_ids:
                objects = verify_billing_logic.default.add_new_agreement_by_block_ids(billing_id, block_ids)
            return redirect(url_for('VerifyBillingView:index', billing_id=billing_id))

    def export_content_contracts(self, obj):
        land = land_logic.default.view_land_by(block_id=obj.block_id, farmer_id=obj.farmer_id)
        if not land.all():
            return
        total_area = sum([itm.area if itm.status_id == 'irrigate' else 0 for itm in land.all()])
        data = DataView(query=land)
        tariffs = tariff_logic.default.search().all()
        for item in land.all():
            try:
                if not item.tariff:
                    setattr(item, 'tariff_id', '4')  # default tariff
                for tariff in tariffs:
                    setattr(LandVerifyTabel, tariff.id,
                            LamdaColumn(tariff.name, render_kw={"tariff_id": tariff.id, 'width': '10%'},
                                        get_value=lambda
                                            x: u'<input type="checkbox" tariff_id={tariff_id}>'.format(
                                            tariff_id=x.tariff_id)))
            except Exception as e:
                pass
        if obj.billing.contract_template.contract_template_id == '2':
            # land.table = LandVerifyTabel_Ha(data=data.result)
            # total_area = to_ha(total_area[0])
            pass
        else:
            land.table = LandVerifyTabel(data=data.result)
        before = '<div style="page-break-before: always;"></div>'
        location = location_logic.default.get_location(obj.farmer.id)
        try:
            content = render_template_string(obj.billing.contract_template.content_template,
                                             contract_number=obj.contract_number,
                                             billing=obj.billing.name.split('-'),
                                             block=obj.block,
                                             farmer=obj.farmer,
                                             location=location,
                                             land=land,
                                             total_area=total_area,
                                             before=before
                                             )
            return content
        except:
            return ''

    @route('/preview_pdf/<billing_id>/<block_id>', methods=['GET', 'POST'])
    def preview_pdf(self, billing_id='', block_id=''):
        try:
            obj = self.v_logic.preview_to_pdf(billing_id, block_id)
            block = block_logic.default.find(obj[0].block_id)
            contents = []
            paths = []
            for item in obj:
                try:
                    content = self.export_content_contracts(item)
                    if content:
                        contents.append(content)
                except Exception, e:
                    pass
                    # print item
            if len(contents) > 100:
                from PyPDF2 import PdfFileMerger
                merger = PdfFileMerger()
                lists = self.chunks(contents, 50)
                for list in lists:
                    data = render_template("verify/tasuong_contract.html", contents=list)
                    path = self.export_pdf(data)
                    merger.append(path['path'] + '\\' + path['name'])
                    paths.append(path['path'] + '\\' + path['name'])
                merger.write(path['path'] + '\\' + path['name'].split('.pdf')[0] + '_' + block.code + '.pdf')
                merger.close()
                url = request.url_root[:-1] + url_for('download_file',
                                                      filename=path['name'].split('.pdf')[
                                                                   0] + '_' + block.code + '.pdf')

            else:
                data = render_template("verify/tasuong_contract.html", contents=contents)
                path = self.export_pdf(data)
                url = request.url_root[:-1] + url_for('download_file',
                                                      filename=path['name'])

            # remove paths
            if paths:
                for path in paths:
                    import os
                    os.remove(path)
            return url
        except Exception as e:
            raise ValueError(e.message)

    def export_verify_content_contracts(self, obj, tariffs):
        content_template = obj.billing.contract_template
        billing = obj.billing.name.split('-')
        location = location_logic.default.get_location(obj.farmer_id)
        lands = obj.ext__lands
        total_area = 0
        new_lands = []
        if lands:
            for land in lands:
                o = ExtDictObj(land)
                if o.land_status_id == 'irrigate':
                    total_area += o.land_area
                for tariff in tariffs:
                    if tariff.id == o.tariff_id:
                        setattr(LandVerifiedTabel, o.tariff_id,
                                LamdaColumn(o.tariff_name, render_kw={"tariff_id": o.tariff_id, 'width': '10%'},
                                            get_value=lambda
                                                x: u'<input type="checkbox" tariff_id={tariff_id}>'.format(
                                                tariff_id=o.tariff_id)))

                    else:
                        setattr(LandVerifiedTabel, tariff.id,
                                LamdaColumn(tariff.name, render_kw={"tariff_id": tariff.id, 'width': '10%'},
                                            get_value=lambda
                                                x: u'<input type="checkbox" tariff_id={tariff_id}>'.format(
                                                tariff_id=tariff.id)))

                new_lands.append(o)

            def convert(value):
                try:
                    v = int(value)
                except:
                    v = 0
                return v

            newlist = sorted(new_lands, key=lambda x: '{0:4d}'.format(convert(x.land_code)))
            table = LandVerifiedTabel(data=newlist)
            before = '<div style="page-break-before: always;"></div>'
            try:
                content = render_template_string(content_template.content_template,
                                                 contract_number=obj.contract_number,
                                                 billing=billing,
                                                 block=obj.block,
                                                 farmer=obj.farmer,
                                                 location=location,
                                                 land=dict(table=table),
                                                 total_area=total_area,
                                                 before=before
                                                 )
                return content
            except:
                return ''

    @route('/preview_pdf_verify/<billing_id>/<block_id>', methods=['GET', 'POST'])
    def preview_pdf_verify(self, billing_id='', block_id=''):
        try:
            obj = self.v_logic.preview_to_pdf(billing_id, block_id)
            block = block_logic.default.find(obj[0].block_id)
            contents = []
            paths = []
            tariffs = tariff_logic.default.search().all()
            for item in obj:
                try:
                    content = self.export_verify_content_contracts(item, tariffs)
                    contents.append(content)
                except Exception, e:
                    # print item
                    pass
            if len(contents) > 100:
                from PyPDF2 import PdfFileMerger
                merger = PdfFileMerger()
                lists = self.chunks(contents, 50)
                for list in lists:
                    data = render_template("verify/tasuong_contract.html", contents=list)
                    path = self.export_pdf(data)
                    merger.append(path['path'] + '\\' + path['name'])
                    paths.append(path['path'] + '\\' + path['name'])
                merger.write(path['path'] + '\\' + path['name'].split('.pdf')[0] + '_' + block.code + '.pdf')
                merger.close()
                url = request.url_root[:-1] + url_for('download_file',
                                                      filename=path['name'].split('.pdf')[
                                                                   0] + '_' + block.code + '.pdf')

            else:
                data = render_template("verify/tasuong_contract.html", contents=contents)
                path = self.export_pdf(data)
                url = request.url_root[:-1] + url_for('download_file',
                                                      filename=path['name'])

            # remove paths
            if paths:
                for path in paths:
                    import os
                    os.remove(path)
            return url
        except Exception as e:
            raise ValueError(e.message)

    def chunks(self, l, n):
        """Yield successive n-sized chunks from l."""
        lists = []
        for i in range(0, len(l), n):
            lists.append(l[i:i + n])
        return lists

    def export_pdf(self, data):
        import os
        current_dir = os.path.dirname(os.path.realpath(__file__))
        target_dir = os.sep.join(current_dir.split(os.sep)[0:-2])
        output_path = os.path.join(app.config.get('SHARE_IMAGES'))
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        # export pdf
        path = export_pdf(data, css='', output_path=output_path)
        # url = request.url_root[:-1]
        return path

    @route('/land_data/<billing_id>', methods=['get', 'post'])
    def land_data(self, billing_id):
        from flask import session

        form = VerifiedFilterForm()
        form.billing_id.data = billing_id
        billing = billing_logic.default.find(billing_id)
        back_url = url_for('BillingView:detail', id=billing_id)
        session['land_data_filter'] = filter_data = session.get('land_data_filter') or form.data

        obj = verify_billing_logic.default.find_total_land(**filter_data)

        if form.validate_on_submit() or request.method == 'POST':
            session['land_data_filter'] = filter_data = form.data
            obj = verify_billing_logic.default.find_total_land(**filter_data)

        if filter_data:
            for k, v in filter_data.iteritems():
                if k in form:
                    form[k].data = v
        session['land_data_page'] = page = int(request.args.get('page', session.get('land_data_page', 1)))
        per_page = int(request.args.get('per_page', 10))
        import math
        total = obj.count()
        num_page = math.ceil(total / float(per_page))
        next_num = page + 1
        if next_num > num_page:
            next_num = 0
        if per_page:
            obj = obj.limit(per_page)
        if page:
            obj = obj.offset((page - 1) * per_page)

        pagging = {"page": page, "next_num": next_num, "total": total, "per_page": per_page,
                   "pages": '{0:,.0f}'.format(num_page)}

        # total_land = {int} 1
        obj = obj.all()
        new_obj = []
        for o in obj:
            o = ExtDictObj(o._asdict())
            total_land = 0
            total_area = 0
            total_area_irrigate = 0
            detail = json.loads(o.ext_data)
            if detail:
                for x in detail.get("lands"):
                    total_land += 1
                    total_area += x.get('land_area')
                    if x.get('land_status_id') == "irrigate":
                        total_area_irrigate += x.get('land_area')
                setattr(o, 'total_land', total_land)
                setattr(o, 'total_area', total_area)
                setattr(o, 'total_area_irrigate', total_area_irrigate)
                new_obj.append(o)
        data = DataView(query=new_obj)
        table = VerifyLandDataTable(data=data.result)
        return render_template("verify/land_data.html",
                               data=data,
                               table=table,
                               form=form,
                               pagging=pagging,
                               billing=billing,
                               billing_id=billing_id,
                               back_url=back_url,
                               view={},
                               title=_('Land Data')
                               )


VerifyBillingView.register(app)
