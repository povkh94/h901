from flask_babel import lazy_gettext as _
from ...share.view.base_view import *
from ...agri_bill.model import *
from ..logic import billing_cycle_logic, station_logic, season_logic


class BillingCycleForm(FlaskForm):
    id = HiddenField()
    name = TextField(
        _(u'Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    ext__code = TextField(
        _(u'Code'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    season_id = DynamicSelectField(
        _(u'Season'),
        query_factory=season_logic.default.search,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    note = TextAreaField(_(u'Note'))
    ext__blocks = TreeViewField(
        _(u'Block'),
        query=station_logic.default.search,
        disable_parent=True,
        get_child='children',
        all_visible_text=_('ALL')
    )


class BillingCycleTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    name = LinkColumn(_(u'Name'), get_url=lambda row: url_for('.BillingCycleView:detail', id=row.id), get_value=lambda row: row.name)
    ext__code = DataColumn(_(u"Code"))
    season = DataColumn(_(u'Season'))
    action = ActionColumn('', endpoint='.BillingCycleView')


class BillingCycleView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = BillingCycle
        self.v_logic = billing_cycle_logic.default
        self.v_table_index_class = BillingCycleTable
        self.v_form_add_class = BillingCycleForm
        self.v_template = 'billing_cycle'
        self.v_breadcrum.append({'url': "/setting/index", 'title': _('Setting')})


    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _(u'Billing Cycle')

    def _on_index_rendering(self, *args, **kwargs):
        self.v_data['back_url'] = request.args.get('back_url') or ''
        if self.v_data['back_url'] == '':
            self.v_data['back_url'] = url_for('.SettingView:index')

    def _on_remove_rendering(self, *args, **kwargs):
        self.v_breadcrum.append({'url': "/setting/index", 'title': _('Setting')})

    @route('/get_existing_block/<season_id>', methods=['GET'])
    def get_existing_block(self, season_id):
        blocks = self.v_logic.get_existing_block(season_id=season_id)
        return json.dumps([col[0] for col in blocks])

    @route('/filter_land/<billing_cycle_id>/<station_id>')
    def filter_land(self,billing_cycle_id,station_id):
        result = self.v_logic.filter_land(id=billing_cycle_id,station_id=station_id)
        return 0

BillingCycleView.register(app)
