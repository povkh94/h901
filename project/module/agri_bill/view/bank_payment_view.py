from flask_babel import lazy_gettext as _

from project.module.agri_bill import enum
from project.module.system.logic import lookup_logic
from ..logic import bank_payment_logic
from ..model import *
from ...share.view.base_view import *


import sys

reload(sys)
sys.setdefaultencoding('utf8')



class BankPaymentForm(FlaskForm):
    pass


class BankPaymentFilterForm(FilterForm):
    pass


class BankPaymentTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    start_date = LamdaColumn(_('Date'), get_value=lambda r: r.start_date.strftime("%Y-%m-%d %H:%M:%S"))
    created_by = DataColumn(_('Downloaded By'))
    file_name = DataColumn(_('File Name'))
    total_record = LinkColumn(_('Total Record'), get_url=lambda r: url_for('.BankPaymentView:detail', id=r.id),
                              get_value=lambda r: r.total_record)


class BankPaymentDetailTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    customer_code = DataColumn(_('Farmer Code'))
    customer_name = DataColumn(_('Farmer Name'))
    pay_amount = DecimalColumn(_('Total Amount'))
    paid_amount = DecimalColumn(_('Paid Amount'))
    currency_sign = DataColumn('')
    pay_date = DateColumn(_('Date'))
    bank = DataColumn(_('Bank'))
    branch = DataColumn(_('Branch'))


class BankPaymentDetailFilterForm(FilterForm):
    log_id = HiddenField()
    result = DynamicSelectField(
        _(u'Result'),
        query=lookup_logic.default.get_values(lookup_id=str(enum.BankPaymentResult.__val__)),
        get_pk='id',
        get_label='value',
        allow_blank=True,
        blank_text=_("All Result"),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )


class BankPaymentView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = BankPaymentLog
        self.v_logic = bank_payment_logic.default
        self.v_table_index_class = BankPaymentTable
        self.v_form_add_class = BankPaymentForm
        self.v_form_filter_class = BankPaymentFilterForm
        self.v_template = 'bank_payments'
        self.v_logic_order_by = ''

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _(u'Bank Payment')

    def _on_index_rendering(self, *args, **kwargs):
        # self.v_data['pending_download'] = bank_payment_logic.default.get_pending_download_count()
        self.v_data['remaining_amount'] = bank_payment_logic.default.get_remaining_amount_count()
        notification = request.args.get('notification') or ''
        self.v_data['notification'] = notification

    def _on_detail_rendering(self, *args, **kwargs):
        self.v_data['filter_form'] = BankPaymentDetailFilterForm()
        self.v_data['table'] = BankPaymentDetailTable()
        self.v_data['filter_form'].log_id.data = self.v_data['obj'].id

    @route('/update-data-to-gateway', methods=['GET', 'POST'])
    def update_data_to_gateway(self):
        if request.form.get('force_all'):
            result = bank_payment_logic.default.update_customer_data(force_all=True)
        else:
            result = bank_payment_logic.default.update_customer_data(force_all=False)

        return redirect(
            url_for('.BankPaymentView:bank_payment_service_confirm_upload', notification=json.dumps(result)))

    def bank_payment_service_confirm_upload(self):
        view = dict()
        view['back_url'] = url_for('.BankPaymentView:index')
        last_log = bank_payment_logic.default.get_last_log()
        view['get_pending_customers_count'] = bank_payment_logic.default.get_pending_customers(
            last_log.start_date if last_log else to_date('2000-01-01')).count()

        notification = request.args.get('notification') or '{}'
        view['notification'] = json.loads(notification)

        return render_template('bank_payments/confirm_upload.html', view=view)

    def bank_payment_service_confirm_download(self):
        view = dict()
        view['back_url'] = url_for('.BankPaymentView:index')
        view['get_pending_download_count'] = bank_payment_logic.default.get_pending_download_count()

        notification = request.args.get('notification') or '{}'
        view['notification'] = json.loads(notification)

        return render_template('bank_payments/confirm_download.html', view=view)


    def download_payment(self):
        result = bank_payment_logic.default.get_payment()
        return redirect(
            url_for('.BankPaymentView:bank_payment_service_confirm_download', notification=json.dumps(result)))

    def remaining_amount(self):
        view = dict()
        view['back_url'] = url_for('.BankPaymentView:index')

        data = bank_payment_logic.default.get_remaining_amount()
        table = BankPaymentDetailTable(data=data)
        return render_template('bank_payments/remaining_amount.html', view=view, table=table)

    def resettle_payment(self):
        view = dict()
        view['back_url'] = url_for('.BankPaymentView:index')
        remain_amount = bank_payment_logic.default.get_remaining_amount()
        bank_payment_logic.default.update_payment(remain_amount)
        data = bank_payment_logic.default.get_remaining_amount()
        table = BankPaymentDetailTable(data=data)
        return render_template('bank_payments/remaining_amount.html', view=view, table=table)

    @route('/detail-filter', methods=['GET', 'POST'])
    def detail_filter(self):
        form = BankPaymentDetailFilterForm()
        q = bank_payment_logic.default.get_payment_detail(**form.data)
        table = BankPaymentDetailTable(data=q)

        return self.render_template('detail_filter.html', table=table)


BankPaymentView.register(app)
