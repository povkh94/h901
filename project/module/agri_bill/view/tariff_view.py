from flask_babel import lazy_gettext as _
from wtforms.widgets import HTMLString

from project.module.agri_bill.logic import tariff_logic
from project.module.agri_bill.model import *
from project.module.agri_bill.table import AmountColumn
from project.module.share.view.base_view import *


class TariffForm(FlaskForm):
    id = HiddenField()
    name = TextField(
        _(u'Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )
    price = TextField(
        _(u'Price'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete':'off'
        }
    )
    note = TextAreaField(_(u'Note'), render_kw={'row': 2})


def lamda_link_col(row):
    if row.id != 'none':
        result = "<a href='%s'>%s</a>" % (url_for('.TariffView:detail', id=row.id), _(row.name))
    else:
        result = _("%s" % row.name)
    return HTMLString(result)


class TariffTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    name = LamdaColumn(_(u'Name'), get_value=lamda_link_col)
    price = AmountColumn(_(u'Price'))
    action = ActionColumn('', endpoint='.TariffView')

class TariffView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Tariff
        self.v_logic = tariff_logic.default
        self.v_table_index_class = TariffTable
        self.v_form_add_class = TariffForm
        self.v_template = 'tariff'
        self.v_breadcrum.append({'url': "/setting/index", 'title': _('Setting')})
        self.v_logic_order_by = 'name'

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _(u'Tariff')

    def _on_index_rendering(self, *args, **kwargs):
        self.v_data['back_url'] = request.args.get('back_url') or ''
        if self.v_data['back_url'] == '':
            self.v_data['back_url'] = url_for('.SettingView:index')
TariffView.register(app)
