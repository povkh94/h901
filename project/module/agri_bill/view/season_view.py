from flask_babel import lazy_gettext as _
from project.module.agri_bill.logic import season_logic
from project.module.agri_bill.model import *
from project.module.share.view.base_view import *

class SeasonForm(FlaskForm):
    id = HiddenField()
    name = TextField(
        _(u'Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    note = TextAreaField(_(u'Note'), render_kw={'row': 2})


class SeasonTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    name = LinkColumn(_(u'Name'), get_url=lambda row: url_for('.SeasonView:detail', id=row.id), get_value=lambda row: row.name)
    action = ActionColumn('', endpoint='.SeasonView')


class SeasonView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Season
        self.v_logic = season_logic.default
        self.v_table_index_class = SeasonTable
        self.v_form_add_class = SeasonForm
        self.v_template = 'season'
        self.v_breadcrum.append({'url': "/setting/index", 'title': _('Setting')})
        self.v_logic_order_by = 'name'

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _(u'Season')

    def _on_index_rendering(self, *args, **kwargs):
        self.v_data['back_url'] = request.args.get('back_url') or ''
        if self.v_data['back_url'] == '':
            self.v_data['back_url'] = url_for('.SettingView:index')
SeasonView.register(app)
