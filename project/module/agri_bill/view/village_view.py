from edf_admin.table import Table
from flask import request
from flask_babel import lazy_gettext as _
from flask_classy import route
from project.module.agri_bill.model import *
from project.module.share.view.base_view import *
from project.module.agri_bill.model import Location
from project.module.agri_bill.logic import location_logic

class VillageForm(FlaskForm):
    id = HiddenField()
    type = HiddenField()
    province_id = DynamicSelectField(
        _(u'Province'),
        query=location_logic.default.all(type='1'),
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All Provinces'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    district_id = DynamicSelectField(
        _(u'District'),
        query=location_logic.default.all(type='2'),
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All District'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    parent_id = DynamicSelectField(
        _(u'Commune'),
        query=location_logic.default.all(type='3'),
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All Commune'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    code = TextField(
        _(u'Village Code'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )
    name = TextField(
        _(u'Village Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )
    latin_name = TextField(
        _(u'Village Latin Name'),
        render_kw={
            'data-val': 'true',
            'autocomplete':'off'
        }
    )

class VillageFilterForm(FlaskForm):
      type = HiddenField()


class VillageTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    code = LinkColumn(_(u'Village Code'), get_url=lambda row: url_for('.VillageView:detail', id=row.id),
                      get_value=lambda row: row.code)
    name = DataColumn(_(u'Village Name'))
    latin_name = DataColumn(_(u'Village Latin Name'))
    action = ActionColumn('', endpoint='.VillageView')


class VillageView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Location
        self.v_logic = location_logic.default
        self.v_table_index_class = VillageTable
        self.v_form_add_class = VillageForm
        self.v_form_filter_class = VillageFilterForm
        self.v_template = 'village'
        self.v_breadcrum.append({'url': "/setting/index", 'title': _('Setting')})
        self.v_logic_order_by = 'code'

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _(u'Village')

    def _on_index_rendering(self, **kwargs):
        self.v_data['form'].type.data = '4'

    def _on_edit_rendering(self, *args, **kwargs):
        form = self.v_data['form']
        commune_id = form.parent_id.data
        commune = location_logic.default.find(commune_id)
        district = location_logic.default.find(commune.parent_id)
        province = location_logic.default.find(district.parent_id)
        form.parent_id.data = commune_id
        form.district_id.data = district.id
        form.province_id.data = province.id

    def _on_detail_rendering(self, *args, **kwargs):
        form = self.v_data['form']
        commune_id = form.parent_id.data
        commune = location_logic.default.find(commune_id)
        district = location_logic.default.find(commune.parent_id)
        province = location_logic.default.find(district.parent_id)
        form.parent_id.data = commune.id
        form.district_id.data = district.id
        form.province_id.data = province.id

VillageView.register(app)
