#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_babel import lazy_gettext as _

from project.module.agri_bill.logic import station_logic, invoice_logic, contract_template_logic
from project.module.payment.logic import payment_logic
from project.module.system.model import Setting
from ...share.view.base_view import *


class SettingView(AdminSecureView):
    route_base = '/setting'

    @route('/index')
    def index(self):
        templates = invoice_logic.default.find_template()
        contract_templates = contract_template_logic.default.find_content_template(is_active=True)
        receipt_templates = payment_logic.default.find_receipt_template()
        if not receipt_templates:
            obj = Setting()
            obj.name = u'គំរូទី 1'
            obj.value = 'receipt_preview-1.html'
            obj.is_active = 1
            receipt_templates = [obj]
        return render_template(
            'setting/index.html',
            title=_(u'Settings'),
            templates=templates,
            receipt_templates=receipt_templates,
            contract_templates=contract_templates,
            layout=session.get('layout', 'breadcrumb'),
            view={}
        )


SettingView.register(app)
