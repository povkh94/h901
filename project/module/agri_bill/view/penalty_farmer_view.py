from flask_babel import lazy_gettext as _
from wtforms import DecimalField

from project.module.agri_bill import enum
from project.module.agri_bill.convert import to_ha, format_value
from project.module.agri_bill.table import AreaColumn, AmountColumn

from project.module.agri_bill.logic import contract_template_logic, penalty_farmer_logic, tariff_logic

from ...share.view.base_view import *
from ...agri_bill.model import *
from ..logic import billing_cycle_logic, billing_logic, block_logic, land_logic, verify_billing_logic
from project.module.system.logic import lookup_logic
from project.module.agri_bill.view import land_view

from flask import render_template_string
from project.module.agri_bill.logic import farmer_logic, location_logic
from random import randint


def lambda_farmer(row):
    try:
        if row.spouse_name:
            return u'{name} - {spouse_name} - {code}'.format(name=row.name, spouse_name=row.spouse_name,
                                                             code=row.code)
        else:
            return u'{name} - {code}'.format(name=row.name, code=row.code)
    except Exception as e:
        return ''


class ProcessPenaltyForm(FlaskForm):
    value = DecimalField(
        _(u'Penalty'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'group_text': _('As')
        }
    )

    penalty_type = DynamicSelectField(
        _(u'Penalty Type'),
        query=lookup_logic.default.get_values(enum.PenaltyType.__val__),
        get_pk='id',
        get_label='value',
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    issue_date = DateField(
        _('Issue Date'),
        render_kw={
            'role': 'date',
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    due_date = DateField(
        _('Due Date'),
        render_kw={
            'role': 'date',
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    ext__invoice = HiddenField()


class PenaltyFarmerTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    farmer = LamdaColumn(_('Farmer'), get_value=lambda row: lambda_farmer(row), render_kw={'width': '20%'})
    total_invoices = AmountColumn(_('Total Invoices'), render_kw={'width': '15%'})
    total_amount = AmountColumn(_('Total Amount'), render_kw={'width': '15%'})
    currency = DataColumn(_('Currency'))
    option = LamdaColumn(
        _(u'Options'),
        get_value=lambda row: u' <div class="checkbox">'
                              u'<label style="font-size:1.3em;" >'
                              u'<input type="checkbox" class="options" data-farmer="{farmer_id}" data-billing="{billing_id}">'
                              u'<span class="cr">' u'<i class="cr-icon glyphicon glyphicon-ok"></i></span>'
                              u'</label>'
                              u'</div>'.format(farmer_id=row.farmer_id, billing_id=row.billing_id)

        , render_kw={'width': '10%'})


class PenaltyFarmerFilterForm(FilterForm):
    billing_id = HiddenField()


class PenaltyFarmerView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Invoice
        self.v_logic = penalty_farmer_logic.default
        self.v_table_index_class = PenaltyFarmerTable
        self.v_form_filter_class = PenaltyFarmerFilterForm
        self.v_template = 'farmer_penalty'
        self.v_logic_order_by = ''

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _('Farmer Penalty')

    def _on_index_rendering(self, *args, **kwargs):

        self.v_data['filter_form'] = PenaltyFarmerFilterForm()
        self.v_data['table'] = PenaltyFarmerTable()
        billing_id = request.args.get('billing_id')
        self.v_data['filter_form'].billing_id.data = billing_id
        billing = billing_logic.default.find(billing_id)
        message = request.args.get('message')
        billing_name = billing.name
        self.v_data['billing_name'] = billing_name
        self.v_data['billing_id'] = billing_id
        form = self.v_data['form']
        form.billing_id.data = billing_id
        self.v_data['process_form'] = ProcessPenaltyForm()
        self.v_data['process_form'].issue_date.data = datetime.now()
        self.v_data['process_form'].due_date.data = datetime.now() + timedelta(days=15)
        if message:
            self.v_data['message'] = message
        else:
            self.v_data['message'] = False

    @route('/process', methods=['get', 'post'])
    def process(self):
        form = ProcessPenaltyForm()
        if form.validate_on_submit():
            value = form.value.data
            penalty_type = form.penalty_type.data
            issue_date = form.issue_date.data
            due_date = form.due_date.data
            ext__invoice = form.ext__invoice.data
            billing_id = penalty_farmer_logic.default.process(value, penalty_type, issue_date, due_date, ext__invoice)
            return redirect(url_for('.PenaltyFarmerView:index', billing_id=billing_id, message=True))

    @route('/detail-filter', methods=['GET', 'POST'])
    def detail_filter(self):
        form = PenaltyFarmerFilterForm()
        q = penalty_farmer_logic.default.search(**form.data)
        table = PenaltyFarmerTable(data=q)
        return self.render_template('detail_filter.html', table=table)


PenaltyFarmerView.register(app)
