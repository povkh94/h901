from flask_babel import lazy_gettext as _
from project.module.agri_bill.logic import billing_logic
from project.module.system.logic import lookup_logic
from ...share.view.base_view import *
from ...agri_bill.model import *
from ..logic import contract_template_logic
from ...share.tinymcefield import TinyMCEField


class ContractTemplateForm(FlaskForm):
    id = HiddenField()
    ext_data = HiddenField()
    name = TextField(
        _(u'Name'),
        render_kw={
            'data-val': 'true',
            'is_half_width': 'False',
            'data-val-required': _(u'Input Required')
        }
    )

    content_template = TinyMCEField(
        _(u'Content'),
        render_kw={
            'data-val': 'true',
            'is_half_width': 'False',
            'data-val-required': _(u'Input Required')
        },
        height="300px",
        marcro_toolbar_data=[
            {
                "toolbar_name": "marcro",
                "toolbar_text": "Output",
                "marcro_menu": [{
                    "menu": [{
                         "menu_name": "Contract",
                         "menu_data": [
                             {
                                 "marcro_text": "Contract Number","marcro_value": '{{ contract_number or "-"}}'
                             }
                         ]
                        }
                        ,{
                        "menu_name": "Billing",
                        "menu_data": [
                            {
                                "marcro_text": "name", "marcro_value": '{{ billing[1] or "-"}}',

                            },
                            {
                                "marcro_text": "Year", "marcro_value": '{{ billing[0] or "-"}}',
                            }
                        ]}, {
                        "menu_name": "Block",
                        "menu_data": [
                            {
                                "marcro_text": "Block Manager", "marcro_value": '{{block.gm_block or "-"}}'
                            },
                            {
                                "marcro_text": "Block Name", "marcro_value": '{{block.name or "-"}}'
                            }
                        ]},
                        {
                            "menu_name": "Farmer",
                            "menu_data": [

                                {
                                    "marcro_text": "Name", "marcro_value": '{{ farmer.code + " - " + farmer.name if farmer.code and farmer.name else farmer.name if farmer.name else "-" }}'
                                },
                                {
                                    "marcro_text": "Sex", "marcro_value": '{{farmer.sex_kh or "-"}}'
                                },
                                {
                                    "marcro_text": "Age", "marcro_value": '{{farmer.age or "-"}}'
                                }
                                ,
                                {
                                    "marcro_text": "Phone", "marcro_value": '{{farmer.phone or "-"}}'
                                },
                                {
                                    "marcro_text": "Spouse Name", "marcro_value": '{{farmer.spouse_name or "-"}}'
                                },
                                {
                                    "marcro_text": "Village", "marcro_value": '{{location.village or "-"}}'
                                },
                                {
                                    "marcro_text": "Commune", "marcro_value": '{{location.commune or "-"}}'
                                }
                                ,
                                {
                                    "marcro_text": "District", "marcro_value": '{{location.district or "-"}}'
                                }
                                ,
                                {
                                    "marcro_text": "Province", "marcro_value": '{{location.province or "-"}}'
                                }
                            ]}, {
                            "menu_name": "Land",
                            "menu_data": [
                                {
                                    "marcro_text": "table", "marcro_value":'{{ land.table | to_table | safe or "-" }} '
                                },
                                {
                                    "marcro_text": "total land area", "marcro_value": '{{total_area | format_currency() or "-"}}'
                                }
                            ]},{
                            "menu_name": "Break Point",
                            "menu_data": [
                                {
                                    "marcro_text": "Break", "marcro_value": '{{ before | safe or "-" }} '
                                }
                            ]
                        },{
                            "menu_name": "Unit",
                            "menu_data": [
                                {
                                    "marcro_text": "Unit", "marcro_value": '{{ label  or "-") }} ',

                                }
                            ]
                        }

                    ]}
                ]
            }
        ]

    )

    contract_template_id = DynamicSelectField(
        _(u'For'),
        query=lookup_logic.default.get_values('con_tem_id'),
        get_pk="value",
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )


class ContractTemplateFilterForm(FlaskForm):
    search = TextField(
        _(u'Search'),
        render_kw={
            "placeholder": "Search",
            "class": "form-control no-print"
        }
    )


class ContractTemplateTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    id = DataColumn('Id', visible=False)
    name = DataColumn(_(u'Name'))
    action = ActionColumn('', endpoint='.ContractTemplateView')


class ContractTemplateView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = ContractTemplate
        self.v_logic = contract_template_logic.default
        self.v_table_index_class = ContractTemplateTable
        self.v_form_add_class = ContractTemplateForm
        self.v_template = 'contract'
        self.v_form_filter_class = ContractTemplateFilterForm
        self.v_logic_order_by = 'created_date desc'
        self.v_breadcrum.append({'url': "/setting/index", 'title': _('Setting')})

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _(u"Contract Template")

    def _on_index_rendering(self, *args, **kwargs):
        self.v_data['back_url'] = request.args.get('back_url') or ''
        if self.v_data['back_url'] == '':
            self.v_data['back_url'] = url_for('.SettingView:index')

    @route('/update_template/<id>')
    def update_template(self, id=''):
        if id:
            update = contract_template_logic.default.update_template(id=id)
            return update
        return 0

ContractTemplateView.register(app)
