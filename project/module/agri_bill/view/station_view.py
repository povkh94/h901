from flask_babel import lazy_gettext as _
from ...share.view.base_view import *
from ..model import *
from ..logic import station_logic


class StationForm(FlaskForm):
    id = HiddenField()
    code = TextField(
        _(u'Station Code'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )
    name = TextField(
        _(u'Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )
    note = TextAreaField(_(u'Note'))


def lamda_link_col(row):
    if row.id != 'none':
        result = "<a href='%s'>%s</a>" % (url_for('.StationView:detail', id=row.id), _(row.name))
    else:
        result = _("%s" % row.name)
    from wtforms.widgets import HTMLString
    return HTMLString(result)


class StationTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    code = LamdaColumn(_(u'Code'), get_value=lambda row: _(row.code))
    name = LamdaColumn(_(u'Name'), get_value=lamda_link_col)
    action = ActionColumn('', endpoint='.StationView')


class StationView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Station
        self.v_logic = station_logic.default
        self.v_table_index_class = StationTable
        self.v_form_add_class = StationForm
        self.v_template = 'station'

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _(u'Station')


StationView.register(app)
