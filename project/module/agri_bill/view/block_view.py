from flask_babel import lazy_gettext as _
from wtforms.widgets import HTMLString

from ...share.view.base_view import *
from ..model import *
from ..logic import station_logic
from ..logic import block_logic
import tkMessageBox


class HiddenForm(FlaskForm):
    id = HiddenField()
    is_remove = HiddenField()

class BlockForm(FlaskForm):
    id = HiddenField()
    company_id = HiddenField()

    code = TextField(
        _(u'Block Code'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )
    name = TextField(
        _(u'Block Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )
    gm_block = TextField(
        _(u'Block Manager'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )

    station_id = DynamicSelectField(
        _(u'Station'),
        query_factory=station_logic.default.search,
        get_pk='id',
        get_label='name',
        blank_text=_('All Stations'),
        allow_blank=True,
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    note = TextAreaField(_(u'Note'))


class BlockFilterForm(FilterForm):
    station_id = DynamicSelectField(
        _(u'Station'),
        query_factory=station_logic.default.search,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All Stations'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )


def lamda_link_col(row):
    if row.id != 'none':
        result = "<a href='%s'>%s</a>" % (url_for('.BlockView:detail', id=row.id), _(row.name))
    else:
        result = _("%s" % row.name)
    return HTMLString(result)


class BlockTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    code = LamdaColumn(_(u'Code'), get_value=lambda row: _(row.code))
    name = LamdaColumn(_(u'Block Name'),
                       get_value=lamda_link_col)
    gm_block = DataColumn(_(u'Block Manager'))
    station = DataColumn(_(u'Station'))
    action = ActionColumn('', endpoint='.BlockView')


class BlockView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Block
        self.v_logic = block_logic.default
        self.v_table_index_class = BlockTable
        self.v_form_add_class = BlockForm
        self.v_form_filter_class = BlockFilterForm
        self.v_template = 'block'
        self.v_logic_order_by = Block.id

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _(u'Block')

    def _on_remove_rendering(self, *args, **kwargs):
        id = self.v_data['form'].id.data
        block = block_logic.default.find(id)
        self.v_data['is_active'] = block.is_active
        self.v_data['canremove'] = True

    # @route('/remove/<id>')
    # def remove(self, id=0):
    #     obj = self.v_logic.find(id)
    #     canremove = self.v_logic.canremove(id)
    #     form = self.v_forms['remove'](obj=obj)
    #     if form.validate():
    #         tkMessageBox.showinfo("Say Hello", "Hello World")
    #
    #     return self.render_template(self.v_templates.get('remove'), canremove=canremove)

    @route('/remove', methods=['GET', 'POST'])
    def remove(self):
        ids = request.args.get('id') or None
        form = HiddenForm()
        block = block_logic.default.find_block_have_land(ids)
        id = form.id.data
        if form.validate_on_submit():
            res = block_logic.default.remove_block(id)
            if res:
                return redirect(url_for(".BlockView:index"))
            else:
                render_template('land/remove-all.html', is_active=False)
            return redirect(url_for(".BlockView:index"))
        if block:
            return render_template('block/remove.html', form=form, is_active=True, ids=ids, text="False")
        else:
            return render_template('block/remove.html', form=form, is_active=True, ids=ids, text="True")



BlockView.register(app)
