# - *- coding: utf- 8 - *-
from flask_babel import lazy_gettext as _

from project.module.agri_bill import enum
from project.module.agri_bill.logic import land_logic, block_logic
from ...share.view.base_view import *
from ...agri_bill.model import *
from project.module.system.logic import lookup_logic
from ..logic import farmer_logic, location_logic
from wtforms import SelectField


class HiddenForm(FlaskForm):
    id = HiddenField()
    is_remove = HiddenField()


class EditForm(FlaskForm):
    farmer_ids = HiddenField()
    province_id = DynamicSelectField(
        _(u'Province'),
        query=location_logic.default.all(type='1'),
        get_pk='id',
        get_label='name'
    )
    district_id = DynamicSelectField(
        _(u'District'),
        query=location_logic.default.all(type='2'),
        get_pk='id',
        get_label='name',

    )
    commune_id = DynamicSelectField(
        _(u'Commune'),
        query=location_logic.default.all(type='3'),
        get_pk='id',
        get_label='name'
    )
    village_id = DynamicSelectField(
        _(u'village'),
        query=location_logic.default.all(type='4'),
        allow_blank=True,
        get_pk='id',
        get_label='name'
    )

    is_active = DynamicSelectField(
        _(u'Active'),
        allow_blank=True,
        blank_text=_(u"All Status"),
        query=lookup_logic.default.get_values('activate'),
        get_pk="value"
    )

    marital_status = DynamicSelectField(
        _(u'Marital'),
        allow_blank=True,
        blank_text=_("All Marital Status"),
        query=lookup_logic.default.get_values('mar_status'),
        get_pk="value",
        render_kw={
            'data-val': 'true',
        }
    )

    sex = DynamicSelectField(
        _(u'Sex'),
        query=lookup_logic.default.get_values('sex'),
        get_pk="value",
        allow_blank=True,
        blank_text=_(u"Sex"),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )


class FarmerForm(FlaskForm):
    id = HiddenField()
    code = TextField(
        _(u'Farmer Code'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
            # 'readonly': 'readonly'
        }
    )
    name = TextField(
        _('Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off',
        }
    )
    sex = DynamicSelectField(
        _(u'Sex'),
        query=lookup_logic.default.get_values('sex'),
        get_pk="value",
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    phone = TextField(
        _(u'Phone'),
        render_kw={
            'data-val': 'true',
            'autocomplete': 'off'
            # 'data-val-required': _(u'Input Required'),
            # 'required': 'required'
        }
    )
    marital_status = DynamicSelectField(
        _(u'Marital'),
        query=lookup_logic.default.get_values('mar_status'),
        get_pk="id",
        render_kw={
            'data-val': 'true',
            # 'data-val-required': _(u'Input Required')
        }
    )
    spouse_name = TextField(_(u'Spouse Name'))
    address = TextAreaField(
        _(u'Note'),
        render_kw={
            'rows': 5,
            'data-val': 'true',
            'clear_top': True,
            'is_half_width': 'False',
            'style': 'resize:none;'
        }
    )
    national_id = TextField(
        _(u'Identity Card Number'),
        render_kw={
            'data-val': 'true',
            # 'data-val-required': _(u'Input Required')
        }
    )
    delivery_code = TextField(_(u'Delivery Code '))
    province_id = DynamicSelectField(
        _(u'Province'),
        query=location_logic.default.all(type='1'),
        get_pk='id',
        get_label='name'
    )
    district_id = DynamicSelectField(
        _(u'District'),
        query=location_logic.default.all(type='2'),
        get_pk='id',
        get_label='name',
        render_kw={
            'data-val': 'false'

        }
    )
    commune_id = DynamicSelectField(
        _(u'Commune'),
        query=location_logic.default.all(type='3'),
        get_pk='id',
        get_label='name'
    )
    village_id = DynamicSelectField(
        _(u'village'),
        query=location_logic.default.all(type='4'),
        allow_blank=True,
        get_pk='id',
        get_label='name'
    )
    active = DynamicSelectField(
        _(u'Active'),
        query=lookup_logic.default.get_values('activate'),
        get_pk="value",
        render_kw={
            'data-val': 'true'
        }
    )
    ext__photo = UploadImageField(_(u'Photo'), multiple=False)


class FarmerFilterForm(FilterForm):
    village_id = DynamicSelectField(
        _(u'Village'),
        # query_factory=location_logic.default.all,
        query=location_logic.default.get_villages(type=4),
        get_pk='id',
        get_label='filter_name',
        allow_blank=True,
        blank_text=_(u'All Villages'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    marital_status = DynamicSelectField(
        _(u'Marital'),
        query=lookup_logic.default.get_values('mar_status'),
        get_pk="id",
        allow_blank=True,
        blank_text=_('Marital'),
        render_kw={
            'data-val': 'true'
        }
    )

    active = DynamicSelectField(
        _(u'Active'),
        query=lookup_logic.default.get_values('activate'),
        get_pk="value",
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )


def lambda_village(row):
    try:
        if row.location:
            return row.location.name
        else:
            return '-'
    except:
        print row.id


def wrap_note(row):
    text = row.address
    return "<div title={0} style='width: 250px;overflow:hidden; white-space:nowrap; text-overflow: ellipsis;'>{1}</div>".format(text,text)

class FarmerTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    code = DataColumn(_(u'Farmer Code'))
    name = LinkColumn(_(u'Name'), get_url=lambda row: url_for('.FarmerView:detail', id=row.id),
                      get_value=lambda row: row.name)
    sex = LamdaColumn(_(u'Sex'), get_value=lambda r: farmer_logic.default.get_marital_status(r.sex).name
    if r.marital_status and farmer_logic.default.get_marital_status(r.sex)
    else '-')
    marital_status = LamdaColumn(_(u'Marital'),
                                 get_value=lambda r: farmer_logic.default.get_marital_status(r.marital_status).name
                                 if r.marital_status and farmer_logic.default.get_marital_status(r.marital_status)
                                 else '-'
                                 )
    # marital_status = DataColumn(_(u'Marital'))
    spouse_name = DataColumn(_(u'Spouse Name'))
    phone = DataColumn(_(u'Phone'))
    village = LamdaColumn(_(u'Village'), get_value=lambda row: lambda_village(row))
    address = LamdaColumn(_(u'Note'), get_value=lambda row: wrap_note(row))
    action = LamdaColumn(
        get_value=lambda
            row: u'<div class="btn-group btn-group-sm pull-right" style="display:flex;">'
                 u'<a href="{url_edit}" class="action btn btn-default btn-sm edit" style="height: 30px;">'
                 u'<span class="glyphicon glyphicon-pencil" style="font-family:Glyphicons Halflings !important;"></span>'
                 u'</a>'
                 u'<a href="{url_remove}" class="action btn btn-default btn-sm edit" style="height: 30px;">'
                 u'<span class="glyphicon glyphicon-trash" style="font-family:Glyphicons Halflings !important;"></span>'
                 u'</a>'
                 u' <div class="checkbox" style="border: 1px solid #d3dbe2;border-left:none;margin-top: 0px;">'
                 u'<label style="font-size:1em;">'
                 u'<input class="check" type="checkbox" id="{id}">'
                 u'<span class="cr" style="border: 1px solid #0078bd ;margin-left: 10px;margin-top:5px">' u'<i class="cr-icon glyphicon glyphicon-ok"></i></span>'
                 u'</label>'
                 u'</div>'
                 u'</div>'
            .format(
            url_edit=url_for(".FarmerView:edit", id=row.id),
            url_remove=url_for(".FarmerView:remove", id=row.id),
            id=row.id
        ),
        render_kw={
            'class': 'no-export'
        }
    )


class FarmerView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Farmer
        self.v_logic = farmer_logic.default
        self.v_table_index_class = FarmerTable
        self.v_form_filter_class = FarmerFilterForm
        self.v_form_add_class = FarmerForm
        self.v_template = 'farmer'
        self.v_logic_order_by = Farmer.code

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _(u'Farmer')

    def _on_index_rendering(self, *args, **kwargs):
        self.v_data['edit_form'] = EditForm()

    def _on_edit_rendering(self, *args, **kwargs):
        form = self.v_data['form']
        obj = farmer_logic.default.find(form.id.data)
        try:
            village = location_logic.default.find(obj.village_id)
            if not village:
                village = location_logic.default.find(id='4')
            commune = location_logic.default.find(village.parent_id)
            district = location_logic.default.find(commune.parent_id)
            province = location_logic.default.find(district.parent_id)
            form.village_id.data = village.id
            form.commune_id.data = commune.id
            form.district_id.data = district.id
            form.province_id.data = province.id
            form.village_id.data = village.id
        except:
            form.commune_id.data = ''
            form.district_id.data = ''
            form.province_id.data = ''
            form.village_id.data = ''

        # if is_active == 'False' or is_active == '0':
        #     form.is_active.data = 'disactive'
        # else:endpoints
        #     form.is_active.data = 'active'

    def _on_detail_rendering(self, *args, **kwargs):
        form = self.v_data['form']
        obj = farmer_logic.default.find(form.id.data)
        try:
            village = location_logic.default.find(obj.village_id)
            if not village:
                village = location_logic.default.find('4')
            commune = location_logic.default.find(village.parent_id)
            district = location_logic.default.find(commune.parent_id)
            province = location_logic.default.find(district.parent_id)
            form.village_id.data = village.id
            form.commune_id.data = commune.id
            form.district_id.data = district.id
            form.province_id.data = province.id
        except:
            form.village_id.data = ''
            form.commune_id.data = ''
            form.district_id.data = ''
            form.province_id.data = ''

        self.v_data['invoices'] = farmer_logic.default.find_invoice_by_farmer(form.id.data)
        self.v_data['lands'] = land_logic.default.find(farmer_id=form.id.data)
        self.v_data['rent'] = land_logic.default.find(rent_id=form.id.data)

    def _on_remove_rendering(self, *args, **kwargs):
        id = self.v_data['form'].id.data
        farmer = farmer_logic.default.find(id)
        self.v_data['is_active'] = farmer.is_active
        self.v_data['canremove'] = farmer_logic.default.check_exist_farmer_in_land(id)

    @route('/suggestion')
    def suggestion(self):
        term = request.args.get('term', '')
        limit = request.args.get('limit', 25)
        suggests = farmer_logic.default.search(search=term, active = 'active')
        suggests = suggests.limit(limit)
        results = [dict(
            id=s.id,
            text='{name}{spouse_name}{code}'.format(name=s.name + ' - ',
                                                    spouse_name=s.spouse_name if s.spouse_name and s.spouse_name != '' else s.code,
                                                    code=' - ' + s.code if s.spouse_name else ''),
            obj=s
        ) for s in suggests]
        data = {'results': results}
        return json.dumps(data)

    @route('/update-all-farmers', methods=['POST', 'GET'])
    def update_all_farmers(self):
        form = EditForm()
        if form.validate_on_submit():
            ids = json.loads(form.farmer_ids.data)
            marital_status = form.marital_status.data
            village_id = form.village_id.data
            is_active = form.is_active.data
            sex = form.sex.data
            data = {
                'marital_status': marital_status,
                'village_id': village_id,
                'active': is_active,
                'sex': sex
            }
            if is_active == "disactive":
                for id in ids:
                    check = farmer_logic.default.check_exist_farmer_in_land(id)
                    if check == False:
                        return render_template('farmer/remove-all.html', view={"canremove": "False"})
            message = self.v_logic.update_all_farmers(ids, data)
            return redirect(url_for(".FarmerView:index"))

    @route('/remove-all-farmers', methods=['GET', 'POST'])
    def remove_all_farmers(self):
        ids = request.args.get('id') or None
        form = HiddenForm()
        if form.validate_on_submit():
            is_remove = form.is_remove.data
            data = form.id.data.replace("&#34;", "")
            ids = data.replace("[", "").replace("]", "").split(",")
            res = farmer_logic.default.is_remove_all(ids, is_remove)
            if res == "success":
                return redirect(url_for(".FarmerView:index"))
            else:
                return render_template('farmer/remove-all.html', view={"canremove":"False"})

        return render_template('farmer/remove-all.html', form=form, ids=ids, is_active=True)

    @route('/edit/<id>', methods=['GET', 'POST'])
    def edit(self, id=0):
        self.v_data['obj'] = self.v_logic.find(id)
        form = self._get_form(name='edit')(request.form, obj=self.v_data['obj'])
        if request.method == 'POST' and form.validate():
            self.v_data['obj'] = from_dict(form.data, to_cls=self.v_class, defaults={})
            try:
                check = self.v_logic.update(self.v_data['obj'])
                if check == False:
                    return render_template('farmer/remove-all.html', view={"canremove":"False"})
                return redirect(url_for(".FarmerView:index"))
            except LogicError, e:
                db.rollback()
                if e.field in form:
                    form[e.field].errors.append(str(e))
        self.v_data['form'] = form

        self.v_data['return_url'] = ''

        # add breadcrumb to view object
        self.v_data['breadcrumb'] = [
            {'url': url_for(self.v_data['endpoints']['index']), 'title': self.v_data['title']},
            {'url': url_for(self.v_data['endpoints']['detail'], id=form.id.data),
             'title': self.v_data['obj'].name},
            {'url': '', 'title': _('Edit')}
        ]
        return self.render_template(self.v_templates.get('edit'))


    @route('/add', methods=['GET', 'POST'])
    def add(self):
        form = FarmerForm()
        auto_farmer_code = farmer_logic.default.find_last_farmer_code()
        form.code.data = auto_farmer_code
        if form.validate_on_submit():
            form = FarmerForm()
            obj = from_dict(form.data, to_cls=self.v_class, ignores=['id'])
            # obj = from_dict(form.data, to_cls=self.v_class)
            try:
                farmer_logic.default.add(obj)
                return redirect(url_for(".FarmerView:index"))
            except LogicError, e:
                db.rollback()
                if e.field in form:
                    form[e.field].errors.append(str(e))

            # fill default value

        # add breadcrumb to view object
        self.v_data['breadcrumb'] = [
            {'url': url_for(self.v_data['endpoints']['index']), 'title': self.v_data['title']},
            {'url': '', 'title': _('Add')}
        ]

        self.events.on_add_rendering()

        self.v_data['breadcrumb'] = self.v_breadcrum + self.v_data['breadcrumb']

        return self.render_template(self.v_templates.get('add'), form = form)


FarmerView.register(app)
