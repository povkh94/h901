from flask_babel import lazy_gettext as _
from wtforms.widgets import HTMLString

from project.module.agri_bill.logic import invoice_logic
from ...share.view.base_view import *
from ...agri_bill.model import *
from ..logic import billing_cycle_logic, billing_logic, block_logic, land_logic, verify_billing_logic


class ProcessForm(FlaskForm):
    billing_id = HiddenField()
    issue_date = DateField(
        _('Issue Date'),
        render_kw={
            'data-val': 'true',
            'role': 'date',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )

    due_date = DateField(
        _('Due Date'),
        render_kw={
            'role': 'date',
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )


class ExtendDueDateForm(FlaskForm):
    billing_id = HiddenField()
    due_date = DateField(
        _('Due Date'),
        render_kw={
            'role': 'date',
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )


class BillingForm(FlaskForm):
    id = HiddenField()
    ext_data = HiddenField()
    billing_status_id = HiddenField()
    name = TextField(
        _('Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )

    season_id = HiddenField()

    billing_cycle_id = DynamicSelectField(
        _('Billing Cycle'),
        query_factory=billing_cycle_logic.default.search,
        get_pk='id',
        allow_blank=True,
        get_label='name',
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )

    start_date = DateField(
        _('From'),
        render_kw={
            'role': 'date',
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'addon': 'fa-calendar'
        }
    )

    end_date = DateField(
        _('To'),
        render_kw={
            'role': 'date',
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )


class VerifyBillingTabel(Table):
    rowno = RowNumberColumn(_(u'No.'))
    block_name = LinkColumn(_(u'Block'), get_value=lambda row:
    u'<a href="" data-ajax="false">{block_code} - {block_name}<a/>'
                            .format(
        block_code=row.block_code,
        block_name=row.block_name)
                            )

    farmer_name = LamdaColumn(_(u'Owner'),
                              get_value=lambda row: row.farmer_code + ' - ' + row.farmer_name)
    total_land = LamdaColumn(_(u'Total Land'), get_value=lambda row: row.total_land)
    renting = LamdaColumn(_(u'Rent'), get_value=lambda row: u"{result}"
                          .format(result=_(u'-')
    if row.renting == 0 else row.renting
                                  ))
    remain = LamdaColumn(_(u'Remain'),
                         get_value=lambda row: u"<span>{result}</span>"
                         .format(result=_(u'-') if row.total_land - row.renting == 0 else row.total_land - row.renting)
                         )
    confirm = LamdaColumn(
        _(u'Agreement'),
        get_value=lambda row: u' <div class="checkbox">'
                              u'<label style="font-size:1.3em;" >'
                              u'<input type="checkbox" data-block-id="{block_id}" data-billing-id="{billing_id}" data-farmer-id="{farmer_id}" {status}/>'
                              u'<span class="cr">' u'<i class="cr-icon glyphicon glyphicon-ok"></i></span>'
                              u'</label>'
                              u'</div>'.format(
            status='checked' if row.status == 'verified' else '',
            block_id=row.block_id,
            billing_id=row.billing_id,
            farmer_id=row.farmer_id,

        )
    )
    action = LamdaColumn(
        # _(u'Status'),
        get_value=lambda
            row: u'<a href="{url}" class="action btn btn-default btn-sm edit" style="height: 30px;">'
                 u'<span class="glyphicon glyphicon-print" style="font-family:Glyphicons Halflings !important;"></span>'
                 u'</a>'.format(
            url=url_for(".BillingView:agreement", id=row.billing_id, block_id=row.block_id, farmer_id=row.farmer_id)
        )
    )


class VerifyForm(Form):
    Search = TextField(
        _('search'),
        render_kw={
            'placeholder': 'search'
        }
    )
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.search,
        get_pk='id',
        get_label='filter_name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )


def lamda_link_col(row):
    if row.billing_cycle_active:
        result = "<a href='%s'>%s</a>" % (url_for('.BillingView:detail', id=row.id), row.name)
    else:
        result = "%s" % row.name
    return HTMLString(result)


class BillingTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    # name = LamdaColumn(_(u'Name'), get_value=lamda_link_col)
    name = LamdaColumn(_(u"Name"), get_value=lambda row: lamda_link_col(row))
    billing_cycle = DataColumn(_('Billing Cycle'))
    start_date = DateColumn(_('From'))
    end_date = DateColumn(_('To'))
    invoice_due_date = DateColumn(_('Invoice due date'))
    billing_status_value = LamdaColumn(_(u'Status'),
                                       get_value=lambda
                                           row: u'<span style="text-align:center;color:white"><div style="min-width:20px;text-align=center;border-radius: .25em; color: #fff;font-size: 75%;padding: .2em .6em .3em;" class="{class_name}">{text}</div></span>'
                                       .format(class_name='label-success'
                                       if row.billing_status_id == 'close' else 'label-warning' if row.billing_status_id == 'verified' else 'label-danger',
                                               text=_(u'VERIFY')
                                               if row.billing_status_id == 'verified' else _(u'START')
                                               if row.billing_status_id == 'start' else _(u'CLOSE')
                                               ),
                                       render_kw={'width': '100px;'}

                                       )
    action = ActionColumn('', endpoint='.BillingView')


class InvoiceTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    invoice_farmer_name = DataColumn(_(u'Farmer'))
    total_area = DecimalColumn(_(u'Total Area'))
    total_cost = DecimalColumn(_(u'Total Cost'))
    action = ActionColumn('', endpoint='.BillingView')


class BillingView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Billing
        self.v_logic = billing_logic.default
        self.v_table_index_class = BillingTable
        self.v_form_add_class = BillingForm
        self.v_template = 'billing'
        self.v_logic_order_by = 'created_date desc'

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _('Billing')

    def _on_add_rendering(self, *args, **kwargs):
        form = self.v_data['form']
        form.billing_status_id.data = 'start'
        self.v_data['form'].end_date.data = datetime.now() + timedelta(days=90)

    def _on_edit_rendering(self, *args, **kwargs):
        id = self.v_data['form'].id.data
        obj = self.v_logic.find(id)
        self.v_data['form'].ext_data.data = obj.billing_station_json
        self.v_data['status'] = False
        self.v_data['obj'] = obj
        status = invoice_logic.default.check_invoice_status(billing_id=id)
        if status:
            self.v_data['status'] = True

    def _on_remove_rendering(self, *args, **kwargs):
        self.v_data['canremove'] = True
        id = self.v_data['form'].id.data
        status = invoice_logic.default.check_invoice_status(billing_id=id)
        if status:
            self.v_data['canremove'] = False

    def _on_detail_rendering(self, *args, **kwargs):
        id = self.v_data['form'].id.data
        billing = self.v_logic.find(id)
        self.v_data['form'].billing_cycle_id.data = billing.billing_cycle_id
        obj = self.v_logic.update_billing_station(billing)
        self.v_data['form'].ext_data.data = obj.billing_station_json
        self.v_data['process_form'] = ProcessForm()
        self.v_data['process_form'].billing_id.data = id
        self.v_data['process_form'].issue_date.data = datetime.now()
        self.v_data['process_form'].due_date.data = datetime.now() + timedelta(days=15)
        self.v_data['action_url'] = url_for('.BillingView:process_billing')
        self.v_data['summary'] = None
        self.v_data['obj'] = obj
        if obj.billing_status_id == 'close':
            summary = billing_logic.default.count_invoice(id)
            invoice_by_block = billing_logic.default.invoice_by_block(id)
            invoice_by_village = billing_logic.default.invoice_by_village(id)
            non_irrigated = billing_logic.default.find_non_irrigated(id)
            self.v_data['summary'] = summary
            self.v_data['invoice_by_block'] = invoice_by_block
            self.v_data['invoice_by_village'] = invoice_by_village
            self.v_data['non_irrigated'] = non_irrigated
            self.v_data['extend_due_date_form'] = ExtendDueDateForm()
            self.v_data['extend_due_date_form'].billing_id.data = id
            self.v_data['extend_due_date_form'].due_date.data = obj.invoice_due_date
            self.v_data['action_url'] = url_for('.BillingView:extend_due_date')

            extend_message = request.args.get('extend_message') or ''
            if extend_message:
                self.v_data['extend_message'] = extend_message

    @route('/get_station/<billing_cycle_id>', methods=['Get'])
    def get_station(self, billing_cycle_id):
        block_ids = billing_cycle_logic.default.get_existing_block(id=billing_cycle_id)
        sum_land_area = land_logic.default.sum_land_area(block_ids)

        return json.dumps(sum_land_area)

    def process(self, id):
        form = ProcessForm()
        if form.validate_on_submit():
            issue_date = form.issue_date.data
            due_date = form.due_date.data
            status = verify_billing_logic.default.check_status(billing_id=id)
            if status == 'unverified':
                billing_status = self.v_logic.billing_status(id=id, status='start')
                return redirect(url_for(".BillingView:detail", id=id, status=status))
            else:
                id = billing_logic.default.process(id, issue_date, due_date)
                return redirect(url_for(".BillingView:success", id=id))

    @route('/success/<id>')
    def success(self, id=''):
        invoice = billing_logic.default.count_invoice(billing_id=id)
        return render_template('billing/success.html', id=id, invoice=invoice)

    @route('/verify_billing/<id>', methods=['get', 'post'])
    def verify_billing(self, id):
        billing = billing_logic.default.get_billing_cycle_id(id)
        form = VerifyForm()
        obj = billing_logic.default.get_verify_billing(id)
        if form.validate_on_submit():
            search = form.Search.data or ''
            block_id = form.block_id.data or ''
            if search:
                obj = obj.filter(or_(
                    func.lower(Land.code).like('%' + search.lower() + '%'),
                    func.lower(Farmer.name).like('%' + search.lower() + '%'),
                    func.lower(Farmer.code).like('%' + search.lower() + '%'),
                    func.lower(Block.name).like('%' + search.lower() + '%'),
                    func.lower(Block.code).like('%' + search.lower() + '%'),
                ))
            if block_id:
                obj = obj.filter(Land.block_id == block_id)
        obj = obj.order_by(Verify.block_id)

        page = int(request.args.get('page', 1))
        per_page = int(request.args.get('per_page', 10))
        import math
        total = obj.count()
        num_page = math.ceil(total / float(per_page))
        next_num = page + 1
        if next_num > num_page:
            next_num = 0
        if per_page:
            obj = obj.limit(per_page)
        if page:
            obj = obj.offset((page - 1) * per_page)

        pagging = {"page": page, "next_num": next_num, "total": total, "per_page": per_page}
        data = DataView(query=obj)
        table = VerifyBillingTabel(data=data.result)

        # return render_template('verify/verify.html', form=form,
        #                        data=data,
        #                        table=table,
        #                        pagging=pagging,
        #                        id=id, billing=billing)

    @route('process-billing', methods=['GET', 'POST'])
    def process_billing(self):
        form = ProcessForm()
        if form.validate_on_submit():
            id = billing_logic.default.process_billing(form.billing_id.data, form.issue_date.data, form.due_date.data)
            return redirect(url_for('.BillingView:success', id=id))

    @route('extend-due-date', methods=['POST'])
    def extend_due_date(self):
        form = ExtendDueDateForm()
        if form.validate_on_submit():
            billing_id = invoice_logic.default.update_invoice_due_date(form.billing_id.data, form.due_date.data)
            if billing_id:
                from project.module.agri_bill.logic import bank_payment_logic
                bank_payment_logic.default.update_customer_data(force_all=True)
                return redirect(url_for('.BillingView:detail', id=billing_id,
                                        extend_message=_('extend invoice due date success')))


BillingView.register(app)
