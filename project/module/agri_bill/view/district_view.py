from edf_admin.table import Table
from flask import request
from flask_babel import lazy_gettext as _
from flask_classy import route
from project.module.agri_bill.model import *
from project.module.share.view.base_view import *
from project.module.agri_bill.model import Location
from project.module.agri_bill.logic import location_logic

class DistrictForm(FlaskForm):
    id = HiddenField()
    type = HiddenField()
    parent_id = DynamicSelectField(
        _(u'Province'),
        query=location_logic.default.all(type='1'),
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All Provinces'),
        render_kw={
            'data-val': 'true',
            # 'data-val-required': _(u'Input Required')
        }
    )
    code = TextField(
        _(u'District Code'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )
    name = TextField(
        _(u'District Name'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )
    latin_name = TextField(
        _(u'District Latin Name'),
        render_kw={
            'data-val': 'true',
            'autocomplete':'off'
        }
    )

class DistrictFilterForm(FlaskForm):
      type = HiddenField()


class DistrictTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    code = LinkColumn(_(u'District Code'), get_url=lambda row: url_for('.DistrictView:detail', id=row.id),
                      get_value=lambda row: row.code)
    name = DataColumn(_(u'District Name'))
    latin_name = DataColumn(_(u'District Latin Name'))
    action = ActionColumn('', endpoint='.DistrictView')


class DistrictView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Location
        self.v_logic = location_logic.default
        self.v_table_index_class = DistrictTable
        self.v_form_add_class = DistrictForm
        self.v_form_filter_class = DistrictFilterForm
        self.v_template = 'district'
        self.v_breadcrum.append({'url': "/setting/index", 'title': _('Setting')})
        self.v_logic_order_by = 'code'

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _(u'District')

    def _on_index_rendering(self, **kwargs):
        self.v_data['form'].type.data = '2'





DistrictView.register(app)
