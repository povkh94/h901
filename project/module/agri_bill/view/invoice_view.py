from wtforms.widgets import HTMLString

from project.module.agri_bill import enum
from project.module.agri_bill.convert import to_ha
from project.module.agri_bill.export import export_pdf
from flask_babel import lazy_gettext as _

from project.module.agri_bill.logic import block_logic, verify_billing_logic, land_logic, tariff_logic
from project.module.agri_bill.table import AmountColumn, DecimalColumn
from project.module.agri_bill.view.verify_billing_view import LandVerifyTabel, LandVerifiedTabel
from project.module.share.app_filter import to_riel
from project.module.system.logic import lookup_logic
from ...share.view.base_view import *
from ...currency.logic import currency_logic
from ...agri_bill.model import *
from ..logic import billing_logic, invoice_logic, station_logic, farmer_logic, location_logic


class CustomActionColumn(ActionColumn):

    def value(self, row):
        if self.editonly:
            return render_template_string(
                '''<div class="btn-group btn-group-sm pull-right">
                      <a href="{{url_for(endpoint+':edit',id=id)}}" class="action btn btn-default btn-sm edit" style="height: 30px;">
                      <span class="glyphicon glyphicon-pencil" style="font-family:Glyphicons Halflings !important;"></span></a>
                      </div>'''
                , endpoint=self.endpoint
                , id=row.id)

        if self.removeonly:
            return render_template_string(
                '''<div class="btn-group btn-group-sm pull-right">
                      <a href="{{url_for(endpoint+':remove',id=id)}}" class="action btn btn-default btn-sm remove" style="height: 30px;">
                      <span class="glyphicon glyphicon-trash" style="font-family:Glyphicons Halflings !important;"></span>
                      </a>
                      </div>'''
                , endpoint=self.endpoint
                , id=row.id)

        more_actions = []
        if self.more:
            for item in self.more(row):
                if 'url' in item:
                    action = '<li><a href="%s">%s</a></li>' % (item.get('url'), item.get('label'))
                else:
                    action = '<li><a class="%s" data-id="%s">%s</a></li>' % (
                        item.get('class'), item.get("data-id"), item.get('label'))

                more_actions.append(action)

        dropdown = render_template_string(
            '''
            <ul class="dropdown-menu" role="menu">
                {{ more_actions }}
            </ul>
            '''
            , modal_id='modal-%s' % row.id
            , more_actions=HTMLString(''.join(more_actions))
        )

        return render_template_string(
            '''
            <div class="btn-group btn-group-sm pull-right" style="display:flex;">
                <a href="{{url_for(endpoint+':edit',id=id)}}" class="action btn btn-default btn-sm edit" style="height: 30px;">
                    <span class="glyphicon glyphicon-pencil" style="font-family:Glyphicons Halflings !important;"></span>
                </a>
                <a href="{{url_for(endpoint+':remove',id=id)}}" class="action btn btn-default btn-sm remove" style="height: 30px;">
                    <span class="glyphicon glyphicon-trash" style="font-family:Glyphicons Halflings !important;"></span>
                </a>
                {% if dropdown %}
                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                </button>
                {{ dropdown }}
                {% endif %}
            </div>
            '''
            , endpoint=self.endpoint
            , id=row.id
            , dropdown=HTMLString(dropdown) if self.more else '')


class InvoiceFrom(FlaskForm):
    id = HiddenField()
    ext_data = HiddenField()
    status_id = HiddenField()
    is_active = HiddenField()
    code = TextField(
        _(u'Code'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'readonly': 'readonly'
        }
    )
    billing_id = DynamicSelectField(
        _(u'Billing Name'),
        query_factory=billing_logic.default.find_billing,
        get_pk="id",
        get_label="name",
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    block_id = DynamicSelectField(
        _('Block'),
        query_factory=block_logic.default.search,
        get_pk='id',
        get_label='name'

    )

    farmer_id = Select2Field(
        label=_(u'Farmer'),
        remote_url='/farmer/suggestion',
        min_input_length=0,
        text_factory=lambda x: farmer_logic.default.find(x),
        multiple=False
    )
    total_lands = Select2Field(
        label=_(u'Land'),
        remote_url='/land/suggestion',
        allow_blank=True,
        blank_text=_(u'Land Code'),
        min_input_length=0,
        max_selection_length=10000,
        text_factory=lambda x: land_logic.default.find(x),
        multiple=True
    )
    ext__unit_cost = TextField(
        _(u'Tariff'), render_kw={
            'data-val': 'true',
            # 'data-val-required': _(u'Input Required'),
            # 'required': 'required',
            'autocomplete': 'off'
        }
    )
    total_area = TextField(
        _(u'Area(Are)'),
        render_kw={
            'readonly': 'readonly'
        }
    )
    total_amount = TextField(
        _(u'Total Cost')
    )

    station_id = DynamicSelectField(
        _('Station'),
        query_factory=station_logic.default.search,
        get_pk='id',
        allow_blank=True,
        get_label='name'
    )
    paid_amount = TextField(
        _(u'Paid Amount')
    )

    currency_id = DynamicSelectField(
        _('Currency'),
        query_factory=currency_logic.default.search,
        get_pk='id',
        allow_blank=True,
        get_label='sign'
    )

    issue_date = DateField(
        _('Issue Date')
    )

    due_date = DateField(
        _('Due Date'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    note = TextAreaField(_(u'Note'))
    company_id = HiddenField()


# render action column
def more(row):
    more = []
    more.append(
        {'label': _(u'Preview Invoice'), 'url': url_for('.InvoiceView:preview_invoice', id=row.id, code=row.code)})
    if row.contract_number:
        more.insert(len(more) - 1, {'label': _(u'Preview Contract'),
                                    'url': url_for(".InvoiceView:preview_invoice", id=row.id, billing_id=row.billing_id,
                                                   contract_number=row.contract_number)}
                    )
    if row.status_id != 'paid' and row.status_id != 'void':
        more.insert(0, {'label': _(u'Pay Now'), 'url': url_for('.PaymentView:add', invoice_code=row.code,
                                                               redirect_url=url_for('.InvoiceView:index'))})
    if row.status_id == 'paid' or row.status_id == 'partial':
        from project.module.payment.logic import payment_logic
        payment = payment_logic.default.find_receipt(row.id)
        if payment:
            more.insert(0,
                        {'label': _(u'Preview Receipt'), 'data-id': row.id, 'class': 'preview-receipt'}
                        )
    return more


def lamda_link_col(row):
    if row.invoice_type != str(enum.InvoiceType.PENALTY):
        result = "<a href='%s'>%s</a>" % (url_for('.InvoiceView:detail', id=row.id), row.code)
    else:
        result = "%s" % row.code
    return HTMLString(result)


def lambda_invoice_type(row):
    type = lookup_logic.default.get_values(
        lookupvalue_id=row.invoice_type).first().value
    return type


def lamda_farmer(row, type='owner'):
    try:
        if type == 'owner':
            farmer = getattr(row, 'owner_farmer')
        else:
            farmer = getattr(row, 'farmer')
        if farmer:
            return farmer.farmer
    except Exception:
        pass


class InvoiceTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    code = LamdaColumn(_('Code'), get_value=lamda_link_col)
    season = DataColumn(_(u'Season'))
    contract_number = LamdaColumn(_(u'Contract Number'),
                                  get_value=lambda row: row.contract_number if row.contract_number else _(u'NA'))
    station = LamdaColumn(_(u'Station'), get_value=lambda row: getattr(row.station, 'name',''))
    block = LamdaColumn(_(u'Block'), get_value=lambda row: getattr(row.block,'name',''))
    owner = LamdaColumn(_(u'Owner'), get_value=lambda row: lamda_farmer(row))
    farmer = LamdaColumn(_(u'Farmer'), get_value=lambda row: lamda_farmer(row, type='farmer'))
    village = LamdaColumn(_(u'Village'), get_value=lambda row: getattr(row.farmer.location,'name','')) #row.farmer.location.name
    total_lands = LamdaColumn(_(u'Total Lands'), get_value=lambda row: u"{value}".format(value=int(row.total_lands)))
    add_or_minu_area = LamdaColumn(_(u'Add or Minus Area'), get_value=lambda row: row.ext__area or '-')
    total_area = DecimalColumn(_(u'Total Area(Are)'), render_kw={
        'data-t': 'n',
        'data-a-h': 'left'
    })
    amount = LamdaColumn(_(u'Credit or Debit Amount'), get_value=lambda row: to_riel(row.ext__amount or '-') or '-',
                         render_kw={
                             'style': 'text-align:right',
                         })
    total_amount = AmountColumn(_(u'Total Amounts'), render_kw={'data-a-h': 'right'})
    paid_amount = AmountColumn(_(u'Paid Amount'), render_kw={'data-a-h': 'right'})
    currency = LamdaColumn(_(u' '), get_value=lambda row: row.currency.sign)
    status_value = LamdaColumn(_(u'Status Payment'),
                               get_value=lambda
                                   row: u'<span style="text-align:center;color:white"><div style="min-width:75px;text-align=center;border-radius: .25em; color: #fff;font-size: 75%;padding: .2em .6em .3em;" class="{class_name}">{text}</div></span>'
                               .format(class_name='label-success'
                               if row.status_id == 'paid' else 'label-warning' if row.status_id == 'partial' else 'label-danger' if row.status_id == 'pending' else 'label-primary',
                                       text=_(u'PENDING')
                                       if row.status_id == 'pending' else _(u'PARTIAL')
                                       if row.status_id == 'partial' else _(u'PAID')
                                       if row.status_id == 'paid' else _(u"VOID")
                                       )
                               )
    invoice_type = LamdaColumn(_(u'Status'), get_value=lambda_invoice_type)
    action = CustomActionColumn(
        '',
        endpoint='.InvoiceView',
        more=more
    )


class InvoiceFilterForm(FilterForm):
    billing_id = DynamicSelectField(
        _(u'Billing'),
        query_factory=billing_logic.default.find_billing,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_(u'All Billing'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.filter,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    village_id = DynamicSelectField(
        _(u'Village'),
        # query_factory=location_logic.default.all,
        query=location_logic.default.get_villages(type=4),
        get_pk='id',
        get_label='filter_name',
        allow_blank=True,
        blank_text=_(u'All Villages'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    invoice_status_id = DynamicSelectField(
        _(u'Status Payment'),
        query=lookup_logic.default.get_values('in_status'),
        get_pk="id",
        get_label="name",
        allow_blank=True,
        blank_text=_(u'Status Payment'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )

    invoice_type = DynamicSelectField(
        _(u'Invoice Status'),
        query=lookup_logic.default.get_values(enum.InvoiceType.__val__),
        get_pk='id',
        get_label='value',
        allow_blank=True,
        blank_text=_(u'All Status'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    ext_data = HiddenField()


class InvoiceView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Invoice
        self.v_logic = invoice_logic.default
        self.v_table_index_class = InvoiceTable
        self.v_form_filter_class = InvoiceFilterForm
        self.v_template = 'invoice'
        self.v_form_add_class = InvoiceFrom
        self.v_data['title'] = "Invoice"
        self.v_logic_order_by = Invoice.code

    def _on_index_rendering(self, *args, **kwargs):
        self.v_data['title'] = "Invoice"
        id = request.args.get('id')
        self.v_data['back_url'] = request.args.get('back_url') or ''
        block_id = request.args.get('block_id')
        village_id = request.args.get('village_id')
        if id:
            self.v_data['form'].billing_id.data = id
        if block_id == '0' or village_id == '0':
            self.v_data['form'].block_id.data = ''
            self.v_data['form'].village_id.data = ''
        if block_id:
            self.v_data['form'].village_id.data = village_id
            self.v_data['form'].block_id.data = block_id
        if village_id:
            self.v_data['form'].village_id.data = village_id
            self.v_data['form'].block_id.data = ''
        self.v_data['date'] = datetime.now()

    def _on_detail_rendering(self, *args, **kwargs):
        form = self.v_data['form']
        id = form.id.data
        invoice = invoice_logic.default.find(id)
        self.v_data['status'] = invoice.status_id
        self.v_data['note'] = invoice.note
        self.v_data['title'] = "Invoice"
        self.v_data['obj'] = invoice
        invoice_detail = self.v_logic.find_invoice_detail(invoice_id=id)
        if invoice_detail:
            self.v_data['invoice_detail'] = invoice_detail

    def _on_edit_rendering(self, *args, **kwargs):
        form = self.v_data['form']
        id = form.id.data
        invoice = invoice_logic.default.find(id)
        self.v_data['status_id'] = invoice.status_id
        self.v_data['activate'] = invoice.is_active
        self.v_data['note'] = invoice.note
        self.v_data['title'] = "Invoice"
        invoice_detail = self.v_logic.find_invoice_detail(invoice_id=id)
        if invoice_detail:
            self.v_data['invoice_detail'] = invoice_detail

    def _on_remove_rendering(self, *args, **kwargs):
        self.v_data['canremove'] = True
        id = self.v_data['form'].id.data
        status = invoice_logic.default.check_invoice_status(id=id)
        invoice = invoice_logic.default.find(id=id)
        if status or invoice.status_id == 'void':
            self.v_data['canremove'] = False

    @route('/preview_invoice/<id>', methods=['GET'])
    def preview_invoice(self, id):
        code = request.args.get('code') or ''
        billing_id = request.args.get('billing_id') or ''
        contract_number = request.args.get('contract_number') or ''
        back_url = request.args.get('back_url', url_for(".InvoiceView:index"))
        breadcrumb_title = request.args.get('breadcrumb_title') or ''
        if code:
            return render_template('invoice/preview_invoice.html', id=id, code=code, back_url=back_url,
                                   breadcrumb_title=breadcrumb_title)

        return render_template('invoice/preview_invoice.html', id=id, billing_id=billing_id,
                               contract_number=contract_number,
                               back_url=back_url, breadcrumb_title=breadcrumb_title)

    @route('/re-contract-content')
    def re_contract_content(self):
        """
            obj: Verify
        """
        from flask import request, render_template, render_template_string
        contract_number = request.args.get('contract_number')
        id = request.args.get('id')
        billing_id = request.args.get('billing_id')
        obj = db.query(Verify).filter(Verify.billing_id == billing_id,
                                      Verify.contract_number == contract_number).first()
        if not obj.ext__lands:
            return redirect(url_for('InvoiceView:contract_content', id=id, contract_number=contract_number))
        content_template = obj.billing.contract_template
        billing = obj.billing.name.split('-')
        total_area = 0
        lands = obj.ext__lands
        tariffs = tariff_logic.default.search().all()
        new_lands = []
        if lands:
            for land in lands:
                o = ExtDictObj(land)
                if o.land_status_id == 'irrigate':
                    total_area += o.land_area

                for tariff in tariffs:
                    if tariff.id == o.tariff_id:
                        setattr(LandVerifiedTabel, o.tariff_id,
                                LamdaColumn(o.tariff_name, render_kw={"tariff_id": o.tariff_id, 'width': '10%'},
                                            get_value=lambda
                                                x: u'<input type="checkbox" tariff_id={tariff_id}>'.format(
                                                tariff_id=o.tariff_id)))

                    else:
                        setattr(LandVerifiedTabel, tariff.id,
                                LamdaColumn(tariff.name, render_kw={"tariff_id": tariff.id, 'width': '10%'},
                                            get_value=lambda
                                                x: u'<input type="checkbox" tariff_id={tariff_id}>'.format(
                                                tariff_id=tariff.id)))

                new_lands.append(o)

            def convert(value):
                try:
                    v = int(value)
                except:
                    v = 0
                return v

            newlist = sorted(new_lands, key=lambda x: '{0:4d}'.format(convert(x.land_code)))
            table = LandVerifiedTabel(data=newlist)
            before = '<div style="page-break-before: always;"></div>'
            location = location_logic.default.get_location(obj.farmer.id)
            try:
                content = render_template_string(content_template.content_template,
                                                 contract_number=obj.contract_number,
                                                 billing=billing,
                                                 block=obj.block,
                                                 farmer=obj.farmer,
                                                 location=location,
                                                 land=dict(table=table),
                                                 total_area=total_area,
                                                 before=before
                                                 )
                return render_template("invoice/contract_content.html", content=content)
            except:
                return ''

    @route('/contract-content')
    def contract_content(self):
        from flask import request, render_template, render_template_string
        from ..logic import contract_template_logic
        contract_number = request.args.get('contract_number')
        invoice_id = request.args.get('id') or ''
        obj = invoice_logic.default.find(invoice_id)
        content_template = obj.billing.contract_template
        billing = obj.billing.name.split('-')
        total_area = 0
        # FIND INVOICE DETAIL ID
        q_invoice_detail_id = db.query(InvoiceDetail.id).filter(InvoiceDetail.invoice_id == obj.id)
        # find land in billing land via invoice_detail
        Rent = aliased(Farmer)
        lands = (db.query(
            BillingLand.owner_farmer_id,
            BillingLand.invoice_farmer_id,
            BillingLand.land_id.label('land_id'),
            BillingLand.tariff_id.label('tariff_id'),
            BillingLand.area.label('land_area'),
            Farmer.name.label('farmer_name'),
            Farmer.code.label('farmer_code'),
            Farmer.spouse_name.label('farmer_spouse_name'),
            Rent.name.label('rent_name'),
            Rent.code.label('rent_code'),
            Rent.spouse_name.label('rent_spouse_name'),
            Block.name.label('block_name'),
            Tariff.id.label('tariff_id'),
            Tariff.name.label('tariff_name'),
            Land.code.label('land_code')
        )
                 .join(Farmer, Farmer.id == BillingLand.owner_farmer_id)
                 .join(Rent, Rent.id == BillingLand.invoice_farmer_id)
                 .join(Block, Block.id == BillingLand.block_id)
                 .join(Tariff, Tariff.id == BillingLand.tariff_id)
                 .join(Land, Land.id == BillingLand.land_id)
                 .filter(BillingLand.invoice_detail_id.in_(q_invoice_detail_id)).all()
                 )
        lands = [ o._asdict() for o in lands]
        tariffs = tariff_logic.default.search().all()
        new_lands = []
        if lands:
            for land in lands:
                o = ExtDictObj(land)
                setattr(o,'land_status_id','irrigate')
                total_area += o.land_area
                for tariff in tariffs:
                    if tariff.id == o.tariff_id:
                        setattr(LandVerifiedTabel, o.tariff_id,
                                LamdaColumn(o.tariff_name, render_kw={"tariff_id": o.tariff_id, 'width': '10%'},
                                            get_value=lambda
                                                x: u'<input type="checkbox" tariff_id={tariff_id}>'.format(
                                                tariff_id=o.tariff_id)))

                    else:
                        setattr(LandVerifiedTabel, tariff.id,
                                LamdaColumn(tariff.name, render_kw={"tariff_id": tariff.id, 'width': '10%'},
                                            get_value=lambda
                                                x: u'<input type="checkbox" tariff_id={tariff_id}>'.format(
                                                tariff_id=tariff.id)))

                new_lands.append(o)

            def convert(value):
                try:
                    v = int(value)
                except:
                    v = 0
                return v

            newlist = sorted(new_lands, key=lambda x: '{0:4d}'.format(convert(x.land_code)))
            table = LandVerifiedTabel(data=newlist)
            before = '<div style="page-break-before: always;"></div>'
            farmer = farmer_logic.default.find(obj.farmer_id)
            try:
                content = render_template_string(content_template.content_template,
                                                 contract_number=contract_number,
                                                 billing=billing,
                                                 block=block_logic.default.find(obj.block_id),
                                                 farmer=farmer,
                                                 location=farmer.location,
                                                 land=dict(table=table),
                                                 total_area=total_area,
                                                 before=before
                                                 )
                return render_template("invoice/contract_content.html", content=content)
            except:
                return ''

    @route('/invoice_content')
    def invoice_content(self):
        id = request.args.get('id') or ''
        back_url = request.args.get('back_url') or ''
        obj = self.v_logic.preview(id=id)
        template = self.v_logic.find_template(is_active=True)
        path = 'invoice/previews/'
        if template.id == '2' or template.id == '3':
            return render_template(path + 'template2.html', invoice=obj, back_url=back_url)
        else:
            return render_template(path + template.value, invoice=obj, back_url=back_url)

    @route('/preview_pdf/<billing_id>/<block_id>/<village_id>', methods=['GET', 'POST'])
    def preview_pdf(self, billing_id, block_id, village_id):
        obj = invoice_logic.default.preview_to_pdf(billing_id, block_id, village_id)
        for o in obj:
            for detail in o.details:
                try:
                    detail.billing_lands.sort(key=lambda x: int(x.land.code))
                except Exception as e:
                    continue

        template = invoice_logic.default.find_template(is_active=True)
        template = 'invoice/pdf_templates/' + template.value
        paths = []
        url = None
        start = 0
        if len(obj) > 100:
            from PyPDF2 import PdfFileMerger

            merger = PdfFileMerger()
            lists = self.chunks(obj, 50)

            for list in lists:
                data = render_template(template, objects=list, start=start)
                path = self.export_pdf(data=data)
                merger.append(path['path'] + '\\' + path['name'])
                paths.append(path['path'] + '\\' + path['name'])
                start += 50
            merger.write(path['path'] + '\\' + path['name'].split('.pdf')[0] + '_result.pdf')
            merger.close()
            url = request.url_root[:-1] + url_for('download_file',
                                                  filename=path['name'].split('.pdf')[0] + '_result.pdf')
        else:
            data = render_template(template, objects=obj, start=start)
            path = self.export_pdf(data)
            url = request.url_root[:-1] + url_for('download_file',
                                                  filename=path['name'])
        # remove paths
        if paths:
            for path in paths:
                import os
                os.remove(path)
        return url

    def export_pdf(self, data):
        import os
        current_dir = os.path.dirname(os.path.realpath(__file__))
        target_dir = os.sep.join(current_dir.split(os.sep)[0:-2])
        css = os.sep.join([target_dir, 'share', 'static', 'styles', 'export.css'])
        output_path = os.path.join(app.config.get('SHARE_IMAGES'))
        # output_path = 'D:\\app'
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        # export pdf

        path = export_pdf(input=data, css=css, output_path=output_path)
        return path

    def chunks(self, l, n):
        """Yield successive n-sized chunks from l."""
        lists = []
        for i in range(0, len(l), n):
            lists.append(l[i:i + n])
        return lists

    @route("/find_invoice_by_farmer_id")
    def find_invoice_by_farmer_id(self):
        farmer_id = request.args.get('farmer_id')
        invoices = invoice_logic.default.find_invoice_by_farmer_id(farmer_id)
        return json.dumps(invoices)

    @route("/update_template/<id>")
    def update_template(self, id):
        if id:
            update = invoice_logic.default.update_template(id=id)
            return update
        return 0

    @route("/get-receipts/<invoice_id>")
    def get_receipts(self, invoice_id):
        return invoice_logic.default.get_receipts(invoice_id)


InvoiceView.register(app)
