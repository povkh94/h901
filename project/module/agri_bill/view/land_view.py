#!/usr/bin/env python
# -*- coding: utf-8 -*-
from random import randint

from flask_babel import lazy_gettext as _

from project.module.agri_bill.logic import verify_billing_logic
from project.module.system.logic import lookup_logic
from ...share.view.base_view import *
from ..model import *
from ..logic import farmer_logic
from ..logic import land_logic
from ..logic import block_logic
import sys

reload(sys)
sys.setdefaultencoding("utf-8")


class HiddenForm(FlaskForm):
    id = HiddenField()
    is_remove = HiddenField()

def lamda_select2_rent(row):
    if not row.rent or not row.rent.is_active:
        row.rent_id = row.farmer_id
    try:
        class FormDummy(FlaskForm):
            id = HiddenField("Id")

        rnd = randint(100, 200000000)
        field_name = 'rent_id_%s' % rnd
        setattr(FormDummy, field_name,
                Select2Field(
                    label=_(u'Rent For'),
                    remote_url='/farmer/suggestion',
                    allow_blank=True,
                    blank_text=_(u'Not Rent'),
                    min_input_length=0,
                    text_factory=lambda x: farmer_logic.default.find(x).farmer,
                    multiple=False
                ))
        form = FormDummy()
        getattr(form, field_name).data = row.rent_id
        return render_template_string('{{ form.rent_id_%s }}' % rnd, form=form)
    except Exception as e:
        pass


def lamda_select2_farmer(row):
    try:
        class FormDummy(FlaskForm):
            id = HiddenField("Id")

        rnd = randint(100, 200000000)
        field_name = 'farmer_id_%s' % rnd
        setattr(FormDummy, field_name,
                Select2Field(
                    label=_(u'Owner'),
                    remote_url='/farmer/suggestion',
                    allow_blank=True,
                    blank_text=_(u'Owner'),
                    min_input_length=0,
                    text_factory=lambda x: farmer_logic.default.find(x).farmer,
                    multiple=False,
                ))
        form = FormDummy()
        getattr(form, field_name).data = row.farmer_id
        return render_template_string('{{ form.farmer_id_%s }}' % rnd, form=form)
    except Exception as e:
        pass


def lamda_land_note(row):
    class FormLandNote(FlaskForm):
        id = HiddenField("Id")

    rnd = randint(100, 2000000)
    field_name = 'note_%s' % rnd
    setattr(FormLandNote, field_name,
            TextField(
                _(u'Note'),
                render_kw={
                    'data-val': 'true',
                    'class': 'form-control'
                }
            ))
    form = FormLandNote()
    getattr(form, field_name).data = row.note
    return render_template_string('{{ form.note_%s }}' % rnd, form=form)


def lamda_land_area(row):
    class FormLandArea(FlaskForm):
        id = HiddenField("Id")

    rnd = randint(100, 2000000)
    field_name = 'land_area_%s' % rnd
    setattr(FormLandArea, field_name,
            TextField(
                _(u'Area(Are)'),
                render_kw={
                    'data-val': 'true',
                    'data-val-required': _(u'Input Required'),
                    'required': 'required',
                    'class': 'form-control'
                }
            ))
    form = FormLandArea()
    getattr(form, field_name).data = '{0:.2F}'.format(row.area)
    return render_template_string('{{ form.land_area_%s }}' % rnd, form=form)


def lamda_block(row):
    class FormBlock(FlaskForm):
        id = HiddenField("Id")

    if not row.block_id:
        row.block_id = 'none'
    rnd = randint(100, 2000000)
    field_name = 'block_id_%s' % rnd
    setattr(FormBlock, field_name,
            DynamicSelectField(
                _(u'Block'),
                query_factory=block_logic.default.search,
                get_pk='id',
                get_label='name',
                render_kw={
                    'class': 'form-control',
                    'data-val': 'true',
                    'data-val-required': _(u'Input Required'),
                    'required': 'required'
                }
            )
            )
    form = FormBlock()
    getattr(form, field_name).data = row.block_id
    return render_template_string('{{ form.block_id_%s }}' % rnd, form=form)


def lamda_tariff(row):
    class FormTariff(FlaskForm):
        id = HiddenField("Id")

    if row.tariff and not row.tariff.is_active:
        row.tariff_id = 'none'
    rnd = randint(100, 2000000)
    from ..logic import tariff_logic
    field_name = 'tariff_id_%s' % rnd
    setattr(FormTariff, field_name,
            DynamicSelectField(
                _(u'Tariff'),
                query_factory=tariff_logic.default.search,
                get_pk='id',
                get_label='name',
                render_kw={
                    'data-val': 'true',
                    'data-val-required': _(u'Input Required'),
                    'required': 'required',
                    'class': 'form-control'
                }
            ))
    form = FormTariff()
    getattr(form, field_name).data = row.tariff_id
    return render_template_string('{{ form.tariff_id_%s }}' % rnd, form=form)


def lamda_land_status(row):
    class FormStatus(FlaskForm):
        id = HiddenField("Id")

    if not row.status_id:
        row.status_id = 'none'
    rnd = randint(100, 2000000)
    field_name = 'land_status_%s' % rnd
    setattr(FormStatus, field_name,
            DynamicSelectField(
                _(u'Status'),
                query=lookup_logic.default.get_values('l_status'),
                get_pk="id",
                render_kw={
                    'data-val': 'true',
                    'data-val-required': _(u'Input Required'),
                    'required': 'required',
                    'class': 'form-control'
                }
            ))
    form = FormStatus()
    getattr(form, field_name).data = row.status_id
    return render_template_string('{{ form.land_status_%s }}' % rnd, form=form)


def lamda_hidden(row):
    class FormLandArea(FlaskForm):
        id = HiddenField("Id")

    rnd = randint(100, 2000000)
    field_name = 'hidden_id_%s' % rnd
    setattr(FormLandArea, field_name,
            # TextField(
            #     _(u'Area(Are)'),
            #     render_kw={
            #         'data-val': 'true',
            #         'data-val-required': _(u'Input Required'),
            #         'required': 'required',
            #         'class': 'form-control'
            #     }
            # )
            HiddenField()
            )
    form = FormLandArea()
    getattr(form, field_name).data = row.id
    return render_template_string('{{ form.hidden_id_%s }}' % rnd, form=form)


def lambda_rent(row, type='rent'):
    try:
        if type == 'rent':
            if row.rent:
                return row.rent.name + ' - {code}'.format(code=row.rent.code) + '{spouse_name}'.format(
                    spouse_name=' - ' + row.rent.spouse_name if row.rent.spouse_name else '')
            else:
                return row.farmer.name + ' - {code}'.format(
                    code=row.farmer.code) + '{spouse_name}'.format(
                    spouse_name=' - ' + row.farmer.spouse_name if row.farmer.spouse_name else '')
        else:
            return row.farmer.name + ' - {code}'.format(
                code=row.farmer.code) + '{spouse_name}'.format(
                spouse_name=' - ' + row.farmer.spouse_name if row.farmer.spouse_name else '')
    except:
        print row.id

class EditForm(FlaskForm):
    land_ids = HiddenField()
    farmer_id = Select2Field(
        label=_(u'Farmer'),
        remote_url='/farmer/suggestion',
        min_input_length=0,
        blank_text=_('All Farmers'),
        text_factory=lambda x: farmer_logic.default.find(x),
        multiple=False
    )
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.search,
        get_pk='id',
        get_label='filter_name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    rent_id = Select2Field(
        label=_(u'Rent For'),
        remote_url='/farmer/suggestion',
        allow_blank=True,
        blank_text=_(u'Not Rent'),
        min_input_length=0,
        text_factory=lambda x: farmer_logic.default.find(x),
        multiple=False
    )

    status_id = DynamicSelectField(
        _(u'Land Status'),
        allow_blank=True,
        blank_text=_(u"All Land Status"),
        query=lookup_logic.default.get_values('l_status'),
        get_pk="id",
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )

    from ..logic import tariff_logic
    tariff = DynamicSelectField(
        _(u'Tariff'),
        query_factory=tariff_logic.default.search,
        get_pk='id',
        get_label='name',
        blank_text=_(u'All Tariff'),
        allow_blank=True,
        render_kw={
            'data-val': 'true',
        }
    )

    active = DynamicSelectField(
        _(u'Active'),
        blank_text=_(u"All Status"),
        allow_blank=True,
        query=lookup_logic.default.get_values('activate'),
        get_pk="value",
        default='active',
        render_kw={
            'data-val': 'true',
        }
    )
    note = TextAreaField(
        _(u'Note')
    )


class LandForm(FlaskForm):
    id = HiddenField()
    status_id = DynamicSelectField(
        _(u'Land Status'),
        query=lookup_logic.default.get_values('l_status'),
        get_pk="id",
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    code = TextField(
        _(u'Land Code'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off'
        }
    )
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.search,
        get_pk='id',
        get_label='filter_name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    from ..logic import tariff_logic
    tariff_id = DynamicSelectField(
        _(u'Tariff'),
        query_factory=tariff_logic.default.search,
        get_pk='id',
        get_label='name',
        blank_text=_('All Tariff'),
        allow_blank=True,
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    farmer_id = Select2Field(
        label=_(u'Farmer'),
        remote_url='/farmer/suggestion',
        min_input_length=0,
        blank_text=_('All Farmers'),
        text_factory=lambda x: farmer_logic.default.find(x),
        multiple=False
    )
    rent_id = Select2Field(
        label=_(u'Rent For'),
        remote_url='/farmer/suggestion',
        allow_blank=True,
        blank_text=_(u'Not Rent'),
        min_input_length=0,
        text_factory=lambda x: farmer_logic.default.find(x),
        multiple=False
    )
    area = TextField(
        _(u'Area(Are)'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required',
            'autocomplete': 'off',
            'addon': _('Are')
        }
    )
    active = DynamicSelectField(
        _(u'Active'),
        query=lookup_logic.default.get_values('activate'),
        get_pk="value",
        default='active',
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )
    note = TextAreaField(_('Note'))

class LandFilterForm(FilterForm):
    block_id = DynamicSelectField(
        _(u'Block'),
        query_factory=block_logic.default.filter_block_active,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_('All Blocks'),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )
    from ..logic import tariff_logic
    tariff_id = DynamicSelectField(
        _(u'Tariff'),
        query_factory=tariff_logic.default.search,
        get_pk='id',
        get_label='name',
        allow_blank=True,
        blank_text=_("All Tariff"),
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required'),
            'required': 'required'
        }
    )

    status_id = DynamicSelectField(
        _(u'Land Status'),
        allow_blank=True,
        blank_text=_(u"All Land Status"),
        query=lookup_logic.default.get_values('l_status'),
        get_pk="id",
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )

    active = DynamicSelectField(
        _(u'Active'),
        query=lookup_logic.default.get_values('activate'),
        get_pk="value",
        default='active',
        render_kw={
            'data-val': 'true',
            'data-val-required': _(u'Input Required')
        }
    )


class LandTable(Table):
    rowno = RowNumberColumn(_(u'No.'))
    # id = LamdaColumn('', get_value=lambda row:lamda_hidden(row))
    code = LinkColumn(_(u'Land Code'), get_url=lambda row: url_for('.LandView:detail', id=row.id),
                      get_value=lambda row: row.code)
    block = LamdaColumn(_(u'Block'), get_value=lambda row: lamda_block(row), render_kw={'width': '7%'})
    farmer = LamdaColumn(_(u'Owner'), get_value=lambda row: lamda_select2_farmer(row))
    rent_id = LamdaColumn(_(u'Rent For'), get_value=lambda row: lamda_select2_rent(row))
    area = LamdaColumn(_(u'Area(Are)'), get_value=lambda row: lamda_land_area(row), render_kw={
        'width': '7%',
        'data-t': 'n',
        'data-a-h': 'left'
    })
    land_status_value = LamdaColumn(_(u'Land Status'), get_value=lambda row: lamda_land_status(row),
                                    render_kw={'width': '150px'})

    tariff = LamdaColumn(_(u'Tariff'), get_value=lambda row: lamda_tariff(row), render_kw={'width': '150px'})
    # note = DataColumn(_(u'Note'))
    note = LamdaColumn(_(u'Note'), get_value=lambda row: lamda_land_note(row), render_kw={
        'width': '16%',
        'data-t': 'n',
        'data-a-h': 'left'
    })
    action = LamdaColumn('',
                         get_value=lambda
                             row: u'<div class="btn-group btn-group-sm pull-right" style="display:flex;">'
                                  u'<a href="{url_edit}" class="action btn btn-default btn-sm edit" style="height: 30px;">'
                                  u'<span class="glyphicon glyphicon-pencil" style="font-family:Glyphicons Halflings !important;"></span>'
                                  u'</a>'
                                  u'<a href="{url_remove}" class="action btn btn-default btn-sm edit" style="height: 30px;">'
                                  u'<span class="glyphicon glyphicon-trash" style="font-family:Glyphicons Halflings !important;"></span>'
                                  u'</a>'
                                  u' <div class="checkbox" style="border: 1px solid #d3dbe2;border-left:none;margin-top: 0px;">'
                                  u'<label style="font-size:1em;">'
                                  u'<input class="check" type="checkbox" id="{id}">'
                                  u'<span class="cr" style="border: 1px solid #0078bd ;margin-left: 10px;margin-top:5px">' u'<i class="cr-icon glyphicon glyphicon-ok"></i></span>'
                                  u'</label>'
                                  u'</div>'
                                  u'</div>'
                         .format(
                             url_edit=url_for(".LandView:edit", id=row.id),
                             url_remove=url_for(".LandView:remove", id=row.id),
                             id=row.id
                         ),
                         render_kw={
                             'class': 'no-export'
                         }
                         )



class LandView(CRUDView):
    def _on_initializing(self, *args, **kwargs):
        self.v_class = Land
        self.v_logic = land_logic.default
        self.v_table_index_class = LandTable
        self.v_form_add_class = LandForm
        self.v_form_filter_class = LandFilterForm
        self.v_template = 'land'
        self.v_logic_order_by = ''

    def _on_initialized(self, *args, **kwargs):
        self.v_data['title'] = _(u'Land')

    def _on_index_rendering(self, *args, **kwargs):
        self.v_data['edit_form'] = EditForm()

    def _on_add_rendering(self, *args, **kwargs):
        form = self.v_data['form']
        form.status_id.data = 'irrigate'

    def _on_edit_rendering(self, *args, **kwargs):
        form = self.v_data['form']
        rent_id = form.rent_id.data
        if not rent_id or rent_id == '__None' or rent_id == 'None':
            form.rent_id.data = form.farmer_id.data
        if not form.tariff_id.data:
            form.tariff_id.data = 'none'
        if not form.status_id.data:
            form.status_id.data = 'none'

    def _on_detail_rendering(self, *args, **kwargs):
        # form = self.v_data['form']
        # rent_id = form.rent_id.data
        # if not rent_id or rent_id == '__None' or rent_id == 'None':
        #     form.rent_id.data = form.farmer_id.data
        form = self.v_data['form']
        obj = land_logic.default.find(form.id.data)

        self.v_data['invoices'] = land_logic.default.find_invoice_by_land(form.id.data)


    def _on_remove_rendering(self, *args, **kwargs):
        id = self.v_data['form'].id.data
        land = land_logic.default.find(id)
        self.v_data['is_active'] = land.is_active
        # self.v_data['canremove'] = False
        self.v_data['canremove'] = land_logic.default.check_land_have_invoice(id)

    @route("/update", methods=['GET', 'POST'])
    def update(self):
        from ..logic import land_logic
        try:
            id = request.args.get('id') or None
            status_id = request.args.get('status_id') or None
            tariff_id = request.args.get('tariff_id') or None
            rent_id = request.args.get('rent_id') or None
            block_id = request.args.get('block_id') or None
            farmer_id = request.args.get('farmer_id') or None
            note = request.args.get('note') or None
            update = 'No Record Change'
            if id:
                if rent_id:
                    update = land_logic.default.update_land(id=id, rent_id=rent_id)
                if status_id:
                    update = land_logic.default.update_land(id=id, status_id=status_id)
                if tariff_id:
                    update = land_logic.default.update_land(id=id, tariff_id=tariff_id)
                if farmer_id:
                    update = land_logic.default.update_land(id=id, farmer_id=farmer_id)
                if block_id:
                    update = land_logic.default.update_land(id=id, block_id=block_id)
                if note:
                    update = land_logic.default.update_land(id=id,note=note)

            if request.method == "POST":
                data = request.get_json()
                billing_id = data.get('billing_id')
                for item in data['lands']:
                    land = land_logic.default.find(id=item.get("id"))
                    if land:
                        if land.rent_id != item['rent_id']:
                            update = land_logic.default.update_land(id=land.id, rent_id=item['rent_id'])
                        if land.area != to_decimal(item['area']):
                            update = land_logic.default.update_land(id=land.id, area=item['area'])

                        if land.status_id != item['status_id']:
                            update = land_logic.default.update_land(id=land.id, status_id=item['status_id'])

                        if land.tariff_id != item['tariff_id']:
                            update = land_logic.default.update_land(id=land.id, tariff_id=item['tariff_id'])
                        if land.farmer_id != item['farmer_id']:
                            update = land_logic.default.update_land(id=land.id, farmer_id=item['farmer_id'])
                        if land.block_id != item['block_id']:
                            update = land_logic.default.update_land(id=land.id, block_id=item['block_id'])
                        if land.note != item.get('note') and item.get('note'):
                            update = land_logic.default.update_land(id=land.id, note=item.get('note'))
                        verify_billing_logic.default.update_verify_billing_land(land, billing_id)

            return update
        except Exception as e:
            return e.message

    @route('/suggestion')
    def suggestion(self):
        term = request.args.get('term', '')
        limit = request.args.get('limit', 50)
        suggests = land_logic.default.find_lands(search=term)
        suggests = suggests.limit(limit)
        results = [dict(
            id=s.id,
            text=s.code,
            obj=s
        ) for s in suggests]
        data = {'results': results}
        return json.dumps(data)

    @route('/sum_area', methods=['GET', 'POST'])
    def sum_area(self):
        import json
        land_ids = json.loads(request.data)
        if land_ids:
            data = land_logic.default.sum_land_area(land_ids=land_ids)
            return data
        return 0

    @route('/update-all-lands', methods=['POST', 'GET'])
    def update_all_lands(self):
        form = EditForm()
        if form.validate_on_submit():
            ids = json.loads(form.land_ids.data)
            farmer_id = form.farmer_id.data
            rent_id = form.rent_id.data
            status_id = form.status_id.data
            tariff_id = form.tariff.data
            active = form.active.data
            block_id = form.block_id.data
            note = form.note.data
            data = {
                'farmer_id': farmer_id,
                'rent_id': rent_id,
                'block_id': block_id,
                'status_id': status_id,
                'tariff_id': tariff_id,
                'active': active,
                'note': note
            }
            if form.active.data == "disactive":
                for id in ids:
                    res = land_logic.default.check_land_have_invoice(id)
                    if res == False:
                        return render_template('land/remove-all.html', view={"canremove": "False"})

            message = self.v_logic.update_all_lands(ids, data)
            return redirect(url_for(".LandView:index"))

    @route('/remove-all-lands', methods=['GET', 'POST'])
    def remove_all_lands(self):
        ids = request.args.get('id') or None
        form = HiddenForm()
        if form.validate_on_submit():
            is_remove = form.is_remove.data
            data = form.id.data.replace("&#34;", "")
            ids = data.replace("[", "").replace("]", "").split(",")
            res = land_logic.default.is_remove_all(ids, is_remove)
            if res =="success":
                return redirect(url_for(".LandView:index"))
            else:
                return render_template('land/remove-all.html',view={"canremove":"False"})
                # return redirect(url_for(".LandView:remove",))

        return render_template('land/remove-all.html', form=form, ids=ids, is_active=True)

    @route('/export', methods=['POST'])
    def export(self):
        kwargs = request.form.to_dict()

        for k in kwargs:
            if kwargs[k] == '__None':
                kwargs[k] = None

        q = land_logic.default.search(**kwargs)
        data = DataView(query=q)
        table = LandTable(data=data.result)

        columns = [k for k in getattr(table, '_fields', []) if
                   ('_' not in k and k not in ['rowno', 'action']) or k == 'land_status_value']
        mydic = []

        for land in table.data:
            obj = {}
            for c in columns:
                obj[c] = unicode(getattr(land, c))
                if c == 'farmer':
                    obj[c] = unicode(' ' + lambda_rent(row=land, type='farmer'))
                    continue
                if c == 'rent':
                    obj[c] = unicode(' ' + lambda_rent(row=land))
                    continue

            mydic.append(obj)

        output_path = os.path.join(app.config.get('SHARE_IMAGES'))
        import pyexcel
        default_output_type = '.xlsx'
        default_output_name = 'land' + (str(datetime.today()).replace('-', '')
                                        .replace(':', '')
                                        .replace(' ', '_')
                                        .replace('.', '_'))
        output_name = default_output_name + default_output_type
        dest_file = os.sep.join([output_path, output_name])
        output = os.path.join(output_path, output_name) if output_path else None
        try:
            os.remove(output)
        except Exception as e:
            pass
        pyexcel.save_as(records=mydic, dest_file_name=dest_file)

        url = request.url_root[:-1] + url_for('download_file',
                                              filename=output_name)
        return url

    @route('/edit/<id>', methods=['GET', 'POST'])
    def edit(self, id=0):
        self.v_data['obj'] = self.v_logic.find(id)
        form = self._get_form(name='edit')(request.form, obj=self.v_data['obj'])
        if request.method == 'POST' and form.validate():
            self.v_data['obj'] = from_dict(form.data, to_cls=self.v_class, defaults={})
            try:
                check = self.v_logic.update(self.v_data['obj'])
                if check == False:
                    return render_template('land/remove-all.html', view={"canremove": "False"})
                return redirect(url_for(".LandView:index"))
            except LogicError, e:
                db.rollback()
                if e.field in form:
                    form[e.field].errors.append(str(e))
        self.v_data['form'] = form

        self.v_data['return_url'] = ''

        # add breadcrumb to view object
        self.v_data['breadcrumb'] = [
            {'url': url_for(self.v_data['endpoints']['index']), 'title': self.v_data['title']},
            {'url': url_for(self.v_data['endpoints']['detail'], id=form.id.data),
             'title': self.v_data['obj'].code},
            {'url': '', 'title': _('Edit')}
        ]
        return self.render_template(self.v_templates.get('edit'))


    @route('/add', methods=['GET', 'POST'])
    def add(self):
        self.v_data['modal'] = bool(request.args.get('x-ajax-modal'))
        self.v_data['obj'] = self.v_logic.new()
        form = self._get_form(name='add')(obj=self.v_data['obj'])
        self.v_data['form'] = form

        if form.validate_on_submit():
            obj = from_dict(form.data, to_cls=self.v_class, ignores=['id'])
            # obj = from_dict(form.data, to_cls=self.v_class)
            try:
                self.v_logic.add(obj)
                return redirect(url_for(".LandView:index"))
            except LogicError, e:
                db.rollback()
                if e.field in form:
                    form[e.field].errors.append(str(e))
        self.v_data['breadcrumb'] = [
            {'url': url_for(self.v_data['endpoints']['index']), 'title': self.v_data['title']},
            {'url': '', 'title': _('Add')}
        ]
        self.v_data['breadcrumb'] = self.v_breadcrum + self.v_data['breadcrumb']
        return self.render_template(self.v_templates.get('add'))


LandView.register(app)
