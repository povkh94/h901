from project.module.agri_bill import enum
from flask_babel import gettext as _
from project.module.agri_bill.logic import component_logic
from project.module.system_security.model import Component
from ...share.view.base_view import *
from project.module.share.reports import SystemReportCenter, ReportGroup
from ..view import report_view_init

def customized_groups():
    gcs = component_logic.default.search(component_type=enum.component_type.report_group,
                                         scope=current.company().id).all()
    groups = []
    for gc in gcs:
        group = ReportGroup(name=gc.name, title=gc.title)
        rcs = component_logic.default.search(component_type=enum.component_type.report,
                                             scope=current.company().id).filter(Component.parent_id == str(gc.id)).all()
        for rc in rcs:
            group.register_report(rc.component)
        groups.append(group)
    return groups


class ReportView(CRUDView):
    @route('/center', methods=['GET', 'POST'])
    def report(self):
        tmp = []
        tmp.extend(SystemReportCenter.center.groups)
        tmp.extend(customized_groups())
        view = {"title": _(u'Reports')}
        return render_template(
            'reports/report.html',
            title=_(u'Reports'),
            groups=tmp,
            layout=session.get('layout', 'breadcrumb'),
            view=view
        )

    @route('/<report_name>', methods=['GET', 'POST'])
    def index(self, report_name):

        report = SystemReportCenter.reports.get(report_name)
        component = component_logic.default.find(name=report_name)
        if not report:
            report = component.component

        if component and hasattr(report, 'templates'):
            more_templates = [('c:%s' % c.id, c.title) for c in
                              component_logic.default.search(component_type=enum.component_type.report_template,
                                                             parent_id=str(component.id)).all()]
            if more_templates:
                if not hasattr(report, 'default_templates'):
                    report.default_templates = report.templates
                report.templates = report.default_templates + more_templates

        return report.render_index()

    @route('/<report_name>/result', methods=['GET', 'POST'])
    def result(self, report_name):
        report = SystemReportCenter.reports.get(report_name)
        component = component_logic.default.find(name=report_name)
        if not report:
            report = component.component

        if component and hasattr(report, 'templates'):
            more_templates = [('c:%s' % c.id, c.title) for c in
                              component_logic.default.search(component_type=enum.component_type.report_template,
                                                             parent_id=str(component.id)).all()]
            if more_templates:
                if not hasattr(report, 'default_templates'):
                    report.default_templates = report.templates
                report.templates = report.default_templates + more_templates

        return report.render_result()

    @route('/report_template', methods=['GET', 'POST'])
    def report_template(self):
        return render_template(
            'reports/purchases/template2.html',
            layout=session.get('layout', 'breadcrumb'),
            view={}
        )


ReportView.register(app)