from sqlalchemy.orm import object_session
from sqlalchemy import select, func

from ..share.model import *
from ..system.model import *


class Station(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_station'
    id = c.PkColumn()
    company_id = c.FkColumn()
    code = c.CodeColumn()
    name = c.TitleColumn()
    note = c.NoteColumn()
    company = relationship('Company', primaryjoin='foreign(Station.company_id) == remote(Company.id)')
    children = relationship('Block',
                            primaryjoin='and_(foreign(Station.id) == remote(Block.station_id),Block.is_active == True)',
                            uselist=True, order_by='Block.name')


class Block(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_block'
    id = c.PkColumn()
    company_id = c.FkColumn()
    gm_block = c.TitleColumn()
    code = c.CodeColumn()
    name = c.TitleColumn()
    note = c.NoteColumn()
    station_id = c.FkColumn()
    company = relationship('Company', primaryjoin='foreign(Block.company_id) == remote(Company.id)')
    station = relationship('Station', primaryjoin='foreign(Block.station_id) == remote(Station.id)')

    @property
    def filter_name(self):
        return u'{station_name} - {block_name}'.format(station_name=self.station.name, block_name=self.name)



class Land(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_land'
    id = c.PkColumn()
    farmer_id = c.FkColumn()
    block_id = c.FkColumn()
    tariff_id = c.FkColumn()
    code = c.CodeColumn()
    note = c.NoteColumn()
    area = c.DecimalColumn()
    rent_id = c.FkColumn()
    status_id = c.FkColumn()
    block = relationship('Block', primaryjoin='foreign(Land.block_id) == remote(Block.id)')
    farmer = relationship('Farmer', primaryjoin='foreign(Land.farmer_id) == remote(Farmer.id)')
    rent = relationship('Farmer', primaryjoin='foreign(Land.rent_id) == remote(Farmer.id)')
    land_status_value = relationship('LookupValue',
                                     primaryjoin='and_(foreign(Land.status_id) == remote(LookupValue.id), remote(LookupValue.lookup_id == "l_status"))')
    tariff = relationship('Tariff', primaryjoin='foreign(Land.tariff_id) == remote(Tariff.id)')


# class LandHistory(Base, TrackMixin, ExtMixin):
#     __tablename__ = 'agri_land_history'
#     id = c.PkColumn()
#     history_date = c.DateTimeColumn()
#     history_title = c.TitleColumn()
#     land_id = c.FkColumn()
#     farmer_id = c.FkColumn()
#     block_id = c.FkColumn()
#     code = c.CodeColumn()
#     note = c.NoteColumn()
#     area = c.DecimalColumn()
#     used = c.BooleanColumn()
#     block = relationship('Block', primaryjoin='foreign(LandHistory.block_id) == remote(Block.id)')
#     farmer = relationship('Farmer', primaryjoin='foreign(LandHistory.farmer_id) == remote(Farmer.id)')


class Farmer(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_farmer'
    __table_args__ = dict(extend_existing=True)
    id = c.PkColumn()
    company_id = c.FkColumn()
    code = c.CodeColumn()
    name = c.TitleColumn()
    sex = c.TitleColumn()
    marital_status = c.TitleColumn()
    spouse_name = c.TitleColumn()
    phone = c.TitleColumn()
    address = c.NoteColumn()
    village_id = c.FkColumn()
    national_id = c.TitleColumn()
    delivery_code = c.CodeColumn()
    location = relationship('Location',
                           primaryjoin='foreign(Farmer.village_id) == remote(Location.id)')

    @property
    def village(self):
        return u'{p} {n}'.format(p=self.location.prefix,n=self.location.name)

    @property
    def commune(self):
        return object_session(self). \
            scalar(
            select([Location.prefix +" "+ Location.name]). \
                where(Location.id == self.location.parent_id)
        )

class Verify(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_billing_verify'
    id = c.PkColumn()
    billing_id = c.FkColumn()
    station_id = c.FkColumn()
    block_id = c.FkColumn()
    farmer_id = c.FkColumn()
    status = c.FkColumn()


class Location(Base, TrackMixin, ExtMixin):
    __tablename__ = 'com_location'
    id = c.PkColumn()  # 10-digit code
    type = c.FkColumn()  # '0'=country', '1'=province,'2'=district/krong,'3'=commune/sangkat','4'=village ,'5'=group/block
    name = c.TitleColumn()
    latin_name = c.TitleColumn()
    prefix = c.TitleColumn()
    latin_prefix = c.TitleColumn()
    parent_id = c.FkColumn()



    @property
    def title(self):
        return '{p} {n}'.format(p=self.prefix, n=self.name)

    @property
    def latin_title(self):
        return '{n} {p}'.format(p=self.latin_prefix, n=self.latin_name)




class Season(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_season'
    id = c.PkColumn()
    company_id = c.FkColumn()
    name = c.TitleColumn()
    note = c.NoteColumn()
    company = relationship('Company', primaryjoin='foreign(Season.company_id) == remote(Company.id)')

class BillingCycle(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_billing_cycle'
    id = c.PkColumn()
    company_id = c.FkColumn()
    name = c.TitleColumn()
    note = c.NoteColumn()
    season_id = c.FkColumn()
    season = relationship('Season', primaryjoin='foreign(BillingCycle.season_id) == remote(Season.id)')


class BillingCycleBlock(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_billing_cycle_block'
    id = c.PkColumn()
    billing_cycle_id = c.FkColumn()
    block_id = c.FkColumn()


class Billing(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_billing'
    id = c.PkColumn()
    company_id = c.FkColumn()
    season_id = c.FkColumn()
    billing_cycle_id = c.FkColumn()
    name = c.TitleColumn()
    start_date = c.DateTimeColumn()
    end_date = c.DateTimeColumn()
    billing_date = c.DateTimeColumn()
    billing_status_id = c.FkColumn()
    contract_template_id = c.FkColumn()
    season = relationship('Season', primaryjoin='foreign(Billing.season_id) == remote(Season.id)')
    billing_cycle = relationship('BillingCycle',
                                 primaryjoin='foreign(Billing.billing_cycle_id) == remote(BillingCycle.id)')

    billing_status_value = relationship('LookupValue',
                                        primaryjoin='and_(foreign(Billing.billing_status_id) == remote(LookupValue.id), remote(LookupValue.lookup_id == "b_status"))')

    billing_station = relationship('BillingStation',
                                   primaryjoin='foreign(Billing.id) == remote(BillingStation.billing_id)',
                                   uselist=True, viewonly=True)

    @property
    def billing_station_json(self):
        return json.dumps({'billing_station': [to_dict(d, defaults={'ext_data': d.ext}) for d in
                                               self.billing_station
                                               if d.is_active]})
class BillingStation(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_billing_station'
    id = c.PkColumn()
    billing_id = c.FkColumn()
    station_id = c.FkColumn()
    tariff_id = c.FkColumn()
    total_farmer = c.DecimalColumn()
    total_land = c.DecimalColumn()
    total_area = c.DecimalColumn()
    total_cost = c.DecimalColumn()
    unit_cost = c.DecimalColumn()
    station = relationship('Station',
                           primaryjoin='foreign(Station.id) == remote(BillingStation.station_id)')
    tariff = relationship('Tariff',
                          primaryjoin='foreign(Tariff.id) == remote(BillingStation.tariff_id)')


class BillingLand(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_billing_land'
    id = c.PkColumn()
    billing_id = c.FkColumn()
    land_id = c.FkColumn()
    block_id = c.FkColumn()
    station_id = c.FkColumn()
    invoice_farmer_id = c.FkColumn()
    owner_farmer_id = c.FkColumn()
    tariff_id = c.FkColumn()
    area = c.DecimalColumn()
    unit_cost = c.DecimalColumn()
    total_cost = c.DecimalColumn()
    invoice_detail_id = c.FkColumn()

    invoice_detail = relationship('InvoiceDetail',
                                  primaryjoin='foreign(InvoiceDetail.id) == remote(BillingLand.invoice_detail_id)')

    station = relationship('Station',
                           primaryjoin='foreign(BillingLand.station_id) == remote(Station.id)')
    land = relationship('Land',
                        primaryjoin='foreign(BillingLand.land_id) == remote(Land.id)',
                        )


class InvoiceDetail(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_invoice_detail'
    id = c.PkColumn()
    invoice_id = c.FkColumn()
    item_id = c.FkColumn()
    item_name = c.TitleColumn()
    tariff_id = c.FkColumn()
    total_land = c.DecimalColumn()
    price = c.DecimalColumn()
    quantity = c.DecimalColumn()
    line_total = c.DecimalColumn()
    block_id = c.FkColumn()

    invoice = relationship('Invoice', primaryjoin='foreign(InvoiceDetail.invoice_id) == remote(Invoice.id)')
    # lands = relationship('BillingLand', primaryjoin='foreign(InvoiceDetail.id) == remote(BillingLand.invoice_detail_id)',
    #                        uselist=True, viewonly=True)
    tariff = relationship('Tariff', primaryjoin='foreign(InvoiceDetail.tariff_id) == remote(Tariff.id)',
                          uselist=False, viewonly=True)


class Invoice(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_invoice'
    id = c.PkColumn()
    billing_id = c.FkColumn()
    company_id = c.FkColumn()
    farmer_id = c.FkColumn()
    station_id = c.FkColumn()
    currency_id = c.FkColumn()
    village_id = c.FkColumn()
    code = c.CodeColumn()
    issue_date = c.DateTimeColumn()
    due_date = c.DateTimeColumn()
    # change 12/25/2017
    total_lands = c.DecimalColumn()
    total_area = c.DecimalColumn()
    total_amount = c.DecimalColumn()
    paid_amount = c.DecimalColumn()
    status_id = c.FkColumn()  # pending, active, paid, void.
    note = c.NoteColumn()

    station = relationship('Station', primaryjoin='foreign(Invoice.station_id) == remote(Station.id)')
    farmer = relationship('Farmer', primaryjoin='foreign(Invoice.farmer_id) == remote(Farmer.id)')
    billing = relationship('Billing', primaryjoin='foreign(Invoice.billing_id) == remote(Billing.id)')
    details = relationship('InvoiceDetail', primaryjoin='foreign(Invoice.id) == remote(InvoiceDetail.invoice_id)',
                           uselist=True, order_by=InvoiceDetail.item_name)
    currency = relationship('Currency', primaryjoin='foreign(Invoice.currency_id) == remote(Currency.id)')
    status_value = relationship('LookupValue',
                                primaryjoin='and_(foreign(Invoice.status_id) == remote(LookupValue.id), remote(LookupValue.lookup_id == "in_status"))')


class Tariff(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_tariff'
    id = c.PkColumn()
    name = c.TitleColumn()
    price = c.DecimalColumn()
    note = c.NoteColumn()


class ContractTemplate(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_contract_template'
    id = c.PkColumn()
    name = c.TitleColumn()
    content_template = c.TextColumn()
    contract_template_id = c.FkColumn()



