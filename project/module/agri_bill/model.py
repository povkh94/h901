import sqlalchemy
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import object_session, session
from sqlalchemy import select, func

from ..share.model import *
from ..system.model import *


class Station(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_station'
    id = c.PkColumn()
    company_id = Column(Key(), default=lambda: current.company_id())
    code = c.CodeColumn()
    name = c.TitleColumn()
    note = c.NoteColumn()
    company = relationship('Company', primaryjoin='foreign(Station.company_id) == remote(Company.id)')
    children = relationship('Block',
                            primaryjoin='and_(foreign(Station.id) == remote(Block.station_id),Block.is_active == True)',
                            uselist=True, order_by='Block.name')


class Block(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_block'
    id = c.PkColumn()
    company_id = Column(Key(), default=lambda: current.company_id())
    gm_block = c.TitleColumn()
    code = c.CodeColumn()
    name = c.TitleColumn()
    note = c.NoteColumn()
    station_id = c.FkColumn()
    company = relationship('Company', primaryjoin='foreign(Block.company_id) == remote(Company.id)')
    station = relationship('Station', primaryjoin='foreign(Block.station_id) == remote(Station.id)')

    @property
    def filter_name(self):
        return u'{station_name} - {block_name}'.format(station_name=self.station.name if self.station else '',
                                                       block_name=self.name)


class Land(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_land'
    id = c.PkColumn()
    # company_id = Column(Key(), default=lambda: current.company_id())
    farmer_id = c.FkColumn()
    block_id = c.FkColumn()
    tariff_id = c.FkColumn()
    code = c.CodeColumn()
    note = c.NoteColumn()
    area = c.DecimalColumn()
    rent_id = c.FkColumn()
    status_id = c.FkColumn()
    block = relationship('Block', primaryjoin='foreign(Land.block_id) == remote(Block.id)')
    farmer = relationship('Farmer', primaryjoin='foreign(Land.farmer_id) == remote(Farmer.id)')
    rent = relationship('Farmer', primaryjoin='foreign(Land.rent_id) == remote(Farmer.id)')
    land_status_value = relationship('LookupValue',
                                     primaryjoin='and_(foreign(Land.status_id) == remote(LookupValue.id), remote(LookupValue.lookup_id == "l_status"))')
    tariff = relationship('Tariff', primaryjoin='foreign(Land.tariff_id) == remote(Tariff.id)')
    active = Column(String(10), default='active')


class Farmer(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_farmer'
    __table_args__ = dict(extend_existing=True)
    id = c.PkColumn()
    company_id = Column(Key(), default=lambda: current.company_id())
    code = c.CodeColumn()
    name = c.TitleColumn()
    sex = c.TitleColumn()
    marital_status = c.TitleColumn()
    spouse_name = c.TitleColumn()
    phone = c.TitleColumn()
    address = c.NoteColumn()
    village_id = c.FkColumn()
    national_id = c.TitleColumn()
    delivery_code = c.CodeColumn()
    location = relationship('Location',
                            primaryjoin='foreign(Farmer.village_id) == remote(Location.id)')
    invoice = relationship('Invoice',
                           primaryjoin='foreign(Farmer.id) == remote(Invoice.farmer_id)',
                           uselist=True, viewonly=True)

    active = Column(String(10), default='active')

    @property
    def village(self):
        try:
            village_name = u'{p} {n}'.format(
                p=self.location.prefix if hasattr(self.location, 'prefix') and self.location.prefix else '',
                n=self.location.name)
        except Exception, e:
            village_name = u'NA'
        return village_name

    @property
    def commune(self):
        return object_session(self). \
            scalar(
            select([Location.prefix + " " + Location.name]). \
                where(Location.id == self.location.parent_id and Location.is_active == True)
        )

    @property
    def farmer(self):
        self.spouse_name = str(self.spouse_name).strip()
        try:
            if self.spouse_name:
                return u'{name} - {spouse_name} - {code}'.format(name=self.name, spouse_name=self.spouse_name,
                                                                 code=self.code)
            else:
                return u'{name} - {code}'.format(name=self.name, code=self.code)
        except Exception as e:
            return ''

    def __str__(self):
        try:
            if self.spouse_name:
                return u'{name} - {spouse_name} - {code}'.format(name=self.name, spouse_name=self.spouse_name,
                                                                 code=self.code)
            else:
                return u'{name} - {code}'.format(name=self.name, code=self.code)
        except Exception as e:
            return ''


class Verify(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_billing_verify'
    id = c.PkColumn()
    billing_id = c.FkColumn()
    station_id = c.FkColumn()
    block_id = c.FkColumn()
    farmer_id = c.FkColumn()
    status = c.FkColumn()
    contract_number = Column(String(50))
    billing = relationship('Billing', primaryjoin='foreign(Verify.billing_id) == remote(Billing.id)')
    farmer = relationship('Farmer', primaryjoin='foreign(Verify.farmer_id) == remote(Farmer.id)')
    block = relationship('Block', primaryjoin='foreign(Verify.block_id) == remote(Block.id)')


class Location(Base, TrackMixin, ExtMixin):
    __tablename__ = 'com_location'
    id = c.PkColumn()  # 10-digit code
    code = c.CodeColumn()  # just add new for Tasoung
    type = c.FkColumn()  # '0'=country', '1'=province,'2'=district/krong,'3'=commune/sangkat','4'=village ,'5'=group/block
    name = c.TitleColumn()
    latin_name = c.TitleColumn()
    prefix = c.TitleColumn()
    latin_prefix = c.TitleColumn()
    parent_id = c.FkColumn()

    @property
    def title(self):
        return '{p} {n}'.format(p=self.prefix, n=self.name)

    @property
    def latin_title(self):
        return '{n} {p}'.format(p=self.latin_prefix, n=self.latin_name)


class Season(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_season'
    id = c.PkColumn()
    company_id = Column(Key(), default=lambda: current.company_id())
    name = c.TitleColumn()
    note = c.NoteColumn()
    company = relationship('Company', primaryjoin='foreign(Season.company_id) == remote(Company.id)')


class BillingCycle(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_billing_cycle'
    id = c.PkColumn()
    company_id = Column(Key(), default=lambda: current.company_id())
    name = c.TitleColumn()
    note = c.NoteColumn()
    season_id = c.FkColumn()
    season = relationship('Season', primaryjoin='foreign(BillingCycle.season_id) == remote(Season.id)')


class BillingCycleBlock(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_billing_cycle_block'
    id = c.PkColumn()
    billing_cycle_id = c.FkColumn()
    block_id = c.FkColumn()


class Billing(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_billing'
    id = c.PkColumn()
    company_id = Column(Key(), default=lambda: current.company_id())
    season_id = c.FkColumn()
    billing_cycle_id = c.FkColumn()
    name = c.TitleColumn()
    start_date = c.DateTimeColumn()
    end_date = c.DateTimeColumn()
    billing_date = c.DateTimeColumn()
    billing_status_id = c.FkColumn()
    contract_template_id = c.FkColumn()
    season = relationship('Season', primaryjoin='foreign(Billing.season_id) == remote(Season.id)')
    billing_cycle = relationship('BillingCycle',
                                 primaryjoin='foreign(Billing.billing_cycle_id) == remote(BillingCycle.id)')

    billing_status_value = relationship('LookupValue',
                                        primaryjoin='and_(foreign(Billing.billing_status_id) == remote(LookupValue.id), remote(LookupValue.lookup_id == "b_status"))')

    billing_station = relationship('BillingStation',
                                   primaryjoin='foreign(Billing.id) == remote(BillingStation.billing_id)',
                                   uselist=True, viewonly=True)
    contract_template = relationship('ContractTemplate',
                                        primaryjoin='foreign(Billing.contract_template_id) == remote(ContractTemplate.contract_template_id)')


    @property
    def billing_station_json(self):
        return json.dumps({'billing_station': [to_dict(d, defaults={'ext_data': d.ext}) for d in
                                               self.billing_station
                                               if d.is_active]})

    @property
    def invoice_due_date(self):
        if self.billing_status_id == 'close':
            from project.module.agri_bill.logic import invoice_logic
            invoice = invoice_logic.default.get_invoice_due_date(self.id)
            return invoice[1]
        else:
            return u'-'

    @property
    def billing_cycle_active(self):
        from project.module.agri_bill.logic import billing_cycle_logic
        obj = billing_cycle_logic.default.find(self.billing_cycle_id)
        if obj:
            return True
        return False

    @property
    def season_billing(self):
        name = ''
        try:
            name = '%s-%s' % (getattr(self.season, 'name', ''), getattr(self, 'name', ''))
        except Exception as e:
            pass
        return name


class BillingStation(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_billing_station'
    id = c.PkColumn()
    billing_id = c.FkColumn()
    station_id = c.FkColumn()
    tariff_id = c.FkColumn()
    total_farmer = c.DecimalColumn()
    total_land = c.DecimalColumn()
    total_area = c.DecimalColumn()
    total_cost = c.DecimalColumn()
    unit_cost = c.DecimalColumn()
    station = relationship('Station',
                           primaryjoin='foreign(Station.id) == remote(BillingStation.station_id)')
    tariff = relationship('Tariff',
                          primaryjoin='foreign(Tariff.id) == remote(BillingStation.tariff_id)')


class BillingLand(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_billing_land'
    id = c.PkColumn()
    billing_id = c.FkColumn()
    land_id = c.FkColumn()
    block_id = c.FkColumn()
    station_id = c.FkColumn()
    invoice_farmer_id = c.FkColumn()
    owner_farmer_id = c.FkColumn()
    tariff_id = c.FkColumn()
    area = c.DecimalColumn()
    unit_cost = c.DecimalColumn()
    total_cost = c.DecimalColumn()
    invoice_detail_id = c.FkColumn()

    invoice_detail = relationship('InvoiceDetail',
                                  primaryjoin='foreign(InvoiceDetail.id) == remote(BillingLand.invoice_detail_id)')

    station = relationship('Station',
                           primaryjoin='foreign(BillingLand.station_id) == remote(Station.id)')
    land = relationship('Land',
                        primaryjoin='foreign(BillingLand.land_id) == remote(Land.id)', order_by=Land.code
                        )

    tariff = relationship('Tariff', primaryjoin='foreign(BillingLand.tariff_id) == remote(Tariff.id)',
                          uselist=False, viewonly=True)


class InvoiceDetail(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_invoice_detail'
    id = c.PkColumn()
    invoice_id = c.FkColumn()
    item_id = c.FkColumn()
    item_name = c.TitleColumn()
    tariff_id = c.FkColumn()
    total_land = c.DecimalColumn()
    price = c.DecimalColumn()
    quantity = c.DecimalColumn()
    line_total = c.DecimalColumn()
    block_id = c.FkColumn()
    invoice = relationship('Invoice', primaryjoin='foreign(InvoiceDetail.invoice_id) == remote(Invoice.id)')
    billing_lands = relationship('BillingLand',
                                 primaryjoin='foreign(InvoiceDetail.id) == remote(BillingLand.invoice_detail_id)',
                                 uselist=True, viewonly=True)
    tariff = relationship('Tariff', primaryjoin='foreign(InvoiceDetail.tariff_id) == remote(Tariff.id)',
                          uselist=False, viewonly=True)


class Invoice(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_invoice'
    id = c.PkColumn()
    billing_id = c.FkColumn()
    company_id = Column(Key(), default=lambda: current.company_id())
    farmer_id = c.FkColumn()
    owner_farmer_id = c.FkColumn()
    station_id = c.FkColumn()
    block_id = c.FkColumn()
    currency_id = c.FkColumn()
    village_id = c.FkColumn()
    code = c.CodeColumn()
    issue_date = c.DateTimeColumn()
    due_date = c.DateTimeColumn()
    # change 12/25/2017
    total_lands = c.DecimalColumn()
    total_area = c.DecimalColumn()
    total_amount = c.DecimalColumn()
    paid_amount = c.DecimalColumn()
    status_id = c.FkColumn()  # pending, active, paid, void.
    invoice_type = c.FkColumn()
    note = c.NoteColumn()
    contract_number = Column(String(50))
    station = relationship('Station', primaryjoin='foreign(Invoice.station_id) == remote(Station.id)')
    block = relationship('Block', primaryjoin='foreign(Invoice.block_id) == remote(Block.id)')
    farmer = relationship('Farmer', primaryjoin='foreign(Invoice.farmer_id) == remote(Farmer.id)')
    owner_farmer = relationship('Farmer', primaryjoin='foreign(Invoice.owner_farmer_id) == remote(Farmer.id)')
    billing = relationship('Billing', primaryjoin='foreign(Invoice.billing_id) == remote(Billing.id)')
    details = relationship('InvoiceDetail', primaryjoin='foreign(Invoice.id) == remote(InvoiceDetail.invoice_id)',
                           uselist=True, order_by=InvoiceDetail.item_name)
    currency = relationship('Currency', primaryjoin='foreign(Invoice.currency_id) == remote(Currency.id)')
    status_value = relationship('LookupValue',
                                primaryjoin='and_(foreign(Invoice.status_id) == remote(LookupValue.id), remote(LookupValue.lookup_id == "in_status"))')
    status_invoice = relationship('LookupValue',
                                  primaryjoin='and_(foreign(Invoice.invoice_type) == remote(LookupValue.id), remote(LookupValue.lookup_id == "1004"))')

    @property
    def farmer_name(self):
        return self.farmer.name

    @property
    def season(self):
        try:
            return self.billing.season_billing
        except:
            return '-'


class Tariff(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_tariff'
    id = c.PkColumn()
    name = c.TitleColumn()
    price = c.DecimalColumn()


class ContractTemplate(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_contract_template'
    id = c.PkColumn()
    name = c.TitleColumn()
    content_template = c.TextColumn()
    contract_template_id = c.FkColumn()


class BankPaymentLog(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_bank_payment_log'
    id = Column(String(100), primary_key=True, default=gid)
    user_id = c.FkColumn()
    action_type = c.FkColumn()
    is_completed = c.BooleanColumn()
    start_date = c.DateTimeColumn()
    end_date = c.DateTimeColumn()
    total_record = c.DecimalColumn()
    total_amount = c.DecimalColumn()
    total_paid = c.DecimalColumn()
    file_name = c.TitleColumn()
    data_file = c.TextColumn()


class BankPaymentDetail(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_bank_payment_detail'
    id = c.PkColumn()
    bank_payment_log_id = Column(String(100), default=gid)
    bank_payment_id = Column(String(100), default=gid)
    bank = c.NoteColumn()
    branch = c.NoteColumn()
    currency = c.CodeColumn()
    customer_code = c.CodeColumn()
    pay_date = c.DateTimeColumn()
    pay_amount = c.DecimalColumn()
    note = c.TextColumn()
    cashier = c.TextColumn()
    payment_method = c.CodeColumn()
    payment_type = c.CodeColumn()
    result = c.FkColumn()
    result_note = c.NoteColumn()
    paid_amount = c.DecimalColumn()
    customer_id = c.FkColumn()
    currency_id = c.FkColumn()
    is_void = c.BooleanColumn()

    customer = relationship('Farmer', primaryjoin='foreign(BankPaymentDetail.customer_id) == remote(Farmer.id)')
    currencys = relationship('Currency', primaryjoin='foreign(BankPaymentDetail.currency_id) == remote(Currency.id)')
    bank_payment_log = relationship('BankPaymentLog',
                                    primaryjoin='foreign(BankPaymentDetail.bank_payment_log_id) == remote(BankPaymentLog.id)')

    @property
    def customer_name(self):
        if self.customer:
            return self.customer.name
        return '-'

    @property
    def currency_sign(self):
        if self.currencys:
            return self.currency.sign
        return '-'


class InvoicePenalty(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_invoice_penalty'
    id = c.PkColumn()
    invoice_id = c.FkColumn()
    penalty_invoice_id = c.FkColumn()
    currency_id = c.FkColumn()
    amount = c.DecimalColumn()


class Penalty(Base, TrackMixin, ExtMixin):
    __tablename__ = 'agri_penalty'
    id = c.PkColumn()
    name = c.TitleColumn()
    percentag = c.TitleColumn()
    amount = c.DecimalColumn()
