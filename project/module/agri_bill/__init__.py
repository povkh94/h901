__info__ = {
    "name": "company",
    "title": "Company Module",
    "summary": "System Module for Sequence, Component, Role, Permission, Setting etc.",
    "author": "RY Rith",
    "website": "http://project.com",
    "depends": ['share','company']
}


def activate(**kwargs):

    from . import view
    from . import widget
    from . import current
    from project.module.agri_bill.hook import payment_hook


def install():
    pass


def uninstall():
    pass
