from datetime import datetime
import pdfkit
import os

def export_pdf(input, input_type='string', css='', output_path='', output_name='', template='a4'):
    # custom config for wkthtmltopdf
    # https://stackoverflow.com/questions/27673870/cant-create-pdf-using-python-pdfkit-error-no-wkhtmltopdf-executable-found/33645363
    # https://github.com/wkhtmltopdf/wkhtmltopdf/releases
    wkthmltopdf_path = os.getenv('WKHTMLTOPDF_PATH') or r'C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe'
    config = pdfkit.configuration(wkhtmltopdf=wkthmltopdf_path)

    template_a4 = {
        'page-size': 'A4',
        'encoding': "UTF-8",
        'no-outline': None
    }

    template_a5 = {
        'page-size': 'A5',
        'encoding': "UTF-8",
        'no-outline': None
    }

    default_output_type = '.pdf'
    default_output_name = 'export_report_' + (str(datetime.today()).replace('-', '')
                                                                   .replace(':', '')
                                                                   .replace(' ', '_')
                                                                   .replace('.', '_'))
    output_name = (output_name if output_name else default_output_name) + default_output_type
    output = os.path.join(output_path, output_name) if output_path else None

    if template.lower() == 'a5':
        template = template_a5
    else:
        template = template_a4

    pdf = pdfkit.from_string(input, output, css=css, options=template, configuration=config)

    obj = {
        'path': output_path,
        'name': output_name
    }

    return obj if output else pdf
