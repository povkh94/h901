# change entry dir
# - *- coding: utf- 8 - *-
import calendar

from flask import request


def to_month_name(val):
    val = calendar.month_name[val]
    if request.cookies.get('language') == 'km' or request.cookies.get('language') == None:
        return val.replace('January', u'មករា') \
            .replace('February', u'កុម្ភៈ') \
            .replace('March', u'មីនា') \
            .replace('April', u'មេសា') \
            .replace('May', u'ឧសភា') \
            .replace('June', u'មិថុនា') \
            .replace('July', u'កក្កដា') \
            .replace('August', u'សីហា') \
            .replace('September', u'កញ្ញា') \
            .replace('October', u'តុលា') \
            .replace('November', u'វិច្ឆិកា') \
            .replace('December', u'ធ្នូ')
    else:
        return val[:3]


def round_riel(val):
    import numpy as np
    if val == "-":
        val = 0
    value = np.ceil(float(val))
    v1 = value % 100  # get value the last two digits for round if v1 > 0 and v<= 99 to 100 riel
    if v1 > 0 and v1 <= 99:
        value = (value - v1) + 100
    return value


# Land Area format
# {0:.3F}
# param land area

def area_format(val):
    if val == '-':
        val = 0


def format_value(value):
    if value == "-":
        value = 0
    value = abs(value)
    return '{0:,.0F}'.format(float(value))


def to_ha(value):
    if value:
        value = value / 100
        return '{0:,.2F}'.format(float(value))
