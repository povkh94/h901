from edf_admin.table import Column, HTMLString
from edf.helper import convert
from wtforms.widgets import html_params


class AmountColumn(Column):
    def value(self, row):
        import numpy as np
        v = convert.to_decimal(super(AmountColumn, self).value(row))
        if v:
            value = np.ceil(float(v))  # round up
            return '{0:,.0f}'.format(value)
        return '-'

    def render(self, row, tag='td'):
        result = '<td class="decimal" %s>%s</td>' % (html_params(**self.render_kw), self.value(row))
        return HTMLString(result)

    def render_header(self, tag='th'):
        result = '<th class="decimal" %s>%s</th>' % (html_params(**self.render_kw), self.label if self.label else '')
        return HTMLString(result)


class AreaColumn(Column):
    def value(self, row):
        v = convert.to_decimal(super(AreaColumn, self).value(row))
        if v:
            v = v / 100
            value = round(v, 3)  # round up
            return '{0:,.2f}'.format(value)
        return '-'

    def render(self, row, tag='td'):
        result = '<td class="decimal">%s</td>' % self.value(row)
        return HTMLString(result)

    def render_header(self, tag='th'):
        result = '<th class="decimal">%s</th>' % (self.label if self.label else '')
        return HTMLString(result)


class DecimalColumn(Column):
    def value(self, row):
        v = convert.to_decimal(super(DecimalColumn, self).value(row))
        if v:
            return '{0:,.2f}'.format(v)
        return '-'

    def render(self, row, tag='td'):
        result = '<td class="decimal" data-column="%s" %s>%s</td>' % (
            self.label, html_params(**self.render_kw), self.value(row))
        return HTMLString(result)

    def render_header(self, tag='th'):
        result = '<th class="decimal" %s>%s</th>' % (html_params(**self.render_kw), self.label if self.label else '')
        return HTMLString(result)
