import requests
from flask import session, request

from project.module.company.logic import company_logic
from project.module.company.model import Company
from project.module.system_security.logic import user_logic
from project.module.system_security.model import User
from ..share.current import current, json
from ..company.current import current


# from ..company.model import Company
#
#
# @current.promote('company_id')
# def dummy_company_id():
#     return '1'
#
#
# @current.promote('company')
# def dummy_company():
#     com = Company()
#     com.id = 1
#     com.name = 'ABC Inc.'
#     com.is_active = True
#     com.ext_data = '{"seq": {"payment-code": "REDcfco7", "invoice-code": "b5CjVySS", "farmer-code": "5FutBfCZ"}, "default_currency_id": 1, "facebook_page_name": "", "facebook_page_id": "", "facebook_page_token": "", "calendar_id": 1}'
#     return com
#

@current.promote('user_id')
def _current_user_id():
    #return  request.get
    response = session.get('user_id')
    user_id = request.cookies.get('user_id')
    return user_id
    #return session.get('user_id',0)



@current.promote('user')
def _current_user():
    uid = request.cookies.get('user_id')
    u = user_logic.default.find(uid)
    return u or User(id= 0,name ='-')

@current.promote('company')
def _current_company():
    c = company_logic.default.current()
    return c or Company(id=0, name='-')

@current.promote('company_id')
def _current_company_id():
    return current.user().ext.get('current_scope', 0)

@current.promote('default_company_id')
def _current_default_company_id():
    return company_logic.default.find_root_company_id(current.company_id())
