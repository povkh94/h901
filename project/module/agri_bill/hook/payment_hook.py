from edf.helper.convert import to_decimal

from project.module.agri_bill.model import Invoice, randomstring, current
from project.module.payment.model import Payment

try:

    from project.module.payment.logic import payment_logic
    from project.module.agri_bill.logic import farmer_logic, invoice_logic, billing_logic
    from edf.base.database import db

    payment_logic.default.get_customer_list = farmer_logic.default.invoice_payment_suggestion
    payment_logic.default.get_invoices = invoice_logic.default.find_invoice_suggetion
    payment_logic.default.get_customer = farmer_logic.default.find
    payment_logic.default.find_billing = billing_logic.default.find_billing
    payment_logic.default.find_invoice = invoice_logic.default.find
    current._gid = lambda: randomstring(8, 'ABCDEFGHIJKLMNOPQRSTUVWYZabcdefghijklmnopqrstuvwyz0123456789')


    @payment_logic.default.on('added')
    def update_invoice(obj):
        invoices = obj.ext__details
        if invoices:
            for item in invoices:
                id = item['ext_data']['invoice_id']
                invoice = invoice_logic.default.find(id=id)
                invoice.code = item['code']
                if not item['pay_amount']:
                    item['pay_amount'] = 0
                if item['pay_amount'] == '-':
                    item['pay_amount'] = 0
                if item['ending_balance'] == '-':
                    item['ending_balance'] = 0
                if item['pay_amount'] == 0:
                    invoice.status_id = 'pending'
                elif item['pay_amount'] != 0 and item['ending_balance'] != 0:
                    invoice.status_id = 'partial'
                else:
                    invoice.status_id = 'paid'

                if isinstance(item['paid_amount'], str) or isinstance(item['paid_amount'], unicode):
                    item['paid_amount'] = item['paid_amount'].replace(',', '') if ',' in item['paid_amount'] else item[
                        'paid_amount']
                if isinstance(item['pay_amount'], str) or isinstance(item['pay_amount'], unicode):
                    item['pay_amount'] = item['pay_amount'].replace(',', '') if ',' in item['pay_amount'] else item[
                        'pay_amount']
                invoice.paid_amount += to_decimal(item['pay_amount'])
                db.add(invoice)
                db.flush()


    @payment_logic.default.on('removed')
    def update_invoice_paid_amount(obj, _commit=False):
        payment = payment_logic.default.find(obj.id)
        if payment and payment.ext__details:
            for detail in payment.ext__details:
                # update invoice pay amount
                invoice = invoice_logic.default.find(detail['ext_data']['invoice_id'])
                paid_amount = detail['paid_amount'] or detail.get('ext_data').get('paid_amount')
                if isinstance(payment, str) or isinstance(paid_amount, unicode):
                    paid_amount = paid_amount.replace(',', '')
                if detail['pay_amount'] != '-':
                    invoice.paid_amount -= to_decimal(paid_amount)
                if invoice.paid_amount == 0:
                    invoice.status_id = 'pending'
                elif invoice.paid_amount < invoice.total_amount:
                    invoice.status_id = 'partial'
                else:
                    invoice.status_id = 'paid'
                db.add(invoice)
                payment_detail = payment_logic.default.delete_payment(obj.id)
        else:

            # payment = db.query(Payment).filter(Payment.id == obj.id).delete()
            payment = payment_logic.default.delete_payment(obj.id)
            _commit = False
            db.commit()



except ImportError:
    pass
