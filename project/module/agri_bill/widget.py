from flask import url_for
from flask_babel import lazy_gettext as _

from project.module.agri_bill import current
from ..share.lib.widget import *

#sidebar navigation register
# register_widget('sidebar', order=200, widget=NavWidget(
#     id='farmer', title=_(u'Logout'), icon='fa fa-user',url=lambda: url_for('.FarmerView:index'))
# )
# register_widget('sidebar', order=200, widget=NavWidget(
#     id='station', title=_(u'My Profile'), icon='fa fa-industry',url=lambda: url_for('.StationView:index'))
# )
# register_widget('sidebar', order=200, widget=NavWidget(
#     id='block', title=_(u'Company Info'), icon='fa fa-file',url=lambda: url_for('.BlockView:index'))
# )

register_widget('sidebar', order=200, widget=NavWidget(
    id='farmer', title=_(u'Farmer'), icon='fa fa-user',url=lambda: url_for('.FarmerView:index'))
)
register_widget('sidebar', order=200, widget=NavWidget(
    id='station', title=_(u'Station'), icon='fa fa-industry',url=lambda: url_for('.StationView:index'))
)
register_widget('sidebar', order=200, widget=NavWidget(
    id='block', title=_(u'Block'), icon='fa fa-file',url=lambda: url_for('.BlockView:index'))
)
register_widget('sidebar', order=200, widget=NavWidget(
    id='land', title=_(u'Land'), icon='fa fa-map-o',url=lambda: url_for('.LandView:index'))
)


register_widget('sidebar', order=200, widget=NavWidget(
    id='billing', title=_(u'Farmer Agreement'), icon='fa fa-file',url=lambda: url_for('.BillingView:index'))
)

register_widget('sidebar', order=200, widget=NavWidget(
    id='invoice', title=_(u'Invoice'), icon='fa fa-file',url=lambda: url_for('.InvoiceView:index'))
)

register_widget('sidebar', order=200, widget=NavWidget(
    id='payment', title=_(u'Payment'), icon='fa fa-credit-card-alt',url=lambda: url_for('.PaymentView:index'))
)
register_widget('sidebar', order=200, widget=NavWidget(
    id='bank-payment', title=_(u'Bank Payment'), icon='fa fa-credit-card-alt',url=lambda: url_for('.BankPaymentView:index'))
)
register_widget('sidebar', order=200, widget=NavWidget(
    id='report', title=_(u'Reports'), icon='fa fa-book',url=lambda: url_for('.ReportView:report'))
)

register_widget('sidebar', order=200, widget=NavWidget(
    id='setting', title=_('Setting'), icon='fa fa-cogs',url=lambda: url_for('.SettingView:index'))
)
