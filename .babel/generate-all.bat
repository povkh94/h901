@echo off

SET /P m="Module (/project/module/<name>):"
mkdir "../project/module/%m%/translate"
pybabel extract -F babel.cfg -o "../project/module/%m%/translate/messages.pot" "../project/module/%m%" 
pybabel extract -F babel.cfg -o "../project/module/%m%/translate/messages.pot" -k _ "../project/module/%m%" 

pybabel init -i "../project/module/%m%/translate/messages.pot" -d "../project/module/%m%/translate" -l "km"
pybabel init -i "../project/module/%m%/translate/messages.pot" -d "../project/module/%m%/translate" -l "en"
pybabel compile -d "../project/module/%m%/translate"

echo "DONE :)"
pause