@echo off

SET /P m="Module (/project/module/<name>):"
mkdir "../project/module/%m%/translate"
pybabel extract -F babel.cfg -o ../project/module/%m%/translate/messages.pot ../project/module/%m%

echo "DONE :)"
pause